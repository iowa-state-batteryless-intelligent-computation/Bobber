/* Copyright 2018 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "tensorflow/lite/micro/kernels/micro_ops.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/testing/micro_test.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/version.h"
#include "LeNet/LeNet_Full_Integer_Quantized_Model.h"
//#include "LeNet/Test_Inputs/test_input_float_digit2.h"
//#include "LeNet/Test_Inputs/test_input_float_digit0.h"
#include "LeNet/Test_Inputs/test_input_float.h"
#include "uarttx.h"
//#include "LeNet/Test_Inputs/test_input_uint8.h"

#if defined(__MSP430__)
#include <msp430.h>
#include <acc_interface/acc_interface.h>
#include "system_peripherals/peripherals.h"
#include <fram-utilities/ctpl/ctpl.h>

extern int extern_adc_en;
extern int intermittent_power_flag;
extern uint32_t numcount;

#define FILL_16 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
#define FILL_64 FILL_16,FILL_16,FILL_16,FILL_16
#define FILL_256 FILL_64,FILL_64,FILL_64,FILL_64
#define FILL_1024 FILL_256,FILL_256,FILL_256,FILL_256
#define FILL_4096 FILL_1024,FILL_1024,FILL_1024,FILL_1024
int32 ISR_RET_SP = 0x00;
char uart_stx[40]={0};


static void __attribute__((naked, section(".crt_0500run_preinit_array"),used))
_system_pre_init(void){
//  P7OUT  = 0; P7DIR  = 0xFF;
//  P7OUT |=BIT2;
  ctpl_init();
//  init_accelerator();
}

TF_LITE_MICRO_TESTS_BEGIN

char readstr[40]={0};
uint16_t num_exp=100,i,j;
uint16_t inf_out= 0;


WDTCTL = WDTPW | WDTHOLD;               // Stop WDT
// Configure GPIO

if (!extern_adc_en){
    initGpio();
    uartConfig();
//    UART_config_back_channel();
    init_spi();
//    initCompMonitor();
    initAdcMonitor();
    fpga_core_power_enable();
    fpga_3v3_power_enable();
//    mac_ret_assert();
//    P1OUT |= BIT2;

//    init_accelerator_all();
}

else {
    initGpio_ext_adc();
    uartConfig();
    init_spi();
//    initCompMonitor();
    initAdcMonitor_ext();
//    fpga_core_power_disable();
//    fpga_core_power_enable();
//    __delay_cycles(1040000);
//    mac_ret_deassert();

    init_accelerator_all();
}
//    P1OUT |= BIT0;
/* Enable global interrupts. */
__enable_interrupt();
//uartTX("Prog start\n");
//fpga_3v3_power_enable();

#else
TF_LITE_MICRO_TESTS_BEGIN
#endif
    

TF_LITE_MICRO_TEST(TestInvoke) {
  // Set up logging.

   // uartTX("started\n");


  tflite::MicroErrorReporter micro_error_reporter;
  tflite::ErrorReporter* error_reporter = &micro_error_reporter;

  // Map the model into a usable data structure. This doesn't involve any
  // copying or parsing, it's a very lightweight operation.
  const tflite::Model* model =
      ::tflite::GetModel(LeNet5_model);
  if (model->version() != TFLITE_SCHEMA_VERSION) {
    error_reporter->Report(
        "Model provided is schema version %d not equal "
        "to supported version %d.\n",
        model->version(), TFLITE_SCHEMA_VERSION);
  }

  // Pull in only the operation implementations we need.
  // This relies on a complete list of all the ops needed by this graph.
  // An easier approach is to just use the AllOpsResolver, but this will
  // incur some penalty in code space for op implementations that are not
  // needed by this graph.
  //
  // tflite::ops::micro::AllOpsResolver resolver;
// __attribute__((section(".arena")))
  static tflite::MicroMutableOpResolver micro_mutable_op_resolver;
  //tflite::MicroMutableOpResolver micro_mutable_op_resolver;


  micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_QUANTIZE,
                                                              tflite::ops::micro::Register_QUANTIZE());

           micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEPTHWISE_CONV_2D,
                                                                        tflite::ops::micro::Register_DEPTHWISE_CONV_2D(),3,3);

           micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_CONV_2D,
                                                                    tflite::ops::micro::Register_CONV_2D(), 3, 3);



           micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_AVERAGE_POOL_2D,
                                                                                 tflite::ops::micro::Register_AVERAGE_POOL_2D(),2,2);



           micro_mutable_op_resolver.AddBuiltin(
                          tflite::BuiltinOperator_FULLY_CONNECTED,
                          tflite::ops::micro::Register_FULLY_CONNECTED(), 4, 4);


           micro_mutable_op_resolver.AddBuiltin(
                   tflite::BuiltinOperator_SOFTMAX,
                   tflite::ops::micro::Register_SOFTMAX(), 2, 2);

           micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEQUANTIZE,
                                                tflite::ops::micro::Register_DEQUANTIZE(), 2, 2);



  // Create an area of memory to use for input, output, and intermediate arrays.
  //const int tensor_arena_size = 10 * 1024;
  const int tensor_arena_size = 10*1024;
  __attribute__((section(".upper.text")))
   static uint8_t tensor_arena[tensor_arena_size] = {FILL_4096,FILL_4096,FILL_1024,FILL_1024};

  //static uint8_t tensor_arena[tensor_arena_size] = {FILL_4096,FILL_1024};

  // Build an interpreter to run the model with.
    tflite::MicroInterpreter interpreter(model, micro_mutable_op_resolver,
                                       tensor_arena, tensor_arena_size,
                                       error_reporter);
  interpreter.AllocateTensors();

  // Get information about the memory area to use for the model's input.
  TfLiteTensor* input = interpreter.input(0);

  // Make sure the input has the properties we expect.
  TF_LITE_MICRO_EXPECT_NE(nullptr, input);
  TF_LITE_MICRO_EXPECT_EQ(4, input->dims->size);
  TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[0]);
  TF_LITE_MICRO_EXPECT_EQ(28, input->dims->data[1]);
  TF_LITE_MICRO_EXPECT_EQ(28, input->dims->data[2]);
  TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[3]);
  TF_LITE_MICRO_EXPECT_EQ(kTfLiteFloat32, input->type);

//   Copy a spectrogram created from a .wav audio file of someone saying "Yes",
//   into the memory area used for the input.
  const float* yes_features_data = image_float32;
  for (int i = 0; i < (int)(input->bytes)/4; ++i) {
    input->data.f[i] = yes_features_data[i];
  }


  // Run the model on this input and make sure it succeeds.
  for(int i=0;i<num_exp;i++){
   TfLiteStatus invoke_status = interpreter.Invoke();
   if (invoke_status != kTfLiteOk) {
     error_reporter->Report("Invoke failed\n");
   }
   TF_LITE_MICRO_EXPECT_EQ(kTfLiteOk, invoke_status);


  // Get the output from the model, and make sure it's the expected size and
  // type.
  TfLiteTensor* output = interpreter.output(0);
  TF_LITE_MICRO_EXPECT_EQ(2, output->dims->size);
  TF_LITE_MICRO_EXPECT_EQ(1, output->dims->data[0]);
  TF_LITE_MICRO_EXPECT_EQ(10, output->dims->data[1]);
  TF_LITE_MICRO_EXPECT_EQ(kTfLiteFloat32, output->type);
//  early_die();

  uartConfig();
  P7OUT ^= BIT2;
  __delay_cycles(100);
  P7OUT ^= BIT2;
  uint8_t k=0;
  while(k<20)                        // Increment through array, look for null pointer (0) at end of string
      {
      inf_out = output->data.i16[k]; // Increment variable for array address
//      send_data(inf_out);
      sprintf(readstr, "%04x", (inf_out));
      uartTX(readstr);
      k++;
      }
    sprintf(readstr, ",%04ld", (numcount));
    uartTX(readstr);
    numcount =0;
  __delay_cycles(24000);
  P7OUT |= BIT2;
  __delay_cycles(500);
    P7OUT &= ~BIT2;
    opt_1v5_Reg_flag=1;
//    fpga_core_power_enable();
    fpga_3v3_power_enable();
//  yes_features_data = image_float32;
  for (int i = 0; i < (int)(input->bytes)/4; ++i) {
     input->data.f[i] = yes_features_data[i];
    }

//  P7OUT ^= BIT4;
//  uartTX("\n");
}
//  P7OUT |= BIT4;

}

TF_LITE_MICRO_TESTS_END

//
////#pragma vector = ADC12_VECTOR;
__attribute__((__interrupt__(ADC12_VECTOR)))

void ADC12_ISR(void)
{   ISR_RET_SP=__get_SP_register()+0x18;
//    ISR_RET_SR=__get_SR_register();
    switch(__even_in_range(ADC12IV, ADC12IV_ADC12LOIFG)) {
        case ADC12IV_NONE:        break;        // Vector  0: No interrupt
        case ADC12IV_ADC12OVIFG:  break;        // Vector  2: ADC12MEMx Overflow
        case ADC12IV_ADC12TOVIFG: break;        // Vector  4: Conversion time overflow
        case ADC12IV_ADC12HIIFG:                // Vector  6: Window comparator high side
            /* Disable the high side and enable the low side interrupt. */
//            P1OUT^=BIT5;
            ADC12IER2 &= ~ADC12HIIE;
            ADC12IER2 |= ADC12LOIE;
            ADC12IFGR2 &= ~ADC12LOIFG;
//            P1OUT^=BIT5;
            break;
        case ADC12IV_ADC12LOIFG:                // Vector  8: Window comparator low side
            /* Stop the ADC monitor and enter device shutdown with 64ms timeout. */
//            stopAdcMonitor();
//            P1OUT^=BIT5;
//            mac_ret_assert();
//            __delay_cycles(300);
//            P7OUT |= BIT2;
            ctpl_enterShutdown(CTPL_SHUTDOWN_TIMEOUT_64_MS);
            WDTCTL = WDTPW | WDTHOLD;               // Stop WDT
//            P7OUT &= ~BIT2;

//            P1OUT |= BIT2;
            initGpio();
//            P1OUT |= BIT0;

            if(opt_1v5_Reg_flag){
//                fpga_core_power_enable();
                fpga_3v3_power_enable();
                if(!ACCEL_FUNC_RESTORE_FLAG){
                    wait_for_fpga_boot();
                }
            }
//            P1OUT &= ~BIT0;

//            P1OUT |= BIT0;

//            initGpio_ext_adc();
//            PM5CTL0 &= ~LOCKLPM5;
//            uartConfig();
        //    UART_config_back_channel();
            init_spi();

//            mac_ret_deassert();
//            __delay_cycles(300);
//            P7OUT ^= BIT0;
//            P1OUT^=BIT5;


//            init_accelerator();
//            P7OUT ^= BIT0;
            /* Reinitialize the ADC monitor since the ADC state is not retained. */
//            initAdcMonitor();
            ADC12IER2 &= ~ADC12LOIE;
            ADC12IER2 |= ADC12HIIE;
            ADC12IFGR2 &= ~ADC12HIIFG;
//            P7OUT ^= BIT0;
            break;
        default: break;
    }

}


