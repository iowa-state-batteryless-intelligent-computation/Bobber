#ifndef __MSP430__
#include "array_printer.h"
using namespace std;

void print_type(FILE* myfile, TfLitePtrUnion data, int idx, TfLiteType type,const char* end){
    switch(type) {
        case kTfLiteNoType: // unsupported types
        case kTfLiteComplex64:
        case kTfLiteFloat16:
        case kTfLiteString:
            return;
        case kTfLiteFloat32: // float types
            fprintf(myfile,"%f%s",data.f[idx],end);
            return;
        case kTfLiteInt32: // int types
            fprintf(myfile,"%d%s",data.i32[idx],end);
            return;
        case kTfLiteUInt8:
            fprintf(myfile,"%d%s",data.uint8[idx],end);
            return;
        case kTfLiteInt64:
            fprintf(myfile,"%lld%s",data.i64[idx],end);
            return;
        case kTfLiteBool:
            fprintf(myfile,"%d%s",data.b[idx],end);
            return;
        case kTfLiteInt16:
            fprintf(myfile,"%d%s",data.i16[idx],end);
            return;
        case kTfLiteInt8:
            fprintf(myfile,"%d%s",data.int8[idx],end);
            return;
    }
}

void output_array(char* filename, TfLitePtrUnion data, int size, int col_width, TfLiteType type){
    FILE* myfile = fopen(filename,"w");
    int remaining_in_line = col_width;
    for(int i = 0; i<size;i++){
        if(--remaining_in_line == 0){
            print_type(myfile,data,i,type,"\n");
            remaining_in_line = col_width;
        }else{
            print_type(myfile,data,i,type," ");
        }
    }
    fprintf(myfile,"\n");
}

void output_tensor(int tensor_idx, TfLiteTensor tensor){
    /* generate filename */
    char filename[200];
    snprintf(filename,200,"t%d_%s_output.txt",tensor_idx,tensor.name);
    for(int i = 0; i<strlen(filename); i++){ //replace '/' with '_'
        if(filename[i]=='/'){filename[i]='_';}
    }

    /* determine formatting */
    int num_cols=10; // choose first dimension != 1. e.g. [1,14,14,1]->14
    for(int i = 0; i < tensor.dims->size; i++){
        if(tensor.dims->data[i] != 1){
            num_cols= tensor.dims->data[i];
            break;
        }
    }
    int size = 1; // get size of tensor
    for(int i = 0; i < tensor.dims->size; i++){
        size *= tensor.dims->data[i];
    }

    /* print_tensor */
    output_array(filename,tensor.data,size,num_cols,tensor.type);
}

void clear_outputs(){
    system("rm t*output.txt");
}
#endif





