#ifndef MSP430_TENSORFLOW_ARRAY_PRINTER_H
#define MSP430_TENSORFLOW_ARRAY_PRINTER_H

#ifndef __MSP430__
#include <cstdio>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include "tensorflow/lite/c/c_api_internal.h"

void clear_outputs();
void output_array(char* filename, TfLitePtrUnion data, int size, int col_width, TfLiteType type);
void output_tensor(int tensor_idx, TfLiteTensor tensor);
#endif

#endif
