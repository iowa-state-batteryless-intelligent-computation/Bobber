# BOBBER Software Platform
Intermittent systems have to deal with frequent power failures and the associated correctness and forward progress issues. BOBBER takes a two-pronged approach: (1) for execution on the MCU we use a full checkpointing-based system to guarantee correctness and forward progress; (2) an idempotent interface to the FPGA accelerator. The flexibility of this interface allows the application to craft sufficiently small kernels to guarantee forward progress.

BOBBER uses a modified version of TI's Compute Through Power Loss (CTPL) ported manually to work with a GCC compiler.

## Compute Through Power Loss (CTPL): 

CTPL (https://www.ti.com/tool/TIDM-FRAM-CTPL) is a checkpointing utility which is provided by TI for intelligent system restoration after a power failure. By default CTPL requires the use of 
a TI compiler and an ADC is allowed to monitor the internal power rail to detect a brownout/RESET failure and efficiently checkpoints the volatile state
of the device.

## Adding ctpl_init

By default with the gcc compiler, the system_pre_init doesn't include the ctpl_init function. In order to do that efficiently without watchdog timer
the following can be defined before the start of the main() function. In MSP430FR5994, the .crt_0500run_preinit_array is called before the main function
is called

```
static void __attribute__((naked, section(".crt_0500run_preinit_array"),used))
_system_pre_init(void){

  ctpl_init();

}
```

## Linking the compiler

In order to efficiently use the CTPL, a manual linker file should be used. The FRAM memory in MSP430 is divided into lower and upper region. By default
MSP430FR5994 has its lower memory for read only purposes.
For the CTPL to work, the lower FRAM region is to be partioned to have a read and write memory region. The FRAM is partioned as below
```
  ROM (rx)         : ORIGIN = 0x4000, LENGTH = 0x9C40 /* END =0xDC40, size 40000 */
  FRAM (w) 	   : ORIGIN = 0xDC41, LENGTH = 0x2340 /* END =FF81 , size = 9024*/
  HIFRAM (rxw)     : ORIGIN = 0x00010000, LENGTH = 0x00033FFF
```

We use a specific section named as .persistent in the partioned FRAM, for checkpointing and storing the volatile state as a part of CTPL.

```
.persistent (ORIGIN (FRAM)) :
  {
    . = ALIGN(2);
    PROVIDE (__persistent_start = .);
    *(.persistent)
    . = ALIGN(2);
    PROVIDE (__persistent_end = .);
  } > FRAM = 0x0

```

If we have a program code large enough such that it has to be defined in the upper memory region, then in the MCU/CTPL/ctpl_debug_gcc_working/fram-utilities/ctpl_low_level.s 
assembly file, the following change has to be made to properly link with the code 

```
.sect ".text.ctpl_low_level"  :: to

.sect ".upper.text.ctpl_low_level"

```

## Defining the CTPL stack size

In order to save the volatile stack and heap in the non volatile memory (FRAM) we have a ctpl_stack_copy state variable, whose size can be defined
at compile time and varies with respect to the program size and amount of peripherals to be saved.

In order to change this in the project properties, go to gcc compiler-symbols-Define symbols -D and the following is defined
```
__MSP430FR5994__
_MPU_ENABLE
CTPL_STACK_SIZE= 160 (default value) - varied depending on the program stack to be saved

```

## Peripherals to be saved

The ported CTPL library is a superset utilities which can be used any type and number of peripherals can be saved. You just need to define which peripheral
has to be saved in the ctpl.c file in fram-utilities of the ctpl code or the manually can be defined as a symbol in the project properties
In the project properties, go to gcc compiler-symbols-Define symbols -D and the following is defined

In order to change this in the project properties, go to gcc compiler-symbols-Define symbols -D and the following is defined to save the timer0, REF_A voltage
ADC_12B and EUSCIA0 peripheral

```
CTPL_SAVE_TA0
CTPL_SAVE_REF_A
CTPL_SAVE_ADC12_B
CTPL_SAVE_EUSCIA0

```

In the ctpl_msp430fr5994.c located in  file MCU/CTPL/ctpl_debug_gcc_working/ctpl_msp430fr5994.c, just uncomment the peripherals that needs to be saved

```
#define CTPL_SAVE_SYS
#define CTPL_SAVE_SFR
#define CTPL_SAVE_RTC_C
#define CTPL_SAVE_MPY32
//#define CTPL_SAVE_CRC16
//#define CTPL_SAVE_CRC32
//#define CTPL_SAVE_TA2

```
## Modified CTPL save and restore routine for idempotent execution:

Whenever the program execution is inside the idempotent kernel function, and if a power failure occurs, the ctpl assembly goes to an alternate save routine ctpl_alternate_save(). This makes sure that upon a hard restart, the program execution resumes from the start of the idempotent kernel function.
In order to do that, at the start of the ADC ISR code, we need to checkpoint the stack pointer, pointing to the start of the ADC ISR in the stack using.

```
ISR_RET_SP=__get_SP_register()+0x18;

```

To save and restore the activation record of any arbitrary kernels, the following helper methed can be used inside the kernel functions being designed, located in fram-utilities/ctpl/ctpl.h

```
save_kernelactivation_record();
restore_kernelactivation_record();

```

To save and restore argument registers of a kernel function, the following helper functions was added to the standard TI library in430.h, using extended GCC ASM. As an example, to save and restore a general purpose register like R12, the follwing helper function can be used:
__get_R12_register(x);
__set_R12_register(x); where x is any variable stored in FRAM.
To reuse BOBBER's Software Platform for any other benchmarks or application, the programmer should manually(using the debugger available in CCS) verify what argument registers are used as a part of the function prologue by the compiler. Once these registers are identified, custom helper functions should be added in the library in430.h, if not available already. The template is as below.

\#define_get_R12_register(x) \
({ \
        \__asm__ \__volatile__ ("movx.w R12, %0" \
        :: "m"((unsigned long int) x) \
        );\
})

\#define __get_R12_register(x) _get_R12_register(x)


\#define _set_R12_register(x) \
({ \
        \__asm__\__volatile__ ("movx.w %0, R12" \
        :: "m"((unsigned long int) x) \
        );\
})

\#define __set_R12_register(x) _set_R12_register(x)

# Demonstration: Intermittency-aware CNN HW-SW Accelerator Framework

We evaluate a use case for accelerating a common benchmark LENET. The LENET benchmark ws first quantized to 8 bits and the trained model was deployed in the MSP430. This was done by manually porting the TFlite runtime (with the source code available at https://github.com/tensorflow/tensorflow) to work with the MSP430 running at 16 Mhz:
## BOBBER Convolutional Accelerator
The convolutional MAC is a 3-stage pipeline and has a variable-width instruction set architecture (ISA). Each instruction starts with a 4-bit opcode. The ISA comprises three instructions
1. Load data from MCU: 11-bit address, 10-bit count, and up to
210 bytes
2.  2x2 MAC: 11-bit addresses, 6-bit count, and up to 26 bytes
3.  Store data to MCU: 11-bit address and a single byte

## BOBBER-based CNN Application

The acc_interface.h file provides several macros and functions that can be used when an idempotent kernel function has to be created. These function and variables are automatically used by the assembled which are linked in the ctpl assembly located in fram-utilities/ctpl_low_level.s. In this platform the Depthwise and first Convolutional layer MAC operations are offloaded to the FPGA. 
// Extern variables used in the idempotent kernel calls and used for depthwise and conv kernels

|variable/function name |  Operation     |
| ------- | --------  |
| ACCEL_FUNC_RESTORE_FLAG   | A flag to indicate the program execution is inside the computation kernel and ctpl to switch to a different ctpl_save sub assembly|
| RESTORE_SP_flag  | A flag set only once to get the Stack pointer pointing to the base of the function	|
| RESTORE_Reg_flag   | A flag to indicate a power failure has occured and the argument registers, along with SP need to be restored |
| Rx_val | A variable used inside the kernel to save argument register values(here x denotes the register (Eg: R1, R15, R9 etc)	|
|  ACCEL_FUNC_RESTORE_PC   | A variable to store the Program counter which points to the start of idempotent kernel function       |  
|  ACCEL_FUNC_RESTORE_SP   | A varibale to store the Stack pointer pointing to the base of the function |  
|  OUTSIDE_KERNEL_SP   | A varibale to store the Stack pointer which is used to calculate the activation record size |
|  cont_load_en   | A flag set to indicate continuous loading of data	|
|  weight_addr   | A variable to indicate the base address for weights/inputs/outputs in the FPGA	|
|  len_weights	 | A variable to indicate the number of continuous load packets goint to the FPGA	|
|  kernel_execute_counter   | A variable used to count the number of executions of the idempotent kernel   |
|  early_die_kernel_size   | A variable to indicate when the system should "early die" |
|  wait_for_fpga_boot()   |helper method used to wait till the FPGA boots back after a power failure |
|  init_accelerator_all ()  |helper method to Reset all banks of FPGA memory    |
|  cont_load_inst ()  | helper method to continuously load data to the FPGA memory. Note: len_weights should be set before calling this instruction|
|  bulk_two_by_two ()  | helper method to perform num_bulk +1 multiple 2by2's and returns an accumulated 32 bit output   |
|  read_data ()  | helper method for reading 32 bit output    |
|  load_data ()  | helper method for loading 8 bit input  |
|  load_data_weights ()  | helper method for loading 8 bit weights    |
|  mac_ret_deassert()   | helper method for deasserting the FPGA to Flash freeze mode    |
|  mac_ret_assert()   | helper method to assert the FPGA into Flash freeze mode    |

# License
Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.