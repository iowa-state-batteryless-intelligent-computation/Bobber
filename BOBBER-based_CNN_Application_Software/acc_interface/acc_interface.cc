
#include <stdio.h>
#include "acc_interface/acc_interface.h"
#include "msp430.h"
#include "uarttx.h"

#define debug_uart 0

//char readstr_acc[40]={0};

//#include <stdint.h>
int32_t membank[4][32]={0};
int16_t received_ch=0;
uint32_t i,j,numcount=0;
int extern_adc_en= 0;
int intermittent_power_flag=0;
int32_t TXdata;
// Extern variables used in the idempotent kernel calls and used for depthwise and conv kernels
int opt_1v5_Reg_flag =1;
int cont_load_en ;
unsigned int weight_addr = 0;
unsigned int len_weights= 0;
unsigned long int kernelstack_size;
int ACCEL_FUNC_RESTORE_FLAG = 0x00;
int RESTORE_SP_flag = 0x00;
int RESTORE_Reg_flag= 0x00;
int kernel_execute_counter=0x00;
int early_die_kernel_size=0x00;
unsigned long int ACCEL_FUNC_RESTORE_PC;
unsigned long int ACCEL_FUNC_RESTORE_SP;
unsigned long int OUTSIDE_KERNEL_SP;
unsigned long int R12_val;
unsigned long int R13_val;
unsigned long int R14_val;
unsigned long int R15_val;
unsigned long int R4_val;
unsigned long int R5_val;
unsigned long int R6_val;
unsigned long int R11_val;


void inline send_data(int8_t data) {
//    uartTX("S\n");
    numcount++;
//    P1OUT ^= BIT5;
//    __delay_cycles(50);
//    P1OUT ^= BIT5;
    TXdata = (int8_t)data;
    UCB1IE |= UCTXIE;
//    __bis_SR_register(LPM0_bits | GIE); // CPU off, enable interrupts
    while(0x1 & UCB1STATW);
//    uartTX("SD\n");
//    UCB1IE &= ~UCTXIE;
}

char mac_ret_deassert() {
    // clear out the memory banks
    P1OUT &= (~BIT4);
    __delay_cycles(80);
    return 1;
}

char mac_ret_assert() {
    P1OUT |= BIT4;
    __delay_cycles(80);
    return 1;
}

void fpga_core_power_enable(){

    P1OUT |= BIT3;
    __delay_cycles(24000);

}

void fpga_3v3_power_enable(){
    P1OUT |= BIT2;
    __delay_cycles(800);
}

void fpga_core_power_disable(){
    P1OUT &= ~BIT3;
    __delay_cycles(800);

}

void fpga_3v3_power_disable(){
    P1OUT &= ~BIT2;
    __delay_cycles(800);

}
void wait_for_fpga_boot(){
    if(!extern_adc_en){
        __delay_cycles(1200000);
    }
}

void early_die(){
    P1OUT |= BIT0;
    if(intermittent_power_flag){
        __delay_cycles(8000000);

    }

}

 char init_accelerator_all() {
     // clear out the memory banks
     P1OUT &= (~BIT5);
        // do the same thing, but now using the interface
     P1OUT |= BIT5; // reset the board
     __delay_cycles(500);
     P1OUT &= (~BIT5);
     return 1;
}


 void cont_load_inst(const int8_t* data){
     uint8_t inst3, inst2, inst1, inst0;

     if(cont_load_en == 1){
         inst3 = (uint8_t)(0b11110000 | (0b0000011110000000 & weight_addr) >> 7);
         inst2 = (uint8_t)((0b0000000001111111 & weight_addr) << 1);
         inst1 = (uint8_t)((0b0000001100000000 & len_weights) >> 8);
         inst0 = (uint8_t)((0b0000000011111111 & len_weights)) ;
//         printf("\nval %02x",inst0);
//         printf("\nval %02x",inst1);
//         printf("\nval %02x",inst2);
//         printf("\nval %02x",inst3);


         send_data(inst0);
         send_data(inst1);
         send_data(inst2);
         send_data(inst3);

         cont_load_en = 0;
     }

     TXdata = (int8_t)(*data);
     UCB1IE |= UCTXIE;
     while(0x1 & UCB1STATW);
 }


 // transfer data from MSP to FPGA
 // address is 11 bit, data is 8 bit
 void load_data(unsigned char bank_idx, unsigned char address, int8_t data) {
     unsigned char inst3, inst2, inst1, inst0;

     inst3 = 0x00;
     inst2 = bank_idx;
     inst1 = address;
     inst0 = (int8_t)data;

     send_data(inst0);
     send_data(inst1);
     send_data(inst2);
     send_data(inst3);

 }

// transfer data from FPGA to MSP
// 7 but address
int32_t read_data(unsigned char bank_idx,unsigned char address){
    //    uartTX("R\n");
        int32_t result;
        // read address+0
//        read_byte(bank_idx,address); //buffer instruction
        read_byte(bank_idx,address);
        // store result from previous read and read address+1
        result =  read_byte(bank_idx,address+1) ;
        // store result from previous, and read address + 2
        result = result | read_byte(bank_idx,address+2) << 8;
        result = result | read_byte(bank_idx,address+3) << 16;
        result = result | read_byte(bank_idx,address+3) << 24;
    //    result = result << 16;
//        result = result << 24;
//        __delay_cycles(1000);
        // store result from previous, and read address + 3
    //    result = result | read_byte(address+3) << 16;
        // store result from previous, and read address +3 again (doesnt matter what address it is)
    //    result = result | read_byte(address+3) << 24;
    //    uartTX("RD\n");
        return (int32_t)result;
}

int32_t read_byte(unsigned char bank_idx,unsigned char address){
//    uartTX("B\n");
    unsigned char inst3, inst2, inst1, inst0;

    inst3 = 0b01000000 | (bank_idx << 1);
    inst2 = (address & 0b00001111111) << 1;
    inst1 = 0x00;
    inst0 = 0x00;

    UCB1IE |= UCRXIE;
    int16_t result_byte ;

    send_data(inst0);
    // the data from the previous read is now available

    send_data(inst1);
    send_data(inst2);
    result_byte =  received_ch;

    send_data(inst3);
    UCB1IE &= ~UCRXIE;
//    uartTX("BD\n");
    return  result_byte;
}


// perform 1 single 2x2 m-m
void two_by_two(unsigned short addr_data, unsigned short addr_weight, unsigned char output_addr){
    unsigned char inst3, inst2, inst1, inst0;

//    inst3 = 0b11000000 | addr_data >> 3;
//    inst2 = ((addr_data & 0b0000111) << 5) | addr_weight >> 2;
//    inst1 = addr_weight < 7;
//    inst0 = output_addr;

    inst3 = 0b11100000 | ((0b0000011110000000 & addr_data) >> 7) ;
    inst2 =((0b0000000001111111 & addr_data) << 1) | ((0b0000010000000000 & addr_weight) >> 10) ;
    inst1 =(0b0000001111111100 & addr_weight) >> 2 ;
    inst0 =((0b0000000000000011 & addr_weight)<< 6)| (0b00111111 & output_addr);

    send_data(inst0);
    send_data(inst1);
    send_data(inst2);
    send_data(inst3);

}

void bulk_two_by_two(unsigned short addr_data, unsigned short addr_weight, unsigned char num_bulk){
    unsigned char inst3, inst2, inst1, inst0;

//    inst3 = 0b11000000 | addr_data >> 3;
//    inst2 = ((addr_data & 0b0000111) << 5) | addr_weight >> 2;
//    inst1 = addr_weight < 7;
//    inst0 = output_addr;

    inst3 = 0b01100000 | ((0b0000011110000000 & addr_data) >> 7) ;
    inst2 =((0b0000000001111111 & addr_data) << 1) | ((0b0000010000000000 & addr_weight) >> 10) ;
    inst1 =(0b0000001111111100 & addr_weight) >> 2 ;
    inst0 =((0b0000000000000011 & addr_weight)<< 6)| (0b00111111 & num_bulk);

    send_data(inst0);
    send_data(inst1);
    send_data(inst2);
    send_data(inst3);

}

void two_by_two_avgpool(unsigned char addr_bank, unsigned char input_addr) {
    unsigned char inst3, inst2, inst1, inst0;

        inst3 = 0b01110000 | (addr_bank << 1)|(input_addr & 0b11000000);
        inst2 = (input_addr & 0b00001111111) << 1;
        inst1 = 0x00;
        inst0 = 0x00;

        send_data(inst0);
        // the data from the previous read is now available
        send_data(inst1);
        send_data(inst2);
        send_data(inst3);
    //    uartTX("BD\n");



}



#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
//#pragma vector=EUSCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(EUSCI_B1_VECTOR))) USCI_B1_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(UCB1IV, USCI_SPI_UCTXIFG))
    {
        case USCI_NONE: break;
        case USCI_SPI_UCRXIFG:
//            P7OUT |= BIT4;
            received_ch = UCB1RXBUF;
            UCB1IFG &= ~UCRXIFG;
//            UCB1IFG |= UCTXIFG;

//            __bic_SR_register_on_exit(LPM0_bits); // Wake up to setup next RX
            break;
        case USCI_SPI_UCTXIFG:
//            P7OUT &= ~BIT4;
            P7OUT &= ~BIT3;      // Pull SS bit low
            UCB1TXBUF = TXdata; //send some data
            while(0x1 & UCB1STATW);
            P7OUT |= BIT3;      // Set SS bit high
            UCB1IE &= ~UCTXIE;
//            __bic_SR_register_on_exit(LPM0_bits); // Wake up to setup next TX
            break;
        default: break;
    }
}
