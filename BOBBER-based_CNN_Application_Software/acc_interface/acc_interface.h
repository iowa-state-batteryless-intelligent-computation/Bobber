

#ifndef ACC_INTERFACE_ACC_INTERFACE_H_
#define ACC_INTERFACE_ACC_INTERFACE_H_

#include<stdint.h>
extern int opt_1v5_Reg_flag;
extern int cont_load_en;
extern unsigned int weight_addr;
extern unsigned int len_weights;

extern int ACCEL_FUNC_RESTORE_FLAG;
extern int RESTORE_STACKFRAME_flag;
extern int RESTORE_SP_flag;
extern int RESTORE_Reg_flag;
extern int kernel_execute_counter;
extern int early_die_kernel_size;
extern unsigned long int ACCEL_FUNC_RESTORE_PC;
extern unsigned long int ACCEL_FUNC_RESTORE_SP;
extern unsigned long int OUTSIDE_KERNEL_SP;

extern unsigned long int R12_val;
extern unsigned long int R13_val;
extern unsigned long int R14_val;
extern unsigned long int R15_val;
extern unsigned long int R4_val;
extern unsigned long int R5_val;
extern unsigned long int R6_val;
extern unsigned long int R11_val;
extern unsigned long int kernelstack_size;


//Interface instructions for the FPGA

char init_accelerator_all(); //Reset all banks of FPGA memory
char mac_ret_deassert(); // signal to de assert flash freeze mode
char mac_ret_assert(); //signal to assert the Flash freeze mode in fpga
void fpga_core_power_enable();
void fpga_core_power_disable();
void fpga_3v3_power_disable();
void fpga_3v3_power_enable();
void wait_for_fpga_boot();
void cont_load_inst(const int8_t* data); // Continuously load data to the FPGA memory. Note: len_weights should be set before calling this instruction, which denotes the number
void early_die(); //Early die pin                                         // of data packets being sent. cont_load_en=1 sends the opcode and base address once.)


// transfer data from MSP to FPGA
// data is 8 bit
void load_data(unsigned char bank_idx, unsigned char address, int8_t data); //loads a single byte of data
void bulk_two_by_two(unsigned short addr_data, unsigned short addr_weight, unsigned char num_bulk); //performs num_bulk +1 multiple 2by2's and returns an accumulated 32 bit output

void load_data_weights(unsigned char address, int8_t data, int w_i, int w_j);
// transfer data from FPGA to MSP
// 7 bit address
int32_t read_data(unsigned char bank_idx,unsigned char address);
// helper method for read_data
int32_t read_byte(unsigned char bank_idx,unsigned char address);
// perform 1 single 2x2 m-m
//void two_by_two_with_offset(unsigned char addr_data, unsigned char addr_weight, unsigned char offset_addr, unsigned char output_addr);

void two_by_two(unsigned short addr_data, unsigned short addr_weight, unsigned char output_addr);

void two_by_two_avgpool(unsigned char addr_bank, unsigned char input_addr);

// perform multiple 2x2 m-m
// calcamt is the number of calculation to perform, 0-3

#endif /* ACC_INTERFACE_ACC_INTERFACE_H_ */
