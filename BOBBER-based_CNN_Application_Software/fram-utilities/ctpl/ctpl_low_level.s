; --COPYRIGHT--,FRAM-Utilities
;  Copyright (c) 2015, Texas Instruments Incorporated
;  All rights reserved.
; 
;  This source code is part of FRAM Utilities for MSP430 FRAM Microcontrollers.
;  Visit http://www.ti.com/tool/msp-fram-utilities for software information and
;  download.
; --/COPYRIGHT--

#include <msp430.h> 	/*	# Include device header file*/

#include "ctpl_benchmark.h"	 /*      CTPL benchmark file*/
#include "ctpl_low_level.h"  /*CTPL header file */

#include "ctpl_low_level_macros.s"

.arch msp430fr5969
.cpu 430xv2

#define PC R0
#define SP R1
#define SR R2
 /*STACK_END*/
; Define RAM address

.set CTPL_RAM_START,      1c00h

; State keys
.set  CTPL_STATE_VALID,          0xa596
.set  CTPL_STATE_INVALID,        0x0000

; FRAM stack copy Used .space in place of .usect
/*ctpl_stackCopy:      .usect ".TI.persistent",CTPL_STACK_SIZE,2 */

.sect .persistent,"aw", @progbits
.align 2
; FRAM stack copy
ctpl_stackCopy: .space CTPL_STACK_SIZE
.align 2

kernel_activation_record_copy: .space KERNEL_STACK_FRAME_SIZE
.align 2

/*ctpl_stackCopy: .sect .persistent,"awM", @progbits,CTPL_STACK_SIZE
.align 2*/

; RAM copy
#if defined(CTPL_RAM_SIZE)
/*ctpl_ramCopy:        .usect ".TI.persistent",CTPL_RAM_SIZE,2  */
; FRAM stack copy
/*ctpl_ramCopy: .sect .persistent,"awM", @progbits,CTPL_RAM_SIZE
.align 2*/
ctpl_ramCopy: .space CTPL_RAM_SIZE
.align 2
#endif

; Low level state variables
/*ctpl_mode           .usect ".TI.persistent",2,2
ctpl_state          .usect ".TI.persistent",2,2
ctpl_stackUsage     .usect ".TI.persistent",2,2*/
/*
ctpl_mode: .sect .persistent,"awM", @progbits,2
.align 2

ctpl_state: .sect .persistent,"awM", @progbits,2
.align 2
ctpl_stackUsage: .sect .persistent,"awM", @progbits,2
.align 2*/

; Low level state variables
ctpl_mode: .space 2
ctpl_adc_int_SP_copy: .space 2
//ctpl_isr_restore: .space 2
ctpl_state: .space 2
ctpl_stackUsage: .space 2
kernel_stacksize: .space 2

; Global symbols
    #.global __STACK_END
	.global __stack

    .sect ".upper.text.ctpl_low_level"
	.align 2
; Declare functions globally
    .global ctpl_init
    .global ctpl_saveCpuStackEnterLpm
    .global ctpl_unlockFRAM
    .global ctpl_restoreFRAM
	.global save_kernelactivation_record
	.global restore_kernelactivation_record
ctpl_init:
    /*unlockFRAM                                 ; Unlock FRAM (FR2xx and FR4xx only)*/
    cmpx.w  #CTPL_STATE_VALID,&ctpl_state       ; Valid ctpl state?
    jne     ctpl_initReturn                     ; No, return
    movx.w  &ctpl_mode,R12                      ; Move ctpl mode to local
    and.b   #CTPL_MODE_BITS,R12                 ; Mask ctpl mode bits
    cmp.b   #CTPL_MODE_SHUTDOWN,R12             ; Shutdown mode?
    jz      ctpl_wakeup                         ; Yes, jump to wakeup (always restore)
    bit.w   #PMMRSTIFG, &PMMIFG                 ; Was there a RST/NMI?
    jnz     ctpl_initResetPowerup               ; Yes, jump to reset/powerup routine
ctpl_initNonReset:
    bit.w   #PMMLPM5IFG, &PMMIFG                ; Was reset due to LPMx.5 wakeup?
    jz      ctpl_initResetPowerup               ; No, jump to reset/powerup routine
    bisx.w  #CTPL_MODE_LPMX5_WAKEUP,&ctpl_mode  ; Yes, set the LPMx.5 wakeup status flag
    jmp     ctpl_wakeup                         ; Jump to wakeup
ctpl_initResetPowerup:
    bitx.w  #CTPL_MODE_RESTORE_RESET,&ctpl_mode ; Allow wakeup from reset/powerup?
   // bis.b   #BIT3,&P3OUT
    jnz     ctpl_wakeup                         ; Yes, jump to wakeup
ctpl_initReturn:
    movx.w  #CTPL_MODE_NONE,&ctpl_mode          ; Reset the mode to none
    movx.w  #CTPL_STATE_INVALID,&ctpl_state     ; Mark the state as invalid
   /* restoreFRAM:                                 ; Restore FRAM state (FR2xx and FR4xx only)*/
    reta                                        /*  Return*/

ctpl_saveCpuStackEnterLpm:
    pushx.a SR                                  ; Save SR to stack
    dint                                        ; disable interrupts
    nop                                         ; disable interrupts
   /*unlockFRAM                                 ; Unlock FRAM (FR2xx and FR4xx only)*/
  	movx.w  R12,&ctpl_mode                      ; Save CTPL mode
    and.b   #CTPL_MODE_BITS,R12                 ; Mask ctpl mode bits
    cmp.b   #CTPL_MODE_NONE,R12                 ; None mode?
    jz      ctpl_return                         ; Yes, return to function*/
    //movx.w  #ACCEL_FUNC_RESTORE_FLAG,&ctpl_alt_restore ;check if alternate function restore is required
    cmpx.w  #CTPL_MODE_NONE,&ACCEL_FUNC_RESTORE_FLAG	;compare if alt_restore is valid
    jnz		ctpl_alternate_save					;

ctpl_save_stack:
    pushm.a #8,R11                              ; Save R4-R11 to stack
    movx.w  #__stack,R4                         ; Calculate stack usage
    subx.a  SP,R4                               ; Calculate stack usage
    movx.w  R4,&ctpl_stackUsage                 ; Save stack usage
    movx.a  #ctpl_stackCopy,R6                  ; dest ptr
    movx.a  SP,R5                               ; src ptr
   /* copyx   R5,R6,R4                            ; copy the stack*/
    #ifdef __MSP430_HAS_DMA__
        clr.b   &DMA0CTL      ; sw trigger, channel 0
        movx.a  R5,&DMA0SA     ; set src address
        movx.a  R6,&DMA0DA     ; set dst address
        rra.w   R4             ; divide length by 2
        mov.w   R4, &DMA0SZ    ; set copy size
        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
    #endif
    /*movx.w  #CTPL_STATE_VALID,&ctpl_state     ; Mark the state as  valid*/
    #ifdef  CTPL_RAM_SIZE
	    movx.w  #CTPL_RAM_START,R5				;
	    movx.a  #ctpl_ramCopy,R6				;
	    movx.w  #CTPL_RAM_SIZE,R4				; copy the RAM contents
	    #ifdef __MSP430_HAS_DMA__
	        clr.b   &DMA0CTL      ; sw trigger, channel 0
	        movx.a  R5,&DMA0SA     ; set src address
	        movx.a  R6,&DMA0DA     ; set dst address
	        rra.w   R4             ; divide length by 2
	        mov.w   R4, &DMA0SZ    ; set copy size
	        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
	    #endif
    #endif
   /* bic.b 	#BIT2,&P7OUT;*/
ctpl_setStateValid:
    movx.w  #CTPL_STATE_VALID,&ctpl_state       ; Mark the state as valid
    /*restoreFRAM                                 ; Restore FRAM state (FR2xx and FR4xx only)*/
    cmp.b   #CTPL_MODE_SHUTDOWN,R12             ; Check for shutdown mode and disable SVSH
    jnz     ctpl_enterLpm                       ; No, jump to ctpl_enterLpm
ctpl_enterShutdownWithTimeout:
   /* configureDcoShutdown R13                    ; Reconfigure DCO for shutdown*/
    mov.b   #CSKEY_H,&CSCTL0_H                  ; Unlock CS registers
    mov.w   #DIVM__32+DIVS__32,&CSCTL3          ; Set maximum dividers
    mov.w   #DCOFSEL_6,&CSCTL1                  ; Set DCO 8MHz
    mov.w   #SELM_3+SELS_3,&CSCTL2              ; Source MCLK and SMCLK from DCO
    mov.b   R13,&CSCTL3_L                       ; Set timeout dividers
    clr.b   &CSCTL0_H                           ; Lock CS registers
    bic.w   #255,R13                            ; Clear lower bytes
    swpb    R13                                 ; Swap bytes
    add.w   #WDTPW+WDTCNTCL,R13                 ; Set WDT timeout
    mov.w   R13,&WDTCTL                         ; Set WDT timeout
    mov.b   #PMMPW_H,&PMMCTL0_H                 ; open PMM
    bis.b   #SVSHE,&PMMCTL0_L                   ; enable SVSH
    mov.b   #0,&PMMCTL0_H                       ; close PMM
ctpl_shutdownWaitForSvs:
    benchmark                                   ; Toggle the CTPL benchmark pin
    //xor.b 	#BIT5,&P1OUT
    jmp     ctpl_shutdownWaitForSvs             ; Wait for SVSH to put device into BOR
ctpl_enterLpm:
    benchmark                                   ; Toggle the CTPL benchmark pin
    lpmDebug                                    ; Optional LPMx.5 debug mode*
    mov.b   #PMMPW_H,&PMMCTL0_H                 ; Set LPMx.5 bit
    mov.b   #PMMREGOFF,&PMMCTL0_L               ; Set LPMx.5 bit
    bis.w   #LPM4_bits,SR                           ; Enter LPMx.5 mode
    nop
ctpl_alternate_save:
	//movx.a  #ISR_RET_SP,&ctpl_isr_restore		;
	//movx.a 	&ISR_RET_SP,R13					;
	//movx.w  #0x9570,R14 				   		; change this value
	//movx.w	&ISR_RET_SR,R15					;
	//movx.w  R7,-2(R8)							;
	//movx.w  R9,-4(R8)							;
	//movx.w  &ISR_RET_SR,&ctpl_alt_restore_add	;
	movx.a 	SP,&ctpl_adc_int_SP_copy			;
	movx.a 	&ISR_RET_SP,SP						;
	//movx.w 	#0x477c,-4(SP)					;
	pushx.w &ACCEL_FUNC_RESTORE_PC				;
	and.w	#0x0FFF,-2(SP)						;
	add.w	#0x3000,-2(SP)						;
	movx.w  #0001,&RESTORE_Reg_flag				;
	movx.w  #0000,&ACCEL_FUNC_RESTORE_FLAG		;
//	pushx.w &ctpl_adc_int_SP_copy				; change this value
	movx.a 	&ctpl_adc_int_SP_copy,SP			;
	jmp 	ctpl_save_stack						;

ctpl_wakeup:
    mov.w   #WDTPW+WDTHOLD,&WDTCTL              ; stop WDT
   /* configureDcoWakeup                          ; Reconfigure DCO for wakeup*/
 	mov.b   #CSKEY_H,&CSCTL0_H                  ; unlock CS registers
 	mov.w   #DIVM__2,&CSCTL3                    ; set DCO to 4MHz (maximum boot freq)
    clr.b   &CSCTL0_H                           ; lock CS registers

    #ifdef  CTPL_RAM_SIZE
	    movx.w  #CTPL_RAM_START,R6				;
	    movx.a  #ctpl_ramCopy,R5				;
	    movx.w  #CTPL_RAM_SIZE,R4				;Copy all the RAM contents
		    #ifdef __MSP430_HAS_DMA__
		        clr.b   &DMA0CTL      ; sw trigger, channel 0
		        movx.a  R5,&DMA0SA     ; set src address
		        movx.a  R6,&DMA0DA     ; set dst address
		        rra.w   R4             ; divide length by 2
		        mov.w   R4, &DMA0SZ    ; set copy size
		        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
		    #endif
	    #endif
    movx.a  #__stack,SP                     ; Reset stack pointer
    movx.w  &ctpl_stackUsage,R4                 ; loop counter
    subx.a  R4,SP                               ; Reset stack pointer
    movx.a  SP,R6                               ; dest ptr
    movx.a  #ctpl_stackCopy,R5                  ; src ptr
    /*copyx   R5,R6,R4                            ; copy the stack*/
    #ifdef __MSP430_HAS_DMA__
        clr.b   &DMA0CTL      ; sw trigger, channel 0
        movx.a  R5,&DMA0SA     ; set src address
        movx.a  R6,&DMA0DA     ; set dst address
        rra.w   R4             ; divide length by 2
        mov.w   R4, &DMA0SZ    ; set copy size
        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
    #endif
    popm.a  #8,R11                              ; Restore R4-R11 from stack
    movx.w  #CTPL_STATE_INVALID,&ctpl_state     ; Mark the state as invalid
    #bic.b 	#BIT2,&P7OUT						;

ctpl_return:
    movx.w  &ctpl_mode,R12                      ; Return CTPL mode
   /* restoreFRAM                                 ; Restore FRAM state (FR2xx and FR4xx only)*/
    popx.a  R13                                 ; Restore interrupts
    nop                                         ; Required NOP
    movx.a  R13,SR                              ; Restore interrupts
    nop                                         ; Required NOP
    reta

;functions for checkpointing and restoring activation record of a current kernel call
save_kernelactivation_record:
	pushm.a #8,R11                             ; Save R4-R11 to stack
    movx.a 	SP,&ctpl_adc_int_SP_copy		   ;
	movx.a 	&ACCEL_FUNC_RESTORE_SP,SP		   ;
	//cmpx.w  #0x0000,&RESTORE_SP_flag   ; check if SP as well as stack is not checkpointed before
	//jnz kernelactivation_record_return 		   ;
	movx.w  &OUTSIDE_KERNEL_SP,R4			   ; stack frame size for a single activation:Note this number has to be changed based on the prologue of the kernel function
    subx.a  SP,R4                      		   ; Calculate stack usage
	movx.a  R4,&kernel_stacksize       		   ; activation record usage size
	movx.a  #kernel_activation_record_copy,R6  ; dest ptr
    movx.a  SP,R5                              ; src ptr
    #ifdef __MSP430_HAS_DMA__
        clr.b   &DMA0CTL       ; sw trigger, channel 0
        movx.a  R5,&DMA0SA     ; set src address
        movx.a  R6,&DMA0DA     ; set dst address
        rra.w   R4             ; divide length by 2
        mov.w   R4, &DMA0SZ    ; set copy size
        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
    #else
ctpl_copyLoop?:
        movx.w  @src+, 0(dst)   ; copy stack word and increment src ptr
        addx.a  #2,R6           ; increment dst ptr
        subx.a  #2,R4           ; decrement stack usage
        jnz     ctpl_copyLoop?  ; loop if usage > 0
    #endif
kernelactivation_record_return:
	movx.a 	&ctpl_adc_int_SP_copy,SP			;
    popm.a  #8,R11                              ; Restore R4-R11 from stack
	reta

restore_kernelactivation_record:
	pushm.a #8,R11                              ; Save R4-R11 to stack
    movx.a 	SP,&ctpl_adc_int_SP_copy			;
	cmpx.w  #0x0001,&RESTORE_Reg_flag           ; check if SP as well as stack is not checkpointed before
	jnz restore_kernelactivation_record_return  ;
	movx.a 	&ACCEL_FUNC_RESTORE_SP,SP		    ;
    movx.w  &kernel_stacksize,R4                ; loop counter
    movx.a  SP,R6                               ; dest ptr
    movx.a  #kernel_activation_record_copy,R5                  ; src ptr
    /*copyx   R5,R6,R4                          ; copy the stack*/
    #ifdef __MSP430_HAS_DMA__
        clr.b   &DMA0CTL      ; sw trigger, channel 0
        movx.a  R5,&DMA0SA     ; set src address
        movx.a  R6,&DMA0DA     ; set dst address
        rra.w   R4             ; divide length by 2
        mov.w   R4, &DMA0SZ    ; set copy size
        mov.w   #DMASWDW+DMADT_1+DMASRCINCR_3+DMADSTINCR_3+DMAEN+DMAREQ,&DMA0CTL    ; trigger DMA copy
    #else
ctpl_copyLoop?:
        movx.w  @src+, 0(dst)   ; copy stack word and increment src ptr
        addx.a  #2,R6          ; increment dst ptr
        subx.a  #2,R4          ; decrement stack usage
        jnz     ctpl_copyLoop?  ; loop if usage > 0
    #endif
restore_kernelactivation_record_return:
	movx.a 	&ctpl_adc_int_SP_copy,SP			;
    popm.a  #8,R11                              ; Restore R4-R11 from stack
    //movx.w  0x0000,&RESTORE_SP_flag				;
	reta
