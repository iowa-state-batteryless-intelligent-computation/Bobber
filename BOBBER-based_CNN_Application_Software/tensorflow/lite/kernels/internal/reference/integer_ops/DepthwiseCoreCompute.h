
#ifndef TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISECORECOMPUTE_H_
#define TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISECORECOMPUTE_H_

#include "tensorflow/lite/kernels/internal/common.h"
#include "acc_interface/acc_interface.h"
#include "uarttx.h"
#include "fram-utilities/ctpl/ctpl.h"
#include "fram-utilities/ctpl/ctpl_low_level.h"



namespace tflite {
    namespace reference_integer_ops {
        inline void DepthwiseCoreCompute(
                const int pad_width, const int stride_width, const int input_depth,
                const int stride_height, const int pad_height, const int filter_width,const int filter_height,
                const int out_y_tile, const int out_x_tile, const int tile_size_width, const int tile_size_height,
                const int dilation_width_factor, const int dilation_height_factor,
                const int input_width, const int input_height, const int8 *input_data,
                const RuntimeShape &input_shape,const int batch, const int depth_multiplier,const int8 *filter_data,
                const RuntimeShape& filter_shape,const int32 input_offset,const int32* bias_data,
                const int32 *output_multiplier, const int32 *output_shift,const int32 output_offset,const int32 output_activation_min,
                const int32 output_activation_max, const RuntimeShape &output_shape, int8* output_data){
                __get_R12_register(R12_val);
                __get_R13_register(R13_val);
                __get_R14_register(R14_val);
                __get_R15_register(R15_val);
                __get_R4_register(R4_val);
//                __get_R6_register(R6_val);
//                __get_R5_register(R5_val);
    //            __get_R11_register(R11_val);
                if(!RESTORE_SP_flag){
                    __get_SP_register_mem(ACCEL_FUNC_RESTORE_SP);
                    RESTORE_SP_flag=1;
                    }


                save_kernelactivation_record();
                ACCEL_FUNC_RESTORE_FLAG = 0x01;
                if(kernel_execute_counter==early_die_kernel_size && early_die_kernel_size!=0){
                    kernel_execute_counter=0;
                    early_die();
                }
                ACCEL_FUNC_RESTORE_PC=__get_PC_register()+0x14;

                if(RESTORE_Reg_flag){
                    __set_SP_register_mem(ACCEL_FUNC_RESTORE_SP);

                    __set_R12_register(R12_val);
                    __set_R13_register(R13_val);
        //            __set_R9_register(R9_val);
                    __set_R14_register(R14_val);
        //            __set_R8_register(R8_val);
                    __set_R15_register(R15_val);
                    __set_R4_register(R4_val);
                    restore_kernelactivation_record();
                    __enable_interrupt();
                    opt_1v5_Reg_flag=1;

//                    wait_for_fpga_boot();

    //                __set_R6_register(R6_val);
    //                __set_R5_register(R5_val);
        //            __set_R11_register(R11_val);
                    RESTORE_Reg_flag=0;

                    }
//                early_die();


//                ACCEL_FUNC_RESTORE_FLAG = 0x01;

//                uartTX("\nFI");
                char readstr[40]={0};

                init_accelerator_all();
                cont_load_en = 1; // to send instruction for cont load once
                len_weights = (input_depth*depth_multiplier*filter_height*filter_width); //depthwise load 5*5*6 weights at once for all the output channels, which can be reused
                weight_addr = 1024;// third memory bank (1)
                int32_t offsets_all[6] ={0};
                int8_t const_input_val = 0x00; // input value to be sent if outside image
                int8_t data_append = 0x01;
                int cont_input_load = 0; //default cont load = 0
                int cont_weight_load = 1;
                if(cont_weight_load == 1){
//                    uartTX("\nLW");

                    for (int in_channel = 0; in_channel < input_depth; ++in_channel) {
                        for (int m = 0; m < depth_multiplier; ++m) {
                            const int output_channel = m + in_channel * depth_multiplier;
                            for (int filter_x = 0; filter_x < filter_width; ++filter_x) {
                                for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
                                    cont_load_inst(filter_data + Offset(filter_shape, 0, filter_y, filter_x, output_channel)); //load weights
                                    int8_t filter_val = filter_data[Offset(filter_shape, 0, filter_y, filter_x, output_channel)];
                                    // Zero padding by omitting the areas outside the image.
                                    offsets_all[m] += input_offset * filter_val;
                                    }
                                }
                             }
                        }
                    if(len_weights%4!=0){
                        for (unsigned int i=len_weights%4; i<4; i++){
                             cont_load_inst(&data_append); //load weights
                         }
                    }

                }
//                uartTX("\nLW done");

//                sprintf(readstr, "\n tile_size_height: %d",tile_size_height);
//                uartTX(readstr);
//                sprintf(readstr, ",tile_size_width: %d",tile_size_width);
//                uartTX(readstr);
                cont_weight_load = 0;
                cont_load_en = 0;
                int index_update=0;
                unsigned char output_bank_idx=0;
                unsigned char output_addr = 0;// First memory bank (3)
                for (int in_channel = 0; in_channel < input_depth; ++in_channel) {
                for (int out_y = out_y_tile; out_y < out_y_tile+tile_size_height;++out_y){
                         cont_input_load =1;  //enable cont load and send inputs everytime for each vertical stride
                         weight_addr = 512; // second memory bank (0)
                         cont_load_en = 1; // To send the instruction
                         len_weights = (tile_size_width+2*pad_width)*filter_width;     //len of inputs to be sent

                         for (int out_x = out_x_tile; out_x < out_x_tile+tile_size_width; ++out_x) {
//                           sprintf(readstr, "\nout_x: %d",out_x);
//                           uartTX(readstr);
//                           sprintf(readstr, ",out_y: %d",out_y);
//                           uartTX(readstr);
                             if(cont_input_load ==1){
//                                     uartTX("\nLI");

                                     for(int out_x_contload= out_x_tile; out_x_contload< out_x_tile+tile_size_width+2*pad_width; out_x_contload+=5){
                                     const int in_x_origin_contload = (out_x_contload * stride_width) - pad_width;
                                     const int in_y_origin_contload = (out_y * stride_height) - pad_height;
                                     for (int filter_x_contload = 0; filter_x_contload < filter_width; ++filter_x_contload) {
                                         for (int filter_y_contload = 0; filter_y_contload < filter_height; ++filter_y_contload) {
                                             const int in_x_contload = in_x_origin_contload + dilation_width_factor * filter_x_contload;
                                             const int in_y_contload = in_y_origin_contload + dilation_height_factor * filter_y_contload;
                                             if((in_x_contload < out_x_tile+tile_size_width+pad_width)){
                                                const bool is_point_inside_image =(in_x_contload >= 0) && (in_x_contload < input_width) && (in_y_contload >= 0) && (in_y_contload < input_height);
//                                                uartTX("\nbefore send");

                                                if (!is_point_inside_image) {
                                                    cont_load_inst(&const_input_val);
                                                    }
                                                else
                                                    {
                                                    cont_load_inst(input_data+ Offset(input_shape, batch, in_y_contload,in_x_contload, in_channel));
                                                    }
                                                }
                                             }
                                         }
                                  }
//                                     uartTX("\nafter send");

                                  if(len_weights%4!=0){
                                      for (unsigned int i=len_weights%4; i<4; i++){
                                           cont_load_inst(&data_append); //load weights
                                       }
                                  }
//                                  uartTX("\ndata append");

                             }
//                             uartTX("\nLI done");

                             cont_input_load =0;
                             cont_load_en = 0;
                             for (int m = 0; m < depth_multiplier; ++m) {
                                 const int output_channel = m + in_channel * depth_multiplier;
                                 const int in_x_origin = (out_x * stride_width) - pad_width;
                                 const int in_y_origin = (out_y * stride_height) - pad_height;
                                 int32 acc = 0;

                                 const int in_x = in_x_origin + dilation_width_factor * 0;
                                 const int in_y = in_y_origin + dilation_height_factor * 0;
                                 // Zero padding by omitting the areas outside the image.

                                 const bool is_point_inside_image = (in_x >= 0) && (in_x < input_width) && (in_y >= 0) && (in_y < input_height);
//                                 uartTX("\nMult");

                                 if (is_point_inside_image) {
                                     int8_t input_val = input_data[Offset(input_shape, batch, in_y,in_x, in_channel)];
                                     int8_t filter_val = filter_data[Offset(filter_shape, 0, 0, 0, output_channel)];
                                     acc += filter_val * input_val;
                                     // skip the first element to  get 6 2by2 from 24 input/filters and compute the one on msp
                                     }
                                 index_update = 5*(out_x-out_x_tile);

                                 unsigned int num_2by2= 5;

                                 bulk_two_by_two(513+index_update,1025 + 25 * m, num_2by2);
//                                 sprintf(readstr, "\n 2BY2: %lx", read_data(output_bank_idx,output_addr));
//                                 uartTX(readstr);
                                 acc += read_data(output_bank_idx,output_addr)+offsets_all[m];

                                 if (bias_data) {
                                     acc += bias_data[output_channel];

                                 }
//                                 uartTX("\no/p write");

                                 acc = MultiplyByQuantizedMultiplier(
                                     acc, output_multiplier[output_channel],
                                     output_shift[output_channel]);
                                 acc += output_offset;
                                 acc = std::max(acc, output_activation_min);
                                 acc = std::min(acc, output_activation_max);
                                 output_data[Offset(output_shape, batch, out_y, out_x,
                                                    output_channel)] = static_cast<int8_t>(acc);
//                                 uartTX("\no/p write done");

                             }
                         }
                     }
             }
            kernel_execute_counter +=1;
//        ACCEL_FUNC_RESTORE_FLAG = 0x00;
       }
    }
}

#endif /* TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISECORECOMPUTE_H_ */
