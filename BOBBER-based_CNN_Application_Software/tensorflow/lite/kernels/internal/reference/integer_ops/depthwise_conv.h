/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#ifndef TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISE_CONV_H_
#define TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISE_CONV_H_


#include "tensorflow/lite/kernels/internal/common.h"
#include "acc_interface/acc_interface.h"
//#include "uarttx.h"
#include <tensorflow/lite/kernels/internal/reference/integer_ops/DepthwiseCoreCompute.h>

namespace tflite {
namespace reference_integer_ops {
inline void DepthwiseConvPerChannel(
    const DepthwiseParams& params, const int32* output_multiplier,
    const int32* output_shift, const RuntimeShape& input_shape,
    const int8* input_data, const RuntimeShape& filter_shape,
    const int8* filter_data, const RuntimeShape& bias_shape,
    const int32* bias_data, const RuntimeShape& output_shape,
    int8* output_data) {
  // Get parameters.
  // TODO(b/141565753): Re-introduce ScopedProfilingLabel on Micro.
  const int stride_width = params.stride_width;
  const int stride_height = params.stride_height;
  const int dilation_width_factor = params.dilation_width_factor;
  const int dilation_height_factor = params.dilation_height_factor;
  const int pad_width = params.padding_values.width;
  const int pad_height = params.padding_values.height;
  const int depth_multiplier = params.depth_multiplier;
  const int32 input_offset = params.input_offset;
  const int32 output_offset = params.output_offset;
  const int32 output_activation_min = params.quantized_activation_min;
  const int32 output_activation_max = params.quantized_activation_max;

  // Check dimensions of the tensors.
  TFLITE_DCHECK_EQ(input_shape.DimensionsCount(), 4);
  TFLITE_DCHECK_EQ(filter_shape.DimensionsCount(), 4);
  TFLITE_DCHECK_EQ(output_shape.DimensionsCount(), 4);

  TFLITE_DCHECK_LE(output_activation_min, output_activation_max);
  const int batches = MatchingDim(input_shape, 0, output_shape, 0);
  const int output_depth = MatchingDim(filter_shape, 3, output_shape, 3);
  const int input_height = input_shape.Dims(1);
  const int input_width = input_shape.Dims(2);
  const int input_depth = input_shape.Dims(3);
  const int filter_height = filter_shape.Dims(1);
  const int filter_width = filter_shape.Dims(2);
  const int output_height = output_shape.Dims(1);
  const int output_width = output_shape.Dims(2);
  TFLITE_DCHECK_EQ(output_depth, input_depth * depth_multiplier);
  TFLITE_DCHECK_EQ(bias_shape.FlatSize(), output_depth);
  const int tile_size_width = output_width/7;//14 tile width(eg::)
  const int tile_size_height= output_height/7; //14 tile height (eg::)
  RESTORE_SP_flag =0x00;
  opt_1v5_Reg_flag=1;

  early_die();
  kernel_execute_counter=0;

  early_die_kernel_size=0x01;
    for (int batch = 0; batch < batches; ++batch) {
        for ( int out_y_tile = 0; out_y_tile < output_height; out_y_tile+=tile_size_height){ //outer loops to move along tile size
            for ( int out_x_tile = 0; out_x_tile < output_width; out_x_tile+=tile_size_width) {
                    OUTSIDE_KERNEL_SP =__get_SP_register()+0xA0; //Get the stack pointer before allocation of the activation record.
//                    P1OUT ^= BIT0;                                            //20 comes 4bytes(PC+SR being pushed to stack for a callA instruction+ R4-R10 being pushed to stack for a function prologue
                    reference_integer_ops::DepthwiseCoreCompute(
                    pad_width, stride_width, input_depth,stride_height, pad_height, filter_width, filter_height,
                    out_y_tile,out_x_tile, tile_size_width,tile_size_height,
                    dilation_width_factor, dilation_height_factor, input_width, input_height, input_data,
                    input_shape,batch, depth_multiplier, filter_data,filter_shape,input_offset,bias_data,
                    output_multiplier, output_shift,output_offset,output_activation_min,output_activation_max,output_shape,output_data);
                    ACCEL_FUNC_RESTORE_FLAG = 0x00;
//                    P1OUT ^= BIT0;                                            //20 comes 4bytes(PC+SR being pushed to stack for a callA instruction+ R4-R10 being pushed to stack for a function prologue
                    }
                }
            }

        }
    }
}

#endif  // TENSORFLOW_LITE_KERNELS_INTERNAL_REFERENCE_INTEGER_OPS_DEPTHWISE_CONV_H_
