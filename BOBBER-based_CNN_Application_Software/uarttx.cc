
#include <msp430fr5994.h>


void uartTX(char * tx_data)                 // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
        while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
        {
            while ((UCA0STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
            UCA0TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
            i++;                                 // Increment variable for array address
        }
}


void uartConfig() {
    // Set P2.0 and P2.1 for USCI_A0 UART operation
        P2SEL0 &= ~(BIT0 | BIT1);
        P2SEL1 |= BIT0 | BIT1;
        UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
        UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
        UCA0BRW = 4;                             // 8000000/16/115200
    //    UCA0BRW = 52;
        UCA0MCTLW |= UCOS16 | UCBRF_5 | 0x55;
    //    UCA0MCTLW |= UCOS16 | UCBRF_1 | 0x49;
        UCA0CTLW0 &= ~UCSWRST;

}
//
//void UART_TX_A3(char * tx_data)                 // Define a function which accepts a character pointer to an array
//{
//    unsigned int i=0;
//    while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
//    {
//        while ((UCA3STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
//        UCA3TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
//        i++;                                 // Increment variable for array address
//    }
//}
//
//void UART_config_back_channel() {
//    // setup pin TX 6.0 and RX 6.1 for back-channel UART
//    P6SEL1 &= ~(BIT0 | BIT1);
//    P6SEL0 |= (BIT0 | BIT1);                // USCI_A3 UART operation
//    UCA3CTLW0 = UCSWRST;                      // Put eUSCI in reset
//    UCA3CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
//    UCA3BRW = 4;                             // 8000000/16/115200
//    UCA3MCTLW |= UCOS16 | UCBRF_5 | 0x55;
//    UCA3CTLW0 &= ~UCSWRST;
//}
//
