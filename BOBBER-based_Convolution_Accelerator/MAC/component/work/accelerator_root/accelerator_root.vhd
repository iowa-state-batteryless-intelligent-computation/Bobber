----------------------------------------------------------------------
-- Created by SmartDesign Tue Nov 29 11:00:10 2022
-- Version: v11.9 11.9.0.4
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library igloo;
use igloo.all;
----------------------------------------------------------------------
-- accelerator_root entity declaration
----------------------------------------------------------------------
entity accelerator_root is
    -- Port list
    port(
        -- Inputs
        PAD     : in  std_logic;
        i_CLK   : in  std_logic;
        i_CS_N  : in  std_logic;
        i_MOSI  : in  std_logic;
        i_RST   : in  std_logic;
        i_SCLK  : in  std_logic;
        -- Outputs
        o_CALC  : out std_logic;
        o_DONE  : out std_logic;
        o_MISO  : out std_logic;
        o_READ  : out std_logic;
        o_WRITE : out std_logic;
        o_boot  : out std_logic
        );
end accelerator_root;
----------------------------------------------------------------------
-- accelerator_root architecture body
----------------------------------------------------------------------
architecture RTL of accelerator_root is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- acCylator
-- using entity instantiation for component acCylator
-- Debounce
component Debounce
    -- Port list
    port(
        -- Inputs
        CLK     : in  std_logic;
        DATA    : in  std_logic;
        -- Outputs
        OP_DATA : out std_logic
        );
end component;
-- INBUF
component INBUF
    -- Port list
    port(
        -- Inputs
        PAD : in  std_logic;
        -- Outputs
        Y   : out std_logic
        );
end component;
-- INBUF_FF
component INBUF_FF
    -- Port list
    port(
        -- Inputs
        PAD : in  std_logic;
        -- Outputs
        Y   : out std_logic
        );
end component;
-- SPI_SLAVE
component SPI_SLAVE
    -- Port list
    port(
        -- Inputs
        CLK      : in  std_logic;
        CS_N     : in  std_logic;
        DIN      : in  std_logic_vector(7 downto 0);
        DIN_VLD  : in  std_logic;
        MOSI     : in  std_logic;
        RST      : in  std_logic;
        SCLK     : in  std_logic;
        -- Outputs
        DOUT     : out std_logic_vector(7 downto 0);
        DOUT_VLD : out std_logic;
        MISO     : out std_logic;
        READY    : out std_logic
        );
end component;
-- unified_inst_module
component unified_inst_module
    -- Port list
    port(
        -- Inputs
        i_CLK         : in  std_logic;
        i_INST_IN_SPI : in  std_logic_vector(7 downto 0);
        i_RST         : in  std_logic;
        i_SPI_OUT_VLD : in  std_logic;
        -- Outputs
        o_DONE        : out std_logic;
        o_INST        : out std_logic_vector(31 downto 0);
        o_boot        : out std_logic;
        o_cnt         : out std_logic_vector(1 downto 0);
        o_mult_done   : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal acCylator_0_o_ps_0_0              : std_logic_vector(7 downto 0);
signal Debounce_0_OP_DATA                : std_logic;
signal Debounce_1_OP_DATA                : std_logic;
signal INBUF_0_Y                         : std_logic;
signal INBUF_1_Y                         : std_logic;
signal o_boot_net_0                      : std_logic;
signal o_CALC_net_0                      : std_logic_vector(0 to 0);
signal o_DONE_net_0                      : std_logic;
signal o_MISO_net_0                      : std_logic;
signal o_READ_net_0                      : std_logic_vector(1 to 1);
signal o_WRITE_net_0                     : std_logic;
signal SPI_SLAVE_0_DOUT                  : std_logic_vector(7 downto 0);
signal unified_inst_module_0_o_INST      : std_logic_vector(31 downto 0);
signal unified_inst_module_0_o_mult_done : std_logic;
signal o_DONE_net_1                      : std_logic;
signal o_MISO_net_1                      : std_logic;
signal o_READ_net_1                      : std_logic;
signal o_CALC_net_1                      : std_logic;
signal o_WRITE_net_1                     : std_logic;
signal o_boot_net_1                      : std_logic;
signal o_cnt_net_0                       : std_logic_vector(1 downto 0);

begin
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 o_DONE_net_1  <= o_DONE_net_0;
 o_DONE        <= o_DONE_net_1;
 o_MISO_net_1  <= o_MISO_net_0;
 o_MISO        <= o_MISO_net_1;
 o_READ_net_1  <= o_READ_net_0(1);
 o_READ        <= o_READ_net_1;
 o_CALC_net_1  <= o_CALC_net_0(0);
 o_CALC        <= o_CALC_net_1;
 o_WRITE_net_1 <= o_WRITE_net_0;
 o_WRITE       <= o_WRITE_net_1;
 o_boot_net_1  <= o_boot_net_0;
 o_boot        <= o_boot_net_1;
----------------------------------------------------------------------
-- Slices assignments
----------------------------------------------------------------------
 o_CALC_net_0(0) <= o_cnt_net_0(0);
 o_READ_net_0(1) <= o_cnt_net_0(1);
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- acCylator_0
acCylator_0 : entity work.acCylator
    generic map( 
        D => ( 8 ),
        M => ( 7 ),
        N => ( 32 )
        )
    port map( 
        -- Inputs
        i_RST       => INBUF_0_Y,
        i_mult_done => unified_inst_module_0_o_mult_done,
        i_CLK       => i_CLK,
        i_inst      => unified_inst_module_0_o_INST,
        -- Outputs
        o_done      => o_DONE_net_0,
        o_ps_rw     => OPEN,
        o_WRITE     => OPEN,
        o_READ      => OPEN,
        o_CALC      => OPEN,
        o_ps_0      => acCylator_0_o_ps_0_0,
        o_ps_1      => OPEN 
        );
-- Debounce_0
Debounce_0 : Debounce
    port map( 
        -- Inputs
        DATA    => i_SCLK,
        CLK     => i_CLK,
        -- Outputs
        OP_DATA => Debounce_0_OP_DATA 
        );
-- Debounce_1
Debounce_1 : Debounce
    port map( 
        -- Inputs
        DATA    => i_MOSI,
        CLK     => i_CLK,
        -- Outputs
        OP_DATA => Debounce_1_OP_DATA 
        );
-- INBUF_0
INBUF_0 : INBUF
    port map( 
        -- Inputs
        PAD => i_RST,
        -- Outputs
        Y   => INBUF_0_Y 
        );
-- INBUF_1
INBUF_1 : INBUF
    port map( 
        -- Inputs
        PAD => i_CS_N,
        -- Outputs
        Y   => INBUF_1_Y 
        );
-- INBUF_FF_0
INBUF_FF_0 : INBUF_FF
    port map( 
        -- Inputs
        PAD => PAD,
        -- Outputs
        Y   => OPEN 
        );
-- SPI_SLAVE_0
SPI_SLAVE_0 : SPI_SLAVE
    port map( 
        -- Inputs
        CLK      => i_CLK,
        RST      => INBUF_0_Y,
        SCLK     => Debounce_0_OP_DATA,
        CS_N     => INBUF_1_Y,
        MOSI     => Debounce_1_OP_DATA,
        DIN_VLD  => o_DONE_net_0,
        DIN      => acCylator_0_o_ps_0_0,
        -- Outputs
        MISO     => o_MISO_net_0,
        READY    => OPEN,
        DOUT_VLD => o_WRITE_net_0,
        DOUT     => SPI_SLAVE_0_DOUT 
        );
-- unified_inst_module_0
unified_inst_module_0 : unified_inst_module
    port map( 
        -- Inputs
        i_CLK         => i_CLK,
        i_RST         => INBUF_0_Y,
        i_SPI_OUT_VLD => o_WRITE_net_0,
        i_INST_IN_SPI => SPI_SLAVE_0_DOUT,
        -- Outputs
        o_DONE        => OPEN,
        o_mult_done   => unified_inst_module_0_o_mult_done,
        o_boot        => o_boot_net_0,
        o_INST        => unified_inst_module_0_o_INST,
        o_cnt         => o_cnt_net_0 
        );

end RTL;
