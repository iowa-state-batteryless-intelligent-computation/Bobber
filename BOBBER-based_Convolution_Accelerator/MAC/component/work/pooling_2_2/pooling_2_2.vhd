----------------------------------------------------------------------
-- Created by SmartDesign Fri Feb 18 19:58:20 2022
-- Version: v11.9 11.9.0.4
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library igloo;
use igloo.all;
----------------------------------------------------------------------
-- pooling_2_2 entity declaration
----------------------------------------------------------------------
entity pooling_2_2 is
    -- Port list
    port(
        -- Inputs
        i_D0 : in  std_logic_vector(7 downto 0);
        i_D1 : in  std_logic_vector(7 downto 0);
        i_D2 : in  std_logic_vector(7 downto 0);
        i_D3 : in  std_logic_vector(7 downto 0);
        -- Outputs
        o_O  : out std_logic_vector(7 downto 0)
        );
end pooling_2_2;
----------------------------------------------------------------------
-- pooling_2_2 architecture body
----------------------------------------------------------------------
architecture RTL of pooling_2_2 is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- adder_8_libero
component adder_8_libero
    -- Port list
    port(
        -- Inputs
        DataA : in  std_logic_vector(7 downto 0);
        DataB : in  std_logic_vector(7 downto 0);
        -- Outputs
        Sum   : out std_logic_vector(7 downto 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal adder_8_libero_0_Sum : std_logic_vector(7 downto 0);
signal adder_8_libero_1_Sum : std_logic_vector(7 downto 0);
signal i_D0_slice_0         : std_logic_vector(7 to 7);
signal i_D0_slice_1         : std_logic_vector(6 downto 2);
signal i_D1_slice_0         : std_logic_vector(7 to 7);
signal i_D1_slice_1         : std_logic_vector(6 downto 2);
signal i_D2_slice_0         : std_logic_vector(7 to 7);
signal i_D2_slice_1         : std_logic_vector(6 downto 2);
signal i_D3_slice_0         : std_logic_vector(7 to 7);
signal i_D3_slice_1         : std_logic_vector(6 downto 2);
signal o_O_net_0            : std_logic_vector(7 downto 0);
signal o_O_net_1            : std_logic_vector(7 downto 0);
signal DataA_net_0          : std_logic_vector(7 downto 0);
signal DataB_net_0          : std_logic_vector(7 downto 0);
signal DataA_net_1          : std_logic_vector(7 downto 0);
signal DataB_net_1          : std_logic_vector(7 downto 0);

begin
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 o_O_net_1       <= o_O_net_0;
 o_O(7 downto 0) <= o_O_net_1;
----------------------------------------------------------------------
-- Slices assignments
----------------------------------------------------------------------
 i_D0_slice_0(7) <= i_D0(7);
 i_D0_slice_1    <= i_D0(6 downto 2);
 i_D1_slice_0(7) <= i_D1(7);
 i_D1_slice_1    <= i_D1(6 downto 2);
 i_D2_slice_0(7) <= i_D2(7);
 i_D2_slice_1    <= i_D2(6 downto 2);
 i_D3_slice_0(7) <= i_D3(7);
 i_D3_slice_1    <= i_D3(6 downto 2);
----------------------------------------------------------------------
-- Concatenation assignments
----------------------------------------------------------------------
 DataA_net_0 <= ( i_D0_slice_0(7) & i_D0_slice_0(7) & i_D0_slice_0(7) & i_D0_slice_1 );
 DataB_net_0 <= ( i_D1_slice_0(7) & i_D1_slice_0(7) & i_D1_slice_0(7) & i_D1_slice_1 );
 DataA_net_1 <= ( i_D2_slice_0(7) & i_D2_slice_0(7) & i_D2_slice_0(7) & i_D2_slice_1 );
 DataB_net_1 <= ( i_D3_slice_0(7) & i_D3_slice_0(7) & i_D3_slice_0(7) & i_D3_slice_1 );
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- adder_8_libero_0
adder_8_libero_0 : adder_8_libero
    port map( 
        -- Inputs
        DataA => DataA_net_0,
        DataB => DataB_net_0,
        -- Outputs
        Sum   => adder_8_libero_0_Sum 
        );
-- adder_8_libero_1
adder_8_libero_1 : adder_8_libero
    port map( 
        -- Inputs
        DataA => DataA_net_1,
        DataB => DataB_net_1,
        -- Outputs
        Sum   => adder_8_libero_1_Sum 
        );
-- adder_8_libero_2
adder_8_libero_2 : adder_8_libero
    port map( 
        -- Inputs
        DataA => adder_8_libero_0_Sum,
        DataB => adder_8_libero_1_Sum,
        -- Outputs
        Sum   => o_O_net_0 
        );

end RTL;
