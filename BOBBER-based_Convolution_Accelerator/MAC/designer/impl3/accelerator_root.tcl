# Created by Microsemi Libero Software 11.9.0.4
# Tue Nov 29 11:01:24 2022

# (OPEN DESIGN)

open_design "accelerator_root.adb"

# set default back-annotation base-name
set_defvar "BA_NAME" "accelerator_root_ba"
set_defvar "IDE_DESIGNERVIEW_NAME" {Impl3}
set_defvar "IDE_DESIGNERVIEW_COUNT" "3"
set_defvar "IDE_DESIGNERVIEW_REV0" {Impl1}
set_defvar "IDE_DESIGNERVIEW_REVNUM0" "1"
set_defvar "IDE_DESIGNERVIEW_REV1" {Impl2}
set_defvar "IDE_DESIGNERVIEW_REVNUM1" "2"
set_defvar "IDE_DESIGNERVIEW_REV2" {Impl3}
set_defvar "IDE_DESIGNERVIEW_REVNUM2" "3"
set_defvar "IDE_DESIGNERVIEW_ROOTDIR" {D:\Vishak\Git_files\Bobber\BOBBER-based_Convolution_Accelerator\MAC\designer}
set_defvar "IDE_DESIGNERVIEW_LASTREV" "3"


# import of input files
import_source  \
-format "edif" -edif_flavor "GENERIC" -netlist_naming "VHDL" {../../synthesis/accelerator_root.edn} \
-format "pdc"  {..\..\component\work\accelerator_root\accelerator_root.pdc} -merge_physical "yes" -merge_timing "yes"
compile
report -type "status" {accelerator_root_compile_report.txt}
report -type "pin" -listby "name" {accelerator_root_report_pin_byname.txt}
report -type "pin" -listby "number" {accelerator_root_report_pin_bynumber.txt}

save_design
