library IEEE;

use IEEE.std_logic_1164.all;

entity acCylator is
generic(N : integer := 32; M: integer:= 11; D: integer := 8); -- input data is 8bits, output is 32 bits, address is 11 bits
port (
    i_RST : in std_logic;
    i_mult_done: in std_logic;
    i_CLK : in std_logic;
    i_inst: in std_logic_vector (N-1 downto 0);  -- instruction
    o_done: out std_logic;
    o_ps_0: out std_logic_vector (D-1 downto 0);
    o_ps_1: out std_logic_vector (D-1 downto 0);
    o_ps_rw: out std_logic;

    o_WRITE: out std_logic;
    o_READ: out std_logic;
    o_CALC: out std_logic
);
end acCylator;

architecture architecture_acCylator of acCylator is
    
    -- import control component -- 
     component control
        -- ports
        port( 
            i_opcode: in std_logic_vector(3 downto 0); -- still 4 bit op code
            i_ADDRA: in std_logic_vector (11-1 downto 0); -- 11 bit, memory
            i_ADDRB: in std_logic_vector (11-1 downto 0);
            i_ADDR_OUT: in std_logic_vector (5 downto 0); -- 6 bit output offset
            --i_CALCAMT: in std_logic_vector (1 downto 0); -- calcamt for multi-calculation instructions
            i_CLK: in std_logic;
            i_RST: in std_logic;
            i_inst_mult_done: in std_logic;
            o_ADDRA: out std_logic_vector (11-1 downto 0);
            o_ADDRB: out std_logic_vector (11-1 downto 0);
            o_RWA: out std_logic;
            o_RWB: out std_logic;
            o_load_weight: out std_logic;
            o_load_input: out std_logic;
            o_done: out std_logic;
            o_ps_ADDRA : out std_logic_vector(11-1 downto 0);
            o_ps_ADDRB : out std_logic_vector(11-1 downto 0);
            o_ps_RWA : out std_logic;
            o_ps_RWB : out std_logic;
            o_load_ps : out std_logic;
            o_bulktwobytwo_acc_clk: out std_logic;
            o_bulktwobytwo_done : out std_logic;

            o_mem_mux_sel_a: out std_logic_vector(1 downto 0);
            o_mem_mux_sel_b: out std_logic_vector(1 downto 0);
            o_mem_buf_we_a: out std_logic;
            o_mem_buf_we_b: out std_logic;

            o_bias_vld: out std_logic
        );
    end component;

    -- memory component ---
    component memory_module
        port (
            i_CLK: in std_logic;
            i_RST: in std_logic;

            -- WD read ports
            i_WD_READ_ADDR_A: in std_logic_vector(11-1 downto 0); -- 7 bit address, port A of Weight/Data read
            i_WD_READ_ADDR_B: in std_logic_vector(11-1 downto 0); -- 7 bit address, port B of Weight/Data read
            o_WD_READ_DATA_A: out std_logic_vector(8-1 downto 0); -- 8 bit data
            o_WD_READ_DATA_B: out std_logic_vector(8-1 downto 0); -- 8 bit data

            -- WD write ports
            i_WD_WRITE_ADDR_A: in std_logic_vector(11-1 downto 0); -- 7 bit address, port A of Weight/Data input
            i_WD_WRITE_ADDR_B: in std_logic_vector(11-1 downto 0); -- 7 bit address, port B of Weight/Data input
            i_WD_WRITE_DATA_A: in std_logic_vector(8-1 downto 0); -- 8 bit data in
            i_WD_WRITE_DATA_B: in std_logic_vector(8-1 downto 0); -- 8 bit data in
            i_WD_WRITE_EN_A: in std_logic; -- port A write enable
            i_WD_WRITE_EN_B: in std_logic; -- port B write enable

            -- PS read ports
            i_PS_READ_ADDR_A: in std_logic_vector(11-1 downto 0); -- 7 bit address, port A of partial sum read
            i_PS_READ_ADDR_B: in std_logic_vector(11-1 downto 0); -- 7 bit address, port B of partial sum read
            o_PS_READ_DATA_A: out std_logic_vector(8-1 downto 0); -- 8 bit data
            o_PS_READ_DATA_B: out std_logic_vector(8-1 downto 0); -- 8 bit data

            -- WD write ports
            i_PS_WRITE_ADDR_A: in std_logic_vector(11-1 downto 0); -- 7 bit address, port A of partial sum input
            i_PS_WRITE_ADDR_B: in std_logic_vector(11-1 downto 0); -- 7 bit address, port B of partial sum input
            i_PS_WRITE_DATA_A: in std_logic_vector(8-1 downto 0); -- 8 bit data in
            i_PS_WRITE_DATA_B: in std_logic_vector(8-1 downto 0); -- 8 bit data in
            i_PS_WRITE_EN_A: in std_logic; -- port A write enable
            i_PS_WRITE_EN_B: in std_logic; -- port B write enable

            i_PS_READ: in std_logic
        );
    end component;

    -- systolic array component --
    component sys_array_2_2
        -- ports
        port( 
            -- Inputs
            i_weights_0 : in std_logic_vector(8-1 downto 0);
            i_weights_1 : in std_logic_vector (8-1 downto 0);
            i_input_0 : in std_logic_vector(8-1 downto 0);
            i_input_1 : in std_logic_vector(8-1 downto 0);
            i_ps_0 : in std_logic_vector(32-1 downto 0);
            i_ps_1 : in std_logic_vector(32-1 downto 0);
            i_load_weight : in std_logic;
            i_load_input : in std_logic;
            i_rst : in std_logic;
            i_clk : in std_logic;

            -- Outputs
            o_ps_0 : out std_logic_vector(32-1 downto 0);
            o_ps_1 : out std_logic_vector(32-1 downto 0)

            -- Inouts

        );
    end component;


    -- 8 bit 2t1 mux component --
    component mux2t1_8 -- actually it's 8 bits
        -- ports
        port(
            i_S          : in std_logic;
            i_D0         : in std_logic_vector(D-1 downto 0);
            i_D1         : in std_logic_vector(D-1 downto 0);
            o_O          : out std_logic_vector(D-1 downto 0)
        );
    end component;
    --- 8 bit 4t1 mux --
    component mux_4_to_1_8
        -- ports
        port( Data0_port : in    std_logic_vector(7 downto 0);
              Data1_port : in    std_logic_vector(7 downto 0);
              Data2_port : in    std_logic_vector(7 downto 0);
              Data3_port : in    std_logic_vector(7 downto 0);
              Sel0       : in    std_logic;
              Sel1       : in    std_logic;
              Result     : out   std_logic_vector(7 downto 0)
            );
    end component;

    -- 32 bit buffer register --
    component reg_32
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(N-1 downto 0); -- data in value
		o_D			: out std_logic_vector(N-1 downto 0) -- data out value
	);
    end component;

    -- 7 bit buffer register --
    component reg_8
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(7 downto 0); -- data in value
		o_D			: out std_logic_vector(7 downto 0) -- data out value
	);
    end component;

    component alu_32 
    port (
    i_clk: in std_logic;
    i_rst: in std_logic;
    i_data: in std_logic_vector(D-1 downto 0);        -- input data from the memory 
    i_opcode:   in std_logic_vector(3 downto 0);      -- opcode from instruction
    o_O:        out std_logic_vector(D-1 downto 0)    -- output data
    );
    end component;

    -- 32 bit shift register --
    component unified_inst_module 
    port (
    i_CLK: IN std_logic;
    i_RST: IN std_logic;
	i_INST_IN_SPI : IN  std_logic_vector(7 downto 0); -- 8bit wide transactions from SPI module
    i_SPI_OUT_VLD: IN std_logic; -- input from SPI module, '1' when a transaction is complete
    o_DONE: OUT std_logic;
    o_INST : OUT std_logic_vector(31 downto 0);  -- assembled and multiplied instructions
    o_mult_done: OUT std_logic;

    o_cnt: OUT std_logic_vector(1 downto 0)
);
    end component;
  
    -- 32 bit adder -- 
    component rc_adder_N 
      port(	i_D0         : in std_logic_vector(N-1 downto 0);
		i_D1         : in std_logic_vector(N-1 downto 0);
		i_C          : in std_logic;
		o_S          : out std_logic_vector(N-1 downto 0);
		o_C          : out std_logic;
		o_O			 : out std_logic
		);
    end component;

    component accumulator_32bit 
     port( DataA  : in    std_logic_vector(31 downto 0);
          Enable : in    std_logic;
          Aclr   : in    std_logic;
          Clock  : in    std_logic;
          Sum    : out   std_logic_vector(31 downto 0)
        );

    end component;


 -- signals
	signal s_addr_a: std_logic_vector(10 downto 0);
    signal s_addr_b: std_logic_vector(10 downto 0);
    signal s_rw_a: std_logic;
    signal s_rw_b: std_logic;
    signal s_lw: std_logic;
    signal s_li: std_logic;

    signal s_ps_addr_a: std_logic_vector(10 downto 0);
    signal s_ps_addr_b: std_logic_vector(10 downto 0);
    signal s_ps_rw_a: std_logic;
    signal s_ps_rw_b: std_logic;
    signal s_load_ps: std_logic;

    signal s_data_a: std_logic_vector(8-1 downto 0);
    signal s_data_b: std_logic_vector(8-1 downto 0);
    signal s_ip_a_systolic: std_logic_vector(8-1 downto 0);
    signal s_ip_b_systolic: std_logic_vector(8-1 downto 0);

    signal s_w_a_systolic: std_logic_vector(8-1 downto 0);
    signal s_w_b_systolic: std_logic_vector(8-1 downto 0);
    
    signal s_ps_in_a: std_logic_vector(8-1 downto 0);
    signal s_ps_in_b: std_logic_vector(8-1 downto 0);

    signal s_ps_out_a: std_logic_vector(32-1 downto 0); -- 32 bit ps out, from sys array
    signal s_ps_out_b: std_logic_vector(32-1 downto 0); -- 32 bit ps out, from sys array

    signal s_mem_ps_out_a: std_logic_vector(8-1 downto 0);
    signal s_mem_ps_out_b: std_logic_vector(8-1 downto 0);

    signal s_ps_buffer_out_a: std_logic_vector(32-1 downto 0);
    signal s_ps_buffer_out_b: std_logic_vector(32-1 downto 0);

    signal s_mem_ps_in_a: std_logic_vector(D-1 downto 0); -- 8 bit ps, for memory
    signal s_mem_ps_in_b: std_logic_vector(D-1 downto 0); -- 8 bit ps, for memory
    signal s_mem_ps_in_sel_a: std_logic_vector(1 downto 0);
    signal s_mem_ps_in_sel_b: std_logic_vector(1 downto 0);

    signal s_ps_buf_we_a: std_logic;
    signal s_ps_buf_we_b: std_logic;

    signal s_partial_bias_a: std_logic_vector(7 downto 0);
    signal s_partial_bias_b: std_logic_vector(7 downto 0);
    signal s_bias_ps_in_a: std_logic_vector(31 downto 0);
    signal s_bias_ps_in_b: std_logic_vector(31 downto 0);
    signal s_bias_vld: std_logic;

    -- ALU signals
    signal s_mem_alu_in: std_logic_vector(7 downto 0);
    signal s_mem_ps_mux: std_logic_vector(7 downto 0);
    signal s_acc_done: std_logic;


    signal s_PS_READ: std_logic;

    -- PS adder signals
    signal s_ps_adder_out: std_logic_vector (N-1 downto 0);
    signal s_ps_adder_out_8: std_logic_vector (7 downto 0);
    signal s_alu_mux_out: std_logic_vector (7 downto 0);
    signal s_ps_adder_mux_sel: std_logic;
    signal s_ps_bulk_twobytwo_adder_out: std_logic_vector(31 downto 0);
    signal s_ps_bulk_acc_out: std_logic_vector(31 downto 0);
    signal s_acc_clock: std_logic;
    signal s_mult_done_bias_a: std_logic;
    signal s_mult_done_bias_b: std_logic;

begin
   -- architecture body
    
    -- control module -- 
    control_impl: control port map (
        -- Inputs
        i_opcode => i_inst(31 downto 28), -- 31:28 is the opcode segment
        i_ADDRA => i_inst(27 downto 17),   -- 27:17 is the addra segment
        i_ADDRB => i_inst(16 downto 6),    -- 16:6 is the addrb segment
        i_ADDR_OUT => i_inst(5 downto 0),
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_inst_mult_done => i_mult_done,
        -- Outputs
        o_ADDRA => s_addr_a,
        o_ADDRB => s_addr_b,
        o_RWA => s_rw_a,
        o_RWB => s_rw_b,
        o_load_weight => s_lw,
        o_load_input => s_li,
        o_done => o_done,

        o_ps_ADDRA => s_ps_addr_a,
        o_ps_ADDRB => s_ps_addr_b,
        o_ps_RWA => s_ps_rw_a,
        o_ps_RWB => s_ps_rw_b,
        o_load_ps => s_load_ps,
        o_bulktwobytwo_acc_clk => s_acc_clock,
        o_bulktwobytwo_done => s_acc_done,
        o_mem_mux_sel_a => s_mem_ps_in_sel_a,
        o_mem_mux_sel_b => s_mem_ps_in_sel_b,
        o_mem_buf_we_a => s_ps_buf_we_a,
        o_mem_buf_we_b => s_ps_buf_we_b,
        
        o_bias_vld => s_bias_vld
    );

    -- Memory module --
    memory: memory_module port map (
        i_CLK => i_CLK,
        i_RST => i_RST,

        -- WD read ports
        i_WD_READ_ADDR_A => s_addr_a,
        i_WD_READ_ADDR_B => s_addr_b,
        o_WD_READ_DATA_A => s_data_a,
        o_WD_READ_DATA_B => s_data_b,
        -- WD write ports
        i_WD_WRITE_ADDR_A => i_inst(18 downto 8),
        i_WD_WRITE_ADDR_B => i_inst(18 downto 8),
        i_WD_WRITE_DATA_A => i_inst(8-1 downto 0),
        i_WD_WRITE_DATA_B => i_inst(8-1 downto 0),
        i_WD_WRITE_EN_A => s_rw_a,
        i_WD_WRITE_EN_B => s_rw_b,

        -- PS read ports
        i_PS_READ_ADDR_A => s_ps_addr_a,
        i_PS_READ_ADDR_B => s_ps_addr_b,
        o_PS_READ_DATA_A => s_mem_ps_out_a,
        o_PS_READ_DATA_B => s_mem_ps_out_b,
        -- WD write ports
        i_PS_WRITE_ADDR_A => s_ps_addr_a,
        i_PS_WRITE_ADDR_B => s_ps_addr_b,
        i_PS_WRITE_DATA_A => s_mem_ps_mux,
        i_PS_WRITE_DATA_B => s_mem_ps_in_b,
        i_PS_WRITE_EN_A => s_ps_rw_a,
        i_PS_WRITE_EN_B => s_ps_rw_b,

        i_PS_READ => s_PS_READ
    );
    
  

    s_PS_READ <= i_inst(30) and not i_inst(31);


    -- ps mux a -- 
    ps_mux_a : mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => s_load_ps,
		i_D0 => x"00",
		i_D1 => s_mem_ps_out_a,
		o_O  => s_ps_in_a
    );

    -- ps mux b -- 
    ps_mux_b : mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => s_load_ps,
		i_D0 => x"00",
		i_D1 => s_mem_ps_out_b,
		o_O  => s_ps_in_b
    );

    -- systolic array --
    sys_array : sys_array_2_2 port map (
        i_weights_0 => s_w_a_systolic, -- 8 bits
        i_weights_1 => s_w_b_systolic, -- 8 bits
        i_input_0 => s_ip_a_systolic,
        i_input_1 => s_ip_b_systolic,
        i_ps_0 => s_bias_ps_in_a, -- TODO: may need to be sign extended
        i_ps_1 => s_bias_ps_in_b, -- TODO: may need to be sign extended
        i_load_weight => s_lw,
        i_load_input => s_li,   
        i_rst => i_RST,
        i_clk => i_CLK,

        o_ps_0 => s_ps_out_a,
        o_ps_1 => s_ps_out_b
    );

    -- -------------------------------------------------------------------------
    --  ALU and mux 
    -- -------------------------------------------------------------------------
    alu: alu_32 port map (
        i_clk => i_CLK,
        i_rst => i_RST,
        i_data => s_data_a,
        i_opcode => i_inst(31 downto 28),
        o_O => s_mem_alu_in
    );

    alu_mux: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => i_inst(31) and i_inst(30),
		i_D0 => s_mem_alu_in,
		i_D1 => s_mem_ps_in_a,
		o_O  => s_alu_mux_out
    );

    ip_mux_systolic_a: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => not s_lw,
		i_D0 => b"00000000",
		i_D1 => s_data_a,
		o_O  => s_ip_a_systolic
    );

    ip_mux_systolic_b: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => not s_lw,
		i_D0 => b"00000000",
		i_D1 => s_data_b,
		o_O  => s_ip_b_systolic
    );

    w_mux_systolic_a: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => s_lw,
		i_D0 => b"00000000",
		i_D1 => s_data_a,
		o_O  => s_w_a_systolic
    );

    w_mux_systolic_b: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => s_lw,
		i_D0 => b"00000000",
		i_D1 => s_data_b,
		o_O  => s_w_b_systolic
    );


    ps_adder_mux: mux2t1_8 port map ( -- actually it's 8 bits
        i_S  => i_inst(31) and i_inst(30),
		i_D0 => s_alu_mux_out,
		i_D1 => s_ps_adder_out_8,
		o_O  => s_mem_ps_mux
    );
    -- -------------------------------------------------------------------------
    --  PS buffer and muxes
    -- -------------------------------------------------------------------------

    bias_ps_buf_a: reg_8 port map (
        i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_load_ps,
		i_D	=> s_ps_in_a,
		o_D	=> s_partial_bias_a
    );

    bias_ps_buf_b: reg_8 port map (
        i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_load_ps,
		i_D	=> s_ps_in_b,
		o_D	=> s_partial_bias_b
    );

    ps_buf_a: reg_32 port map (
        i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_ps_buf_we_a,
		i_D	=> s_ps_out_a,
		o_D	=> s_ps_buffer_out_a
    );

    mux_buf_a: mux_4_to_1_8 port map( 
        Data0_port => s_ps_buffer_out_a(7 downto 0),
        Data1_port => s_ps_buffer_out_a(15 downto 8),
        Data2_port => s_ps_buffer_out_a(23 downto 16),
        Data3_port => s_ps_buffer_out_a(31 downto 24),
        Sel0       => s_mem_ps_in_sel_a(0),
        Sel1       => s_mem_ps_in_sel_a(1),
        Result     => s_mem_ps_in_a
    );
 
    ps_buf_b: reg_32 port map (
        i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => s_ps_buf_we_b,
		i_D	=> s_ps_out_b,
		o_D	=> s_ps_buffer_out_b
    );

    mux_buf_b: mux_4_to_1_8 port map( 
        Data0_port => s_ps_buffer_out_b(7 downto 0),
        Data1_port => s_ps_buffer_out_b(15 downto 8),
        Data2_port => s_ps_buffer_out_b(23 downto 16),
        Data3_port => s_ps_buffer_out_b(31 downto 24),
        Sel0       => s_mem_ps_in_sel_b(0),
        Sel1       => s_mem_ps_in_sel_b(1),
        Result     => s_mem_ps_in_b
    );

       ---------------------------------------------------------------------------
    --  32 bit PS adder
    -- -------------------------------------------------------------------------
    ps_adder: rc_adder_N port map (
        i_D0        => s_ps_buffer_out_a,
		i_D1        => s_ps_buffer_out_b,
		i_C         => '0',
		o_S         => s_ps_adder_out,
		o_C         => open,
		o_O			=> open
    );
    

    ps_bulk_twobytwo_acc: accumulator_32bit port map (
       DataA   =>   s_ps_adder_out,
       Enable  =>   not s_acc_done,
       Aclr    =>   s_acc_done,
       Clock   =>   s_acc_clock,
       Sum     =>   s_ps_bulk_twobytwo_adder_out
    );

    ps_acc_buf: reg_32 port map (
        i_CLK => i_CLK,
		i_RST => i_RST,
		i_WE  => not s_acc_done ,
		i_D	=> s_ps_bulk_twobytwo_adder_out,
		o_D	=> s_ps_bulk_acc_out
    );

    mux_ps_adder: mux_4_to_1_8 port map( 
        Data0_port => s_ps_bulk_acc_out(7 downto 0),
        Data1_port => s_ps_bulk_acc_out(15 downto 8),
        Data2_port => s_ps_bulk_acc_out(23 downto 16),
        Data3_port => s_ps_bulk_acc_out(31 downto 24),
        Sel0       => s_mem_ps_in_sel_a(0),
        Sel1       => s_mem_ps_in_sel_a(1),
        Result     => s_ps_adder_out_8
    );

    s_ps_adder_mux_sel <= i_inst(31) and i_inst(30) and i_inst(29) and not i_inst(28);

    

        
       -- -------------------------------------------------------------------------
    --  8 to 32 bit accumulator for PS
    -- -------------------------------------------------------------------------
    ps_accu_a: unified_inst_module port map (
        i_CLK => i_CLK,
        i_RST => s_bias_vld,
        i_INST_IN_SPI => s_partial_bias_a, 
        i_SPI_OUT_VLD => '1',
        o_DONE => open,
        o_INST  => s_bias_ps_in_a,
        o_mult_done => s_mult_done_bias_a,
        o_cnt => open
    );

    ps_accu_b: unified_inst_module port map (
        i_CLK => i_CLK,
        i_RST => s_bias_vld,
        i_INST_IN_SPI => s_partial_bias_b, 
        i_SPI_OUT_VLD => '1',
        o_DONE => open,
        o_INST  => s_bias_ps_in_b,
        o_mult_done => s_mult_done_bias_b,

        o_cnt => open
    );

    o_ps_0 <= s_mem_ps_out_a;
    o_ps_1 <= s_mem_ps_out_b;
    o_ps_rw <= s_ps_rw_a;

    o_WRITE <= '1' when i_inst(31 downto 28) = b"0000" else '0';
    o_READ <= '1' when i_inst(31 downto 28) = b"0100" else '0';
    o_CALC <= '1' when i_inst(31 downto 28) = b"1101" else '0';

    
end architecture_acCylator;
