library IEEE;

use IEEE.std_logic_1164.all;

entity alu_32 is
generic( D: integer := 8); -- input data is 8bits, output is 32 bits
port (
    i_clk: in std_logic;
    i_rst: in std_logic;
    i_data: in std_logic_vector(D-1 downto 0);        -- input data from the memory 
    i_opcode:   in std_logic_vector(3 downto 0);      -- opcode from instruction
    o_O:        out std_logic_vector(D-1 downto 0)    -- output data
);
end alu_32;
architecture architecture_alu_32 of alu_32 is
   -- signal, component etc. declarations
    signal s_decoder_out: std_logic_vector(3 downto 0);
    signal s_reg_0_out: std_logic_vector(D-1 downto 0);
    signal s_reg_1_out: std_logic_vector(D-1 downto 0);
    signal s_reg_2_out: std_logic_vector(D-1 downto 0);
    signal s_reg_3_out: std_logic_vector(D-1 downto 0);
    signal s_relu_out: std_logic_vector (D-1 downto 0);
    signal s_pool_out: std_logic_vector (D-1 downto 0);

    

    --component decoder_libero is 
     --port( Data0 : in    std_logic;
          --Data1 : in    std_logic;
          --Eq    : out   std_logic_vector(3 downto 0)
        --);
    --end component;

    component unified_inst_module is 
        port (
        i_CLK: IN std_logic;
        i_RST: IN std_logic;
        i_INST_IN_SPI : IN  std_logic_vector(7 downto 0); -- 8bit wide transactions from SPI module
        i_SPI_OUT_VLD: IN std_logic; -- input from SPI module, '1' when a transaction is complete
        o_DONE: OUT std_logic;
        o_INST : OUT std_logic_vector(31 downto 0);  -- assembled and multiplied instructions
        o_cnt: OUT std_logic_vector(1 downto 0)
        );
    end component;

    component relu is
        port (
            i_D : in std_logic_vector(D-1 downto 0);
            o_DATA: out std_logic_vector(D-1 downto 0)
        );
    end component;

    component pooling_2_2 is 
         port(
        -- Inputs
        i_D0 : in  std_logic_vector(7 downto 0);
        i_D1 : in  std_logic_vector(7 downto 0);
        i_D2 : in  std_logic_vector(7 downto 0);
        i_D3 : in  std_logic_vector(7 downto 0);
        -- Outputs
        o_O  : out std_logic_vector(7 downto 0)
        );
    end component;

    component mux2t1_32 is -- actually it's 8 bits
        -- ports
        port(
            i_S          : in std_logic;
            i_D0         : in std_logic_vector(D-1 downto 0);
            i_D1         : in std_logic_vector(D-1 downto 0);
            o_O          : out std_logic_vector(D-1 downto 0)
        );
    end component;

    component reg_8 is
        port(
            i_CLK		: in std_logic; -- Clock input
            i_RST		: in std_logic; -- Reset register input
            i_WE		: in std_logic; -- Write enable for register
            i_D			: in std_logic_vector(D-1 downto 0); -- data in value
            o_D			: out std_logic_vector(D-1 downto 0) -- data out value
        );
    end component;

begin

   --decoder: decoder_libero port map (
        --Data0 => i_data_reg(0),
        --Data1 => i_data_reg(1),
        --Eq => s_decoder_out
    --);

    reg0: reg_8 port map (
        i_CLK => i_clk,
        i_RST => i_rst,
        i_WE => '1',
        i_D => i_data,
        o_D => s_reg_0_out
    );

    reg1: reg_8 port map (
        i_CLK => i_clk,
        i_RST => i_rst,
        i_WE => '1',
        i_D => s_reg_0_out,
        o_D => s_reg_1_out
    );

    reg2: reg_8 port map (
        i_CLK => i_clk,
        i_RST => i_rst,
        i_WE => '1',
        i_D => s_reg_1_out,
        o_D => s_reg_2_out
    );

    reg3: reg_8 port map (
        i_CLK => i_clk,
        i_RST => i_rst,
        i_WE => '1',
        i_D => s_reg_2_out,
        o_D => s_reg_3_out
    );

    relu_0: relu port map (
        i_D => s_reg_0_out,
        o_DATA => s_relu_out
    );

    pool: pooling_2_2 port map (
        i_D0 => s_reg_0_out,
        i_D1 => s_reg_1_out,
        i_D2 => s_reg_2_out,
        i_D3 => s_reg_3_out,
        o_O => s_pool_out
    );  

    mux_out: mux2t1_32 port map (
        i_S  => i_opcode(0) and i_opcode(1),
        i_D0 => s_relu_out,
        i_D1 => s_pool_out,
        o_O => o_O
    );

 end architecture_alu_32;
