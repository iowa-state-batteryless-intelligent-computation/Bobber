-- barrelshifter_N.vhd---------------------------------------------------

-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity barrelshifter_N is
	generic(
			N : integer := 8;
       		SFT_WIDTH : integer := 5
	); -- Generic of type integer for input/output/store data width. Default value is 32.
	port(
		i_nLogic_Arith	:in std_logic; 
		i_shamt 		:in std_logic_vector(SFT_WIDTH-1 downto 0);
		i_A				:in std_logic_vector(N-1 downto 0);
		o_F 			:out std_logic_vector(N-1 downto 0);
		i_nRight_Left 	:in std_logic
	);
end barrelshifter_N;

architecture structural of barrelshifter_N is
	--internal signals 
	signal s_extend 		:std_logic; 
	signal s_mux0			:std_logic_Vector(N-1 downto 0);
	signal s_mux1			:std_logic_vector(N-1 downto 0);
	signal s_mux2			:std_logic_vector(N-1 downto 0);
	signal s_mux3			:std_logic_vector(N-1 downto 0);
	signal s_i_flipped	 	:std_logic_vector(N-1 downto 0);
	signal s_o_beforeflip	:std_logic_vector(N-1 downto 0);
	signal s_inv_nRight_Left : std_logic;
	signal s_and1	:std_logic;
	


	--32 bit 2 to 1 mux component 
	component mux2t1_32 is 
		port (
			i_S          :in std_logic;
			i_D0         :in std_logic_vector(N-1 downto 0);
      	 	i_D1         :in std_logic_vector(N-1 downto 0);
       		o_O          :out std_logic_vector(N-1 downto 0));
	end component; 

	--andg2 component 
	component andg2 is 
		port(
				i_A          : in std_logic;
       			i_B          : in std_logic;
       			o_F          : out std_logic);
	end component;
	

begin 
	s_inv_nRight_Left <= not i_nRight_Left; 
	
	
	right_left_andgate : andg2
		port MAP(
		i_A => s_inv_nRight_Left,
		i_B => i_nLogic_Arith,
		o_F => s_and1
		);
	
	logic_arith_andgate : andg2
		port MAP(
		i_A => s_and1, 
		i_B => i_A(N-1), 
		o_F => s_extend 	
	);
	
	

	mux_input : mux2t1_32
		port MAP(
			i_S => i_nRight_Left,
			i_D0 => i_A, 
			i_D1(0) => i_A(31),
			i_D1(1) => i_A(30),
			i_D1(2) => i_A(29),
			i_D1(3) => i_A(28),
			i_D1(4) => i_A(27),
			i_D1(5) => i_A(26),
			i_D1(6) => i_A(25),
			i_D1(7) => i_A(24),
			i_D1(8) => i_A(23),
			i_D1(9) => i_A(22),
			i_D1(10) => i_A(21),
			i_D1(11) => i_A(20),
			i_D1(12) => i_A(19),
			i_D1(13) => i_A(18),
			i_D1(14) => i_A(17),
			i_D1(15) => i_A(16),
			i_D1(16) => i_A(15),
			i_D1(17) => i_A(14),
			i_D1(18) => i_A(13),
			i_D1(19) => i_A(12),
			i_D1(20) => i_A(11),
			i_D1(21) => i_A(10),
			i_D1(22) => i_A(09),
			i_D1(23) => i_A(08),
			i_D1(24) => i_A(07),
			i_D1(25) => i_A(06),
			i_D1(26) => i_A(05),
			i_D1(27) => i_A(04),
			i_D1(28) => i_A(03),
			i_D1(29) => i_A(02),
			i_D1(30) => i_A(01),
			i_D1(31) => i_A(00),
			o_O => s_i_flipped
		);
	mux_output : mux2t1_32
		port MAP(
			i_S => i_nRight_Left,
			i_D0 => s_o_beforeflip,
			i_D1(0) => s_o_beforeflip(31),
			i_D1(1) => s_o_beforeflip(30),
			i_D1(2) => s_o_beforeflip(29),
			i_D1(3) => s_o_beforeflip(28),
			i_D1(4) => s_o_beforeflip(27),
			i_D1(5) => s_o_beforeflip(26),
			i_D1(6) => s_o_beforeflip(25),
			i_D1(7) => s_o_beforeflip(24),
			i_D1(8) => s_o_beforeflip(23),
			i_D1(9) => s_o_beforeflip(22),
			i_D1(10) => s_o_beforeflip(21),
			i_D1(11) => s_o_beforeflip(20),
			i_D1(12) => s_o_beforeflip(19),
			i_D1(13) => s_o_beforeflip(18),
			i_D1(14) => s_o_beforeflip(17),
			i_D1(15) => s_o_beforeflip(16),
			i_D1(16) => s_o_beforeflip(15),
			i_D1(17) => s_o_beforeflip(14),
			i_D1(18) => s_o_beforeflip(13),
			i_D1(19) => s_o_beforeflip(12),
			i_D1(20) => s_o_beforeflip(11),
			i_D1(21) => s_o_beforeflip(10),
			i_D1(22) => s_o_beforeflip(09),
			i_D1(23) => s_o_beforeflip(08),
			i_D1(24) => s_o_beforeflip(07),
			i_D1(25) => s_o_beforeflip(06),
			i_D1(26) => s_o_beforeflip(05),
			i_D1(27) => s_o_beforeflip(04),
			i_D1(28) => s_o_beforeflip(03),
			i_D1(29) => s_o_beforeflip(02),
			i_D1(30) => s_o_beforeflip(01),
			i_D1(31) => s_o_beforeflip(00),
			o_O => o_F
			);
			
	mux0 : mux2t1_32 
		port MAP(
		i_S => i_shamt(0),
		i_D0 => s_i_flipped, 
		i_D1(30 downto 0) => s_i_flipped(31 downto 1),
		i_D1(31) => s_extend, 
		o_O => s_mux0 
	);
	
	mux1 : mux2t1_32
		port MAP(
		o_O => s_mux1,
		i_S => i_shamt(1),
		i_D0 => s_mux0,
		i_D1(29 downto 0) => s_mux0(31 downto 2),
		i_D1(31) => s_extend, 
		i_D1(30) => s_extend
	);

	mux2 : mux2t1_32
		port MAP(
		o_O => s_mux2,
		i_S => i_shamt(2),
		i_D0 => s_mux1,
		i_D1(27 downto 0) => s_mux1(31 downto 4),
		i_D1(31) => s_extend,
		i_D1(30) => s_extend,
		i_D1(29) => s_extend,
		i_D1(28) => s_extend
	);

	mux3 : mux2t1_32
		port MAP(
		o_O => s_mux3,
		i_S => i_shamt(3),
		i_D0 => s_mux2,
		i_D1(23 downto 0) => s_mux2(31 downto 8),
		i_D1(31) => s_extend,
                i_D1(30) => s_extend,
                i_D1(29) => s_extend,
                i_D1(28) => s_extend,
		i_D1(27) => s_extend,
                i_D1(26) => s_extend,
                i_D1(25) => s_extend,
                i_D1(24) => s_extend
	); 

	mux4 : mux2t1_32
		port map( 
		o_O => s_o_beforeflip,
		i_S => i_shamt(4),
		i_D0 => s_mux3,
		i_D1(15 downto 0) => s_mux3(31 downto 16),
		i_D1(31) => s_extend,
                i_D1(30) => s_extend,
                i_D1(29) => s_extend,
                i_D1(28) => s_extend,
                i_D1(27) => s_extend,
                i_D1(26) => s_extend,
                i_D1(25) => s_extend,
                i_D1(24) => s_extend,
		i_D1(23) => s_extend,
                i_D1(22) => s_extend,
                i_D1(21) => s_extend,
                i_D1(20) => s_extend,
                i_D1(19) => s_extend,
                i_D1(18) => s_extend,
                i_D1(17) => s_extend,
                i_D1(16) => s_extend
	);

end structural; 
