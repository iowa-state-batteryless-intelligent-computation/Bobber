-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity cmp_libero_11bit is

    port( DataA : in    std_logic_vector(9 downto 0);
          DataB : in    std_logic_vector(9 downto 0);
          AEB   : out   std_logic
        );

end cmp_libero_11bit;

architecture DEF_ARCH of cmp_libero_11bit is 

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XNOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

    signal XNOR2_3_Y, XNOR2_2_Y, XNOR2_0_Y, XNOR2_1_Y, XNOR2_4_Y, 
        XNOR2_5_Y, AND3_0_Y, AND2_0_Y : std_logic;

begin 


    AND2_0 : AND2
      port map(A => XNOR2_4_Y, B => XNOR2_5_Y, Y => AND2_0_Y);
    
    XNOR2_3 : XNOR2
      port map(A => DataA(0), B => DataB(0), Y => XNOR2_3_Y);
    
    XNOR2_5 : XNOR2
      port map(A => DataA(5), B => DataB(5), Y => XNOR2_5_Y);
    
    AND3_0 : AND3
      port map(A => XNOR2_3_Y, B => XNOR2_2_Y, C => XNOR2_0_Y, Y
         => AND3_0_Y);
    
    XNOR2_4 : XNOR2
      port map(A => DataA(4), B => DataB(4), Y => XNOR2_4_Y);
    
    XNOR2_0 : XNOR2
      port map(A => DataA(2), B => DataB(2), Y => XNOR2_0_Y);
    
    XNOR2_2 : XNOR2
      port map(A => DataA(1), B => DataB(1), Y => XNOR2_2_Y);
    
    AND3_AEB : AND3
      port map(A => XNOR2_1_Y, B => AND3_0_Y, C => AND2_0_Y, Y
         => AEB);
    
    XNOR2_1 : XNOR2
      port map(A => DataA(3), B => DataB(3), Y => XNOR2_1_Y);
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_COMPARE
-- LPM_HINT:EQCOMP
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/git/iFPGA/LiberoProjects/MAC_32bit-Bias-ReLu-Pooling/MAC/smartgen\cmp_libero
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTH:6
-- REPRESENTATION:UNSIGNED
-- GEQRHS_POLARITY:1
-- AEB_POLARITY:1

-- _End_Comments_

