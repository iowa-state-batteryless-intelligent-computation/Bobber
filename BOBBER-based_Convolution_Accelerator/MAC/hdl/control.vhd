library IEEE;

use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity control is
generic(N : integer := 32; M: integer:= 11; D: integer := 8); -- input data is 8bits, output is 32 bits, address is 7 bits
port (
    i_opcode: in std_logic_vector(3 downto 0); -- still 4 bit op code
    i_ADDRA: in std_logic_vector (M-1 downto 0); -- 7 bit, memory
    i_ADDRB: in std_logic_vector (M-1 downto 0);
    i_ADDR_OUT: in std_logic_vector (5 downto 0); -- output offset
    --i_CALCAMT: in std_logic_vector (1 downto 0); -- calcamt for multi-calculation instructions
    i_CLK: in std_logic;
    i_RST: in std_logic;
    i_inst_mult_done: in std_logic;

    o_ADDRA: out std_logic_vector (M-1 downto 0); -- output address for reading from mem
    o_ADDRB: out std_logic_vector (M-1 downto 0); -- output address for reading from mem
    o_RWA: out std_logic;       
    o_RWB: out std_logic;
    o_load_weight: out std_logic;
    o_load_input: out std_logic;
    o_done: out std_logic;
    o_ps_ADDRA : out std_logic_vector(M-1 downto 0);
    o_ps_ADDRB : out std_logic_vector(M-1 downto 0);
    o_ps_RWA : out std_logic;
    o_ps_RWB : out std_logic;
    o_load_ps : out std_logic;
    o_bulktwobytwo_acc_clk: out std_logic;
    o_bulktwobytwo_done: out std_logic;
    o_mem_mux_sel_a: out std_logic_vector(1 downto 0);
    o_mem_mux_sel_b: out std_logic_vector(1 downto 0);
    o_mem_buf_we_a: out std_logic;
    o_mem_buf_we_b: out std_logic;
    
    o_bias_vld: out std_logic
);
end control;
architecture architecture_control of control is
   
    type t_MultState is (LW0, LW1, C0, C1, C2, C3, C4, LW2, LW3, C5, C6, C7, C8, C9, LW4, LW5, LW6, C10, C11, C12, C13, C14, C15, C16, C17 );
    signal state: t_MultState;
    signal s_addr_out: std_logic_vector(10 downto 0);
    
begin
    s_addr_out <= b"00" & i_ADDR_OUT & b"000";
     
    process (i_opcode, i_CLK, state, i_RST) is
    begin
        if i_RST = '1' then
            state <= LW0;
        else 
            if rising_edge(i_CLK) then
                case i_opcode is
                    when "0000" =>
                    ---- load input & weight ---
                        state <= LW0;
                    when "0100" => 
                    ---- read output ----
                        state <= LW0;
                                           
                    when "1101" =>
                    ---- 2x2 with bias ---- 12 cycles ----
                        case state is 
                            when LW0 =>
                                state <= LW1; -- load weight 1
                            when LW1 => 
                                state <= LW2; -- load weight 2                                                    
                           
                                -- one additional load weight cycle for data to reach sys array from mem
                            when LW2 => 
                                state <= C0; 
                            when C0 =>
                                state <= C1;
                            when C1 => 
                                state <= C2;
                            when C2 =>
                                state <= C3;
                            when C3 =>
                                state <= C4; -- export 1 of 4
                            when C4 =>
                                state <= C5; -- export buffer, 2 of 4
                            when C5 =>
                                state <= C6; -- export buffer, 3 of 4 
                            when C6 =>
                                state <= C7; -- export buffer, 4 of 4
                            when C7 =>
                                state <= C8; -- export buffer, 4 of 4
                            when C8 =>
                                state <= C9; -- export buffer, 4 of 4
                    
                            when others => 
                                if(i_inst_mult_done ='1') then
                                    state <= C17;
                                else
                                    state <= LW0;
                                end if;    
                        end case;
                     
                    when others =>
                    ---- noop ----
                        null;
                end case;
            end if;
        end if;
    end process;

-- map opcode and state to outputs
    process (i_opcode, state, i_RST, i_ADDRA, i_ADDRB) is 
    begin
         if i_RST = '1' then
            o_load_weight <= '0';
            o_RWA <= '1';
            o_RWB <= '1';
            o_done <= '0';
            o_ps_ADDRA <= b"00000000000";
            o_ps_ADDRB <= b"00000000000";
            o_ADDRA <= b"00000000000";
            o_ADDRB <= b"00000000000";
            o_ps_RWB <= '1';
            o_ps_RWA <= '1';
            o_load_ps <= '0';
            o_mem_buf_we_a <= '0';
            o_mem_buf_we_b <= '0';
            o_bias_vld <= '1';
            o_load_input <= '0';
            o_bulktwobytwo_acc_clk <='0';
            o_bulktwobytwo_done <='1';
        else     
            case i_opcode is 
                when "0000" =>
                --- load data ---
                    o_ADDRA <= i_ADDRA;
                    o_RWA <= '0'; -- load
                    o_RWB <= '1'; -- read
                    o_done <= '1';
                    o_ps_RWA <= '1'; -- read
                    o_ps_RWB <= '1'; -- read

                
                when "0100" =>
                --- read data ---
                    o_done <= '1';
                    o_ps_ADDRA <= i_ADDRA; 
                    o_ps_ADDRB <= i_ADDRB;
					o_ps_RWA <= '1'; -- read 
                    o_ps_RWB <= '1'; -- read 
                    o_RWA <= '1';
                    o_RWB <= '1';

                  
             
                when "1101" => 
                --- 2x2 with bias ---
                    case state is
                        when LW0 => 
                            o_load_weight <= '1';
                            o_load_input <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            o_load_ps <= '0';
                            o_done <= '0';
                            o_bias_vld <= '0';
                            o_RWA <= '1'; -- read
                            o_RWB <= '1'; -- read
                            o_ADDRA <= i_ADDRB + 0;  -- load B01 (2)    
                            o_ADDRB <= i_ADDRB + 1;  -- load B11 (3)
                            
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read
                            
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';    

                            
                        when LW1 => 
                            o_load_weight <= '1';
							o_load_ps <= '0';
							o_done <= '0';
                            o_bias_vld <= '0';
                            o_load_input <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

							o_RWA <= '1'; -- read
                            o_RWB <= '1'; -- read
                            o_ADDRA <= i_ADDRB + 2; --load B00 (0)  
                            o_ADDRB <= i_ADDRB + 3; -- Load B01 (1)
							
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read
    
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';       

                        when LW2 =>                    ----------------BUFFER CYCLE ------------
                            o_bias_vld <= '0';
                            o_load_input <= '1';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            o_load_weight <= '1';
							o_load_ps <= '0';
							o_done <= '0';
							
							o_RWA <= '1'; -- read
                            o_RWB <= '1'; -- read

                            -- load data needed for c0 
                            o_ADDRA <= i_ADDRA + 0 ; -- load A00 (0)
							o_ADDRB <= i_ADDRA + 1; -- load A01 (1)
							
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read

                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';

                       
						when c0 =>
                            o_bias_vld <= '0';
                            o_load_input <= '1';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

							o_load_weight <= '0';
                            o_load_ps <='0' ;
							o_done <= '0';
							
							o_RWA <= '1'; -- read
							o_RWB <= '1'; -- read
                            -- load dada needed for c1
							o_ADDRA <= i_ADDRA + 2 ; -- load A00 (0)
							o_ADDRB <= i_ADDRA + 3; -- load A01 (1)
							
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read

                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';

                           
                        when c1 =>
                            o_load_weight <= '0';
                            o_load_ps <='0' ;
							o_done <= '0';
                            o_bias_vld <= '0';
                            o_load_input <= '1';
                            o_bulktwobytwo_done <='0';

                            o_RWA <= '1'; -- read
							o_RWB <= '1'; -- read
                            -- load dada needed for c1
                            o_bulktwobytwo_acc_clk <='0';
                            
                        
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read
                                
                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';

    
						when c2 => 
                            o_load_input <= '0';

							o_load_weight <= '0';
							o_done <= '0';
                            o_load_ps <='0' ;
                            o_bias_vld <= '0';
                            o_bulktwobytwo_done <='0';
                          
                            o_bulktwobytwo_acc_clk <='0';

							o_RWA <= '1'; -- read
							o_RWB <= '1'; -- read

                            -- c1's output logic
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read
                            o_ADDRA <= i_ADDRA + 2 ; -- load A00 (0)
							o_ADDRB <= i_ADDRA + 3; -- load A01 (1)

                            -- buffer logic
                            o_mem_buf_we_a <= '1';
                            o_mem_buf_we_b <= '1';


						when c3 => 
                            o_load_input <= '0';
                            o_bulktwobytwo_acc_clk <= '1';
                            o_bulktwobytwo_done <='0';

							o_load_weight <= '0';
							o_done <= '0';
                            o_load_ps <='0' ;

                            o_bias_vld <= '0';
                           -- load data for c3
                            -- doesnt ned any

							o_RWA <= '1'; -- read
							o_RWB <= '1'; -- read
							
                            -- c1's output logic
                            o_ps_RWA <= '1'; -- read
                            o_ps_RWB <= '1'; -- read

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';


						when c4 => 
                            o_bias_vld <= '0';
                            o_load_input <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            -- saving byte 1 of buffer
                            -- output logic
                            o_ps_RWA <= '1'; -- write
                            o_ps_RWB <= '1'; -- read

                           
                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"00";
                            o_mem_mux_sel_b <= b"00";

                                             
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_done <= '0';

                        when c5 =>
                            o_bias_vld <= '0';
                            o_load_input <= '0';
                            o_bulktwobytwo_acc_clk <='0';

                            o_bulktwobytwo_done <='0';
                            -- saving byte 2 of buffer
                            -- output logic
                            o_ps_RWA <= '0'; -- write
                            o_ps_RWB <= '1'; -- read
                            o_ps_ADDRA <= s_addr_out; -- 0x00
							o_ps_ADDRB <= s_addr_out + 4; -- 0x04

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"00";
                            o_mem_mux_sel_b <= b"00";
                            
                          
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_done <= '0';


                        when c6 => 
                            o_bias_vld <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            -- saving byte 3 of buffer
                            -- output logic
                            o_ps_RWA <= '0'; -- write
                            o_ps_RWB <= '1'; -- read
                            o_ps_ADDRA <= s_addr_out + 1; -- 0x02
							o_ps_ADDRB <= s_addr_out + 5; -- 0x06

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"01";
                            o_mem_mux_sel_b <= b"01";

                          
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_done <= '0';
                            o_load_input <= '0';


                        when c7 => 
                            o_bias_vld <= '0';
                            o_bulktwobytwo_acc_clk <='0';

                            o_bulktwobytwo_done <='0';
                            -- saving byte 4 of buffer
                            -- output logic
                            o_ps_RWA <= '0'; -- write
                            o_ps_RWB <= '1'; -- read
                            o_ps_ADDRA <= s_addr_out + 2; -- 0x03
							o_ps_ADDRB <= s_addr_out + 6; -- 0x07

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"10";
                            o_mem_mux_sel_b <= b"10";

                          
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_done <= '0';
                            o_load_input <= '0';

                        when c8 => 
                            o_bias_vld <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            -- buffer instruction
                            -- output logic
                            o_done <= '0';
                            o_ps_RWA <= '0'; -- write
                            o_ps_RWB <= '1'; -- read
                            o_ps_ADDRA <= s_addr_out + 3; -- 0x03
							o_ps_ADDRB <= s_addr_out + 7; -- 0x07

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"11";
                            o_mem_mux_sel_b <= b"11";
                  
                           
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_load_input <= '0';

                        when c9 => 
                            o_bias_vld <= '0';
                            o_bulktwobytwo_acc_clk <='0';
                            o_bulktwobytwo_done <='0';

                            -- buffer instruction
                            -- output logic
                            o_done <= '1';
                            o_ps_RWA <= '0'; -- write
                            o_ps_RWB <= '1'; -- read
                            o_ps_ADDRA <= s_addr_out + 3; -- 0x03
							o_ps_ADDRB <= s_addr_out + 7; -- 0x07

                            -- buffer logic
                            o_mem_buf_we_a <= '0';
                            o_mem_buf_we_b <= '0';
                            
                            o_mem_mux_sel_a <= b"11";
                            o_mem_mux_sel_b <= b"11";
                  
                           
                            o_load_ps <= '0';
                            o_load_weight <= '0';
                            o_load_input <= '0';

                        when others =>
                            o_bulktwobytwo_done <='1';
					end case;              
                when others =>
					null;
                --- noop ---
            end case;
        end if;
    end process;

   -- architecture body
end architecture_control;
