
library IEEE;
use IEEE.std_logic_1164.all;

entity f_adder is 
	port (
		i_D0	: in std_logic; 
		i_D1	: in std_logic;
		i_C 	: in std_logic;
		o_S	: out std_logic;
		o_C	: out std_logic);
end f_adder;

architecture structural of f_adder is 

	component andg2 is 
		 port(
			i_A          : in std_logic;
       		i_B          : in std_logic;
			o_F          : out std_logic);
	end component;
	
	component org2 is 
		 port(
			i_A          : in std_logic;
			i_B          : in std_logic;
			o_F          : out std_logic);
	end component;
	
	component xorg2 is 
		port(
			i_A          : in std_logic;
			i_B          : in std_logic;
			o_F          : out std_logic);
	end component;
	
	--signal from xor1 to xor2 and and1
	signal s_XOR1 : std_logic;
	--signal from and1 to or1 
	signal s_AND1 : std_logic;
	--signal from and2 to or1 
	signal s_AND2 : std_logic;
	
	begin 
		g_XOR1: xorg2
			port MAP(
				i_A => i_D0,
				i_B => i_D1,
				o_F => s_XOR1
			);
		
		g_XOR2 : xorg2 
			port MAP(
				i_A => s_XOR1,
				i_B => i_C,
				o_F => o_S
			);
		
		g_AND1 : andg2 
			port MAP (
				i_A => s_XOR1,
				i_B => i_C,
				o_F => s_AND1
			);
			
		g_AND2 : andg2 
			port MAP ( 
				i_A => i_D0,
				i_B => i_D1,
				o_F => s_AND2
			);
			
		g_OR1 : org2 
			port MAP (
				i_A => s_AND1,
				i_B => s_AND2,
				o_F => o_C
			); 
	end structural;
