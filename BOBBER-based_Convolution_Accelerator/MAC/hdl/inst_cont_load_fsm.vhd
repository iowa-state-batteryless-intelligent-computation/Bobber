--
--------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;


entity inst_cont_load_fsm is
port (
    i_clk: in std_logic; -- system clock
    i_rst: in std_logic; -- reset, async, active high
    i_inst_cont_load : IN  std_logic_vector(31 downto 0); -- instruction input
    i_spi_done_cont_load: in std_logic;
    o_inst_cont_load : OUT std_logic_vector(31 downto 0);  -- output instruction
    o_done_cont_load : OUT std_logic ; -- continuous instruction done signal
    o_inst_cont_load_en: OUT std_logic -- continuous instruction enable signal
);

end inst_cont_load_fsm;
architecture architecture_inst_cont_load_fsm of inst_cont_load_fsm is

     --- 8 bit 4t1 mux --
    component mux_4_to_1_8 --A 4:1 8 bit mux to select 8 bit of data from 1 32 but instruction
        -- ports
        port( Data0_port : in    std_logic_vector(7 downto 0);
              Data1_port : in    std_logic_vector(7 downto 0);
              Data2_port : in    std_logic_vector(7 downto 0);
              Data3_port : in    std_logic_vector(7 downto 0);
              Sel0       : in    std_logic;
              Sel1       : in    std_logic;
              Result     : out   std_logic_vector(7 downto 0)
            );
    end component;

    component reg_7 -- actually 11 bits
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(10 downto 0); -- data in value
		o_D			: out std_logic_vector(10 downto 0) -- data out value
	);
    end component;
    
    component reg_4 --4 bit register for storing the opcode
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(3 downto 0); -- data in value
		o_D			: out std_logic_vector(3 downto 0) -- data out value
	);
    end component;

    component reg_10 --10 bit register for storing the count
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(9 downto 0); -- data in value
		o_D			: out std_logic_vector(9 downto 0) -- data out value
	);
    end component;
    component dffg is -- A register to hold signal values during the operation
    port (
        i_CLK        : in std_logic;     -- Clock input
        i_RST        : in std_logic;     -- Reset input
        i_WE         : in std_logic;     -- Write enable input
        i_D          : in std_logic;     -- Data value input
        o_Q          : out std_logic  -- Data value output
    );
    end component;
    
    component mux2t_32 -- actually 8 bits
        -- ports
        port(
            i_S          : in std_logic;
            i_D0         : in std_logic_vector(32-1 downto 0);
            i_D1         : in std_logic_vector(32-1 downto 0);
            o_O          : out std_logic_vector(32-1 downto 0)
        );
    end component;

    signal s_spi_byte_cnt: unsigned (1 downto 0); -- count the number of transactions
    signal s_instr_cnt_max: std_logic; -- 1 when there are 4 received transcations

    signal s_data_select: unsigned (1 downto 0);  -- data select signal for the mux
        
    type load_state is (ld0, ld1, ld2, ld3); -- define 4 states for loading 4 set of instructions from a single 32 bit data packet
    signal s_prevstate,s_nextstate : load_state;
    -- define signals to be used for the whole module
    signal s_addr_a: std_logic_vector(10 downto 0); -- base addr
    signal s_addr_a_translated : std_logic_vector(10 downto 0); -- addr translated and sent to control state machine
    signal s_opcode: std_logic_vector(3 downto 0); -- opcode 
    signal s_opcode_translated: std_logic_vector(3 downto 0); -- opcode translated and sent to control state machine
    signal s_calcamt: std_logic_vector(9 downto 0); -- 10 bit for calcamt -- no of incoming data packets
    signal s_data_byte: std_logic_vector(7 downto 0); -- 8 bit data packets
    signal s_inst_out: std_logic_vector(32-1 downto 0); -- instruction out from this module
    signal s_inst_cont_load_active: std_logic;    -- signal which indicates that cont load is active
    signal s_data_count: std_logic_vector(9 downto 0); -- incremental value of data packets
    signal delay_count: std_logic ; -- a signal which indicates when the loading of instruction has to start(has to wait till a current 32 bit inst completes
    signal s_counter_done: std_logic; -- counter done signal
    signal s_checking_en: std_logic; -- signal indicating a process should check for 1111 opcode 
    signal s_loading_en: std_logic; -- signal indicating that the loading process should start
    signal s_inst_cmp: std_logic;-- signal indicating the opcode is 1111( cont load)
    signal s_inst_cmp_buf: std_logic; -- buffer to hold the above signal
    signal s_done_cont_load: std_logic; -- active low signal indicating each instuction loading process
    signal s_cont_load_start: std_logic; 
    signal s_enable_inst_out: std_logic;
    begin
    
    s_instr_cnt_max <= '1' when (s_spi_byte_cnt = "11") else '0'; 

    --process to count spi bytes in each 32 bit instruction
    byte_cnt: process (i_rst, i_clk, i_spi_done_cont_load) 
    begin
        if (i_rst = '1') then
            s_spi_byte_cnt <= b"00";
        elsif (rising_edge(i_clk) and i_spi_done_cont_load = '1') then
            if (s_instr_cnt_max = '1') then
                s_spi_byte_cnt <= (others => '0');
            else
                s_spi_byte_cnt <= s_spi_byte_cnt + 1;
            end if;
        end if;

    end process byte_cnt;



    -- process to check for opcode and start the loading process once the opcode is 1111
    opcodecheck: process(i_rst, i_clk, s_inst_cmp_buf, s_counter_done)  
    begin
       if (i_rst = '1') then
            s_checking_en <= '1';
            s_loading_en <= '0' ;
            s_inst_cont_load_active <= '0';

       elsif (rising_edge(i_clk)) then -- if opcode is 1111, stop checking and start loading til l counter = num of data packets
            if( s_counter_done = '0' and s_inst_cmp_buf = '1') then
                s_checking_en <= '0';
                s_loading_en  <= '1';
                s_inst_cont_load_active <='1';
           else
                s_checking_en <= '1';
                s_loading_en  <= '0';
                s_inst_cont_load_active <='0';
           end if;
       end if;
    end process opcodecheck;
    
    -- A process to indicate when the loading of instructions should start
    load_start: process(i_rst, i_clk,s_inst_cont_load_active,s_counter_done,delay_count)
    begin
        if(i_rst = '1') then
            s_cont_load_start <= '0';
        elsif(rising_edge(i_clk)) then
            if(s_inst_cont_load_active ='1') then
                if(s_counter_done = '0' and delay_count = '1') then
                    s_cont_load_start <= '1';
                elsif(s_counter_done = '1' and delay_count = '0') then
                    s_cont_load_start <= '0';
                elsif(s_counter_done = '1' and delay_count = '1') then
                    s_cont_load_start <= '0';
                end if;
            else
                s_cont_load_start <= '0';
            end if;
        end if;
    end process load_start;

    counter_check: process(i_rst, i_clk,s_cont_load_start,s_calcamt,i_spi_done_cont_load, s_data_count)
    begin
        if(i_rst = '1') then
            s_counter_done <='0';
        elsif(rising_edge(i_clk)) then
            if(s_cont_load_start = '1') then
                if(i_spi_done_cont_load = '1' and s_data_count = (s_calcamt-1)) then
                    s_counter_done <= '1';
                end if;
            else
                s_counter_done <= '0';
            end if;
        end if;
    end process counter_check;

    data_count: process(i_rst, i_clk,s_cont_load_start,i_spi_done_cont_load)
    begin
        if(i_rst= '1') then
            s_data_count <= b"0000000000";
        elsif(rising_edge(i_clk)) then
            if(s_cont_load_start ='1') then
                if(i_spi_done_cont_load ='1') then
                   s_data_count <= s_data_count + 1;
                end if;
            else
                s_data_count <= b"0000000000";
            end if;
        end if;
    end process data_count;
            

    
    -- a state machine which has 4 loading states(4 inst out of a single inst)
    seq_load: process(i_rst, i_clk,s_checking_en,s_loading_en,s_inst_cont_load_active) 
    begin 
        if (i_rst = '1') then
            s_prevstate <= ld0;
        elsif(rising_edge(i_clk)) then
            if(s_checking_en= '0' and s_loading_en ='1') then
                if(s_inst_cont_load_active ='1') then -- only start the process when the checking is turned off and loading is On
                    s_prevstate <= s_nextstate; 
                end if;
            else
                s_prevstate <= ld0;
            end if;
        end if;
    end process seq_load;

    -- map variables for each state
    comb_load: process(s_prevstate,s_opcode,i_spi_done_cont_load,s_spi_byte_cnt,s_addr_a,s_data_count)
    begin
         
         s_addr_a_translated <= b"00000000000";
         s_data_select <= b"00";
         s_opcode_translated <= b"0000";
         delay_count <= '0';
         s_enable_inst_out <= '0';       
         s_nextstate <= ld0;                            
         case s_opcode is
            when "1111" => -- opcode for cont load
                case s_prevstate is 
                    when ld0 => 
                        if (i_spi_done_cont_load = '0') then       
                              s_nextstate <= ld0;                            
                              s_addr_a_translated <= s_addr_a + s_data_count;
                              s_opcode_translated <= b"0000";
                              s_data_select <= s_spi_byte_cnt;
                              delay_count <= '0';   
                              s_enable_inst_out <= '1';       
  

                        else
                              s_nextstate <= ld1;
                              s_addr_a_translated <= b"00000000000";
                              s_opcode_translated <= b"0000";
                              s_data_select <= b"00";
                              delay_count <= '0';   
                              s_enable_inst_out <= '0';       
  
                        end if;

                    when ld1 => 
                        if (i_spi_done_cont_load = '0') then       
                              s_nextstate <= ld1;
                              s_addr_a_translated <= s_addr_a + s_data_count;
                              s_opcode_translated <= b"0000";
                              s_data_select <= s_spi_byte_cnt;
                              delay_count <= '0';  
                              s_enable_inst_out <= '1';   
                        else
                              s_nextstate <= ld2;
                              s_addr_a_translated <= b"00000000000";
                              s_opcode_translated <= b"0000";
                              s_data_select <= b"00";
                              delay_count <= '0';
                              s_enable_inst_out <= '0';       
  
                        end if;

                    when ld2 => 
                        if (i_spi_done_cont_load = '0') then       
                              s_nextstate <= ld2;
                              s_addr_a_translated <= s_addr_a + s_data_count;
                              s_opcode_translated <= b"0000";
                              s_data_select <= s_spi_byte_cnt;
                              delay_count <= '0';
                              s_enable_inst_out <= '1';   
                        else
                              s_nextstate <= ld3;
                              s_addr_a_translated <= b"00000000000";
                              s_opcode_translated <= b"0000";
                              s_data_select <= b"00";
                              delay_count <= '0';
                              s_enable_inst_out <= '0';       
  
                        end if;

                    when ld3 => 
                        if (i_spi_done_cont_load = '0') then       
                              s_nextstate <= ld3;
                              s_addr_a_translated <= s_addr_a + s_data_count;
                              s_opcode_translated <= b"0000";
                              s_data_select <= s_spi_byte_cnt;
                              delay_count <= '0';                                
                              s_enable_inst_out <= '1';   

                        else
                              s_nextstate <= ld0;
                              s_addr_a_translated <= b"00000000000";
                              s_opcode_translated <= b"0000";
                              s_data_select <= b"00";
                              delay_count <= '1';                                
                              s_enable_inst_out <= '0';       
  
                        end if;
                    when others => 
                          s_nextstate <= ld0;
                          s_addr_a_translated <= b"00000000000";
                          s_data_select <= b"00";
                          s_opcode_translated <= b"0000";
                          delay_count <= '0';
                          s_enable_inst_out <= '0';       

                end case;
         when others => 
                null;
         end case;
    end process comb_load;    

     mux_data_load: mux_4_to_1_8 port map( 
        Data0_port => i_inst_cont_load(7 downto 0),
        Data1_port => i_inst_cont_load(15 downto 8),
        Data2_port => i_inst_cont_load(23 downto 16),
        Data3_port => i_inst_cont_load(31 downto 24),
        Sel0       => s_data_select(0),
        Sel1       => s_data_select(1),
        Result     => s_data_byte
    );
 
    
    cont_load_base_addr_buf: reg_7 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => s_checking_en and s_inst_cmp,
        i_D => i_inst_cont_load(27 downto 17),
        o_D => s_addr_a
    );
    
    cont_load_opcode_buf: reg_4 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => s_checking_en,
        i_D => i_inst_cont_load(31 downto 28),
        o_D => s_opcode
    );
    
    cont_load_count_buf: reg_10 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => s_checking_en and s_inst_cmp,
        i_D => i_inst_cont_load(9 downto 0),
        o_D => s_calcamt
    );

    cmp_reg: dffg port map (
        i_CLK  => i_clk,     -- Clock input
        i_RST  => i_rst,    -- Reset input
        i_WE   => s_checking_en,    -- Write enable input
        i_D    => s_inst_cmp,    -- Data value input
        o_Q    => s_inst_cmp_buf   -- Data value output
    );

  -- mux 0 -- 
    instmux_out : mux2t_32 port map (
        i_S  => s_enable_inst_out,
		i_D0 => b"00000000000000000000000000000000",
		i_D1 => s_inst_out,
		o_O  => o_inst_cont_load
    );
        s_inst_cmp <= i_inst_cont_load(31) and i_inst_cont_load(30) and i_inst_cont_load(29) and i_inst_cont_load(28); --1 if cont data load opcode 1111
        s_done_cont_load <= not s_inst_cont_load_active or (s_inst_cont_load_active and s_enable_inst_out) ;
        o_done_cont_load <= s_done_cont_load;
        s_inst_out <= s_opcode_translated(3 downto 0) & b"000000000" & s_addr_a_translated & s_data_byte ;
        o_inst_cont_load_en <= s_inst_cont_load_active;

end architecture_inst_cont_load_fsm;
