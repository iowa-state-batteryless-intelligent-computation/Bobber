--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;


entity instruction_cont_load is
port (
    i_clk: in std_logic; -- system clock
    i_rst: in std_logic; -- reset, async, active high
    i_inst_cont_load : IN  std_logic_vector(31 downto 0); -- instruction input
    i_spi_done: in std_logic;
    o_inst_cont_load : OUT std_logic_vector(31 downto 0);  -- output instruction
    o_done_cont_load : OUT std_logic ; -- continuous instruction done signal
    o_inst_cont_load_en: OUT std_logic -- continuous instruction enable signal
);

end instruction_cont_load;
architecture architecture_instruction_cont_load of instruction_cont_load is
    signal s_addr_a: std_logic_vector(10 downto 0);
    signal s_opcode: std_logic_vector(3 downto 0);
    signal s_calcamt: std_logic_vector(9 downto 0); -- 10 bit for calcamt

    signal s_data_byte_cnt: unsigned (9 downto 0); -- count the number of data packets
    signal s_data_cnt_max: std_logic; -- 1 when number of data packets = max count
    signal s_instr_wr: std_logic;  -- 1 when ready for parallel read of the 4 registers
    
    signal s_spi_byte_cnt: unsigned (1 downto 0); -- count the number of transactions
    signal s_instr_cnt_max: std_logic; -- 1 when there are 4 received transcations
    
    signal s_count: std_logic_vector(9 downto 0); -- 10 bit for counter
    signal s_done: std_logic;
    signal s_cnt_reset: std_logic;
    signal s_inst_cmp: std_logic;

    signal s_addr_a_translated: std_logic_vector(10 downto 0); -- output from the adder
    signal s_addr_a_out: std_logic_vector(10 downto 0); -- output from mux

    signal s_counter_done: std_logic;
    signal s_counter_done_buf: std_logic;
    signal s_opcode_translated: std_logic_vector(10 downto 0); -- should be 4 bits, but set to 11 to accomodate the mux
    signal s_inst_out: std_logic_vector(31 downto 0);

    signal s_cycle_cnt: unsigned(3 downto 0); -- count up to 14
    signal s_cycle_cnt_max: std_logic;
    signal s_counter_clk: std_logic;

    signal s_data_0: std_logic_vector(7 downto 0);
    signal s_data_1: std_logic_vector(7 downto 0);
    signal s_data_2: std_logic_vector(7 downto 0);
    signal s_data_3: std_logic_vector(7 downto 0);
    signal s_data_byte: std_logic_vector(7 downto 0);

    component counter_libero_10bit is 
    port( Aclr  : in    std_logic;
          Clock : in    std_logic;
          Q     : out   std_logic_vector(10 downto 0);
          Enable : in    std_logic
        );
    end component;

    component rc_adder_7 is -- Actually 11 bits
    port(	
        i_D0         : in std_logic_vector(10 downto 0);
		i_D1         : in std_logic_vector(10 downto 0);
		i_C          : in std_logic;
		o_S          : out std_logic_vector(10 downto 0);
		o_C          : out std_logic;
		o_O			 : out std_logic
		);
    end component;

    component mux2t1_7 is -- Actually 11 bits
    port(i_S          : in std_logic;
        i_D0         : in std_logic_vector(10 downto 0);
        i_D1         : in std_logic_vector(10 downto 0);
        o_O          : out std_logic_vector(10 downto 0)
    );
    end component;

    component cmp_libero_10bit is --actually 10 bits
    port (
        DataA : in    std_logic_vector(9 downto 0);
        DataB : in    std_logic_vector(9 downto 0);
        AEB   : out   std_logic
    );
    end component;

     --- 8 bit 4t1 mux --
    component mux_4_to_1_8
        -- ports
        port( Data0_port : in    std_logic_vector(7 downto 0);
              Data1_port : in    std_logic_vector(7 downto 0);
              Data2_port : in    std_logic_vector(7 downto 0);
              Data3_port : in    std_logic_vector(7 downto 0);
              Sel0       : in    std_logic;
              Sel1       : in    std_logic;
              Result     : out   std_logic_vector(7 downto 0)
            );
    end component;

    component dffg is 
    port (
        i_CLK        : in std_logic;     -- Clock input
        i_RST        : in std_logic;     -- Reset input
        i_WE         : in std_logic;     -- Write enable input
        i_D          : in std_logic;     -- Data value input
        o_Q          : out std_logic  -- Data value output
    );
    end component;
    

begin

    s_instr_cnt_max <= '1' when (s_spi_byte_cnt = "11") else '0';
    s_data_cnt_max <= '1' when (s_data_byte_cnt = s_calcamt) else '0';
    s_instr_wr <= i_spi_done and s_data_cnt_max;

    process (i_clk, i_rst, s_counter_clk) is 
    begin
        if (i_rst = '1') then
            s_cycle_cnt <=(others => '0');
            s_counter_clk <= '0';
        else
            if (rising_edge(i_clk)) then
                if (s_cycle_cnt_max = '1') then
                    s_cycle_cnt <= (others => '0');
                    s_counter_clk <= '1';
                else
                    s_cycle_cnt <= s_cycle_cnt + 1;
                    s_counter_clk <= '0';
                end if;
            end if;
        end if;
    end process;

    data_cnt: process (i_rst, i_clk, i_spi_done) 
    begin
        if (i_rst = '1') then
            s_data_byte_cnt <= b"00";
        elsif (rising_edge(i_clk) and i_spi_done = '1') then
            if (s_instr_cnt_max = '1') then
                    s_spi_byte_cnt <= (others => '0');
                else
                    s_spi_byte_cnt <= s_spi_byte_cnt + 1;
            end if;
            if (s_data_cnt_max = '1') then
                    s_data_byte_cnt <= (others => '0');
                else
                    s_data_byte_cnt <= s_data_byte_cnt + 1;
            end if;
        end if;

    end process;    


    -- breakout instruction to components
    s_addr_a <= i_inst_cont_load(27 downto 17);
    s_opcode <= i_inst_cont_load(31 downto 28) when s_inst_cmp = '1' else "0000";
    s_calcamt <= i_inst_cont_load(9 downto 0)  when s_inst_cmp = '1';

    s_data_0 <= i_inst_cont_load(7 downto 0);
    s_data_1 <= i_inst_cont_load(15 downto 8);
    s_data_2 <= i_inst_cont_load(23 downto 16);
    
    s_cnt_reset <= (s_done and i_spi_done) or i_rst;

    s_inst_cmp <= s_opcode(0) and s_opcode(1) and s_opcode(2) and s_opcode(3); -- 1 if it's cont load , 0 otherwise (0110)
    
    counter: counter_libero_10bit port map (
        Aclr  => s_cnt_reset,
        Clock => s_counter_clk,
        Q     => s_count,
        Enable => not s_done
    );
    
    cmp: cmp_libero_10bit port map (
        DataA => s_count,
        DataB => s_calcamt,
        AEB   => s_counter_done
    );
    
    cmp_reg: dffg port map (
        i_CLK  => i_clk,     -- Clock input
        i_RST  => i_rst,    -- Reset input
        i_WE   => '1',    -- Write enable input
        i_D    => s_counter_done,    -- Data value input
        o_Q    => s_counter_done_buf   -- Data value output
    );
    adder_a: rc_adder_7 port map (
        i_D0  => s_addr_a, -- 11 bits
		i_D1  => b"000" & s_count & b"00", -- 6 + 5 bits
		i_C   => '0',
		o_S   => s_addr_a_translated,
		o_C   => open,
		o_O	 => open
    );


    mux_addr_a : mux2t1_7 port map (
        i_S   => s_inst_cmp, -- 1 if it's cont load , 0 otherwise
        i_D0  => s_addr_a,
        i_D1  => s_addr_a_translated,
        o_O   => s_addr_a_out
    );
    
    mux_opcode : mux2t1_7 port map ( -- 11 bits
        i_s   => s_inst_cmp,
        i_D0  => b"0000000" & s_opcode,
        i_D1  => b"00000000000",
        o_O   => s_opcode_translated
    );
    
    mux_dataload: mux_4_to_1_8 port map( 
        Data0_port => s_data_0,
        Data1_port => s_data_1,
        Data2_port => s_data_2,
        Data3_port => s_data_3,
        Sel0       => s_spi_byte_cnt(0),
        Sel1       => s_spi_byte_cnt(1),
        Result     => s_data_byte
    );

    s_done <= not s_inst_cmp or (s_inst_cmp and s_counter_done_buf);
    o_done_cont_load <= s_done;

    s_inst_out <= s_opcode_translated(3 downto 0) & b"0000000000" & s_addr_a_out & s_data_byte ;
    o_inst_cont_load <= s_inst_out;
    o_inst_cont_load_en <= s_inst_cmp;

end architecture_instruction_cont_load;
