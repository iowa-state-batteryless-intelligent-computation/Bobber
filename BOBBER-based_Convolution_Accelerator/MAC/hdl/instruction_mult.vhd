
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;


entity instruction_mult is
port (
    i_clk: in std_logic; -- system clock
 	i_inst : IN  std_logic_vector(31 downto 0); -- instruction input\
    i_rst : in std_logic;
    i_spi_done: in std_logic;
    o_inst : OUT std_logic_vector(31 downto 0);  -- output instruction
    o_done : OUT std_logic; -- instruction multiply done signal
    o_mult_en: OUT std_logic --instruction multiply enable signal
);

end instruction_mult;
architecture architecture_instruction_mult of instruction_mult is
    signal s_addr_b: std_logic_vector(10 downto 0); -- 11 bit addresses
    signal s_addr_a: std_logic_vector(10 downto 0);
    signal s_opcode: std_logic_vector(3 downto 0);
    signal s_calcamt: std_logic_vector(5 downto 0); -- 6 bit for calcamt

    signal s_count: std_logic_vector(5 downto 0); -- 6 bit for counter
    signal s_done: std_logic;
    signal s_cnt_reset: std_logic;
    signal s_inst_cmp: std_logic;

    signal s_addr_a_translated: std_logic_vector(10 downto 0); -- output from the adder
    signal s_addr_a_out: std_logic_vector(10 downto 0); -- output from mux

    signal s_addr_b_translated: std_logic_vector(10 downto 0); -- output from the adder
    signal s_addr_b_out: std_logic_vector(10 downto 0); -- output from mux

    signal s_counter_done: std_logic;
    signal s_counter_done_buf: std_logic;
    signal s_opcode_translated: std_logic_vector(10 downto 0); -- should be 4 bits, but set to 11 to accomodate the mux
    signal s_inst_out: std_logic_vector(31 downto 0);

    signal s_cycle_cnt: unsigned(5 downto 0); -- count up to 20
    signal s_cycle_cnt_max: std_logic;
    signal s_counter_clk: std_logic;
 


    component counter_libero is 
    port( Aclr  : in    std_logic;
          Clock : in    std_logic;
          Q     : out   std_logic_vector(5 downto 0);
          Enable : in    std_logic
        );
    end component;

    component rc_adder_7 is -- Actually 11 bits
    port(	
        i_D0         : in std_logic_vector(10 downto 0);
		i_D1         : in std_logic_vector(10 downto 0);
		i_C          : in std_logic;
		o_S          : out std_logic_vector(10 downto 0);
		o_C          : out std_logic;
		o_O			 : out std_logic
		);
    end component;

    component mux2t1_7 is -- Actually 11 bits
    port(i_S          : in std_logic;
        i_D0         : in std_logic_vector(10 downto 0);
        i_D1         : in std_logic_vector(10 downto 0);
        o_O          : out std_logic_vector(10 downto 0)
    );
    end component;

    component cmp_libero is 
    port (
        DataA : in    std_logic_vector(5 downto 0);
        DataB : in    std_logic_vector(5 downto 0);
        AEB   : out   std_logic
    );
    end component;

    component dffg is 
    port (
        i_CLK        : in std_logic;     -- Clock input
        i_RST        : in std_logic;     -- Reset input
        i_WE         : in std_logic;     -- Write enable input
        i_D          : in std_logic;     -- Data value input
        o_Q          : out std_logic  -- Data value output
    );
    end component;

begin
    process (i_clk, i_rst, s_counter_clk) is 
    begin
        if (i_rst = '1') then
            s_cycle_cnt <=(others => '0');
            s_counter_clk <= '0';
        else
            if (rising_edge(i_clk)) then
                if(s_inst_cmp = '1') then
                    if (s_cycle_cnt_max = '1') then
                        s_cycle_cnt <= (others => '0');
                        s_counter_clk <= '1';
                    else
                        s_cycle_cnt <= s_cycle_cnt + 1;
                        s_counter_clk <= '0';
                    end if;
                else
                    s_cycle_cnt <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    s_cycle_cnt_max <= '1' when (s_cycle_cnt = "1100") else '0';


    -- breakout instruction to components
    s_addr_a <= i_inst(27 downto 17);
    s_addr_b <= i_inst(16 downto 6);
    s_opcode <= i_inst(31 downto 28);
    s_calcamt <= i_inst(5 downto 0);
    
    s_cnt_reset <= (s_done and i_spi_done) or i_rst;

    s_inst_cmp <= (not s_opcode(0)) and s_opcode(1) and s_opcode(2) and not s_opcode(3); -- 1 if it's multiplying , 0 otherwise (0110)
    
    counter: counter_libero port map (
        Aclr  => s_cnt_reset and not s_inst_cmp,
        Clock => s_counter_clk,
        Q     => s_count,
        Enable => not s_done
    );
    
    cmp: cmp_libero port map (
        DataA => s_count,
        DataB => s_calcamt,
        AEB   => s_counter_done
    );
    
    cmp_reg: dffg port map (
        i_CLK  => i_clk,     -- Clock input
        i_RST  => i_rst,    -- Reset input
        i_WE   => '1',    -- Write enable input
        i_D    => s_counter_done,    -- Data value input
        o_Q    => s_counter_done_buf   -- Data value output
    );
    adder_a: rc_adder_7 port map (
        i_D0  => s_addr_a, -- 11 bits
		i_D1  => b"000" & s_count & b"00", -- 6 + 5 bits
		i_C   => '0',
		o_S   => s_addr_a_translated,
		o_C   => open,
		o_O	 => open
    );

    adder_b: rc_adder_7 port map (
        i_D0  => s_addr_b, -- 11 bits
		i_D1  => b"000" & s_count & b"00", -- 6 + 5 bits
		i_C   => '0',
		o_S   => s_addr_b_translated,
		o_C   => open,
		o_O	 => open
    );

    mux_addr_a : mux2t1_7 port map (
        i_S   => s_inst_cmp, -- 1 if it's multiplying , 0 otherwise
        i_D0  => s_addr_a,
        i_D1  => s_addr_a_translated,
        o_O   => s_addr_a_out
    );

    mux_addr_b : mux2t1_7 port map (
        i_S   => s_inst_cmp, -- 1 if it's multiplying , 0 otherwise
        i_D0  => s_addr_b,
        i_D1  => s_addr_b_translated,
        o_O   => s_addr_b_out
    );

    
    mux_opcode : mux2t1_7 port map ( -- 11 bits
        i_s   => s_inst_cmp,
        i_D0  => b"0000000" & s_opcode,
        i_D1  => b"00000001101",
        o_O   => s_opcode_translated
    );

    s_done <= not s_inst_cmp or (s_inst_cmp and s_counter_done_buf);
    o_done <= s_done;

    s_inst_out <= s_opcode_translated(3 downto 0) & s_addr_a_out & s_addr_b_out & b"000000" ;
    o_mult_en <= s_inst_cmp;
    o_inst <= s_inst_out;

end architecture_instruction_mult;
