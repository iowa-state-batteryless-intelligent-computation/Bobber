-------------------------------------------------------------------------------- 
-- 
-------------------------------------------------------------------------------- 
 
library IEEE; 
 
use IEEE.std_logic_1164.all; 
use IEEE.numeric_std.all; 
use IEEE.STD_LOGIC_ARITH.ALL; 
use IEEE.STD_LOGIC_SIGNED.ALL; 
 
entity mac_N is 
generic(N : integer := 32; D: integer := 8); -- input data is 8bits, output is 32 bits 
port ( 
     -- Inputs 
    i_weights : in std_logic_vector(D-1 downto 0); 
    i_load_weight : in std_logic; 
    i_load_input : in std_logic; 
    i_rst : in std_logic; 
    i_clk : in std_logic; 
    i_input : in std_logic_vector(D-1 downto 0); 
    i_ps : in std_logic_vector(N-1 downto 0); 
 
    -- Outputs 
    o_weight : out std_logic_vector(D-1 downto 0); 
    o_ps : out std_logic_vector(N-1 downto 0);  
    o_input : out std_logic_vector(D-1 downto 0)  
); 
end mac_N; 
 
 
architecture architecture_mac_N of mac_N is 
   -- signal, component etc. declarations 
	signal s_weight_out : std_logic_vector(8-1 downto 0); 
	signal s_mult_out : std_logic_vector(24-1 downto 0) ;  
    signal s_ps_out: std_logic_vector(32-1 downto 0); 
    signal s_ext_weight: std_logic_vector(11 downto 0); 
    signal s_ext_input: std_logic_vector(11 downto 0); 
 
 
    component reg_N is  
        port ( 
            i_CLK        : in std_logic;    -- Clock input 
			i_RST        : in std_logic;    -- Reset input 
			i_WE         : in std_logic;    -- Write enable input 
			i_D			: in std_logic_vector(32-1 downto 0); -- data in value 
            o_D			: out std_logic_vector(32-1 downto 0) -- data out value 
        ); 
    end component; 
 
    component reg_8 is  
        port( 
		i_CLK		: in std_logic; -- Clock input 
		i_RST		: in std_logic; -- Reset register input 
		i_WE		: in std_logic; -- Write enable for register 
		i_D			: in std_logic_vector(8-1 downto 0); -- data in value 
		o_D			: out std_logic_vector(8-1 downto 0) -- data out value 
	); 
    end component; 
 
    component rc_adder_N is  
        port ( 
            i_D0         : in std_logic_vector(32-1 downto 0); 
            i_D1         : in std_logic_vector(32-1 downto 0); 
            i_C          : in std_logic; 
            o_S          : out std_logic_vector(32-1 downto 0); 
            o_C          : out std_logic; 
            o_O			 : out std_logic 
    ); 
    end component; 
 
    component mult_libero is  
        port ( 
            DataA : in    std_logic_vector(11 downto 0); 
            DataB : in    std_logic_vector(11 downto 0); 
            Mult  : out   std_logic_vector(24-1 downto 0) 
        );  
    end component; 
 
     
 
begin 
    o_weight <= s_weight_out; 
    s_ext_weight <= SXT(s_weight_out, 12); 
    s_ext_input  <= SXT(o_input, 12); 
    -- weight register 
    reg_w: reg_8 port map ( 
        i_CLK => i_clk, 
        i_RST => i_rst, 
        i_WE => i_load_weight, 
        i_D => i_weights, 
        o_D => s_weight_out 
    ); 
 
    -- input data register 
    reg_i: reg_8 port map ( 
        i_CLK => i_clk, 
        i_RST => i_rst, 
        i_WE => i_load_input, 
        i_D => i_input, 
        o_D => o_input 
    ); 
 
    mult: mult_libero port map ( 
        DataA => s_ext_weight, 
        DataB => s_ext_input, 
        Mult => s_mult_out 
    ); 
 
    adder: rc_adder_N port map ( 
        i_D0 => i_ps, 
        i_D1 => SXT(s_mult_out,32), 
        i_C => '0', 
        o_S => s_ps_out, 
        o_C => open, 
        o_O => open 
    ); 
 
 
    o_ps <= s_ps_out; 
 --   reg_ps: reg_N port map ( 
  --      i_CLK => i_clk, 
  --      i_RST => i_rst, 
  --      i_WE => '1', 
  --     i_D => s_ps_out, 
   --     o_D => o_ps 
  --  ); 
 
   -- architecture body 
end architecture_mac_N; 
