--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity memory_module is
generic(N : integer := 8; M : integer := 11); -- Generic of type integer for input/output data width. Memory address are 2+9 bits long.
port (
    i_CLK: in std_logic;
    i_RST: in std_logic;

    -- WD read ports
    i_WD_READ_ADDR_A: in std_logic_vector(M-1 downto 0); -- 11 bit address, port A of Weight/Data read
    i_WD_READ_ADDR_B: in std_logic_vector(M-1 downto 0); -- 11 bit address, port B of Weight/Data read
    o_WD_READ_DATA_A: out std_logic_vector(N-1 downto 0); -- 8 bit data
    o_WD_READ_DATA_B: out std_logic_vector(N-1 downto 0); -- 8 bit data

    -- WD write ports
    i_WD_WRITE_ADDR_A: in std_logic_vector(M-1 downto 0); -- 11 bit address, port A of Weight/Data input
    i_WD_WRITE_ADDR_B: in std_logic_vector(M-1 downto 0); -- 11 bit address, port B of Weight/Data input
    i_WD_WRITE_DATA_A: in std_logic_vector(N-1 downto 0); -- 8 bit data in
    i_WD_WRITE_DATA_B: in std_logic_vector(N-1 downto 0); -- 8 bit data in
    i_WD_WRITE_EN_A: in std_logic; -- port A write enable
    i_WD_WRITE_EN_B: in std_logic; -- port B write enable

    -- PS read ports
    i_PS_READ_ADDR_A: in std_logic_vector(M-1 downto 0); -- 11 bit address, port A of partial sum read
    i_PS_READ_ADDR_B: in std_logic_vector(M-1 downto 0); -- 11 bit address, port B of partial sum read
    o_PS_READ_DATA_A: out std_logic_vector(N-1 downto 0); -- 8 bit data
    o_PS_READ_DATA_B: out std_logic_vector(N-1 downto 0); -- 8 bit data

    -- WD write ports
    i_PS_WRITE_ADDR_A: in std_logic_vector(M-1 downto 0); -- 11 bit address, port A of partial sum input
    i_PS_WRITE_ADDR_B: in std_logic_vector(M-1 downto 0); -- 11 bit address, port B of partial sum input
    i_PS_WRITE_DATA_A: in std_logic_vector(N-1 downto 0); -- 8 bit data in
    i_PS_WRITE_DATA_B: in std_logic_vector(N-1 downto 0); -- 8 bit data in
    i_PS_WRITE_EN_A: in std_logic; -- port A write enable
    i_PS_WRITE_EN_B: in std_logic; -- port B write enable

    i_PS_READ: in std_logic -- enable reading partial sum data
);
end memory_module;
architecture architecture_memory_module of memory_module is
    signal s_00_DIN_SEL_A : std_logic_vector(1 downto 0); -- port A DIN MUX select signal
    signal s_00_DIN_SEL_B : std_logic_vector(1 downto 0); -- port B DIN MUX select signal 
    signal s_00_DIN_A: std_logic_vector(8-1 downto 0); -- port A DIN mux output
    signal s_00_DIN_B: std_logic_vector(8-1 downto 0); -- port B DIN mux output
    signal s_00_DOUT_A: std_logic_vector(8-1 downto 0);  -- port A Output
    signal s_00_DOUT_B: std_logic_vector(8-1 downto 0);  -- port B output
    signal s_00_ADDR_SEL_A: std_logic_vector(1 downto 0); -- port A Addr MUX select signal
    signal s_00_ADDR_SEL_B: std_logic_vector(1 downto 0); -- port B addr MUX select signal
    signal s_00_ADDR_A: std_logic_vector(8 downto 0); -- port A addr MUX output
    signal s_00_ADDR_B: std_logic_vector(8 downto 0); -- port B addr MUX output
    signal s_00_WE_A: std_logic; -- port A write enable
    signal s_00_WE_B: std_logic; -- port B write enable

    signal s_01_DIN_SEL_A : std_logic_vector(1 downto 0); -- port A DIN MUX select signal
    signal s_01_DIN_SEL_B : std_logic_vector(1 downto 0); -- port B DIN MUX select signal 
    signal s_01_DIN_A: std_logic_vector(8-1 downto 0); -- port A DIN mux output
    signal s_01_DIN_B: std_logic_vector(8-1 downto 0); -- port B DIN mux output
    signal s_01_DOUT_A: std_logic_vector(8-1 downto 0);  -- port A Output
    signal s_01_DOUT_B: std_logic_vector(8-1 downto 0);  -- port B output
    signal s_01_ADDR_SEL_A: std_logic_vector(1 downto 0); -- port A Addr MUX select signal
    signal s_01_ADDR_SEL_B: std_logic_vector(1 downto 0); -- port B addr MUX select signal
    signal s_01_ADDR_A: std_logic_vector(8 downto 0); -- port A addr MUX output
    signal s_01_ADDR_B: std_logic_vector(8 downto 0); -- port B addr MUX output
    signal s_01_WE_A: std_logic; -- port A write enable
    signal s_01_WE_B: std_logic; -- port B write enable

    signal s_10_DIN_SEL_A : std_logic_vector(1 downto 0); -- port A DIN MUX select signal
    signal s_10_DIN_SEL_B : std_logic_vector(1 downto 0); -- port B DIN MUX select signal 
    signal s_10_DIN_A: std_logic_vector(8-1 downto 0); -- port A DIN mux output
    signal s_10_DIN_B: std_logic_vector(8-1 downto 0); -- port B DIN mux output
    signal s_10_DOUT_A: std_logic_vector(8-1 downto 0);  -- port A Output
    signal s_10_DOUT_B: std_logic_vector(8-1 downto 0);  -- port B output
    signal s_10_ADDR_SEL_A: std_logic_vector(1 downto 0); -- port A Addr MUX select signal
    signal s_10_ADDR_SEL_B: std_logic_vector(1 downto 0); -- port B addr MUX select signal
    signal s_10_ADDR_A: std_logic_vector(8 downto 0); -- port A addr MUX output
    signal s_10_ADDR_B: std_logic_vector(8 downto 0); -- port B addr MUX output
    signal s_10_WE_A: std_logic; -- port A write enable
    signal s_10_WE_B: std_logic; -- port B write enable

    signal s_11_DIN_SEL_A : std_logic_vector(1 downto 0); -- port A DIN MUX select signal
    signal s_11_DIN_SEL_B : std_logic_vector(1 downto 0); -- port B DIN MUX select signal 
    signal s_11_DIN_A: std_logic_vector(8-1 downto 0); -- port A DIN mux output
    signal s_11_DIN_B: std_logic_vector(8-1 downto 0); -- port B DIN mux output
    signal s_11_DOUT_A: std_logic_vector(8-1 downto 0);  -- port A Output
    signal s_11_DOUT_B: std_logic_vector(8-1 downto 0);  -- port B output
    signal s_11_ADDR_SEL_A: std_logic_vector(1 downto 0); -- port A Addr MUX select signal
    signal s_11_ADDR_SEL_B: std_logic_vector(1 downto 0); -- port B addr MUX select signal
    signal s_11_ADDR_A: std_logic_vector(8 downto 0); -- port A addr MUX output
    signal s_11_ADDR_B: std_logic_vector(8 downto 0); -- port B addr MUX output
    signal s_11_WE_A: std_logic; -- port A write enable
    signal s_11_WE_B: std_logic; -- port B write enable

    signal s_WD_READ_ADDR_BUF_A: std_logic_vector(11-1 downto 0);
    signal s_WD_READ_ADDR_BUF_B: std_logic_vector(11-1 downto 0);

    signal s_PS_READ_ADDR_BUF_A: std_logic_vector(11-1 downto 0);
    signal s_PS_READ_ADDR_BUF_B: std_logic_vector(11-1 downto 0);

    component dist_ram_32_32
        --ports
         port( 
              DINA  : in    std_logic_vector(8-1 downto 0);
              DOUTA : out   std_logic_vector(8-1 downto 0);
              DINB  : in    std_logic_vector(8-1 downto 0);
              DOUTB : out   std_logic_vector(8-1 downto 0);
              ADDRA : in    std_logic_vector(8 downto 0);
              ADDRB : in    std_logic_vector(8 downto 0);
              RWA   : in    std_logic;
              RWB   : in    std_logic;
              BLKA  : in    std_logic;
              BLKB  : in    std_logic;
              CLKA : in    std_logic;
              CLKB : in    std_logic;
              RESET : in    std_logic
            );
    end component;

    component mux_4_to_1_32
    port( Data0_port : in    std_logic_vector(8-1 downto 0);
          Data1_port : in    std_logic_vector(8-1 downto 0);
          Data2_port : in    std_logic_vector(8-1 downto 0);
          Data3_port : in    std_logic_vector(8-1 downto 0);
          Sel0       : in    std_logic;
          Sel1       : in    std_logic;
          Result     : out   std_logic_vector(N-1 downto 0)
        );
    end component;

    component mux_4_to_1_9 -- actually 9 bits
    port( Data0_port : in    std_logic_vector(9-1 downto 0);
          Data1_port : in    std_logic_vector(9-1 downto 0);
          Data2_port : in    std_logic_vector(9-1 downto 0);
          Data3_port : in    std_logic_vector(9-1 downto 0);
          Sel0       : in    std_logic;
          Sel1       : in    std_logic;
          Result     : out   std_logic_vector(9-1 downto 0)
        );
    end component;

    component reg_7 -- actually 11 bits
    port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(10 downto 0); -- data in value
		o_D			: out std_logic_vector(10 downto 0) -- data out value
	);
    end component;

begin
    ------------ADDRESS BUFFER------------------------------------------------------------------------------------------------------------------------------
    -- since memory access is one cycle delayed, we need to delay the address by 1 cycle too, for the output muxes
    wd_addr_a_buf: reg_7 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => '1',
        i_D => i_WD_READ_ADDR_A,
        o_D => s_WD_READ_ADDR_BUF_A
    );

    wd_addr_b_buf: reg_7 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => '1',
        i_D => i_WD_READ_ADDR_B,
        o_D => s_WD_READ_ADDR_BUF_B
    );

    ps_addr_a_buf: reg_7 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => '1',
        i_D => i_PS_READ_ADDR_A,
        o_D => s_PS_READ_ADDR_BUF_A
    );

    ps_addr_b_buf: reg_7 port map(
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => '1',
        i_D => i_PS_READ_ADDR_B,
        o_D => s_PS_READ_ADDR_BUF_B
    );



    ------------- BANK 00 ----------------------------------------------------------------------------------------------------------------------------------
    s_00_DIN_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"000")
                        else b"01";
    s_00_DIN_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"000")
                        else b"01";

    s_00_ADDR_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"000")       -- we write
                        else b"01" when (i_WD_READ_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A & i_PS_READ = b"0010")   -- wd read
                        else b"10" when (i_PS_WRITE_ADDR_A(10 downto 9) = b"00")  -- ps write
                        else b"11";
    s_00_ADDR_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"000") 
                        else b"01" when (i_WD_READ_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"001") 
                        else b"10" when (i_PS_WRITE_ADDR_B(10 downto 9) = b"00")
                        else b"11";
    
    s_00_WE_A <= '0' when i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"000"
                else '0' when i_PS_WRITE_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"000"
                else '1';

    s_00_WE_B <= '0' when i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"000"
                else '0' when i_PS_WRITE_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"000"
                else '1';
   -- s_00_WE_A <= (not i_WD_WRITE_ADDR_A(6) and not i_WD_WRITE_ADDR_A(5) and i_WD_WRITE_EN_A) and (not i_PS_WRITE_ADDR_A(6) and not i_PS_WRITE_ADDR_A(5) and i_PS_WRITE_EN_A);
   -- s_00_WE_B <= (not i_WD_WRITE_ADDR_B(6) and not i_WD_WRITE_ADDR_B(5) and i_WD_WRITE_EN_B) and (not i_PS_WRITE_ADDR_B(6) and not i_PS_WRITE_ADDR_B(5) and i_PS_WRITE_EN_B);

    
    -- DIN mux for port A, bank 00
    mux_din_00_A: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_A,
        Data1_Port => i_PS_WRITE_DATA_A,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_00_DIN_SEL_A(0),
        Sel1 => '0',
        Result => s_00_DIN_A
    );

    -- DIN mux for port B, bank 00
    mux_din_00_B: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_B,
        Data1_Port => i_PS_WRITE_DATA_B,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_00_DIN_SEL_B(0),
        Sel1 => '0',
        Result => s_00_DIN_B
    );

    -- Address mux for port A, bank 00
    mux_addr_00_A: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_A(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_A(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_A(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_A(8 downto 0),
        Sel0 => s_00_ADDR_SEL_A(0),
        Sel1 => s_00_ADDR_SEL_A(1),
        Result => s_00_ADDR_A
    );
    
    -- Address mux for port B, bank 00
    mux_addr_00_B: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_B(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_B(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_B(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_B(8 downto 0),
        Sel0 => s_00_ADDR_SEL_B(0),
        Sel1 => s_00_ADDR_SEL_B(1),
        Result => s_00_ADDR_B
    );

    -- memory address 00xxxxx
    mem_00 : dist_ram_32_32 port map (
        DINA => s_00_DIN_A,
        DOUTA => s_00_DOUT_A,
        DINB => s_00_DIN_B,
        DOUTB => s_00_DOUT_B,
        ADDRA => s_00_ADDR_A,
        ADDRB => s_00_ADDR_B,
        RWA   => s_00_WE_A,
        RWB   => s_00_WE_B,
        BLKA  => '1',
        BLKB  => '1',
        CLKA => i_CLK,
        CLKB => i_CLK,
        RESET => i_RST
    );


   ------------- BANK 01 -----------------------------------------------------------------------------------------------------------------------------------
    s_01_DIN_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"010")
                        else b"01";
    s_01_DIN_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"010")
                        else b"01";


    s_01_ADDR_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"010")       -- we write
                        else b"01" when (i_WD_READ_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"011")   -- wd read
                        else b"10" when (i_PS_WRITE_ADDR_A(10 downto 9) = b"00")  -- ps write
                        else b"11";
    s_01_ADDR_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"010") 
                        else b"01" when (i_WD_READ_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"011") 
                        else b"10" when (i_PS_WRITE_ADDR_B(10 downto 9) = b"00")
                        else b"11";
    
    
    s_01_WE_A <= '0' when i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"010"
                else '0' when i_PS_WRITE_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"010"
                else '1';

    s_01_WE_B <= '0' when i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"010"
                else '0' when i_PS_WRITE_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"010"
                else '1';
-- s_01_WE_A <= not(not i_WD_WRITE_ADDR_A(6) and i_WD_WRITE_ADDR_A(5) and i_WD_WRITE_EN_A) or (not i_PS_WRITE_ADDR_A(6) and i_PS_WRITE_ADDR_A(5) and i_PS_WRITE_EN_A);  -- 01 and write enable
    -- s_01_WE_B <= not(not i_WD_WRITE_ADDR_B(6) and i_WD_WRITE_ADDR_B(5) and i_WD_WRITE_EN_B) or (not i_PS_WRITE_ADDR_B(6) and i_PS_WRITE_ADDR_B(5) and i_PS_WRITE_EN_B);  -- 01 and write enable

    
    -- DIN mux for port A, bank 00
    mux_din_01_A: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_A,
        Data1_Port => i_PS_WRITE_DATA_A,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_01_DIN_SEL_A(0),
        Sel1 => '0',
        Result => s_01_DIN_A
    );

    -- DIN mux for port B, bank 00
    mux_din_01_B: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_B,
        Data1_Port => i_PS_WRITE_DATA_B,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_01_DIN_SEL_B(0),
        Sel1 => '0',
        Result => s_01_DIN_B
    );

    -- Address mux for port A, bank 00
    mux_addr_01_A: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_A(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_A(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_A(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_A(8 downto 0),
        Sel0 => s_01_ADDR_SEL_A(0),
        Sel1 => s_01_ADDR_SEL_A(1),
        Result => s_01_ADDR_A
    );
    
    -- Address mux for port B, bank 00
    mux_addr_01_B: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_B(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_B(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_B(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_B(8 downto 0),
        Sel0 => s_01_ADDR_SEL_B(0),
        Sel1 => s_01_ADDR_SEL_B(1),
        Result => s_01_ADDR_B
    );

    -- memory address 00xxxxx
    mem_01 : dist_ram_32_32 port map (
        DINA => s_01_DIN_A,
        DOUTA => s_01_DOUT_A,
        DINB => s_01_DIN_B,
        DOUTB => s_01_DOUT_B,
        ADDRA => s_01_ADDR_A,
        ADDRB => s_01_ADDR_B,
        RWA   => s_01_WE_A,
        RWB   => s_01_WE_B,
        BLKA  => '1',
        BLKB  => '1',
        CLKA => i_CLK,
        CLKB => i_CLK,
        RESET => i_RST
    );


   ------------- BANK 10 -----------------------------------------------------------------------------------------------------------------------------------
    s_10_DIN_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"100")
                        else b"01";
    s_10_DIN_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"100")
                        else b"01";


    s_10_ADDR_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"100")       -- we write
                        else b"01" when (i_WD_READ_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"101")   -- wd read
                        else b"10" when (i_PS_WRITE_ADDR_A(10 downto 9) = b"00")  -- ps write
                        else b"11";
    s_10_ADDR_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"100") 
                        else b"01" when (i_WD_READ_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"101") 
                        else b"10" when (i_PS_WRITE_ADDR_B(10 downto 9) = b"00")
                        else b"11";
    
    
    s_10_WE_A <= '0' when i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"100"
                else '0' when i_PS_WRITE_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"100"
                else '1';

    s_10_WE_B <= '0' when i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"100"
                else '0' when i_PS_WRITE_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"100"
                else '1';
    -- s_10_WE_A <= not(i_WD_WRITE_ADDR_A(6) and not i_WD_WRITE_ADDR_A(5) and i_WD_WRITE_EN_A) or (i_PS_WRITE_ADDR_A(6) and not i_PS_WRITE_ADDR_A(5) and i_PS_WRITE_EN_A);  -- 10 and write enable
    -- s_10_WE_B <= not(i_WD_WRITE_ADDR_B(6) and not i_WD_WRITE_ADDR_B(5) and i_WD_WRITE_EN_B) or (i_PS_WRITE_ADDR_B(6) and not i_PS_WRITE_ADDR_B(5) and i_PS_WRITE_EN_B);  -- 10 and write enable

    
    -- DIN mux for port A, bank 00
    mux_din_10_A: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_A,
        Data1_Port => i_PS_WRITE_DATA_A,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_10_DIN_SEL_A(0),
        Sel1 => '0',
        Result => s_10_DIN_A
    );

    -- DIN mux for port B, bank 00
    mux_din_10_B: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_B,
        Data1_Port => i_PS_WRITE_DATA_B,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_10_DIN_SEL_B(0),
        Sel1 => '0',
        Result => s_10_DIN_B
    );

    -- Address mux for port A, bank 00
    mux_addr_10_A: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_A(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_A(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_A(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_A(8 downto 0),
        Sel0 => s_10_ADDR_SEL_A(0),
        Sel1 => s_10_ADDR_SEL_A(1),
        Result => s_10_ADDR_A
    );
    
    -- Address mux for port B, bank 00
    mux_addr_10_B: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_B(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_B(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_B(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_B(8 downto 0),
        Sel0 => s_10_ADDR_SEL_B(0),
        Sel1 => s_10_ADDR_SEL_B(1),
        Result => s_10_ADDR_B
    );

    -- memory address 00xxxxx
    mem_10 : dist_ram_32_32 port map (
        DINA => s_10_DIN_A,
        DOUTA => s_10_DOUT_A,
        DINB => s_10_DIN_B,
        DOUTB => s_10_DOUT_B,
        ADDRA => s_10_ADDR_A,
        ADDRB => s_10_ADDR_B,
        RWA   => s_10_WE_A,
        RWB   => s_10_WE_B,
        BLKA  => '1',
        BLKB  => '1',
        CLKA => i_CLK,
        CLKB => i_CLK,
        RESET => i_RST
    );

   ------------- BANK 11 -----------------------------------------------------------------------------------------------------------------------------------
    s_11_DIN_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"110")
                        else b"01";
    s_11_DIN_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"110")
                        else b"01";


    s_11_ADDR_SEL_A <= b"00" when (i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"110")       -- we write
                        else b"01" when (i_WD_READ_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"111")   -- wd read
                        else b"10" when (i_PS_WRITE_ADDR_A(10 downto 9) = b"00")  -- ps write
                        else b"11";
    s_11_ADDR_SEL_B <= b"00" when (i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"110") 
                        else b"01" when (i_WD_READ_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"111") 
                        else b"10" when (i_PS_WRITE_ADDR_B(10 downto 9) = b"00")
                        else b"11";
    
    s_11_WE_A <= '0' when i_WD_WRITE_ADDR_A(10 downto 9) & i_WD_WRITE_EN_A = b"110"
                else '0' when i_PS_WRITE_ADDR_A(10 downto 9) & i_PS_WRITE_EN_A = b"110"
                else '1';

    s_11_WE_B <= '0' when i_WD_WRITE_ADDR_B(10 downto 9) & i_WD_WRITE_EN_B = b"110"
                else '0' when i_PS_WRITE_ADDR_B(10 downto 9) & i_PS_WRITE_EN_B = b"110"
                else '1';
    -- s_11_WE_A <= not(i_WD_WRITE_ADDR_A(6) and i_WD_WRITE_ADDR_A(5) and i_WD_WRITE_EN_A) or (i_PS_WRITE_ADDR_A(6) and i_PS_WRITE_ADDR_A(5) and i_PS_WRITE_EN_A);  -- 01 and write enable
    -- s_11_WE_B <= not(i_WD_WRITE_ADDR_B(6) and i_WD_WRITE_ADDR_B(5) and i_WD_WRITE_EN_B) or (i_PS_WRITE_ADDR_B(6) and i_PS_WRITE_ADDR_B(5) and i_PS_WRITE_EN_B);  -- 01 and write enable

    
    -- DIN mux for port A, bank 00
    mux_din_11_A: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_A,
        Data1_Port => i_PS_WRITE_DATA_A,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_11_DIN_SEL_A(0),
        Sel1 => '0',
        Result => s_11_DIN_A
    );

    -- DIN mux for port B, bank 00
    mux_din_11_B: mux_4_to_1_32 port map (
        Data0_Port => i_WD_WRITE_DATA_B,
        Data1_Port => i_PS_WRITE_DATA_B,
        Data2_Port => x"00",
        Data3_Port => x"00",
        Sel0 => s_11_DIN_SEL_B(0),
        Sel1 => '0',
        Result => s_11_DIN_B
    );

    -- Address mux for port A, bank 00
    mux_addr_11_A: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_A(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_A(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_A(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_A(8 downto 0),
        Sel0 => s_11_ADDR_SEL_A(0),
        Sel1 => s_11_ADDR_SEL_A(1),
        Result => s_11_ADDR_A
    );
    
    -- Address mux for port B, bank 00
    mux_addr_11_B: mux_4_to_1_9 port map (
        Data0_Port => i_WD_WRITE_ADDR_B(8 downto 0),
        Data1_Port => i_WD_READ_ADDR_B(8 downto 0),
        Data2_Port => i_PS_WRITE_ADDR_B(8 downto 0),
        Data3_Port => i_PS_READ_ADDR_B(8 downto 0),
        Sel0 => s_11_ADDR_SEL_B(0),
        Sel1 => s_11_ADDR_SEL_B(1),
        Result => s_11_ADDR_B
    );

    -- memory address 00xxxxx
    mem_11 : dist_ram_32_32 port map (
        DINA => s_11_DIN_A,
        DOUTA => s_11_DOUT_A,
        DINB => s_11_DIN_B,
        DOUTB => s_11_DOUT_B,
        ADDRA => s_11_ADDR_A,
        ADDRB => s_11_ADDR_B,
        RWA   => s_11_WE_A,
        RWB   => s_11_WE_B,
        BLKA  => '1',
        BLKB  => '1',
        CLKA => i_CLK,
        CLKB => i_CLK,
        RESET => i_RST
    );

------------------- OUTPUT MUXES -----------------------

   mux_dw_out_a: mux_4_to_1_32 port map (
        Data0_Port => s_00_DOUT_A,
        Data1_Port => s_01_DOUT_A,
        Data2_Port => s_10_DOUT_A,
        Data3_Port => s_11_DOUT_A,
        Sel0 => s_WD_READ_ADDR_BUF_A(9),
        Sel1 => s_WD_READ_ADDR_BUF_A(10),
        Result => o_WD_READ_DATA_A
    );

   mux_dw_out_b: mux_4_to_1_32 port map (
        Data0_Port => s_00_DOUT_B,
        Data1_Port => s_01_DOUT_B,
        Data2_Port => s_10_DOUT_B,
        Data3_Port => s_11_DOUT_B,
        Sel0 => s_WD_READ_ADDR_BUF_B(9),
        Sel1 => s_WD_READ_ADDR_BUF_B(10),
        Result => o_WD_READ_DATA_B
    );

   mux_ps_out_a: mux_4_to_1_32 port map (
        Data0_Port => s_00_DOUT_A,
        Data1_Port => s_01_DOUT_A,
        Data2_Port => s_10_DOUT_A,
        Data3_Port => s_11_DOUT_A,
        Sel0 => s_PS_READ_ADDR_BUF_A(9),
        Sel1 => s_PS_READ_ADDR_BUF_A(10),
        Result => o_PS_READ_DATA_A
    );

   mux_ps_out_b: mux_4_to_1_32 port map (
        Data0_Port => s_00_DOUT_B,
        Data1_Port => s_01_DOUT_B,
        Data2_Port => s_10_DOUT_B,
        Data3_Port => s_11_DOUT_B,
        Sel0 => s_PS_READ_ADDR_BUF_B(9),
        Sel1 => s_PS_READ_ADDR_BUF_B(10),
        Result => o_PS_READ_DATA_B
    );
end architecture_memory_module;
