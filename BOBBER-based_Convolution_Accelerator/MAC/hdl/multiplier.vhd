--------------------------------------------------------------------------------

--------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity multiplier is
    port(i_a    : in signed(6 downto 0);
         i_b    : in signed(6 downto 0);
         o_p    : out signed(13 downto 0));
end multiplier;

architecture rtl of multiplier is
begin
    o_p <= i_a * i_b;
end rtl;
