
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Mux2t1_df is
	port(
		i_D0	: in std_logic;
		i_D1	: in std_logic;
		i_s		: in std_logic;
		o_O		: out std_logic
	);
end Mux2t1_df;

architecture dataflow of Mux2t1_df is
	begin
		o_O <= 	i_D0 when (i_s = '0') else
				i_D1;
	end dataflow;