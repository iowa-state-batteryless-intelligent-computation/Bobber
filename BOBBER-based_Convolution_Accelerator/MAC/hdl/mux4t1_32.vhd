-------------------------------------------------------------------------

-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux4t1_32 is
  generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
  port(i_S0         : in std_logic;
       i_S1         : in std_logic;
       i_D0         : in std_logic_vector(N-1 downto 0);
       i_D1         : in std_logic_vector(N-1 downto 0);
       i_D2         : in std_logic_vector(N-1 downto 0);
       i_D3         : in std_logic_vector(N-1 downto 0); 
       o_O          : out std_logic_vector(N-1 downto 0));

end mux4t1_32;

architecture structural of mux4t1_32 is

  component mux4t1_df is
    port(i_S0                  : in std_logic;
         i_S1                  : in std_logic;
         i_D0                 : in std_logic;
         i_D1                 : in std_logic;
         i_D2                 : in std_logic;
         i_D3                 : in std_logic;   
         o_O                  : out std_logic);
  end component;

begin

  -- Instantiate N mux instances.
  G_NBit_MUX: for i in 0 to N-1 generate
    MUXI: mux4t1_df port map(
              i_S0     => i_S0,     -- All instances share the same select input.
              i_S1     => i_S1,     -- All instances share the same select input.
              i_D0     => i_D0(i),  -- ith instance's data 0 input hooked up to ith data 0 input.
              i_D1     => i_D1(i),  -- ith instance's data 1 input hooked up to ith data 1 input.
              i_D2     => i_D2(i),  -- ith instance's data 0 input hooked up to ith data 0 input.
              i_D3     => i_D3(i),  -- ith instance's data 1 input hooked up to ith data 1 input.

              o_O      => o_O(i));  -- ith instance's data output hooked up to ith data output.
  end generate G_NBit_MUX;
  
end structural;
