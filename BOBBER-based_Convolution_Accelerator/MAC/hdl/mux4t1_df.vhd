-- mux2t1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 2x1 1 bit
-- multiplexer.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Mux4t1_df is
	port(
		i_D0	: in std_logic;
		i_D1	: in std_logic;
        i_D2	: in std_logic;
		i_D3	: in std_logic;
        
		i_s0    : in std_logic;
        i_s1    : in std_logic;
		o_O		: out std_logic
	);
end Mux4t1_df;

architecture dataflow of Mux4t1_df is
	begin
		o_O <= 	i_D0 when (i_s0 = '0' and i_s1 = '0' ) else
				i_D1 when (i_s0 = '0' and i_s1 = '1' ) else
                i_D2 when (i_s0 = '1' and i_s1 = '0' ) else
				i_D3 when (i_s0 = '1' and i_s1 = '1' );
	end dataflow;