
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity rc_adder_7 is
  generic(N : integer := 11); -- Generic of type integer for input/output data width. Default value is 32.
  port(	i_D0         : in std_logic_vector(N-1 downto 0);
		i_D1         : in std_logic_vector(N-1 downto 0);
		i_C          : in std_logic;
		o_S          : out std_logic_vector(N-1 downto 0);
		o_C          : out std_logic;
		o_O			 : out std_logic
		);

end rc_adder_7;

architecture structural of rc_adder_7 is

  component f_adder is
    port(
         i_D0                 : in std_logic;
         i_D1                 : in std_logic;
	     i_C                  : in std_logic;
         o_S                  : out std_logic;
		 o_C                  : out std_logic
		 );
  end component;

  signal s_C :	std_logic_vector(N-2 downto 0); -- Carry Signals
  signal s_o_c : std_logic; 
begin	
  -- Instantiate N single bit adder instances.
  c0 : f_adder port map(
              i_D0     => i_D0(0),
              i_D1     => i_D1(0),
              i_C      => i_C,
              o_S      => o_S(0),
              o_C      => s_C(0)
			  );
  
  c : for i in 1 to N-2 generate
    add1toNminus2: f_adder port map(
              i_D0     => i_D0(i),
              i_D1     => i_D1(i),
              i_C      => s_C(i-1),
              o_S      => o_S(i),
              o_C      => s_C(i)
			  );
			  
    end generate c;
			 
  cNminus1 : f_adder port map(
              i_D0     => i_D0(N-1),
              i_D1     => i_D1(N-1),
              i_C      => s_C(N-2),
              o_S      => o_S(N-1),
              o_C      => s_o_c
			  );

  o_C <= s_o_c;
  o_O <= s_C(N-2) xor s_o_C;
  
end structural;
