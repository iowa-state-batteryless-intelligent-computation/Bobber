library IEEE;
use IEEE.std_logic_1164.all;

entity reg_16 is
	generic(N : integer := 16); -- Generic of type integer for input/output/store data width. Default value is 32.
	port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(N-1 downto 0); -- data in value
		o_D			: out std_logic_vector(N-1 downto 0) -- data out value
	);
end reg_16;

architecture structural of reg_16 is
	-- D-Flip Flop component
	component dffg is
		port(
			i_CLK        : in std_logic;    -- Clock input
			i_RST        : in std_logic;    -- Reset input
			i_WE         : in std_logic;    -- Write enable input
			i_D          : in std_logic;    -- Data value input
			o_Q          : out std_logic	-- Data value output
		);
	end component;

	begin
		reg : for i in 0 to N-1 generate
			bit_store: dffg port map(
				i_CLK => i_CLK,
				i_RST => i_RST,
				i_WE => i_WE,
				i_D => i_D(i),
				o_Q => o_D(i)
			);
		end generate reg;
end structural;