
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity relu is
generic(N : integer := 8); -- 8 bit 
port (
    i_D : in std_logic_vector(N-1 downto 0);
    o_DATA: out std_logic_vector(N-1 downto 0)
    --<other_ports>;
);
end relu;
architecture architecture_relu of relu is
    -- mux component --
    component mux2t1_32 -- actually 8 bits
        -- ports
        port(
            i_S          : in std_logic;
            i_D0         : in std_logic_vector(N-1 downto 0);
            i_D1         : in std_logic_vector(N-1 downto 0);
            o_O          : out std_logic_vector(N-1 downto 0)
        );
    end component;

begin

     -- mux 0 -- 
    mux_0 : mux2t1_32 port map (
        i_S  => i_D(N-1),
		i_D0 => i_D,
		i_D1 => x"00",
		o_O  => o_DATA
    );

end architecture_relu;
