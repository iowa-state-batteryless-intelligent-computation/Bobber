--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity shift_reg_spi_32 is
generic(N : integer := 32; M: integer:= 7; D: integer := 8); -- input data is 8bits, output is 32 bits, address is 7 bits
port (
   i_DATA : in std_logic_vector(N-1 downto 0);
   i_WE_P   : in std_logic;
   i_SHIFT_EN: in std_logic;
   i_CLK  : in std_logic;
   i_RST  : in std_logic;
   o_DATA : out std_logic_vector(D-1 downto 0)
);
end shift_reg_spi_32;
architecture architecture_shift_reg_spi_32 of shift_reg_spi_32 is

    signal s_reg_3_out : std_logic_vector(D-1 downto 0);
    signal s_reg_2_out : std_logic_vector(D-1 downto 0);
    signal s_reg_1_out : std_logic_vector(D-1 downto 0);

    signal s_reg_2_in  : std_logic_vector(D-1 downto 0);
    signal s_reg_1_in  : std_logic_vector(D-1 downto 0);
    signal s_reg_0_in  : std_logic_vector(D-1 downto 0);

    signal sft_clk_reg : std_logic;
    signal sft_clk_fedge_en : std_logic;
    signal sft_clk_redge_en : std_logic;

    component reg_8 is
        port(
		i_CLK		: in std_logic; -- Clock input
		i_RST		: in std_logic; -- Reset register input
		i_WE		: in std_logic; -- Write enable for register
		i_D			: in std_logic_vector(7 downto 0); -- data in value
		o_D			: out std_logic_vector(7 downto 0) -- data out value
	);
    end component;

begin

    -- The shift clock register is necessary for clock edge detection.
    sft_clk_reg_p : process (i_CLK)
    begin
        if (rising_edge(i_CLK)) then
            if (i_RST = '1') then
                sft_clk_reg <= '0';
            else
                sft_clk_reg <= i_SHIFT_EN;
            end if;
        end if;
    end process;


    -- -------------------------------------------------------------------------
    --  SPI CLOCK EDGES FLAGS
    -- -------------------------------------------------------------------------

    -- Falling edge is detect when i_SHIFT_EN=0 and sft_clk_reg=1.
    sft_clk_fedge_en <= not i_SHIFT_EN and sft_clk_reg;
    -- Rising edge is detect when i_SHIFT_EN=1 and sft_clk_reg=0.
    sft_clk_redge_en <= i_SHIFT_EN and not sft_clk_reg;


    
    s_reg_2_in <= i_DATA(23 downto 16) when i_WE_P    
                    else s_reg_3_out;

    s_reg_1_in <= i_DATA(15 downto 8) when i_WE_P    
                    else s_reg_2_out;

    s_reg_0_in <= i_DATA(7 downto 0) when i_WE_P    
                    else s_reg_1_out;

    reg_3 : reg_8 port map (
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => sft_clk_redge_en,
        i_D   => i_DATA(31 downto 24),
        o_D   => s_reg_3_out
    );

    reg_2 : reg_8 port map (
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => sft_clk_redge_en,
        i_D   => s_reg_2_in,
        o_D   => s_reg_2_out
    );    


    reg_1 : reg_8 port map (
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => sft_clk_redge_en,
        i_D   => s_reg_1_in,
        o_D   => s_reg_1_out
    ); 

    reg_0 : reg_8 port map (
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE  => sft_clk_redge_en,
        i_D   => s_reg_0_in,
        o_D   => o_DATA
    ); 


end architecture_shift_reg_spi_32;
