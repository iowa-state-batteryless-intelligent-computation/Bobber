--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity sys_array_2_2 is
generic(N : integer := 32; D: integer := 8); -- input data is 8bits, output is 32 bits
port (
	i_weights_0: IN std_logic_vector(D-1 downto 0);
    i_weights_1: IN std_logic_vector(D-1 downto 0);
    i_input_0: IN std_logic_vector(D-1 downto 0);
    i_input_1: IN std_logic_vector(D-1 downto 0);
    i_ps_0: IN std_logic_vector(N-1 downto 0);
    i_ps_1: IN std_logic_vector(N-1 downto 0);
    i_load_weight: IN std_logic;
    i_load_input: IN std_logic;
    i_rst: IN std_logic;
    i_clk: IN std_logic;

    o_ps_0: OUT std_logic_vector(N-1 downto 0);
    o_ps_1: OUT std_logic_vector(N-1 downto 0)

);
end sys_array_2_2;
architecture architecture_sys_array_2_2 of sys_array_2_2 is
   -- signal, component etc. declarations
	signal s_weight_11: std_logic_vector(D-1 downto 0);
    signal s_weight_21: std_logic_vector(D-1 downto 0);
    signal s_input_11: std_logic_vector(D-1 downto 0);
    signal s_input_21: std_logic_vector(D-1 downto 0);
    signal s_ps_11: std_logic_vector(N-1 downto 0);
    signal s_ps_12: std_logic_vector(N-1 downto 0);

    component mac_N is 
        port (
            i_weights: IN std_logic_vector(D-1 downto 0);
            i_load_weight: IN std_logic;
            i_load_input: IN std_logic;
            i_rst: IN std_logic;
            i_clk: IN std_logic;
            i_input: IN std_logic_vector(D-1 downto 0);
            i_ps : IN std_logic_vector(N-1 downto 0);

            o_weight: OUT std_logic_vector(D-1 downto 0);
            o_ps: OUT std_logic_vector(N-1 downto 0);
            o_input: OUT std_logic_vector(D-1 downto 0)
        );
        end component;

begin

    mac_11: mac_N port map (
        i_weights => i_weights_0,
        i_input => i_input_0,
        i_load_weight => i_load_weight,
        i_load_input=> i_load_input,
        i_rst => i_rst,
        i_clk => i_clk,
        i_ps => i_ps_0,
        o_weight => s_weight_11,
        o_ps => s_ps_11,
        o_input => s_input_11
    );

    mac_12: mac_N port map (
        i_weights => s_weight_11,
        i_input => s_input_11,
        i_load_weight => i_load_weight,
        i_load_input=> i_load_input,
        i_rst => i_rst,
        i_clk => i_clk,
        i_ps => i_ps_1,
        o_weight => open,
        o_ps => s_ps_12,
        o_input => open
    );

    mac_21: mac_N port map (
        i_weights => i_weights_1,
        i_input => i_input_1,
        i_load_weight => i_load_weight,
        i_load_input=> i_load_input,
        i_rst => i_rst,
        i_clk => i_clk,
        i_ps => s_ps_11,
        o_weight => s_weight_21,
        o_ps => o_ps_0,
        o_input => s_input_21
    );

    mac_22: mac_N port map (
        i_weights => s_weight_21,
        i_input => s_input_21,
        i_load_weight => i_load_weight,
        i_load_input=> i_load_input,
        i_rst => i_rst,
        i_clk => i_clk,
        i_ps => s_ps_12,
        o_weight => open,
        o_ps => o_ps_1,
        o_input => open
    );


end architecture_sys_array_2_2;
