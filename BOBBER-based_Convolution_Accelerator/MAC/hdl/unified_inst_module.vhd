
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity unified_inst_module is
port (
    i_CLK: IN std_logic;
    i_RST: IN std_logic;
	i_INST_IN_SPI : IN  std_logic_vector(7 downto 0); -- 8bit wide transactions from SPI module
    i_SPI_OUT_VLD: IN std_logic; -- input from SPI module, '1' when a transaction is complete
    o_DONE: OUT std_logic;
    o_INST : OUT std_logic_vector(31 downto 0);  -- assembled and multiplied instructions
    o_mult_done: OUT std_logic;
    o_boot: OUT std_logic;
    o_cnt: OUT std_logic_vector(1 downto 0)
    
);
end unified_inst_module;

architecture architecture_unified_inst_module of unified_inst_module is
    -- Components
    component reg_8 is 
        port (
            i_CLK        : in std_logic;    -- Clock input
			i_RST        : in std_logic;    -- Reset input
			i_WE         : in std_logic;    -- Write enable input
			i_D			: in std_logic_vector(7 downto 0); -- data in value
            o_D			: out std_logic_vector(7 downto 0) -- data out value
        );
    end component;

    component reg_N is 
        port (
            i_CLK        : in std_logic;    -- Clock input
			i_RST        : in std_logic;    -- Reset input
			i_WE         : in std_logic;    -- Write enable input
			i_D			: in std_logic_vector(31 downto 0); -- data in value
            o_D			: out std_logic_vector(31 downto 0) -- data out value
        );
    end component;

    component instruction_mult is 
        port (
           i_clk: in std_logic; -- system clock
           i_rst: in std_logic; -- reset, async, active high
           i_inst : IN  std_logic_vector(31 downto 0); -- instruction input
           i_spi_done: in std_logic;
           o_inst : OUT std_logic_vector(31 downto 0);  -- output instruction
           o_done : OUT std_logic ; -- instruction multiply done signal
           o_mult_en: OUT std_logic --instruction multiply enable signal
        );
    end component;
    
    component inst_cont_load_fsm is 
        port (
           i_clk: in std_logic; -- system clock
           i_rst: in std_logic; -- reset, async, active high
           i_inst_cont_load : IN  std_logic_vector(31 downto 0); -- instruction input
           i_spi_done_cont_load: in std_logic;
           o_inst_cont_load : OUT std_logic_vector(31 downto 0);  -- output instruction
           o_done_cont_load : OUT std_logic ; -- instruction multiply done signal
           o_inst_cont_load_en: OUT std_logic -- instruction multiply done signal
        );
    end component;

   -- 32 bit 4t1 mux component --
    component mux4t1_32
        -- ports
        port(
            i_S0         : in std_logic;
            i_S1         : in std_logic;
            i_D0         : in std_logic_vector(32-1 downto 0);
            i_D1         : in std_logic_vector(32-1 downto 0);
            i_D2         : in std_logic_vector(32-1 downto 0);
            i_D3         : in std_logic_vector(32-1 downto 0); 
            o_O          : out std_logic_vector(32-1 downto 0)
        );
    end component;

   --  Signals
    signal s_reg_3_out: std_logic_vector (7 downto 0);
    signal s_reg_2_out: std_logic_vector (7 downto 0);
    signal s_reg_1_out: std_logic_vector (7 downto 0);
    signal s_reg_0_out: std_logic_vector (7 downto 0);

    signal s_spi_byte_cnt: unsigned (1 downto 0); -- count the number of transactions
    signal s_instr_cnt_max: std_logic; -- 1 when there are 4 received transcations
    signal s_instr_wr: std_logic;  -- 1 when ready for parallel read of the 4 registers
    signal s_cont_load_en: std_logic;
    signal s_mult_en: std_logic;
    signal s_inst_full: std_logic_vector (31 downto 0);
    signal s_inst_cont_load_out: std_logic_vector(31 downto 0);
    signal s_inst_inst_mult_out: std_logic_vector(31 downto 0);
    signal s_inst_out: std_logic_vector(31 downto 0);
    signal s_done_mult_out: std_logic;
    signal s_done_cont_load_out: std_logic;
    
    

begin

   -- architecture body

    reg_3: reg_8 port map (
        i_CLK => i_CLK,   -- Clock input
        i_RST => i_RST,   -- Reset input
        i_WE => i_SPI_OUT_VLD, -- Write enable input
        i_D	=> i_INST_IN_SPI, -- data in value
        o_D	=> s_reg_3_out -- data out value
    );

    reg_2: reg_8 port map (
        i_CLK => i_CLK,   -- Clock input
        i_RST => i_RST,   -- Reset input
        i_WE => i_SPI_OUT_VLD, -- Write enable input
        i_D	=> s_reg_3_out, -- data in value
        o_D	=> s_reg_2_out -- data out value
    );

    reg_1: reg_8 port map (
        i_CLK => i_CLK,   -- Clock input
        i_RST => i_RST,   -- Reset input
        i_WE => i_SPI_OUT_VLD, -- Write enable input
        i_D	=> s_reg_2_out, -- data in value
        o_D	=> s_reg_1_out -- data out value
    );

    reg_0: reg_8 port map (
        i_CLK => i_CLK,   -- Clock input
        i_RST => i_RST,   -- Reset input
        i_WE => i_SPI_OUT_VLD, -- Write enable input
        i_D	=> s_reg_1_out, -- data in value
        o_D	=> s_reg_0_out -- data out value
    );

    s_instr_cnt_max <= '1' when (s_spi_byte_cnt = "11") else '0';
    s_instr_wr <= i_SPI_OUT_VLD and s_instr_cnt_max;
    


    reg_instr: reg_N port map (
        i_CLK => i_CLK,
        i_RST => i_RST,
        i_WE => s_instr_wr,
        i_D =>  i_INST_IN_SPI & s_reg_3_out & s_reg_2_out & s_reg_1_out,
        o_D => s_inst_full 
        --o_D => o_INST
    );

    boot_det: process (i_RST, i_CLK)
    begin
        if(i_RST = '1') then
            o_boot <= '0';
        elsif (rising_edge(i_CLK)) then
            o_boot <= '1';
        end if;
    end process;

    byte_cnt: process (i_RST, i_CLK, i_SPI_OUT_VLD) 
    begin
        if (i_RST = '1') then
            s_spi_byte_cnt <= b"00";
        elsif (rising_edge(i_CLK) and i_SPI_OUT_VLD = '1') then
            if (s_instr_cnt_max = '1') then
                    s_spi_byte_cnt <= (others => '0');
                else
                    s_spi_byte_cnt <= s_spi_byte_cnt + 1;
            end if;
        end if;

    end process;


    inst_mult: instruction_mult port map (
        i_clk => i_CLK,
        i_rst => i_RST,
        i_inst => s_inst_full,
        i_spi_done => i_SPI_OUT_VLD,
        o_inst => s_inst_inst_mult_out,
        o_done => s_done_mult_out,
        o_mult_en => s_mult_en
        
    );

    inst_cont_load: inst_cont_load_fsm port map (
        i_clk => i_CLK,
        i_rst => i_RST,
        i_inst_cont_load => s_inst_full,
        i_spi_done_cont_load => i_SPI_OUT_VLD,
        o_inst_cont_load => s_inst_cont_load_out,
        o_done_cont_load => s_done_cont_load_out,
        o_inst_cont_load_en => s_cont_load_en
        
    );
    
    load_inst_mux: mux4t1_32 port map(
        i_S0 => s_cont_load_en,
        i_S1 => s_mult_en and not s_cont_load_en, 
		i_D0 => s_inst_full,
		i_D1 => s_inst_inst_mult_out,
        i_D2 => s_inst_cont_load_out,
        i_D3 => b"00000000000000000000000000000000",   
		o_O  => s_inst_out
    
    );
    
    o_mult_done <= s_done_mult_out;
    o_cnt <= "00" when s_spi_byte_cnt = "00"
            else "01" when s_spi_byte_cnt = "01"
            else "10" when s_spi_byte_cnt = "10"
            else "11" when s_spi_byte_cnt = "11";

    o_INST <= s_inst_out;
    o_DONE <= s_done_mult_out and s_done_cont_load_out;
  
end architecture_unified_inst_module;
