quietly set ACTELLIBNAME IGLOO
quietly set PROJECT_DIR "D:/Vishak/Git_files/Bobber/BOBBER-based_Convolution_Accelerator/MAC"

if {[file exists postsynth/_info]} {
   echo "INFO: Simulation library postsynth already exists"
} else {
   file delete -force postsynth 
   vlib postsynth
}
vmap postsynth postsynth
vmap igloo "C:/Microsemi/Libero_SoC_v11.9/Designer/lib/modelsim/precompiled/vhdl/igloo"
if {[file exists CORESPI_LIB/_info]} {
   echo "INFO: Simulation library CORESPI_LIB already exists"
} else {
   file delete -force CORESPI_LIB 
   vlib CORESPI_LIB
}
vmap CORESPI_LIB "CORESPI_LIB"

vcom -2008 -explicit  -work postsynth "${PROJECT_DIR}/synthesis/accelerator_root.vhd"
vcom -2008 -explicit  -work postsynth "${PROJECT_DIR}/stimulus/tb_aaccelerator_root.vhd"

vsim -L igloo -L postsynth -L CORESPI_LIB  -t 1ps postsynth.root_spi
add wave /root_spi/*
run 10ns
