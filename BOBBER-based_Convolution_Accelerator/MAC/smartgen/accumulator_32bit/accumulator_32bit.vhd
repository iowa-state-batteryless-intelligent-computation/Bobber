-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity accumulator_32bit is

    port( DataA  : in    std_logic_vector(31 downto 0);
          Enable : in    std_logic;
          Aclr   : in    std_logic;
          Clock  : in    std_logic;
          Sum    : out   std_logic_vector(31 downto 0)
        );

end accumulator_32bit;

architecture DEF_ARCH of accumulator_32bit is 

  component XOR3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MAJ3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MX2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          S : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component BUFF
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component DFN0C1
    port( D   : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          CLR : in    std_logic := 'U';
          Q   : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

    signal \Sum[0]\, \Sum[1]\, \Sum[2]\, \Sum[3]\, \Sum[4]\, 
        \Sum[5]\, \Sum[6]\, \Sum[7]\, \Sum[8]\, \Sum[9]\, 
        \Sum[10]\, \Sum[11]\, \Sum[12]\, \Sum[13]\, \Sum[14]\, 
        \Sum[15]\, \Sum[16]\, \Sum[17]\, \Sum[18]\, \Sum[19]\, 
        \Sum[20]\, \Sum[21]\, \Sum[22]\, \Sum[23]\, \Sum[24]\, 
        \Sum[25]\, \Sum[26]\, \Sum[27]\, \Sum[28]\, \Sum[29]\, 
        \Sum[30]\, \Sum[31]\, \SumInt[0]\, \SumInt[1]\, 
        \SumInt[2]\, \SumInt[3]\, \SumInt[4]\, \SumInt[5]\, 
        \SumInt[6]\, \SumInt[7]\, \SumInt[8]\, \SumInt[9]\, 
        \SumInt[10]\, \SumInt[11]\, \SumInt[12]\, \SumInt[13]\, 
        \SumInt[14]\, \SumInt[15]\, \SumInt[16]\, \SumInt[17]\, 
        \SumInt[18]\, \SumInt[19]\, \SumInt[20]\, \SumInt[21]\, 
        \SumInt[22]\, \SumInt[23]\, \SumInt[24]\, \SumInt[25]\, 
        \SumInt[26]\, \SumInt[27]\, \SumInt[28]\, \SumInt[29]\, 
        \SumInt[30]\, \SumInt[31]\, AND2_0_Y, MAJ3_29_Y, 
        MAJ3_25_Y, MAJ3_6_Y, MAJ3_13_Y, MAJ3_1_Y, MAJ3_10_Y, 
        MAJ3_3_Y, MAJ3_18_Y, MAJ3_26_Y, MAJ3_24_Y, MAJ3_28_Y, 
        MAJ3_0_Y, MAJ3_21_Y, MAJ3_4_Y, MAJ3_20_Y, MAJ3_17_Y, 
        MAJ3_27_Y, MAJ3_19_Y, MAJ3_2_Y, MAJ3_12_Y, MAJ3_15_Y, 
        MAJ3_16_Y, MAJ3_11_Y, MAJ3_23_Y, MAJ3_9_Y, MAJ3_5_Y, 
        MAJ3_14_Y, MAJ3_8_Y, MAJ3_22_Y, MAJ3_7_Y, BUFF_8_Y, 
        BUFF_9_Y, BUFF_5_Y, BUFF_3_Y, BUFF_1_Y, BUFF_6_Y, 
        BUFF_7_Y, BUFF_0_Y, BUFF_4_Y, BUFF_2_Y, MX2_8_Y, MX2_23_Y, 
        MX2_19_Y, MX2_27_Y, MX2_14_Y, MX2_16_Y, MX2_0_Y, MX2_13_Y, 
        MX2_3_Y, MX2_6_Y, MX2_26_Y, MX2_17_Y, MX2_1_Y, MX2_10_Y, 
        MX2_5_Y, MX2_12_Y, MX2_9_Y, MX2_24_Y, MX2_28_Y, MX2_18_Y, 
        MX2_30_Y, MX2_2_Y, MX2_25_Y, MX2_11_Y, MX2_29_Y, MX2_31_Y, 
        MX2_20_Y, MX2_7_Y, MX2_22_Y, MX2_21_Y, MX2_15_Y, MX2_4_Y
         : std_logic;

begin 

    Sum(0) <= \Sum[0]\;
    Sum(1) <= \Sum[1]\;
    Sum(2) <= \Sum[2]\;
    Sum(3) <= \Sum[3]\;
    Sum(4) <= \Sum[4]\;
    Sum(5) <= \Sum[5]\;
    Sum(6) <= \Sum[6]\;
    Sum(7) <= \Sum[7]\;
    Sum(8) <= \Sum[8]\;
    Sum(9) <= \Sum[9]\;
    Sum(10) <= \Sum[10]\;
    Sum(11) <= \Sum[11]\;
    Sum(12) <= \Sum[12]\;
    Sum(13) <= \Sum[13]\;
    Sum(14) <= \Sum[14]\;
    Sum(15) <= \Sum[15]\;
    Sum(16) <= \Sum[16]\;
    Sum(17) <= \Sum[17]\;
    Sum(18) <= \Sum[18]\;
    Sum(19) <= \Sum[19]\;
    Sum(20) <= \Sum[20]\;
    Sum(21) <= \Sum[21]\;
    Sum(22) <= \Sum[22]\;
    Sum(23) <= \Sum[23]\;
    Sum(24) <= \Sum[24]\;
    Sum(25) <= \Sum[25]\;
    Sum(26) <= \Sum[26]\;
    Sum(27) <= \Sum[27]\;
    Sum(28) <= \Sum[28]\;
    Sum(29) <= \Sum[29]\;
    Sum(30) <= \Sum[30]\;
    Sum(31) <= \Sum[31]\;

    \XOR3_SumInt[29]\ : XOR3
      port map(A => DataA(29), B => \Sum[29]\, C => MAJ3_8_Y, Y
         => \SumInt[29]\);
    
    MAJ3_16 : MAJ3
      port map(A => MAJ3_15_Y, B => DataA(22), C => \Sum[22]\, Y
         => MAJ3_16_Y);
    
    \XOR3_SumInt[10]\ : XOR3
      port map(A => DataA(10), B => \Sum[10]\, C => MAJ3_26_Y, Y
         => \SumInt[10]\);
    
    \XOR3_SumInt[6]\ : XOR3
      port map(A => DataA(6), B => \Sum[6]\, C => MAJ3_1_Y, Y => 
        \SumInt[6]\);
    
    \XOR3_SumInt[12]\ : XOR3
      port map(A => DataA(12), B => \Sum[12]\, C => MAJ3_28_Y, Y
         => \SumInt[12]\);
    
    MAJ3_19 : MAJ3
      port map(A => MAJ3_27_Y, B => DataA(18), C => \Sum[18]\, Y
         => MAJ3_19_Y);
    
    \XOR3_SumInt[5]\ : XOR3
      port map(A => DataA(5), B => \Sum[5]\, C => MAJ3_13_Y, Y
         => \SumInt[5]\);
    
    MX2_17 : MX2
      port map(A => \Sum[0]\, B => \SumInt[0]\, S => BUFF_8_Y, Y
         => MX2_17_Y);
    
    BUFF_8 : BUFF
      port map(A => Enable, Y => BUFF_8_Y);
    
    MAJ3_8 : MAJ3
      port map(A => MAJ3_14_Y, B => DataA(28), C => \Sum[28]\, Y
         => MAJ3_8_Y);
    
    \DFN0C1_Sum[6]\ : DFN0C1
      port map(D => MX2_13_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[6]\);
    
    \DFN0C1_Sum[5]\ : DFN0C1
      port map(D => MX2_22_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[5]\);
    
    MX2_21 : MX2
      port map(A => \Sum[2]\, B => \SumInt[2]\, S => BUFF_8_Y, Y
         => MX2_21_Y);
    
    \DFN0C1_Sum[17]\ : DFN0C1
      port map(D => MX2_16_Y, CLK => Clock, CLR => BUFF_4_Y, Q
         => \Sum[17]\);
    
    \XOR3_SumInt[26]\ : XOR3
      port map(A => DataA(26), B => \Sum[26]\, C => MAJ3_9_Y, Y
         => \SumInt[26]\);
    
    \DFN0C1_Sum[2]\ : DFN0C1
      port map(D => MX2_21_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[2]\);
    
    \XOR3_SumInt[19]\ : XOR3
      port map(A => DataA(19), B => \Sum[19]\, C => MAJ3_19_Y, Y
         => \SumInt[19]\);
    
    MAJ3_9 : MAJ3
      port map(A => MAJ3_23_Y, B => DataA(25), C => \Sum[25]\, Y
         => MAJ3_9_Y);
    
    MAJ3_23 : MAJ3
      port map(A => MAJ3_11_Y, B => DataA(24), C => \Sum[24]\, Y
         => MAJ3_23_Y);
    
    \DFN0C1_Sum[10]\ : DFN0C1
      port map(D => MX2_15_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[10]\);
    
    MX2_5 : MX2
      port map(A => \Sum[30]\, B => \SumInt[30]\, S => BUFF_6_Y, 
        Y => MX2_5_Y);
    
    \DFN0C1_Sum[25]\ : DFN0C1
      port map(D => MX2_14_Y, CLK => Clock, CLR => BUFF_2_Y, Q
         => \Sum[25]\);
    
    \DFN0C1_Sum[4]\ : DFN0C1
      port map(D => MX2_28_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[4]\);
    
    \DFN0C1_Sum[28]\ : DFN0C1
      port map(D => MX2_11_Y, CLK => Clock, CLR => BUFF_2_Y, Q
         => \Sum[28]\);
    
    MX2_25 : MX2
      port map(A => \Sum[1]\, B => \SumInt[1]\, S => BUFF_8_Y, Y
         => MX2_25_Y);
    
    MAJ3_13 : MAJ3
      port map(A => MAJ3_6_Y, B => DataA(4), C => \Sum[4]\, Y => 
        MAJ3_13_Y);
    
    \XOR3_SumInt[7]\ : XOR3
      port map(A => DataA(7), B => \Sum[7]\, C => MAJ3_10_Y, Y
         => \SumInt[7]\);
    
    MAJ3_22 : MAJ3
      port map(A => MAJ3_8_Y, B => DataA(29), C => \Sum[29]\, Y
         => MAJ3_22_Y);
    
    BUFF_2 : BUFF
      port map(A => Aclr, Y => BUFF_2_Y);
    
    \DFN0C1_Sum[29]\ : DFN0C1
      port map(D => MX2_0_Y, CLK => Clock, CLR => BUFF_2_Y, Q => 
        \Sum[29]\);
    
    MAJ3_12 : MAJ3
      port map(A => MAJ3_2_Y, B => DataA(20), C => \Sum[20]\, Y
         => MAJ3_12_Y);
    
    \XOR3_SumInt[16]\ : XOR3
      port map(A => DataA(16), B => \Sum[16]\, C => MAJ3_20_Y, Y
         => \SumInt[16]\);
    
    MAJ3_27 : MAJ3
      port map(A => MAJ3_17_Y, B => DataA(17), C => \Sum[17]\, Y
         => MAJ3_27_Y);
    
    MX2_20 : MX2
      port map(A => \Sum[27]\, B => \SumInt[27]\, S => BUFF_6_Y, 
        Y => MX2_20_Y);
    
    MX2_19 : MX2
      port map(A => \Sum[3]\, B => \SumInt[3]\, S => BUFF_8_Y, Y
         => MX2_19_Y);
    
    \DFN0C1_Sum[14]\ : DFN0C1
      port map(D => MX2_30_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[14]\);
    
    MAJ3_24 : MAJ3
      port map(A => MAJ3_26_Y, B => DataA(10), C => \Sum[10]\, Y
         => MAJ3_24_Y);
    
    MX2_31 : MX2
      port map(A => \Sum[8]\, B => \SumInt[8]\, S => BUFF_9_Y, Y
         => MX2_31_Y);
    
    \XOR3_SumInt[24]\ : XOR3
      port map(A => DataA(24), B => \Sum[24]\, C => MAJ3_11_Y, Y
         => \SumInt[24]\);
    
    MAJ3_17 : MAJ3
      port map(A => MAJ3_20_Y, B => DataA(16), C => \Sum[16]\, Y
         => MAJ3_17_Y);
    
    \XOR3_SumInt[4]\ : XOR3
      port map(A => DataA(4), B => \Sum[4]\, C => MAJ3_6_Y, Y => 
        \SumInt[4]\);
    
    MX2_14 : MX2
      port map(A => \Sum[25]\, B => \SumInt[25]\, S => BUFF_1_Y, 
        Y => MX2_14_Y);
    
    MAJ3_0 : MAJ3
      port map(A => MAJ3_28_Y, B => DataA(12), C => \Sum[12]\, Y
         => MAJ3_0_Y);
    
    MAJ3_14 : MAJ3
      port map(A => MAJ3_5_Y, B => DataA(27), C => \Sum[27]\, Y
         => MAJ3_14_Y);
    
    \XOR3_SumInt[25]\ : XOR3
      port map(A => DataA(25), B => \Sum[25]\, C => MAJ3_23_Y, Y
         => \SumInt[25]\);
    
    \XOR3_SumInt[31]\ : XOR3
      port map(A => DataA(31), B => \Sum[31]\, C => MAJ3_7_Y, Y
         => \SumInt[31]\);
    
    MX2_8 : MX2
      port map(A => \Sum[18]\, B => \SumInt[18]\, S => BUFF_3_Y, 
        Y => MX2_8_Y);
    
    MAJ3_4 : MAJ3
      port map(A => MAJ3_21_Y, B => DataA(14), C => \Sum[14]\, Y
         => MAJ3_4_Y);
    
    \XOR3_SumInt[14]\ : XOR3
      port map(A => DataA(14), B => \Sum[14]\, C => MAJ3_21_Y, Y
         => \SumInt[14]\);
    
    \DFN0C1_Sum[23]\ : DFN0C1
      port map(D => MX2_29_Y, CLK => Clock, CLR => BUFF_4_Y, Q
         => \Sum[23]\);
    
    \XOR3_SumInt[3]\ : XOR3
      port map(A => DataA(3), B => \Sum[3]\, C => MAJ3_25_Y, Y
         => \SumInt[3]\);
    
    \XOR3_SumInt[15]\ : XOR3
      port map(A => DataA(15), B => \Sum[15]\, C => MAJ3_4_Y, Y
         => \SumInt[15]\);
    
    BUFF_5 : BUFF
      port map(A => Enable, Y => BUFF_5_Y);
    
    MX2_30 : MX2
      port map(A => \Sum[14]\, B => \SumInt[14]\, S => BUFF_5_Y, 
        Y => MX2_30_Y);
    
    MX2_22 : MX2
      port map(A => \Sum[5]\, B => \SumInt[5]\, S => BUFF_8_Y, Y
         => MX2_22_Y);
    
    \XOR3_SumInt[27]\ : XOR3
      port map(A => DataA(27), B => \Sum[27]\, C => MAJ3_5_Y, Y
         => \SumInt[27]\);
    
    MAJ3_21 : MAJ3
      port map(A => MAJ3_0_Y, B => DataA(13), C => \Sum[13]\, Y
         => MAJ3_21_Y);
    
    MX2_0 : MX2
      port map(A => \Sum[29]\, B => \SumInt[29]\, S => BUFF_6_Y, 
        Y => MX2_0_Y);
    
    MAJ3_11 : MAJ3
      port map(A => MAJ3_16_Y, B => DataA(23), C => \Sum[23]\, Y
         => MAJ3_11_Y);
    
    MX2_28 : MX2
      port map(A => \Sum[4]\, B => \SumInt[4]\, S => BUFF_8_Y, Y
         => MX2_28_Y);
    
    \DFN0C1_Sum[1]\ : DFN0C1
      port map(D => MX2_25_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[1]\);
    
    \DFN0C1_Sum[7]\ : DFN0C1
      port map(D => MX2_12_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[7]\);
    
    \DFN0C1_Sum[16]\ : DFN0C1
      port map(D => MX2_1_Y, CLK => Clock, CLR => BUFF_4_Y, Q => 
        \Sum[16]\);
    
    \XOR3_SumInt[1]\ : XOR3
      port map(A => DataA(1), B => \Sum[1]\, C => AND2_0_Y, Y => 
        \SumInt[1]\);
    
    \DFN0C1_Sum[9]\ : DFN0C1
      port map(D => MX2_27_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[9]\);
    
    \DFN0C1_Sum[11]\ : DFN0C1
      port map(D => MX2_7_Y, CLK => Clock, CLR => BUFF_0_Y, Q => 
        \Sum[11]\);
    
    MX2_26 : MX2
      port map(A => \Sum[31]\, B => \SumInt[31]\, S => BUFF_6_Y, 
        Y => MX2_26_Y);
    
    \XOR3_SumInt[17]\ : XOR3
      port map(A => DataA(17), B => \Sum[17]\, C => MAJ3_17_Y, Y
         => \SumInt[17]\);
    
    \DFN0C1_Sum[0]\ : DFN0C1
      port map(D => MX2_17_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[0]\);
    
    \DFN0C1_Sum[12]\ : DFN0C1
      port map(D => MX2_2_Y, CLK => Clock, CLR => BUFF_0_Y, Q => 
        \Sum[12]\);
    
    MX2_2 : MX2
      port map(A => \Sum[12]\, B => \SumInt[12]\, S => BUFF_5_Y, 
        Y => MX2_2_Y);
    
    MX2_23 : MX2
      port map(A => \Sum[13]\, B => \SumInt[13]\, S => BUFF_5_Y, 
        Y => MX2_23_Y);
    
    MAJ3_3 : MAJ3
      port map(A => MAJ3_10_Y, B => DataA(7), C => \Sum[7]\, Y
         => MAJ3_3_Y);
    
    BUFF_0 : BUFF
      port map(A => Aclr, Y => BUFF_0_Y);
    
    MX2_27 : MX2
      port map(A => \Sum[9]\, B => \SumInt[9]\, S => BUFF_9_Y, Y
         => MX2_27_Y);
    
    MX2_11 : MX2
      port map(A => \Sum[28]\, B => \SumInt[28]\, S => BUFF_6_Y, 
        Y => MX2_11_Y);
    
    \DFN0C1_Sum[27]\ : DFN0C1
      port map(D => MX2_20_Y, CLK => Clock, CLR => BUFF_2_Y, Q
         => \Sum[27]\);
    
    \DFN0C1_Sum[8]\ : DFN0C1
      port map(D => MX2_31_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[8]\);
    
    MX2_6 : MX2
      port map(A => \Sum[22]\, B => \SumInt[22]\, S => BUFF_1_Y, 
        Y => MX2_6_Y);
    
    \XOR3_SumInt[30]\ : XOR3
      port map(A => DataA(30), B => \Sum[30]\, C => MAJ3_22_Y, Y
         => \SumInt[30]\);
    
    \XOR3_SumInt[8]\ : XOR3
      port map(A => DataA(8), B => \Sum[8]\, C => MAJ3_3_Y, Y => 
        \SumInt[8]\);
    
    \XOR3_SumInt[21]\ : XOR3
      port map(A => DataA(21), B => \Sum[21]\, C => MAJ3_12_Y, Y
         => \SumInt[21]\);
    
    MX2_7 : MX2
      port map(A => \Sum[11]\, B => \SumInt[11]\, S => BUFF_9_Y, 
        Y => MX2_7_Y);
    
    BUFF_9 : BUFF
      port map(A => Enable, Y => BUFF_9_Y);
    
    \DFN0C1_Sum[20]\ : DFN0C1
      port map(D => MX2_18_Y, CLK => Clock, CLR => BUFF_4_Y, Q
         => \Sum[20]\);
    
    MX2_15 : MX2
      port map(A => \Sum[10]\, B => \SumInt[10]\, S => BUFF_9_Y, 
        Y => MX2_15_Y);
    
    \DFN0C1_Sum[15]\ : DFN0C1
      port map(D => MX2_10_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[15]\);
    
    MAJ3_6 : MAJ3
      port map(A => MAJ3_25_Y, B => DataA(3), C => \Sum[3]\, Y
         => MAJ3_6_Y);
    
    \DFN0C1_Sum[18]\ : DFN0C1
      port map(D => MX2_8_Y, CLK => Clock, CLR => BUFF_4_Y, Q => 
        \Sum[18]\);
    
    \DFN0C1_Sum[30]\ : DFN0C1
      port map(D => MX2_5_Y, CLK => Clock, CLR => BUFF_2_Y, Q => 
        \Sum[30]\);
    
    MX2_3 : MX2
      port map(A => \Sum[24]\, B => \SumInt[24]\, S => BUFF_1_Y, 
        Y => MX2_3_Y);
    
    \DFN0C1_Sum[19]\ : DFN0C1
      port map(D => MX2_24_Y, CLK => Clock, CLR => BUFF_4_Y, Q
         => \Sum[19]\);
    
    BUFF_6 : BUFF
      port map(A => Enable, Y => BUFF_6_Y);
    
    BUFF_7 : BUFF
      port map(A => Aclr, Y => BUFF_7_Y);
    
    MX2_9 : MX2
      port map(A => \Sum[21]\, B => \SumInt[21]\, S => BUFF_3_Y, 
        Y => MX2_9_Y);
    
    \XOR3_SumInt[9]\ : XOR3
      port map(A => DataA(9), B => \Sum[9]\, C => MAJ3_18_Y, Y
         => \SumInt[9]\);
    
    MX2_10 : MX2
      port map(A => \Sum[15]\, B => \SumInt[15]\, S => BUFF_5_Y, 
        Y => MX2_10_Y);
    
    MAJ3_25 : MAJ3
      port map(A => MAJ3_29_Y, B => DataA(2), C => \Sum[2]\, Y
         => MAJ3_25_Y);
    
    \XOR3_SumInt[23]\ : XOR3
      port map(A => DataA(23), B => \Sum[23]\, C => MAJ3_16_Y, Y
         => \SumInt[23]\);
    
    \XOR3_SumInt[11]\ : XOR3
      port map(A => DataA(11), B => \Sum[11]\, C => MAJ3_24_Y, Y
         => \SumInt[11]\);
    
    MX2_29 : MX2
      port map(A => \Sum[23]\, B => \SumInt[23]\, S => BUFF_1_Y, 
        Y => MX2_29_Y);
    
    \XOR3_SumInt[2]\ : XOR3
      port map(A => DataA(2), B => \Sum[2]\, C => MAJ3_29_Y, Y
         => \SumInt[2]\);
    
    MAJ3_15 : MAJ3
      port map(A => MAJ3_12_Y, B => DataA(21), C => \Sum[21]\, Y
         => MAJ3_15_Y);
    
    \DFN0C1_Sum[24]\ : DFN0C1
      port map(D => MX2_3_Y, CLK => Clock, CLR => BUFF_2_Y, Q => 
        \Sum[24]\);
    
    AND2_0 : AND2
      port map(A => DataA(0), B => \Sum[0]\, Y => AND2_0_Y);
    
    \XOR3_SumInt[28]\ : XOR3
      port map(A => DataA(28), B => \Sum[28]\, C => MAJ3_14_Y, Y
         => \SumInt[28]\);
    
    BUFF_3 : BUFF
      port map(A => Enable, Y => BUFF_3_Y);
    
    MX2_24 : MX2
      port map(A => \Sum[19]\, B => \SumInt[19]\, S => BUFF_3_Y, 
        Y => MX2_24_Y);
    
    MAJ3_5 : MAJ3
      port map(A => MAJ3_9_Y, B => DataA(26), C => \Sum[26]\, Y
         => MAJ3_5_Y);
    
    \XOR3_SumInt[13]\ : XOR3
      port map(A => DataA(13), B => \Sum[13]\, C => MAJ3_0_Y, Y
         => \SumInt[13]\);
    
    MAJ3_28 : MAJ3
      port map(A => MAJ3_24_Y, B => DataA(11), C => \Sum[11]\, Y
         => MAJ3_28_Y);
    
    MAJ3_18 : MAJ3
      port map(A => MAJ3_3_Y, B => DataA(8), C => \Sum[8]\, Y => 
        MAJ3_18_Y);
    
    BUFF_4 : BUFF
      port map(A => Aclr, Y => BUFF_4_Y);
    
    MAJ3_7 : MAJ3
      port map(A => MAJ3_22_Y, B => DataA(30), C => \Sum[30]\, Y
         => MAJ3_7_Y);
    
    \XOR3_SumInt[18]\ : XOR3
      port map(A => DataA(18), B => \Sum[18]\, C => MAJ3_27_Y, Y
         => \SumInt[18]\);
    
    MX2_4 : MX2
      port map(A => \Sum[26]\, B => \SumInt[26]\, S => BUFF_1_Y, 
        Y => MX2_4_Y);
    
    MAJ3_20 : MAJ3
      port map(A => MAJ3_4_Y, B => DataA(15), C => \Sum[15]\, Y
         => MAJ3_20_Y);
    
    \DFN0C1_Sum[13]\ : DFN0C1
      port map(D => MX2_23_Y, CLK => Clock, CLR => BUFF_0_Y, Q
         => \Sum[13]\);
    
    MX2_12 : MX2
      port map(A => \Sum[7]\, B => \SumInt[7]\, S => BUFF_9_Y, Y
         => MX2_12_Y);
    
    MAJ3_10 : MAJ3
      port map(A => MAJ3_1_Y, B => DataA(6), C => \Sum[6]\, Y => 
        MAJ3_10_Y);
    
    MX2_18 : MX2
      port map(A => \Sum[20]\, B => \SumInt[20]\, S => BUFF_3_Y, 
        Y => MX2_18_Y);
    
    \XOR3_SumInt[20]\ : XOR3
      port map(A => DataA(20), B => \Sum[20]\, C => MAJ3_2_Y, Y
         => \SumInt[20]\);
    
    MAJ3_2 : MAJ3
      port map(A => MAJ3_19_Y, B => DataA(19), C => \Sum[19]\, Y
         => MAJ3_2_Y);
    
    \XOR3_SumInt[22]\ : XOR3
      port map(A => DataA(22), B => \Sum[22]\, C => MAJ3_15_Y, Y
         => \SumInt[22]\);
    
    \XOR2_SumInt[0]\ : XOR2
      port map(A => DataA(0), B => \Sum[0]\, Y => \SumInt[0]\);
    
    \DFN0C1_Sum[26]\ : DFN0C1
      port map(D => MX2_4_Y, CLK => Clock, CLR => BUFF_2_Y, Q => 
        \Sum[26]\);
    
    MX2_16 : MX2
      port map(A => \Sum[17]\, B => \SumInt[17]\, S => BUFF_3_Y, 
        Y => MX2_16_Y);
    
    MAJ3_1 : MAJ3
      port map(A => MAJ3_13_Y, B => DataA(5), C => \Sum[5]\, Y
         => MAJ3_1_Y);
    
    \DFN0C1_Sum[21]\ : DFN0C1
      port map(D => MX2_9_Y, CLK => Clock, CLR => BUFF_4_Y, Q => 
        \Sum[21]\);
    
    \DFN0C1_Sum[31]\ : DFN0C1
      port map(D => MX2_26_Y, CLK => Clock, CLR => BUFF_2_Y, Q
         => \Sum[31]\);
    
    BUFF_1 : BUFF
      port map(A => Enable, Y => BUFF_1_Y);
    
    MAJ3_26 : MAJ3
      port map(A => MAJ3_18_Y, B => DataA(9), C => \Sum[9]\, Y
         => MAJ3_26_Y);
    
    MX2_1 : MX2
      port map(A => \Sum[16]\, B => \SumInt[16]\, S => BUFF_5_Y, 
        Y => MX2_1_Y);
    
    \DFN0C1_Sum[22]\ : DFN0C1
      port map(D => MX2_6_Y, CLK => Clock, CLR => BUFF_4_Y, Q => 
        \Sum[22]\);
    
    MX2_13 : MX2
      port map(A => \Sum[6]\, B => \SumInt[6]\, S => BUFF_9_Y, Y
         => MX2_13_Y);
    
    \DFN0C1_Sum[3]\ : DFN0C1
      port map(D => MX2_19_Y, CLK => Clock, CLR => BUFF_7_Y, Q
         => \Sum[3]\);
    
    MAJ3_29 : MAJ3
      port map(A => AND2_0_Y, B => DataA(1), C => \Sum[1]\, Y => 
        MAJ3_29_Y);
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_ADD_SUB
-- LPM_HINT:RIPACC
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/Vishak/MIcrosemi/Projects/MAC_32bit-Bias-ReLu-Pooling_bulk2by2_fix/MAC/smartgen\accumulator_32bit
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- CI_POLARITY:2
-- CO_POLARITY:2
-- WIDTH:32
-- EN_POLARITY:1
-- CLR_POLARITY:1
-- CLK_EDGE:FALL
-- FF_TYPE:REGULAR
-- CLR_FANIN:AUTO
-- CLR_VAL:8
-- EN_FANIN:AUTO
-- EN_VAL:6
-- CLK_FANIN:MANUAL
-- CLK_VAL:1
-- DEBUG:0

-- _End_Comments_

