-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity adder_8_libero is

    port( DataA : in    std_logic_vector(7 downto 0);
          DataB : in    std_logic_vector(7 downto 0);
          Sum   : out   std_logic_vector(7 downto 0)
        );

end adder_8_libero;

architecture DEF_ARCH of adder_8_libero is 

  component XOR3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MAJ3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

    signal \Carry[0]\, \Carry[1]\, \Carry[2]\, \Carry[3]\, 
        \Carry[4]\, \Carry[5]\, \Carry[6]\ : std_logic;

begin 


    \XOR3_Sum[6]\ : XOR3
      port map(A => DataA(6), B => DataB(6), C => \Carry[5]\, Y
         => Sum(6));
    
    \MAJ3_Carry[4]\ : MAJ3
      port map(A => \Carry[3]\, B => DataA(4), C => DataB(4), Y
         => \Carry[4]\);
    
    \XOR3_Sum[7]\ : XOR3
      port map(A => DataA(7), B => DataB(7), C => \Carry[6]\, Y
         => Sum(7));
    
    \AND2_Carry[0]\ : AND2
      port map(A => DataA(0), B => DataB(0), Y => \Carry[0]\);
    
    \XOR3_Sum[1]\ : XOR3
      port map(A => DataA(1), B => DataB(1), C => \Carry[0]\, Y
         => Sum(1));
    
    \XOR3_Sum[3]\ : XOR3
      port map(A => DataA(3), B => DataB(3), C => \Carry[2]\, Y
         => Sum(3));
    
    \XOR2_Sum[0]\ : XOR2
      port map(A => DataA(0), B => DataB(0), Y => Sum(0));
    
    \MAJ3_Carry[1]\ : MAJ3
      port map(A => \Carry[0]\, B => DataA(1), C => DataB(1), Y
         => \Carry[1]\);
    
    \XOR3_Sum[2]\ : XOR3
      port map(A => DataA(2), B => DataB(2), C => \Carry[1]\, Y
         => Sum(2));
    
    \XOR3_Sum[4]\ : XOR3
      port map(A => DataA(4), B => DataB(4), C => \Carry[3]\, Y
         => Sum(4));
    
    \MAJ3_Carry[2]\ : MAJ3
      port map(A => \Carry[1]\, B => DataA(2), C => DataB(2), Y
         => \Carry[2]\);
    
    \MAJ3_Carry[3]\ : MAJ3
      port map(A => \Carry[2]\, B => DataA(3), C => DataB(3), Y
         => \Carry[3]\);
    
    \MAJ3_Carry[6]\ : MAJ3
      port map(A => \Carry[5]\, B => DataA(6), C => DataB(6), Y
         => \Carry[6]\);
    
    \XOR3_Sum[5]\ : XOR3
      port map(A => DataA(5), B => DataB(5), C => \Carry[4]\, Y
         => Sum(5));
    
    \MAJ3_Carry[5]\ : MAJ3
      port map(A => \Carry[4]\, B => DataA(5), C => DataB(5), Y
         => \Carry[5]\);
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_ADD_SUB
-- LPM_HINT:RIPADD
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/git/iFPGA/LiberoProjects/MAC_32bit-Bias-ReLu-Pooling/MAC/smartgen\adder_8_libero
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- CI_POLARITY:2
-- CO_POLARITY:2
-- WIDTH:8
-- DEBUG:0

-- _End_Comments_

