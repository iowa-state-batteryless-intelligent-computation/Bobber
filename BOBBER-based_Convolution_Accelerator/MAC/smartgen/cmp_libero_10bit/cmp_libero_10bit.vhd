-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity cmp_libero_10bit is

    port( DataA : in    std_logic_vector(9 downto 0);
          DataB : in    std_logic_vector(9 downto 0);
          AEB   : out   std_logic
        );

end cmp_libero_10bit;

architecture DEF_ARCH of cmp_libero_10bit is 

  component AND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XNOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

    signal XNOR2_7_Y, XNOR2_4_Y, XNOR2_0_Y, XNOR2_9_Y, XNOR2_1_Y, 
        XNOR2_3_Y, XNOR2_6_Y, XNOR2_2_Y, XNOR2_8_Y, XNOR2_5_Y, 
        AND3_3_Y, AND3_0_Y, AND3_1_Y, AND3_2_Y : std_logic;

begin 


    AND3_2 : AND3
      port map(A => XNOR2_9_Y, B => XNOR2_1_Y, C => XNOR2_3_Y, Y
         => AND3_2_Y);
    
    XNOR2_3 : XNOR2
      port map(A => DataA(5), B => DataB(5), Y => XNOR2_3_Y);
    
    XNOR2_5 : XNOR2
      port map(A => DataA(9), B => DataB(9), Y => XNOR2_5_Y);
    
    AND3_0 : AND3
      port map(A => AND3_1_Y, B => AND3_3_Y, C => AND3_2_Y, Y => 
        AND3_0_Y);
    
    AND3_1 : AND3
      port map(A => XNOR2_6_Y, B => XNOR2_2_Y, C => XNOR2_8_Y, Y
         => AND3_1_Y);
    
    XNOR2_8 : XNOR2
      port map(A => DataA(8), B => DataB(8), Y => XNOR2_8_Y);
    
    XNOR2_4 : XNOR2
      port map(A => DataA(1), B => DataB(1), Y => XNOR2_4_Y);
    
    XNOR2_9 : XNOR2
      port map(A => DataA(3), B => DataB(3), Y => XNOR2_9_Y);
    
    XNOR2_0 : XNOR2
      port map(A => DataA(2), B => DataB(2), Y => XNOR2_0_Y);
    
    XNOR2_2 : XNOR2
      port map(A => DataA(7), B => DataB(7), Y => XNOR2_2_Y);
    
    XNOR2_7 : XNOR2
      port map(A => DataA(0), B => DataB(0), Y => XNOR2_7_Y);
    
    XNOR2_6 : XNOR2
      port map(A => DataA(6), B => DataB(6), Y => XNOR2_6_Y);
    
    AND2_AEB : AND2
      port map(A => AND3_0_Y, B => XNOR2_5_Y, Y => AEB);
    
    XNOR2_1 : XNOR2
      port map(A => DataA(4), B => DataB(4), Y => XNOR2_1_Y);
    
    AND3_3 : AND3
      port map(A => XNOR2_7_Y, B => XNOR2_4_Y, C => XNOR2_0_Y, Y
         => AND3_3_Y);
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_COMPARE
-- LPM_HINT:EQCOMP
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/Vishak/MIcrosemi/Projects/MAC_32bit-Bias-ReLu-Pooling_cont_load_rev1.0/MAC/smartgen\cmp_libero_10bit
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTH:10
-- REPRESENTATION:UNSIGNED
-- GEQRHS_POLARITY:1
-- AEB_POLARITY:1

-- _End_Comments_

