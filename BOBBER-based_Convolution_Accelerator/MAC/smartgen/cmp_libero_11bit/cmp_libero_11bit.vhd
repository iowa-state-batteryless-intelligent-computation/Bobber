-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity cmp_libero_1t_11bit is

    port( DataA : in    std_logic_vector(9 downto 0);
          AEB   : out   std_logic
        );

end cmp_libero_1t_11bit;

architecture DEF_ARCH of cmp_libero_1t_11bit is 

  component AND3C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal \Temp[0]\, \Temp[1]\, \Temp[2]\, \Temp[3]\, AND2_0_Y, 
        \Vcc\ : std_logic;
    signal VCC_power_net1 : std_logic;

begin 

    \Vcc\ <= VCC_power_net1;

    \AND3C_Temp[0]\ : AND3C
      port map(A => DataA(0), B => DataA(1), C => DataA(2), Y => 
        \Temp[0]\);
    
    AND2_0 : AND2
      port map(A => \Temp[0]\, B => \Temp[1]\, Y => AND2_0_Y);
    
    \AND3C_Temp[2]\ : AND3C
      port map(A => DataA(6), B => DataA(7), C => DataA(8), Y => 
        \Temp[2]\);
    
    AND3_AEB : AND3
      port map(A => AND2_0_Y, B => \Temp[2]\, C => \Temp[3]\, Y
         => AEB);
    
    \AND3C_Temp[1]\ : AND3C
      port map(A => DataA(3), B => DataA(4), C => DataA(5), Y => 
        \Temp[1]\);
    
    \AND2A_Temp[3]\ : AND2A
      port map(A => DataA(9), B => \Vcc\, Y => \Temp[3]\);
    
    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_COMPARE
-- LPM_HINT:PndgenCdec
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/Vishak/MIcrosemi/Projects/MAC_32bit-Bias-ReLu-Pooling_cont_load_rev1.0/MAC/smartgen\cmp_libero_11bit
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTH:10
-- B_VALUE:000
-- RADIX:HEX
-- AEB_POLARITY:1

-- _End_Comments_

