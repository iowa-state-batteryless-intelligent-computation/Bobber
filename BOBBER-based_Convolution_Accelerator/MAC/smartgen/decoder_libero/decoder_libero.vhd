-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity decoder_libero is

    port( Data0 : in    std_logic;
          Data1 : in    std_logic;
          Eq    : out   std_logic_vector(3 downto 0)
        );

end decoder_libero;

architecture DEF_ARCH of decoder_libero is 

  component AND2A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;


begin 


    \AND2A_Eq[2]\ : AND2A
      port map(A => Data0, B => Data1, Y => Eq(2));
    
    \AND2_Eq[3]\ : AND2
      port map(A => Data1, B => Data0, Y => Eq(3));
    
    \AND2A_Eq[1]\ : AND2A
      port map(A => Data1, B => Data0, Y => Eq(1));
    
    \NOR2_Eq[0]\ : NOR2
      port map(A => Data1, B => Data0, Y => Eq(0));
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_DECODE
-- LPM_HINT:PndgenDecode
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/git/iFPGA/LiberoProjects/MAC/MAC/smartgen\decoder_libero
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- DECODES:4
-- EN_POLARITY:2
-- EQ_POLARITY:1

-- _End_Comments_

