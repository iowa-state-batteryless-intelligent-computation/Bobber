-- Version: v11.9 11.9.0.4

library ieee;
use ieee.std_logic_1164.all;
library igloo;
use igloo.all;

entity mult_libero is

    port( DataA : in    std_logic_vector(11 downto 0);
          DataB : in    std_logic_vector(11 downto 0);
          Mult  : out   std_logic_vector(23 downto 0)
        );

end mult_libero;

architecture DEF_ARCH of mult_libero is 

  component BUFF
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XOR3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AO1
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MAJ3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MX2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          S : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component XNOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component OR3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AOI1
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal \S[0]\, \S[1]\, \S[2]\, \S[3]\, \S[4]\, \S[5]\, \E[0]\, 
        \E[1]\, \E[2]\, \E[3]\, \E[4]\, \E[5]\, EBAR, \PP0[1]\, 
        \PP0[2]\, \PP0[3]\, \PP0[4]\, \PP0[5]\, \PP0[6]\, 
        \PP0[7]\, \PP0[8]\, \PP0[9]\, \PP0[10]\, \PP0[11]\, 
        \PP0[12]\, \PP1[0]\, \PP1[1]\, \PP1[2]\, \PP1[3]\, 
        \PP1[4]\, \PP1[5]\, \PP1[6]\, \PP1[7]\, \PP1[8]\, 
        \PP1[9]\, \PP1[10]\, \PP1[11]\, \PP1[12]\, \PP2[0]\, 
        \PP2[1]\, \PP2[2]\, \PP2[3]\, \PP2[4]\, \PP2[5]\, 
        \PP2[6]\, \PP2[7]\, \PP2[8]\, \PP2[9]\, \PP2[10]\, 
        \PP2[11]\, \PP2[12]\, \PP3[0]\, \PP3[1]\, \PP3[2]\, 
        \PP3[3]\, \PP3[4]\, \PP3[5]\, \PP3[6]\, \PP3[7]\, 
        \PP3[8]\, \PP3[9]\, \PP3[10]\, \PP3[11]\, \PP3[12]\, 
        \PP4[0]\, \PP4[1]\, \PP4[2]\, \PP4[3]\, \PP4[4]\, 
        \PP4[5]\, \PP4[6]\, \PP4[7]\, \PP4[8]\, \PP4[9]\, 
        \PP4[10]\, \PP4[11]\, \PP4[12]\, \PP5[0]\, \PP5[1]\, 
        \PP5[2]\, \PP5[3]\, \PP5[4]\, \PP5[5]\, \PP5[6]\, 
        \PP5[7]\, \PP5[8]\, \PP5[9]\, \PP5[10]\, \PP5[11]\, 
        \PP5[12]\, \SumA[3]\, \SumA[4]\, \SumA[5]\, \SumA[6]\, 
        \SumA[7]\, \SumA[8]\, \SumA[9]\, \SumA[10]\, \SumA[11]\, 
        \SumA[12]\, \SumA[13]\, \SumA[14]\, \SumA[15]\, 
        \SumA[16]\, \SumA[17]\, \SumA[18]\, \SumA[19]\, 
        \SumA[20]\, \SumA[21]\, \SumB[2]\, \SumB[3]\, \SumB[4]\, 
        \SumB[5]\, \SumB[6]\, \SumB[7]\, \SumB[8]\, \SumB[9]\, 
        \SumB[10]\, \SumB[11]\, \SumB[12]\, \SumB[13]\, 
        \SumB[14]\, \SumB[15]\, \SumB[16]\, \SumB[17]\, 
        \SumB[18]\, \SumB[19]\, \SumB[20]\, \SumB[21]\, 
        \SumB[22]\, XOR2_13_Y, AND2_137_Y, XOR3_14_Y, MAJ3_10_Y, 
        XOR3_6_Y, MAJ3_25_Y, XOR2_28_Y, AND2_141_Y, XOR3_4_Y, 
        MAJ3_23_Y, XOR2_61_Y, AND2_54_Y, XOR3_21_Y, MAJ3_21_Y, 
        XOR2_65_Y, AND2_26_Y, XOR3_23_Y, MAJ3_30_Y, XOR2_45_Y, 
        AND2_125_Y, XOR3_17_Y, MAJ3_26_Y, XOR2_66_Y, AND2_16_Y, 
        XOR3_10_Y, MAJ3_24_Y, XOR2_1_Y, AND2_67_Y, XOR3_22_Y, 
        MAJ3_16_Y, XOR3_18_Y, MAJ3_9_Y, XOR3_27_Y, MAJ3_18_Y, 
        XOR3_2_Y, MAJ3_28_Y, XOR3_30_Y, MAJ3_3_Y, XOR3_25_Y, 
        MAJ3_7_Y, XOR3_1_Y, MAJ3_8_Y, XOR3_5_Y, MAJ3_12_Y, 
        XOR3_29_Y, MAJ3_5_Y, XOR3_28_Y, MAJ3_17_Y, XOR2_60_Y, 
        AND2_145_Y, XOR2_74_Y, AND2_77_Y, XOR3_11_Y, MAJ3_2_Y, 
        XOR3_20_Y, MAJ3_11_Y, XOR3_15_Y, MAJ3_14_Y, XOR3_9_Y, 
        MAJ3_19_Y, XOR3_7_Y, MAJ3_22_Y, XOR3_12_Y, MAJ3_29_Y, 
        XOR3_3_Y, MAJ3_20_Y, XOR3_0_Y, MAJ3_1_Y, XOR3_13_Y, 
        MAJ3_13_Y, XOR3_8_Y, MAJ3_6_Y, XOR3_19_Y, MAJ3_15_Y, 
        XOR3_26_Y, MAJ3_27_Y, XOR3_24_Y, MAJ3_0_Y, XOR3_16_Y, 
        MAJ3_4_Y, XOR2_21_Y, AND2_152_Y, BUFF_28_Y, BUFF_9_Y, 
        BUFF_19_Y, BUFF_32_Y, BUFF_26_Y, BUFF_24_Y, BUFF_27_Y, 
        BUFF_5_Y, BUFF_37_Y, BUFF_1_Y, BUFF_45_Y, BUFF_33_Y, 
        BUFF_8_Y, BUFF_14_Y, BUFF_7_Y, BUFF_44_Y, BUFF_15_Y, 
        BUFF_18_Y, BUFF_11_Y, BUFF_36_Y, BUFF_42_Y, BUFF_10_Y, 
        BUFF_41_Y, BUFF_16_Y, BUFF_3_Y, BUFF_46_Y, BUFF_39_Y, 
        BUFF_20_Y, XOR2_59_Y, XOR2_47_Y, AO1_36_Y, XOR2_36_Y, 
        NOR2_11_Y, AND2_100_Y, MX2_35_Y, AND2_87_Y, MX2_56_Y, 
        AND2_86_Y, MX2_26_Y, MX2_57_Y, XNOR2_1_Y, XOR2_34_Y, 
        NOR2_0_Y, AND2_149_Y, MX2_40_Y, AND2_97_Y, MX2_52_Y, 
        AND2_88_Y, MX2_24_Y, AND2_52_Y, MX2_11_Y, XNOR2_2_Y, 
        XOR2_43_Y, NOR2_4_Y, AND2_71_Y, MX2_15_Y, AND2_138_Y, 
        MX2_68_Y, AND2_96_Y, AND2_74_Y, MX2_1_Y, AND2_132_Y, 
        MX2_63_Y, XNOR2_3_Y, OR3_2_Y, AND3_4_Y, BUFF_23_Y, 
        BUFF_17_Y, BUFF_12_Y, BUFF_38_Y, XOR2_69_Y, XOR2_30_Y, 
        AO1_47_Y, XOR2_12_Y, NOR2_1_Y, AND2_76_Y, MX2_9_Y, 
        AND2_13_Y, MX2_3_Y, AND2_140_Y, MX2_5_Y, MX2_47_Y, 
        XNOR2_7_Y, XOR2_11_Y, NOR2_10_Y, AND2_134_Y, MX2_14_Y, 
        AND2_133_Y, MX2_32_Y, AND2_45_Y, MX2_37_Y, AND2_127_Y, 
        MX2_49_Y, XNOR2_14_Y, XOR2_37_Y, NOR2_12_Y, AND2_156_Y, 
        MX2_36_Y, AND2_143_Y, MX2_30_Y, AND2_53_Y, AND2_32_Y, 
        MX2_21_Y, AND2_51_Y, MX2_31_Y, XNOR2_11_Y, OR3_1_Y, 
        AND3_5_Y, BUFF_25_Y, BUFF_22_Y, BUFF_0_Y, XOR2_40_Y, 
        XOR2_62_Y, AND2A_0_Y, AND2_104_Y, MX2_22_Y, AND2_28_Y, 
        MX2_10_Y, AND2_72_Y, MX2_27_Y, MX2_23_Y, AND2A_2_Y, 
        AND2_109_Y, MX2_7_Y, AND2_90_Y, MX2_13_Y, AND2_62_Y, 
        MX2_54_Y, AND2_121_Y, MX2_44_Y, AND2A_1_Y, AND2_115_Y, 
        MX2_48_Y, AND2_19_Y, MX2_53_Y, AND2_12_Y, AND2_68_Y, 
        MX2_0_Y, AND2_56_Y, MX2_20_Y, OR3_0_Y, AND3_1_Y, 
        BUFF_43_Y, BUFF_34_Y, BUFF_30_Y, BUFF_6_Y, XOR2_0_Y, 
        XOR2_51_Y, AO1_2_Y, XOR2_2_Y, NOR2_9_Y, AND2_122_Y, 
        MX2_34_Y, AND2_135_Y, MX2_51_Y, AND2_113_Y, MX2_61_Y, 
        MX2_6_Y, XNOR2_4_Y, XOR2_23_Y, NOR2_8_Y, AND2_147_Y, 
        MX2_12_Y, AND2_57_Y, MX2_41_Y, AND2_8_Y, MX2_59_Y, 
        AND2_124_Y, MX2_2_Y, XNOR2_6_Y, XOR2_41_Y, NOR2_2_Y, 
        AND2_75_Y, MX2_58_Y, AND2_25_Y, MX2_8_Y, AND2_70_Y, 
        AND2_98_Y, MX2_71_Y, AND2_154_Y, MX2_50_Y, XNOR2_13_Y, 
        OR3_5_Y, AND3_0_Y, BUFF_21_Y, BUFF_13_Y, BUFF_4_Y, 
        BUFF_35_Y, XOR2_32_Y, XOR2_39_Y, AO1_9_Y, XOR2_38_Y, 
        NOR2_14_Y, AND2_79_Y, MX2_67_Y, AND2_5_Y, MX2_4_Y, 
        AND2_91_Y, MX2_45_Y, MX2_25_Y, XNOR2_12_Y, XOR2_77_Y, 
        NOR2_6_Y, AND2_111_Y, MX2_62_Y, AND2_120_Y, MX2_33_Y, 
        AND2_95_Y, MX2_55_Y, AND2_17_Y, MX2_39_Y, XNOR2_10_Y, 
        XOR2_16_Y, NOR2_3_Y, AND2_146_Y, MX2_43_Y, AND2_3_Y, 
        MX2_70_Y, AND2_92_Y, AND2_78_Y, MX2_19_Y, AND2_157_Y, 
        MX2_18_Y, XNOR2_0_Y, OR3_3_Y, AND3_3_Y, BUFF_40_Y, 
        BUFF_31_Y, BUFF_29_Y, BUFF_2_Y, XOR2_22_Y, XOR2_55_Y, 
        AO1_11_Y, XOR2_54_Y, NOR2_5_Y, AND2_99_Y, MX2_17_Y, 
        AND2_1_Y, MX2_66_Y, AND2_46_Y, MX2_60_Y, MX2_69_Y, 
        XNOR2_5_Y, XOR2_8_Y, NOR2_7_Y, AND2_21_Y, MX2_64_Y, 
        AND2_106_Y, MX2_29_Y, AND2_93_Y, MX2_46_Y, AND2_131_Y, 
        MX2_16_Y, XNOR2_8_Y, XOR2_73_Y, NOR2_13_Y, AND2_102_Y, 
        MX2_38_Y, AND2_82_Y, MX2_28_Y, AND2_14_Y, AND2_49_Y, 
        MX2_65_Y, AND2_36_Y, MX2_42_Y, XNOR2_9_Y, OR3_4_Y, 
        AND3_2_Y, AND2_4_Y, AND2_11_Y, AND2_6_Y, AND2_105_Y, 
        AND2_61_Y, AND2_94_Y, AND2_84_Y, AND2_112_Y, AND2_48_Y, 
        AND2_148_Y, AND2_101_Y, AND2_116_Y, AND2_81_Y, AND2_155_Y, 
        AND2_30_Y, AND2_50_Y, AND2_7_Y, AND2_35_Y, AND2_114_Y, 
        AND2_2_Y, AND2_117_Y, AND2_130_Y, XOR2_79_Y, XOR2_49_Y, 
        XOR2_75_Y, XOR2_68_Y, XOR2_63_Y, XOR2_52_Y, XOR2_46_Y, 
        XOR2_31_Y, XOR2_20_Y, XOR2_15_Y, XOR2_26_Y, XOR2_81_Y, 
        XOR2_71_Y, XOR2_27_Y, XOR2_44_Y, XOR2_14_Y, XOR2_58_Y, 
        XOR2_29_Y, XOR2_5_Y, XOR2_35_Y, XOR2_57_Y, XOR2_18_Y, 
        XOR2_10_Y, AND2_128_Y, AO1_45_Y, AND2_27_Y, AO1_50_Y, 
        AND2_37_Y, AO1_8_Y, AND2_47_Y, AO1_55_Y, AND2_42_Y, 
        AO1_38_Y, AND2_69_Y, AO1_0_Y, AND2_24_Y, AO1_13_Y, 
        AND2_58_Y, AO1_52_Y, AND2_23_Y, AO1_4_Y, AND2_136_Y, 
        AO1_21_Y, AND2_38_Y, AND2_85_Y, AND2_103_Y, AO1_22_Y, 
        AND2_119_Y, AO1_40_Y, AND2_107_Y, AO1_30_Y, AND2_144_Y, 
        AO1_15_Y, AND2_83_Y, AO1_32_Y, AND2_153_Y, AO1_57_Y, 
        AND2_110_Y, AO1_51_Y, AND2_60_Y, AO1_24_Y, AND2_126_Y, 
        AO1_7_Y, AND2_22_Y, AO1_44_Y, AND2_34_Y, AO1_26_Y, 
        AND2_44_Y, AND2_40_Y, AND2_66_Y, AND2_18_Y, AND2_150_Y, 
        AO1_1_Y, AND2_108_Y, AO1_54_Y, AND2_59_Y, AO1_14_Y, 
        AND2_123_Y, AO1_56_Y, AND2_20_Y, AO1_31_Y, AND2_33_Y, 
        AO1_16_Y, AND2_43_Y, AO1_29_Y, AND2_39_Y, AO1_23_Y, 
        AND2_65_Y, AO1_10_Y, AND2_15_Y, AND2_118_Y, AND2_64_Y, 
        AND2_31_Y, AND2_80_Y, AND2_142_Y, AND2_151_Y, AND2_10_Y, 
        AND2_0_Y, AO1_27_Y, AND2_41_Y, AND2_139_Y, AND2_55_Y, 
        AND2_9_Y, AND2_129_Y, AND2_29_Y, AND2_73_Y, AO1_41_Y, 
        AND2_89_Y, AND2_63_Y, AO1_39_Y, AO1_6_Y, AO1_42_Y, 
        AO1_43_Y, AO1_33_Y, AO1_18_Y, AO1_35_Y, AO1_25_Y, 
        AO1_49_Y, AO1_46_Y, AO1_17_Y, AO1_3_Y, AO1_37_Y, AO1_19_Y, 
        AO1_34_Y, AO1_12_Y, AO1_28_Y, AO1_53_Y, AO1_48_Y, 
        AO1_20_Y, AO1_5_Y, XOR2_72_Y, XOR2_4_Y, XOR2_24_Y, 
        XOR2_53_Y, XOR2_6_Y, XOR2_67_Y, XOR2_80_Y, XOR2_19_Y, 
        XOR2_33_Y, XOR2_76_Y, XOR2_50_Y, XOR2_64_Y, XOR2_9_Y, 
        XOR2_78_Y, XOR2_25_Y, XOR2_7_Y, XOR2_17_Y, XOR2_48_Y, 
        XOR2_42_Y, XOR2_3_Y, XOR2_56_Y, XOR2_70_Y, \VCC\, \GND\
         : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;

begin 

    \GND\ <= GND_power_net1;
    \VCC\ <= VCC_power_net1;

    BUFF_8 : BUFF
      port map(A => DataA(6), Y => BUFF_8_Y);
    
    \XOR2_Mult[8]\ : XOR2
      port map(A => XOR2_80_Y, B => AO1_18_Y, Y => Mult(8));
    
    \XOR3_SumB[16]\ : XOR3
      port map(A => MAJ3_15_Y, B => XOR3_28_Y, C => XOR3_26_Y, Y
         => \SumB[16]\);
    
    AND2_12 : AND2
      port map(A => DataB(0), B => BUFF_28_Y, Y => AND2_12_Y);
    
    AO1_23 : AO1
      port map(A => AND2_38_Y, B => AO1_44_Y, C => AO1_21_Y, Y
         => AO1_23_Y);
    
    MAJ3_9 : MAJ3
      port map(A => \PP4[1]\, B => \PP3[3]\, C => \PP2[5]\, Y => 
        MAJ3_9_Y);
    
    AND2_72 : AND2
      port map(A => DataB(0), B => BUFF_41_Y, Y => AND2_72_Y);
    
    MAJ3_17 : MAJ3
      port map(A => \PP4[9]\, B => \PP3[11]\, C => \E[2]\, Y => 
        MAJ3_17_Y);
    
    XOR2_58 : XOR2
      port map(A => \SumA[16]\, B => \SumB[16]\, Y => XOR2_58_Y);
    
    \XOR2_Mult[13]\ : XOR2
      port map(A => XOR2_64_Y, B => AO1_17_Y, Y => Mult(13));
    
    \XOR2_Mult[2]\ : XOR2
      port map(A => XOR2_72_Y, B => AND2_63_Y, Y => Mult(2));
    
    XOR3_21 : XOR3
      port map(A => \PP1[11]\, B => EBAR, C => \PP2[9]\, Y => 
        XOR3_21_Y);
    
    \XOR2_PP5[4]\ : XOR2
      port map(A => MX2_15_Y, B => BUFF_46_Y, Y => \PP5[4]\);
    
    AND2_49 : AND2
      port map(A => XOR2_73_Y, B => BUFF_26_Y, Y => AND2_49_Y);
    
    AND2_23 : AND2
      port map(A => XOR2_58_Y, B => XOR2_29_Y, Y => AND2_23_Y);
    
    XOR3_7 : XOR3
      port map(A => XOR3_14_Y, B => AND2_137_Y, C => MAJ3_9_Y, Y
         => XOR3_7_Y);
    
    XOR3_18 : XOR3
      port map(A => \PP3[3]\, B => \PP2[5]\, C => \PP4[1]\, Y => 
        XOR3_18_Y);
    
    AO1_22 : AO1
      port map(A => AND2_27_Y, B => AO1_39_Y, C => AO1_45_Y, Y
         => AO1_22_Y);
    
    XOR2_40 : XOR2
      port map(A => AND2_12_Y, B => BUFF_25_Y, Y => XOR2_40_Y);
    
    NOR2_12 : NOR2
      port map(A => XOR2_37_Y, B => XNOR2_11_Y, Y => NOR2_12_Y);
    
    AND2_91 : AND2
      port map(A => XOR2_38_Y, B => BUFF_16_Y, Y => AND2_91_Y);
    
    BUFF_7 : BUFF
      port map(A => DataA(7), Y => BUFF_7_Y);
    
    AO1_54 : AO1
      port map(A => AND2_144_Y, B => AO1_22_Y, C => AO1_30_Y, Y
         => AO1_54_Y);
    
    AND2_69 : AND2
      port map(A => XOR2_26_Y, B => XOR2_81_Y, Y => AND2_69_Y);
    
    MX2_37 : MX2
      port map(A => AND2_45_Y, B => BUFF_8_Y, S => NOR2_10_Y, Y
         => MX2_37_Y);
    
    MX2_54 : MX2
      port map(A => AND2_62_Y, B => BUFF_8_Y, S => AND2A_2_Y, Y
         => MX2_54_Y);
    
    AND2_55 : AND2
      port map(A => AND2_0_Y, B => AND2_23_Y, Y => AND2_55_Y);
    
    \XOR2_Mult[10]\ : XOR2
      port map(A => XOR2_33_Y, B => AO1_25_Y, Y => Mult(10));
    
    MX2_23 : MX2
      port map(A => BUFF_0_Y, B => XOR2_62_Y, S => DataB(0), Y
         => MX2_23_Y);
    
    MAJ3_19 : MAJ3
      port map(A => MAJ3_16_Y, B => XOR2_13_Y, C => \S[4]\, Y => 
        MAJ3_19_Y);
    
    \XOR2_PP2[4]\ : XOR2
      port map(A => MX2_38_Y, B => BUFF_31_Y, Y => \PP2[4]\);
    
    MAJ3_18 : MAJ3
      port map(A => \PP5[0]\, B => \PP4[2]\, C => \PP3[4]\, Y => 
        MAJ3_18_Y);
    
    MX2_65 : MX2
      port map(A => AND2_49_Y, B => BUFF_19_Y, S => NOR2_13_Y, Y
         => MX2_65_Y);
    
    MX2_1 : MX2
      port map(A => AND2_74_Y, B => BUFF_32_Y, S => NOR2_4_Y, Y
         => MX2_1_Y);
    
    \XOR2_PP2[11]\ : XOR2
      port map(A => MX2_60_Y, B => BUFF_2_Y, Y => \PP2[11]\);
    
    \MAJ3_SumA[17]\ : MAJ3
      port map(A => XOR3_26_Y, B => MAJ3_15_Y, C => XOR3_28_Y, Y
         => \SumA[17]\);
    
    AND2_96 : AND2
      port map(A => XOR2_43_Y, B => BUFF_9_Y, Y => AND2_96_Y);
    
    AND2_146 : AND2
      port map(A => XOR2_16_Y, B => BUFF_1_Y, Y => AND2_146_Y);
    
    XOR2_71 : XOR2
      port map(A => \SumA[12]\, B => \SumB[12]\, Y => XOR2_71_Y);
    
    \XOR2_Mult[12]\ : XOR2
      port map(A => XOR2_50_Y, B => AO1_46_Y, Y => Mult(12));
    
    AND2_18 : AND2
      port map(A => AND2_103_Y, B => AND2_107_Y, Y => AND2_18_Y);
    
    MAJ3_21 : MAJ3
      port map(A => \PP2[9]\, B => \PP1[11]\, C => EBAR, Y => 
        MAJ3_21_Y);
    
    BUFF_12 : BUFF
      port map(A => DataB(3), Y => BUFF_12_Y);
    
    AND2_78 : AND2
      port map(A => XOR2_16_Y, B => BUFF_24_Y, Y => AND2_78_Y);
    
    AO1_30 : AO1
      port map(A => AND2_47_Y, B => AO1_50_Y, C => AO1_8_Y, Y => 
        AO1_30_Y);
    
    \MAJ3_SumA[19]\ : MAJ3
      port map(A => XOR3_16_Y, B => MAJ3_0_Y, C => AND2_145_Y, Y
         => \SumA[19]\);
    
    AND2_138 : AND2
      port map(A => XOR2_43_Y, B => BUFF_32_Y, Y => AND2_138_Y);
    
    XNOR2_4 : XNOR2
      port map(A => DataB(8), B => BUFF_6_Y, Y => XNOR2_4_Y);
    
    \XOR2_PP4[7]\ : XOR2
      port map(A => MX2_59_Y, B => BUFF_30_Y, Y => \PP4[7]\);
    
    AND2_40 : AND2
      port map(A => AND2_103_Y, B => XOR2_63_Y, Y => AND2_40_Y);
    
    BUFF_33 : BUFF
      port map(A => DataA(5), Y => BUFF_33_Y);
    
    BUFF_31 : BUFF
      port map(A => DataB(5), Y => BUFF_31_Y);
    
    XOR2_24 : XOR2
      port map(A => \SumA[3]\, B => \SumB[3]\, Y => XOR2_24_Y);
    
    AND2_153 : AND2
      port map(A => AND2_42_Y, B => AND2_69_Y, Y => AND2_153_Y);
    
    BUFF_22 : BUFF
      port map(A => DataB(1), Y => BUFF_22_Y);
    
    XOR2_34 : XOR2
      port map(A => BUFF_3_Y, B => DataB(10), Y => XOR2_34_Y);
    
    AND2_32 : AND2
      port map(A => XOR2_37_Y, B => BUFF_26_Y, Y => AND2_32_Y);
    
    \MAJ3_SumA[9]\ : MAJ3
      port map(A => XOR3_9_Y, B => MAJ3_14_Y, C => XOR3_18_Y, Y
         => \SumA[9]\);
    
    AND2_60 : AND2
      port map(A => AND2_24_Y, B => XOR2_44_Y, Y => AND2_60_Y);
    
    OR3_4 : OR3
      port map(A => DataB(3), B => DataB(4), C => DataB(5), Y => 
        OR3_4_Y);
    
    \XOR2_PP3[0]\ : XOR2
      port map(A => XOR2_32_Y, B => DataB(7), Y => \PP3[0]\);
    
    MX2_0 : MX2
      port map(A => AND2_68_Y, B => BUFF_19_Y, S => AND2A_1_Y, Y
         => MX2_0_Y);
    
    XOR2_43 : XOR2
      port map(A => BUFF_3_Y, B => DataB(10), Y => XOR2_43_Y);
    
    BUFF_4 : BUFF
      port map(A => DataB(7), Y => BUFF_4_Y);
    
    \XOR3_SumB[19]\ : XOR3
      port map(A => XOR2_21_Y, B => \PP5[10]\, C => MAJ3_4_Y, Y
         => \SumB[19]\);
    
    XOR2_61 : XOR2
      port map(A => \PP4[4]\, B => \PP3[6]\, Y => XOR2_61_Y);
    
    XNOR2_2 : XNOR2
      port map(A => DataB(10), B => BUFF_39_Y, Y => XNOR2_2_Y);
    
    XOR2_49 : XOR2
      port map(A => \PP0[2]\, B => \PP1[0]\, Y => XOR2_49_Y);
    
    AND2_85 : AND2
      port map(A => AND2_128_Y, B => XOR2_75_Y, Y => AND2_85_Y);
    
    AND2_147 : AND2
      port map(A => XOR2_23_Y, B => BUFF_14_Y, Y => AND2_147_Y);
    
    AO1_35 : AO1
      port map(A => AND2_144_Y, B => AO1_22_Y, C => AO1_30_Y, Y
         => AO1_35_Y);
    
    AO1_27 : AO1
      port map(A => AND2_43_Y, B => AO1_54_Y, C => AO1_16_Y, Y
         => AO1_27_Y);
    
    MX2_21 : MX2
      port map(A => AND2_32_Y, B => BUFF_19_Y, S => NOR2_12_Y, Y
         => MX2_21_Y);
    
    AND2_53 : AND2
      port map(A => XOR2_37_Y, B => BUFF_28_Y, Y => AND2_53_Y);
    
    XOR3_22 : XOR3
      port map(A => \PP1[6]\, B => \PP0[8]\, C => \PP2[4]\, Y => 
        XOR3_22_Y);
    
    AO1_13 : AO1
      port map(A => XOR2_14_Y, B => AND2_155_Y, C => AND2_30_Y, Y
         => AO1_13_Y);
    
    MX2_14 : MX2
      port map(A => AND2_134_Y, B => BUFF_45_Y, S => NOR2_10_Y, Y
         => MX2_14_Y);
    
    AND2_125 : AND2
      port map(A => \PP4[6]\, B => \PP3[8]\, Y => AND2_125_Y);
    
    MX2_33 : MX2
      port map(A => AND2_120_Y, B => BUFF_1_Y, S => NOR2_6_Y, Y
         => MX2_33_Y);
    
    XOR3_8 : XOR3
      port map(A => XOR3_17_Y, B => MAJ3_30_Y, C => MAJ3_8_Y, Y
         => XOR3_8_Y);
    
    MX2_28 : MX2
      port map(A => AND2_82_Y, B => BUFF_28_Y, S => NOR2_13_Y, Y
         => MX2_28_Y);
    
    \XOR2_Mult[3]\ : XOR2
      port map(A => XOR2_4_Y, B => AO1_39_Y, Y => Mult(3));
    
    AND2_99 : AND2
      port map(A => XOR2_54_Y, B => BUFF_42_Y, Y => AND2_99_Y);
    
    \XOR2_PP5[1]\ : XOR2
      port map(A => MX2_68_Y, B => BUFF_46_Y, Y => \PP5[1]\);
    
    XOR2_47 : XOR2
      port map(A => BUFF_16_Y, B => DataB(11), Y => XOR2_47_Y);
    
    \XOR3_SumB[4]\ : XOR3
      port map(A => \S[2]\, B => \PP2[1]\, C => XOR2_74_Y, Y => 
        \SumB[4]\);
    
    \MAJ3_SumA[14]\ : MAJ3
      port map(A => XOR3_13_Y, B => MAJ3_1_Y, C => XOR3_1_Y, Y
         => \SumA[14]\);
    
    \XOR2_PP4[10]\ : XOR2
      port map(A => MX2_34_Y, B => BUFF_6_Y, Y => \PP4[10]\);
    
    AO1_28 : AO1
      port map(A => AND2_23_Y, B => AO1_34_Y, C => AO1_52_Y, Y
         => AO1_28_Y);
    
    XOR2_9 : XOR2
      port map(A => \SumA[13]\, B => \SumB[13]\, Y => XOR2_9_Y);
    
    \XOR2_PP3[11]\ : XOR2
      port map(A => MX2_45_Y, B => BUFF_35_Y, Y => \PP3[11]\);
    
    AO1_12 : AO1
      port map(A => XOR2_58_Y, B => AO1_34_Y, C => AND2_50_Y, Y
         => AO1_12_Y);
    
    AND2_120 : AND2
      port map(A => XOR2_77_Y, B => BUFF_33_Y, Y => AND2_120_Y);
    
    XOR2_25 : XOR2
      port map(A => \SumA[15]\, B => \SumB[15]\, Y => XOR2_25_Y);
    
    \XOR2_Mult[17]\ : XOR2
      port map(A => XOR2_7_Y, B => AO1_34_Y, Y => Mult(17));
    
    XOR2_35 : XOR2
      port map(A => \SumA[19]\, B => \SumB[19]\, Y => XOR2_35_Y);
    
    \MX2_PP2[12]\ : MX2
      port map(A => MX2_69_Y, B => AO1_11_Y, S => NOR2_5_Y, Y => 
        \PP2[12]\);
    
    AND3_0 : AND3
      port map(A => DataB(7), B => DataB(8), C => DataB(9), Y => 
        AND3_0_Y);
    
    \XOR2_SumB[2]\ : XOR2
      port map(A => \PP1[1]\, B => \PP0[3]\, Y => \SumB[2]\);
    
    XOR3_26 : XOR3
      port map(A => MAJ3_24_Y, B => \PP5[7]\, C => MAJ3_5_Y, Y
         => XOR3_26_Y);
    
    \XOR2_PP4[0]\ : XOR2
      port map(A => XOR2_0_Y, B => DataB(9), Y => \PP4[0]\);
    
    \XOR2_PP1[1]\ : XOR2
      port map(A => MX2_30_Y, B => BUFF_17_Y, Y => \PP1[1]\);
    
    MX2_50 : MX2
      port map(A => AND2_154_Y, B => BUFF_24_Y, S => NOR2_2_Y, Y
         => MX2_50_Y);
    
    MX2_7 : MX2
      port map(A => AND2_109_Y, B => BUFF_45_Y, S => AND2A_2_Y, Y
         => MX2_7_Y);
    
    AND2_17 : AND2
      port map(A => XOR2_77_Y, B => BUFF_18_Y, Y => AND2_17_Y);
    
    AND2_114 : AND2
      port map(A => \SumA[19]\, B => \SumB[19]\, Y => AND2_114_Y);
    
    AND2_77 : AND2
      port map(A => \PP1[3]\, B => \PP0[5]\, Y => AND2_77_Y);
    
    \MAJ3_SumA[7]\ : MAJ3
      port map(A => XOR3_20_Y, B => MAJ3_2_Y, C => XOR2_1_Y, Y
         => \SumA[7]\);
    
    AO1_3 : AO1
      port map(A => AND2_123_Y, B => AO1_1_Y, C => AO1_14_Y, Y
         => AO1_3_Y);
    
    BUFF_16 : BUFF
      port map(A => DataA(11), Y => BUFF_16_Y);
    
    NOR2_2 : NOR2
      port map(A => XOR2_41_Y, B => XNOR2_13_Y, Y => NOR2_2_Y);
    
    AND2_38 : AND2
      port map(A => XOR2_57_Y, B => XOR2_18_Y, Y => AND2_38_Y);
    
    AND2_133 : AND2
      port map(A => XOR2_11_Y, B => BUFF_45_Y, Y => AND2_133_Y);
    
    \AND2_SumB[22]\ : AND2
      port map(A => \PP5[12]\, B => \VCC\, Y => \SumB[22]\);
    
    \XOR3_SumB[20]\ : XOR3
      port map(A => \PP5[11]\, B => \E[4]\, C => AND2_152_Y, Y
         => \SumB[20]\);
    
    OR3_0 : OR3
      port map(A => \GND\, B => DataB(0), C => DataB(1), Y => 
        OR3_0_Y);
    
    MX2_6 : MX2
      port map(A => BUFF_6_Y, B => XOR2_51_Y, S => XOR2_2_Y, Y
         => MX2_6_Y);
    
    NOR2_3 : NOR2
      port map(A => XOR2_16_Y, B => XNOR2_0_Y, Y => NOR2_3_Y);
    
    XOR2_72 : XOR2
      port map(A => \PP0[2]\, B => \PP1[0]\, Y => XOR2_72_Y);
    
    AND2_129 : AND2
      port map(A => AND2_0_Y, B => AND2_34_Y, Y => AND2_129_Y);
    
    MAJ3_27 : MAJ3
      port map(A => MAJ3_5_Y, B => MAJ3_24_Y, C => \PP5[7]\, Y
         => MAJ3_27_Y);
    
    AO1_56 : AO1
      port map(A => AND2_24_Y, B => AO1_57_Y, C => AO1_0_Y, Y => 
        AO1_56_Y);
    
    AO1_0 : AO1
      port map(A => XOR2_27_Y, B => AND2_116_Y, C => AND2_81_Y, Y
         => AO1_0_Y);
    
    AND2_111 : AND2
      port map(A => XOR2_77_Y, B => BUFF_14_Y, Y => AND2_111_Y);
    
    BUFF_26 : BUFF
      port map(A => DataA(2), Y => BUFF_26_Y);
    
    AND2_83 : AND2
      port map(A => AND2_42_Y, B => XOR2_26_Y, Y => AND2_83_Y);
    
    MX2_42 : MX2
      port map(A => AND2_36_Y, B => BUFF_26_Y, S => NOR2_13_Y, Y
         => MX2_42_Y);
    
    AND2_11 : AND2
      port map(A => \S[1]\, B => \SumB[2]\, Y => AND2_11_Y);
    
    XOR2_18 : XOR2
      port map(A => \SumA[21]\, B => \SumB[21]\, Y => XOR2_18_Y);
    
    AND2_90 : AND2
      port map(A => DataB(0), B => BUFF_45_Y, Y => AND2_90_Y);
    
    \MAJ3_SumA[10]\ : MAJ3
      port map(A => XOR3_7_Y, B => MAJ3_19_Y, C => XOR3_27_Y, Y
         => \SumA[10]\);
    
    AND2_71 : AND2
      port map(A => XOR2_43_Y, B => BUFF_1_Y, Y => AND2_71_Y);
    
    BUFF_37 : BUFF
      port map(A => DataA(4), Y => BUFF_37_Y);
    
    XOR2_20 : XOR2
      port map(A => \SumA[8]\, B => \SumB[8]\, Y => XOR2_20_Y);
    
    MX2_31 : MX2
      port map(A => AND2_51_Y, B => BUFF_26_Y, S => NOR2_12_Y, Y
         => MX2_31_Y);
    
    XOR2_76 : XOR2
      port map(A => \SumA[10]\, B => \SumB[10]\, Y => XOR2_76_Y);
    
    XOR2_30 : XOR2
      port map(A => BUFF_41_Y, B => DataB(3), Y => XOR2_30_Y);
    
    MX2_56 : MX2
      port map(A => AND2_87_Y, B => BUFF_18_Y, S => NOR2_11_Y, Y
         => MX2_56_Y);
    
    MX2_38 : MX2
      port map(A => AND2_102_Y, B => BUFF_27_Y, S => NOR2_13_Y, Y
         => MX2_38_Y);
    
    \AND2_S[3]\ : AND2
      port map(A => XOR2_32_Y, B => DataB(7), Y => \S[3]\);
    
    \MAJ3_SumA[12]\ : MAJ3
      port map(A => XOR3_3_Y, B => MAJ3_29_Y, C => XOR3_30_Y, Y
         => \SumA[12]\);
    
    XOR2_62 : XOR2
      port map(A => BUFF_41_Y, B => DataB(1), Y => XOR2_62_Y);
    
    AND2_4 : AND2
      port map(A => \PP0[2]\, B => \PP1[0]\, Y => AND2_4_Y);
    
    MAJ3_29 : MAJ3
      port map(A => MAJ3_18_Y, B => XOR3_6_Y, C => MAJ3_10_Y, Y
         => MAJ3_29_Y);
    
    XOR3_2 : XOR3
      port map(A => \S[5]\, B => \PP5[1]\, C => XOR2_28_Y, Y => 
        XOR3_2_Y);
    
    MAJ3_28 : MAJ3
      port map(A => XOR2_28_Y, B => \S[5]\, C => \PP5[1]\, Y => 
        MAJ3_28_Y);
    
    AND2_0 : AND2
      port map(A => AND2_59_Y, B => AND2_43_Y, Y => AND2_0_Y);
    
    AND2_156 : AND2
      port map(A => XOR2_37_Y, B => BUFF_37_Y, Y => AND2_156_Y);
    
    AO1_34 : AO1
      port map(A => AND2_43_Y, B => AO1_54_Y, C => AO1_16_Y, Y
         => AO1_34_Y);
    
    XOR3_3 : XOR3
      port map(A => XOR3_4_Y, B => MAJ3_25_Y, C => MAJ3_28_Y, Y
         => XOR3_3_Y);
    
    AO1_43 : AO1
      port map(A => XOR2_63_Y, B => AO1_42_Y, C => AND2_105_Y, Y
         => AO1_43_Y);
    
    AND2_16 : AND2
      port map(A => \PP4[7]\, B => \PP3[9]\, Y => AND2_16_Y);
    
    MX2_64 : MX2
      port map(A => AND2_21_Y, B => BUFF_45_Y, S => NOR2_7_Y, Y
         => MX2_64_Y);
    
    AND2_76 : AND2
      port map(A => XOR2_12_Y, B => BUFF_42_Y, Y => AND2_76_Y);
    
    MX2_10 : MX2
      port map(A => AND2_28_Y, B => BUFF_15_Y, S => AND2A_0_Y, Y
         => MX2_10_Y);
    
    AND2_6 : AND2
      port map(A => \SumA[3]\, B => \SumB[3]\, Y => AND2_6_Y);
    
    AO1_17 : AO1
      port map(A => AND2_153_Y, B => AO1_1_Y, C => AO1_32_Y, Y
         => AO1_17_Y);
    
    XNOR2_0 : XNOR2
      port map(A => DataB(6), B => BUFF_13_Y, Y => XNOR2_0_Y);
    
    BUFF_44 : BUFF
      port map(A => DataA(7), Y => BUFF_44_Y);
    
    MX2_49 : MX2
      port map(A => AND2_127_Y, B => BUFF_7_Y, S => NOR2_10_Y, Y
         => MX2_49_Y);
    
    XOR2_66 : XOR2
      port map(A => \PP4[7]\, B => \PP3[9]\, Y => XOR2_66_Y);
    
    \XOR2_PP0[5]\ : XOR2
      port map(A => MX2_13_Y, B => BUFF_22_Y, Y => \PP0[5]\);
    
    AO1_42 : AO1
      port map(A => AND2_27_Y, B => AO1_39_Y, C => AO1_45_Y, Y
         => AO1_42_Y);
    
    AND2_37 : AND2
      port map(A => XOR2_63_Y, B => XOR2_52_Y, Y => AND2_37_Y);
    
    AO1_18 : AO1
      port map(A => AND2_107_Y, B => AO1_42_Y, C => AO1_40_Y, Y
         => AO1_18_Y);
    
    OR3_2 : OR3
      port map(A => DataB(9), B => DataB(10), C => DataB(11), Y
         => OR3_2_Y);
    
    AO1_39 : AO1
      port map(A => XOR2_49_Y, B => AND2_63_Y, C => AND2_4_Y, Y
         => AO1_39_Y);
    
    XOR2_41 : XOR2
      port map(A => BUFF_43_Y, B => DataB(8), Y => XOR2_41_Y);
    
    \XOR2_PP2[8]\ : XOR2
      port map(A => MX2_16_Y, B => BUFF_29_Y, Y => \PP2[8]\);
    
    AO1_21 : AO1
      port map(A => XOR2_18_Y, B => AND2_2_Y, C => AND2_117_Y, Y
         => AO1_21_Y);
    
    XOR2_7 : XOR2
      port map(A => \SumA[16]\, B => \SumB[16]\, Y => XOR2_7_Y);
    
    BUFF_6 : BUFF
      port map(A => DataB(9), Y => BUFF_6_Y);
    
    XOR2_54 : XOR2
      port map(A => BUFF_40_Y, B => DataB(4), Y => XOR2_54_Y);
    
    MAJ3_1 : MAJ3
      port map(A => MAJ3_3_Y, B => XOR3_21_Y, C => MAJ3_23_Y, Y
         => MAJ3_1_Y);
    
    AND2_157 : AND2
      port map(A => XOR2_16_Y, B => BUFF_5_Y, Y => AND2_157_Y);
    
    AND2_44 : AND2
      port map(A => AND2_38_Y, B => XOR2_10_Y, Y => AND2_44_Y);
    
    XOR3_14 : XOR3
      port map(A => \PP1[8]\, B => \PP0[10]\, C => \PP2[6]\, Y
         => XOR3_14_Y);
    
    XOR2_23 : XOR2
      port map(A => BUFF_43_Y, B => DataB(8), Y => XOR2_23_Y);
    
    \XOR2_PP5[2]\ : XOR2
      port map(A => MX2_1_Y, B => BUFF_46_Y, Y => \PP5[2]\);
    
    XOR2_33 : XOR2
      port map(A => \SumA[9]\, B => \SumB[9]\, Y => XOR2_33_Y);
    
    AND2_31 : AND2
      port map(A => AND2_108_Y, B => AND2_83_Y, Y => AND2_31_Y);
    
    MX2_16 : MX2
      port map(A => AND2_131_Y, B => BUFF_7_Y, S => NOR2_7_Y, Y
         => MX2_16_Y);
    
    AND2_122 : AND2
      port map(A => XOR2_2_Y, B => BUFF_10_Y, Y => AND2_122_Y);
    
    XOR2_29 : XOR2
      port map(A => \SumA[17]\, B => \SumB[17]\, Y => XOR2_29_Y);
    
    AND2_64 : AND2
      port map(A => AND2_150_Y, B => AND2_42_Y, Y => AND2_64_Y);
    
    BUFF_39 : BUFF
      port map(A => DataB(11), Y => BUFF_39_Y);
    
    \MX2_PP4[12]\ : MX2
      port map(A => MX2_6_Y, B => AO1_2_Y, S => NOR2_9_Y, Y => 
        \PP4[12]\);
    
    XOR2_39 : XOR2
      port map(A => BUFF_16_Y, B => DataB(7), Y => XOR2_39_Y);
    
    \XOR2_PP0[9]\ : XOR2
      port map(A => MX2_10_Y, B => BUFF_0_Y, Y => \PP0[9]\);
    
    \XOR2_PP3[5]\ : XOR2
      port map(A => MX2_33_Y, B => BUFF_4_Y, Y => \PP3[5]\);
    
    AND2_136 : AND2
      port map(A => XOR2_5_Y, B => XOR2_35_Y, Y => AND2_136_Y);
    
    \XOR2_PP2[6]\ : XOR2
      port map(A => MX2_64_Y, B => BUFF_29_Y, Y => \PP2[6]\);
    
    AND2_19 : AND2
      port map(A => DataB(0), B => BUFF_19_Y, Y => AND2_19_Y);
    
    AND2_79 : AND2
      port map(A => XOR2_38_Y, B => BUFF_10_Y, Y => AND2_79_Y);
    
    MX2_57 : MX2
      port map(A => BUFF_20_Y, B => XOR2_47_Y, S => XOR2_36_Y, Y
         => MX2_57_Y);
    
    XOR2_27 : XOR2
      port map(A => \SumA[13]\, B => \SumB[13]\, Y => XOR2_27_Y);
    
    MX2_8 : MX2
      port map(A => AND2_25_Y, B => BUFF_9_Y, S => NOR2_2_Y, Y
         => MX2_8_Y);
    
    XOR2_6 : XOR2
      port map(A => \SumA[5]\, B => \SumB[5]\, Y => XOR2_6_Y);
    
    XOR2_37 : XOR2
      port map(A => BUFF_23_Y, B => DataB(2), Y => XOR2_37_Y);
    
    BUFF_13 : BUFF
      port map(A => DataB(7), Y => BUFF_13_Y);
    
    BUFF_11 : BUFF
      port map(A => DataA(9), Y => BUFF_11_Y);
    
    AND2_115 : AND2
      port map(A => DataB(0), B => BUFF_37_Y, Y => AND2_115_Y);
    
    BUFF_45 : BUFF
      port map(A => DataA(5), Y => BUFF_45_Y);
    
    BUFF_40 : BUFF
      port map(A => DataB(3), Y => BUFF_40_Y);
    
    XOR2_55 : XOR2
      port map(A => BUFF_41_Y, B => DataB(5), Y => XOR2_55_Y);
    
    \XOR2_Mult[15]\ : XOR2
      port map(A => XOR2_78_Y, B => AO1_37_Y, Y => Mult(15));
    
    AND2A_2 : AND2A
      port map(A => DataB(0), B => BUFF_22_Y, Y => AND2A_2_Y);
    
    AND2_104 : AND2
      port map(A => DataB(0), B => BUFF_42_Y, Y => AND2_104_Y);
    
    AND2_36 : AND2
      port map(A => XOR2_73_Y, B => BUFF_27_Y, Y => AND2_36_Y);
    
    AND2_9 : AND2
      port map(A => AND2_0_Y, B => AND2_22_Y, Y => AND2_9_Y);
    
    AND2A_1 : AND2A
      port map(A => DataB(0), B => BUFF_25_Y, Y => AND2A_1_Y);
    
    MX2_70 : MX2
      port map(A => AND2_3_Y, B => BUFF_9_Y, S => NOR2_3_Y, Y => 
        MX2_70_Y);
    
    XOR3_15 : XOR3
      port map(A => \PP4[0]\, B => \PP3[2]\, C => AND2_67_Y, Y
         => XOR3_15_Y);
    
    AND2_110 : AND2
      port map(A => AND2_42_Y, B => AND2_69_Y, Y => AND2_110_Y);
    
    \XOR3_SumB[13]\ : XOR3
      port map(A => MAJ3_1_Y, B => XOR3_1_Y, C => XOR3_13_Y, Y
         => \SumB[13]\);
    
    BUFF_23 : BUFF
      port map(A => DataB(1), Y => BUFF_23_Y);
    
    \XOR2_PP4[3]\ : XOR2
      port map(A => MX2_50_Y, B => BUFF_34_Y, Y => \PP4[3]\);
    
    BUFF_21 : BUFF
      port map(A => DataB(5), Y => BUFF_21_Y);
    
    \MX2_PP1[12]\ : MX2
      port map(A => MX2_47_Y, B => AO1_47_Y, S => NOR2_1_Y, Y => 
        \PP1[12]\);
    
    NOR2_5 : NOR2
      port map(A => XOR2_54_Y, B => XNOR2_5_Y, Y => NOR2_5_Y);
    
    MX2_60 : MX2
      port map(A => AND2_46_Y, B => BUFF_42_Y, S => NOR2_5_Y, Y
         => MX2_60_Y);
    
    AND2_101 : AND2
      port map(A => \SumA[11]\, B => \SumB[11]\, Y => AND2_101_Y);
    
    AND2_137 : AND2
      port map(A => \PP1[7]\, B => \PP0[9]\, Y => AND2_137_Y);
    
    AO1_47 : AO1
      port map(A => XOR2_30_Y, B => OR3_1_Y, C => AND3_5_Y, Y => 
        AO1_47_Y);
    
    \XOR3_SumB[17]\ : XOR3
      port map(A => MAJ3_27_Y, B => MAJ3_17_Y, C => XOR3_24_Y, Y
         => \SumB[17]\);
    
    AND2_10 : AND2
      port map(A => AND2_59_Y, B => AND2_33_Y, Y => AND2_10_Y);
    
    \XOR2_PP1[2]\ : XOR2
      port map(A => MX2_21_Y, B => BUFF_17_Y, Y => \PP1[2]\);
    
    AND2_119 : AND2
      port map(A => AND2_128_Y, B => AND2_27_Y, Y => AND2_119_Y);
    
    AND2_70 : AND2
      port map(A => XOR2_41_Y, B => BUFF_9_Y, Y => AND2_70_Y);
    
    \AOI1_E[0]\ : AOI1
      port map(A => XOR2_62_Y, B => OR3_0_Y, C => AND3_1_Y, Y => 
        \E[0]\);
    
    \XOR2_PP1[10]\ : XOR2
      port map(A => MX2_9_Y, B => BUFF_38_Y, Y => \PP1[10]\);
    
    MAJ3_16 : MAJ3
      port map(A => \PP2[4]\, B => \PP1[6]\, C => \PP0[8]\, Y => 
        MAJ3_16_Y);
    
    XNOR2_7 : XNOR2
      port map(A => DataB(2), B => BUFF_38_Y, Y => XNOR2_7_Y);
    
    AO1_48 : AO1
      port map(A => AND2_34_Y, B => AO1_34_Y, C => AO1_44_Y, Y
         => AO1_48_Y);
    
    XOR2_42 : XOR2
      port map(A => \SumA[19]\, B => \SumB[19]\, Y => XOR2_42_Y);
    
    AO1_36 : AO1
      port map(A => XOR2_47_Y, B => OR3_2_Y, C => AND3_4_Y, Y => 
        AO1_36_Y);
    
    \XOR2_PP5[9]\ : XOR2
      port map(A => MX2_56_Y, B => BUFF_20_Y, Y => \PP5[9]\);
    
    \XOR2_Mult[21]\ : XOR2
      port map(A => XOR2_3_Y, B => AO1_48_Y, Y => Mult(21));
    
    XOR2_50 : XOR2
      port map(A => \SumA[11]\, B => \SumB[11]\, Y => XOR2_50_Y);
    
    XOR2_0 : XOR2
      port map(A => AND2_70_Y, B => BUFF_34_Y, Y => XOR2_0_Y);
    
    AO1_11 : AO1
      port map(A => XOR2_55_Y, B => OR3_4_Y, C => AND3_2_Y, Y => 
        AO1_11_Y);
    
    AND2_94 : AND2
      port map(A => \SumA[6]\, B => \SumB[6]\, Y => AND2_94_Y);
    
    MX2_17 : MX2
      port map(A => AND2_99_Y, B => BUFF_11_Y, S => NOR2_5_Y, Y
         => MX2_17_Y);
    
    XOR3_10 : XOR3
      port map(A => \PP2[12]\, B => \VCC\, C => \PP3[10]\, Y => 
        XOR3_10_Y);
    
    AND2_22 : AND2
      port map(A => AND2_23_Y, B => XOR2_5_Y, Y => AND2_22_Y);
    
    MAJ3_3 : MAJ3
      port map(A => XOR2_61_Y, B => AND2_141_Y, C => \PP5[2]\, Y
         => MAJ3_3_Y);
    
    MX2_53 : MX2
      port map(A => AND2_19_Y, B => BUFF_28_Y, S => AND2A_1_Y, Y
         => MX2_53_Y);
    
    AND2_39 : AND2
      port map(A => AND2_34_Y, B => XOR2_57_Y, Y => AND2_39_Y);
    
    \XOR2_PP0[1]\ : XOR2
      port map(A => MX2_53_Y, B => BUFF_25_Y, Y => \PP0[1]\);
    
    \XOR2_PP4[1]\ : XOR2
      port map(A => MX2_8_Y, B => BUFF_34_Y, Y => \PP4[1]\);
    
    \XOR2_PP5[3]\ : XOR2
      port map(A => MX2_63_Y, B => BUFF_46_Y, Y => \PP5[3]\);
    
    XOR2_46 : XOR2
      port map(A => \SumA[6]\, B => \SumB[6]\, Y => XOR2_46_Y);
    
    MX2_66 : MX2
      port map(A => AND2_1_Y, B => BUFF_15_Y, S => NOR2_5_Y, Y
         => MX2_66_Y);
    
    MX2_45 : MX2
      port map(A => AND2_91_Y, B => BUFF_10_Y, S => NOR2_14_Y, Y
         => MX2_45_Y);
    
    \AOI1_E[2]\ : AOI1
      port map(A => XOR2_55_Y, B => OR3_4_Y, C => AND3_2_Y, Y => 
        \E[2]\);
    
    \XOR2_Mult[6]\ : XOR2
      port map(A => XOR2_6_Y, B => AO1_43_Y, Y => Mult(6));
    
    BUFF_17 : BUFF
      port map(A => DataB(3), Y => BUFF_17_Y);
    
    AND2_45 : AND2
      port map(A => XOR2_11_Y, B => BUFF_7_Y, Y => AND2_45_Y);
    
    MAJ3_2 : MAJ3
      port map(A => \PP2[2]\, B => \PP1[4]\, C => \PP0[6]\, Y => 
        MAJ3_2_Y);
    
    \XOR2_Mult[16]\ : XOR2
      port map(A => XOR2_25_Y, B => AO1_19_Y, Y => Mult(16));
    
    XOR2_21 : XOR2
      port map(A => \PP4[12]\, B => \VCC\, Y => XOR2_21_Y);
    
    \XOR2_PP0[2]\ : XOR2
      port map(A => MX2_0_Y, B => BUFF_25_Y, Y => \PP0[2]\);
    
    XOR2_31 : XOR2
      port map(A => \SumA[7]\, B => \SumB[7]\, Y => XOR2_31_Y);
    
    AND2_128 : AND2
      port map(A => XOR2_79_Y, B => XOR2_49_Y, Y => AND2_128_Y);
    
    \XOR2_PP0[11]\ : XOR2
      port map(A => MX2_27_Y, B => BUFF_0_Y, Y => \PP0[11]\);
    
    AND2_65 : AND2
      port map(A => AND2_34_Y, B => AND2_38_Y, Y => AND2_65_Y);
    
    XOR2_14 : XOR2
      port map(A => \SumA[15]\, B => \SumB[15]\, Y => XOR2_14_Y);
    
    AO1_EBAR : AO1
      port map(A => XOR2_62_Y, B => OR3_0_Y, C => AND3_1_Y, Y => 
        EBAR);
    
    BUFF_27 : BUFF
      port map(A => DataA(3), Y => BUFF_27_Y);
    
    BUFF_3 : BUFF
      port map(A => DataB(9), Y => BUFF_3_Y);
    
    AND2_30 : AND2
      port map(A => \SumA[15]\, B => \SumB[15]\, Y => AND2_30_Y);
    
    MX2_22 : MX2
      port map(A => AND2_104_Y, B => BUFF_11_Y, S => AND2A_0_Y, Y
         => MX2_22_Y);
    
    MAJ3_7 : MAJ3
      port map(A => XOR2_65_Y, B => AND2_54_Y, C => \PP5[3]\, Y
         => MAJ3_7_Y);
    
    BUFF_38 : BUFF
      port map(A => DataB(3), Y => BUFF_38_Y);
    
    AND2_28 : AND2
      port map(A => DataB(0), B => BUFF_11_Y, Y => AND2_28_Y);
    
    MAJ3_15 : MAJ3
      port map(A => MAJ3_12_Y, B => XOR3_10_Y, C => MAJ3_26_Y, Y
         => MAJ3_15_Y);
    
    XOR2_53 : XOR2
      port map(A => \SumA[4]\, B => \SumB[4]\, Y => XOR2_53_Y);
    
    MX2_51 : MX2
      port map(A => AND2_135_Y, B => BUFF_18_Y, S => NOR2_9_Y, Y
         => MX2_51_Y);
    
    XOR2_4 : XOR2
      port map(A => \S[1]\, B => \SumB[2]\, Y => XOR2_4_Y);
    
    \XOR2_PP5[11]\ : XOR2
      port map(A => MX2_26_Y, B => BUFF_20_Y, Y => \PP5[11]\);
    
    AO1_53 : AO1
      port map(A => AND2_22_Y, B => AO1_34_Y, C => AO1_7_Y, Y => 
        AO1_53_Y);
    
    XOR2_59 : XOR2
      port map(A => AND2_96_Y, B => BUFF_46_Y, Y => XOR2_59_Y);
    
    XNOR2_13 : XNOR2
      port map(A => DataB(8), B => BUFF_34_Y, Y => XNOR2_13_Y);
    
    XOR3_13 : XOR3
      port map(A => XOR3_23_Y, B => MAJ3_21_Y, C => MAJ3_7_Y, Y
         => XOR3_13_Y);
    
    \XOR2_PP1[9]\ : XOR2
      port map(A => MX2_3_Y, B => BUFF_38_Y, Y => \PP1[9]\);
    
    MX2_13 : MX2
      port map(A => AND2_90_Y, B => BUFF_37_Y, S => AND2A_2_Y, Y
         => MX2_13_Y);
    
    AO1_7 : AO1
      port map(A => XOR2_5_Y, B => AO1_52_Y, C => AND2_35_Y, Y
         => AO1_7_Y);
    
    MX2_58 : MX2
      port map(A => AND2_75_Y, B => BUFF_5_Y, S => NOR2_2_Y, Y
         => MX2_58_Y);
    
    XOR3_19 : XOR3
      port map(A => XOR3_10_Y, B => MAJ3_26_Y, C => MAJ3_12_Y, Y
         => XOR3_19_Y);
    
    \XOR2_PP5[6]\ : XOR2
      port map(A => MX2_40_Y, B => BUFF_39_Y, Y => \PP5[6]\);
    
    AND2_112 : AND2
      port map(A => \SumA[8]\, B => \SumB[8]\, Y => AND2_112_Y);
    
    \XOR3_SumB[18]\ : XOR3
      port map(A => MAJ3_0_Y, B => AND2_145_Y, C => XOR3_16_Y, Y
         => \SumB[18]\);
    
    AND2_105 : AND2
      port map(A => \SumA[4]\, B => \SumB[4]\, Y => AND2_105_Y);
    
    AND2_52 : AND2
      port map(A => XOR2_34_Y, B => BUFF_18_Y, Y => AND2_52_Y);
    
    AO1_52 : AO1
      port map(A => XOR2_29_Y, B => AND2_50_Y, C => AND2_7_Y, Y
         => AO1_52_Y);
    
    XOR2_57 : XOR2
      port map(A => \SumA[20]\, B => \SumB[20]\, Y => XOR2_57_Y);
    
    \XOR2_Mult[0]\ : XOR2
      port map(A => XOR2_40_Y, B => DataB(1), Y => Mult(0));
    
    AO1_41 : AO1
      port map(A => AND2_15_Y, B => AO1_27_Y, C => AO1_10_Y, Y
         => AO1_41_Y);
    
    AND2_1 : AND2
      port map(A => XOR2_54_Y, B => BUFF_11_Y, Y => AND2_1_Y);
    
    OR3_3 : OR3
      port map(A => DataB(5), B => DataB(6), C => DataB(7), Y => 
        OR3_3_Y);
    
    MX2_67 : MX2
      port map(A => AND2_79_Y, B => BUFF_36_Y, S => NOR2_14_Y, Y
         => MX2_67_Y);
    
    AND2_7 : AND2
      port map(A => \SumA[17]\, B => \SumB[17]\, Y => AND2_7_Y);
    
    MAJ3_13 : MAJ3
      port map(A => MAJ3_7_Y, B => XOR3_23_Y, C => MAJ3_21_Y, Y
         => MAJ3_13_Y);
    
    XOR3_17 : XOR3
      port map(A => \E[1]\, B => \E[0]\, C => \PP2[11]\, Y => 
        XOR3_17_Y);
    
    \XOR2_PP3[1]\ : XOR2
      port map(A => MX2_70_Y, B => BUFF_13_Y, Y => \PP3[1]\);
    
    \XOR2_PP0[6]\ : XOR2
      port map(A => MX2_7_Y, B => BUFF_22_Y, Y => \PP0[6]\);
    
    AND2_100 : AND2
      port map(A => XOR2_36_Y, B => BUFF_10_Y, Y => AND2_100_Y);
    
    XOR2_15 : XOR2
      port map(A => \SumA[9]\, B => \SumB[9]\, Y => XOR2_15_Y);
    
    AND2_43 : AND2
      port map(A => AND2_110_Y, B => AND2_126_Y, Y => AND2_43_Y);
    
    MAJ3_6 : MAJ3
      port map(A => MAJ3_8_Y, B => XOR3_17_Y, C => MAJ3_30_Y, Y
         => MAJ3_6_Y);
    
    BUFF_5 : BUFF
      port map(A => DataA(3), Y => BUFF_5_Y);
    
    MX2_29 : MX2
      port map(A => AND2_106_Y, B => BUFF_37_Y, S => NOR2_7_Y, Y
         => MX2_29_Y);
    
    MAJ3_5 : MAJ3
      port map(A => AND2_16_Y, B => \PP5[6]\, C => \PP4[8]\, Y
         => MAJ3_5_Y);
    
    MX2_4 : MX2
      port map(A => AND2_5_Y, B => BUFF_18_Y, S => NOR2_14_Y, Y
         => MX2_4_Y);
    
    AND2_144 : AND2
      port map(A => AND2_37_Y, B => AND2_47_Y, Y => AND2_144_Y);
    
    \XOR2_Mult[23]\ : XOR2
      port map(A => XOR2_70_Y, B => AO1_5_Y, Y => Mult(23));
    
    AND3_4 : AND3
      port map(A => DataB(9), B => DataB(10), C => DataB(11), Y
         => AND3_4_Y);
    
    \XOR2_PP2[2]\ : XOR2
      port map(A => MX2_65_Y, B => BUFF_31_Y, Y => \PP2[2]\);
    
    AND2_63 : AND2
      port map(A => \PP0[1]\, B => \S[0]\, Y => AND2_63_Y);
    
    \XOR2_PP1[0]\ : XOR2
      port map(A => XOR2_69_Y, B => DataB(3), Y => \PP1[0]\);
    
    NOR2_1 : NOR2
      port map(A => XOR2_12_Y, B => XNOR2_7_Y, Y => NOR2_1_Y);
    
    BUFF_19 : BUFF
      port map(A => DataA(1), Y => BUFF_19_Y);
    
    XNOR2_8 : XNOR2
      port map(A => DataB(4), B => BUFF_29_Y, Y => XNOR2_8_Y);
    
    AND2_109 : AND2
      port map(A => DataB(0), B => BUFF_8_Y, Y => AND2_109_Y);
    
    MAJ3_10 : MAJ3
      port map(A => \PP2[6]\, B => \PP1[8]\, C => \PP0[10]\, Y
         => MAJ3_10_Y);
    
    AND2_123 : AND2
      port map(A => AND2_153_Y, B => XOR2_71_Y, Y => AND2_123_Y);
    
    MAJ3_26 : MAJ3
      port map(A => \PP2[11]\, B => \E[1]\, C => \E[0]\, Y => 
        MAJ3_26_Y);
    
    NOR2_8 : NOR2
      port map(A => XOR2_23_Y, B => XNOR2_6_Y, Y => NOR2_8_Y);
    
    MX2_32 : MX2
      port map(A => AND2_133_Y, B => BUFF_37_Y, S => NOR2_10_Y, Y
         => MX2_32_Y);
    
    \MX2_PP3[12]\ : MX2
      port map(A => MX2_25_Y, B => AO1_9_Y, S => NOR2_14_Y, Y => 
        \PP3[12]\);
    
    AND2_141 : AND2
      port map(A => \PP4[3]\, B => \PP3[5]\, Y => AND2_141_Y);
    
    \AOI1_E[4]\ : AOI1
      port map(A => XOR2_51_Y, B => OR3_5_Y, C => AND3_0_Y, Y => 
        \E[4]\);
    
    AND2_95 : AND2
      port map(A => XOR2_77_Y, B => BUFF_44_Y, Y => AND2_95_Y);
    
    AND3_1 : AND3
      port map(A => \GND\, B => DataB(0), C => DataB(1), Y => 
        AND3_1_Y);
    
    XOR2_22 : XOR2
      port map(A => AND2_14_Y, B => BUFF_31_Y, Y => XOR2_22_Y);
    
    MX2_11 : MX2
      port map(A => AND2_52_Y, B => BUFF_44_Y, S => NOR2_0_Y, Y
         => MX2_11_Y);
    
    AND2_27 : AND2
      port map(A => XOR2_75_Y, B => XOR2_68_Y, Y => AND2_27_Y);
    
    BUFF_29 : BUFF
      port map(A => DataB(5), Y => BUFF_29_Y);
    
    XOR2_32 : XOR2
      port map(A => AND2_92_Y, B => BUFF_13_Y, Y => XOR2_32_Y);
    
    AND2_14 : AND2
      port map(A => XOR2_73_Y, B => BUFF_28_Y, Y => AND2_14_Y);
    
    \XOR2_Mult[20]\ : XOR2
      port map(A => XOR2_42_Y, B => AO1_53_Y, Y => Mult(20));
    
    AND2_74 : AND2
      port map(A => XOR2_43_Y, B => BUFF_24_Y, Y => AND2_74_Y);
    
    AND2_82 : AND2
      port map(A => XOR2_73_Y, B => BUFF_19_Y, Y => AND2_82_Y);
    
    AND2_58 : AND2
      port map(A => XOR2_44_Y, B => XOR2_14_Y, Y => AND2_58_Y);
    
    XOR2_10 : XOR2
      port map(A => \E[5]\, B => \SumB[22]\, Y => XOR2_10_Y);
    
    MX2_18 : MX2
      port map(A => AND2_157_Y, B => BUFF_24_Y, S => NOR2_3_Y, Y
         => MX2_18_Y);
    
    XOR3_28 : XOR3
      port map(A => \PP3[11]\, B => \E[2]\, C => \PP4[9]\, Y => 
        XOR3_28_Y);
    
    XNOR2_9 : XNOR2
      port map(A => DataB(4), B => BUFF_31_Y, Y => XNOR2_9_Y);
    
    \XOR2_PP3[6]\ : XOR2
      port map(A => MX2_62_Y, B => BUFF_4_Y, Y => \PP3[6]\);
    
    AO1_20 : AO1
      port map(A => AND2_39_Y, B => AO1_27_Y, C => AO1_29_Y, Y
         => AO1_20_Y);
    
    XOR2_80 : XOR2
      port map(A => \SumA[7]\, B => \SumB[7]\, Y => XOR2_80_Y);
    
    \XOR2_Mult[22]\ : XOR2
      port map(A => XOR2_56_Y, B => AO1_20_Y, Y => Mult(22));
    
    XNOR2_12 : XNOR2
      port map(A => DataB(6), B => BUFF_35_Y, Y => XNOR2_12_Y);
    
    \AND2_S[4]\ : AND2
      port map(A => XOR2_0_Y, B => DataB(9), Y => \S[4]\);
    
    XOR2_26 : XOR2
      port map(A => \SumA[10]\, B => \SumB[10]\, Y => XOR2_26_Y);
    
    \XOR2_PP2[1]\ : XOR2
      port map(A => MX2_28_Y, B => BUFF_31_Y, Y => \PP2[1]\);
    
    XOR2_36 : XOR2
      port map(A => BUFF_3_Y, B => DataB(10), Y => XOR2_36_Y);
    
    MX2_44 : MX2
      port map(A => AND2_121_Y, B => BUFF_7_Y, S => AND2A_2_Y, Y
         => MX2_44_Y);
    
    AND2_21 : AND2
      port map(A => XOR2_8_Y, B => BUFF_8_Y, Y => AND2_21_Y);
    
    BUFF_34 : BUFF
      port map(A => DataB(9), Y => BUFF_34_Y);
    
    \XOR2_PP2[10]\ : XOR2
      port map(A => MX2_17_Y, B => BUFF_2_Y, Y => \PP2[10]\);
    
    AO1_57 : AO1
      port map(A => AND2_69_Y, B => AO1_55_Y, C => AO1_38_Y, Y
         => AO1_57_Y);
    
    MX2_63 : MX2
      port map(A => AND2_132_Y, B => BUFF_24_Y, S => NOR2_4_Y, Y
         => MX2_63_Y);
    
    MX2_39 : MX2
      port map(A => AND2_17_Y, B => BUFF_44_Y, S => NOR2_6_Y, Y
         => MX2_39_Y);
    
    AND2_3 : AND2
      port map(A => XOR2_16_Y, B => BUFF_32_Y, Y => AND2_3_Y);
    
    \XOR2_PP1[6]\ : XOR2
      port map(A => MX2_14_Y, B => BUFF_12_Y, Y => \PP1[6]\);
    
    \XOR3_SumB[10]\ : XOR3
      port map(A => MAJ3_22_Y, B => XOR3_2_Y, C => XOR3_12_Y, Y
         => \SumB[10]\);
    
    \XOR2_PP3[4]\ : XOR2
      port map(A => MX2_43_Y, B => BUFF_13_Y, Y => \PP3[4]\);
    
    AO1_25 : AO1
      port map(A => XOR2_20_Y, B => AO1_35_Y, C => AND2_112_Y, Y
         => AO1_25_Y);
    
    \MAJ3_SumA[15]\ : MAJ3
      port map(A => XOR3_8_Y, B => MAJ3_13_Y, C => XOR3_5_Y, Y
         => \SumA[15]\);
    
    XOR2_78 : XOR2
      port map(A => \SumA[14]\, B => \SumB[14]\, Y => XOR2_78_Y);
    
    AND2_93 : AND2
      port map(A => XOR2_8_Y, B => BUFF_7_Y, Y => AND2_93_Y);
    
    AND2A_0 : AND2A
      port map(A => DataB(0), B => BUFF_0_Y, Y => AND2A_0_Y);
    
    AND2_26 : AND2
      port map(A => \PP4[5]\, B => \PP3[7]\, Y => AND2_26_Y);
    
    XOR3_9 : XOR3
      port map(A => XOR2_13_Y, B => \S[4]\, C => MAJ3_16_Y, Y => 
        XOR3_9_Y);
    
    XOR2_51 : XOR2
      port map(A => BUFF_16_Y, B => DataB(9), Y => XOR2_51_Y);
    
    AND2_88 : AND2
      port map(A => XOR2_34_Y, B => BUFF_44_Y, Y => AND2_88_Y);
    
    MAJ3_25 : MAJ3
      port map(A => \PP2[7]\, B => \PP1[9]\, C => \PP0[11]\, Y
         => MAJ3_25_Y);
    
    \XOR2_PP4[8]\ : XOR2
      port map(A => MX2_2_Y, B => BUFF_30_Y, Y => \PP4[8]\);
    
    XOR3_11 : XOR3
      port map(A => \PP1[4]\, B => \PP0[6]\, C => \PP2[2]\, Y => 
        XOR3_11_Y);
    
    \AND2_S[0]\ : AND2
      port map(A => XOR2_40_Y, B => DataB(1), Y => \S[0]\);
    
    AND2_118 : AND2
      port map(A => AND2_150_Y, B => XOR2_20_Y, Y => AND2_118_Y);
    
    XOR2_13 : XOR2
      port map(A => \PP1[7]\, B => \PP0[9]\, Y => XOR2_13_Y);
    
    XOR2_2 : XOR2
      port map(A => BUFF_43_Y, B => DataB(8), Y => XOR2_2_Y);
    
    BUFF_42 : BUFF
      port map(A => DataA(10), Y => BUFF_42_Y);
    
    \XOR2_Mult[19]\ : XOR2
      port map(A => XOR2_48_Y, B => AO1_28_Y, Y => Mult(19));
    
    AND2_57 : AND2
      port map(A => XOR2_23_Y, B => BUFF_33_Y, Y => AND2_57_Y);
    
    MX2_71 : MX2
      port map(A => AND2_98_Y, B => BUFF_32_Y, S => NOR2_2_Y, Y
         => MX2_71_Y);
    
    AND2_34 : AND2
      port map(A => AND2_23_Y, B => AND2_136_Y, Y => AND2_34_Y);
    
    XOR2_3 : XOR2
      port map(A => \SumA[20]\, B => \SumB[20]\, Y => XOR2_3_Y);
    
    XOR2_19 : XOR2
      port map(A => \SumA[8]\, B => \SumB[8]\, Y => XOR2_19_Y);
    
    AND2_102 : AND2
      port map(A => XOR2_73_Y, B => BUFF_37_Y, Y => AND2_102_Y);
    
    AO1_2 : AO1
      port map(A => XOR2_51_Y, B => OR3_5_Y, C => AND3_0_Y, Y => 
        AO1_2_Y);
    
    \XOR2_PP3[7]\ : XOR2
      port map(A => MX2_55_Y, B => BUFF_4_Y, Y => \PP3[7]\);
    
    MAJ3_30 : MAJ3
      port map(A => \PP2[10]\, B => \PP1[12]\, C => EBAR, Y => 
        MAJ3_30_Y);
    
    XNOR2_3 : XNOR2
      port map(A => DataB(10), B => BUFF_46_Y, Y => XNOR2_3_Y);
    
    XOR2_68 : XOR2
      port map(A => \SumA[3]\, B => \SumB[3]\, Y => XOR2_68_Y);
    
    \MX2_PP0[12]\ : MX2
      port map(A => MX2_23_Y, B => EBAR, S => AND2A_0_Y, Y => 
        \PP0[12]\);
    
    MX2_61 : MX2
      port map(A => AND2_113_Y, B => BUFF_10_Y, S => NOR2_9_Y, Y
         => MX2_61_Y);
    
    \XOR2_PP0[4]\ : XOR2
      port map(A => MX2_48_Y, B => BUFF_25_Y, Y => \PP0[4]\);
    
    \XOR2_PP3[10]\ : XOR2
      port map(A => MX2_67_Y, B => BUFF_35_Y, Y => \PP3[10]\);
    
    \XOR2_PP1[8]\ : XOR2
      port map(A => MX2_49_Y, B => BUFF_12_Y, Y => \PP1[8]\);
    
    BUFF_9 : BUFF
      port map(A => DataA(0), Y => BUFF_9_Y);
    
    MAJ3_23 : MAJ3
      port map(A => \PP2[8]\, B => \PP1[10]\, C => \PP0[12]\, Y
         => MAJ3_23_Y);
    
    AO1_33 : AO1
      port map(A => AND2_37_Y, B => AO1_42_Y, C => AO1_50_Y, Y
         => AO1_33_Y);
    
    BUFF_35 : BUFF
      port map(A => DataB(7), Y => BUFF_35_Y);
    
    AND2_126 : AND2
      port map(A => AND2_24_Y, B => AND2_58_Y, Y => AND2_126_Y);
    
    \XOR2_PP3[9]\ : XOR2
      port map(A => MX2_4_Y, B => BUFF_35_Y, Y => \PP3[9]\);
    
    BUFF_30 : BUFF
      port map(A => DataB(9), Y => BUFF_30_Y);
    
    MX2_68 : MX2
      port map(A => AND2_138_Y, B => BUFF_9_Y, S => NOR2_4_Y, Y
         => MX2_68_Y);
    
    MX2_25 : MX2
      port map(A => BUFF_35_Y, B => XOR2_39_Y, S => XOR2_38_Y, Y
         => MX2_25_Y);
    
    \XOR3_SumB[14]\ : XOR3
      port map(A => MAJ3_13_Y, B => XOR3_5_Y, C => XOR3_8_Y, Y
         => \SumB[14]\);
    
    XOR2_17 : XOR2
      port map(A => \SumA[17]\, B => \SumB[17]\, Y => XOR2_17_Y);
    
    \XOR3_SumB[15]\ : XOR3
      port map(A => MAJ3_6_Y, B => XOR3_29_Y, C => XOR3_19_Y, Y
         => \SumB[15]\);
    
    AND2_154 : AND2
      port map(A => XOR2_41_Y, B => BUFF_5_Y, Y => AND2_154_Y);
    
    AND2_145 : AND2
      port map(A => \PP3[12]\, B => \VCC\, Y => AND2_145_Y);
    
    AND2_51 : AND2
      port map(A => XOR2_37_Y, B => BUFF_27_Y, Y => AND2_51_Y);
    
    AO1_10 : AO1
      port map(A => AND2_44_Y, B => AO1_44_Y, C => AO1_26_Y, Y
         => AO1_10_Y);
    
    OR3_1 : OR3
      port map(A => DataB(1), B => DataB(2), C => DataB(3), Y => 
        OR3_1_Y);
    
    AO1_32 : AO1
      port map(A => AND2_69_Y, B => AO1_55_Y, C => AO1_38_Y, Y
         => AO1_32_Y);
    
    \XOR2_PP5[8]\ : XOR2
      port map(A => MX2_11_Y, B => BUFF_39_Y, Y => \PP5[8]\);
    
    BUFF_1 : BUFF
      port map(A => DataA(4), Y => BUFF_1_Y);
    
    MAJ3_14 : MAJ3
      port map(A => AND2_67_Y, B => \PP4[0]\, C => \PP3[2]\, Y
         => MAJ3_14_Y);
    
    AND2_29 : AND2
      port map(A => AND2_41_Y, B => AND2_39_Y, Y => AND2_29_Y);
    
    BUFF_18 : BUFF
      port map(A => DataA(8), Y => BUFF_18_Y);
    
    MX2_40 : MX2
      port map(A => AND2_149_Y, B => BUFF_33_Y, S => NOR2_0_Y, Y
         => MX2_40_Y);
    
    AND2_140 : AND2
      port map(A => XOR2_12_Y, B => BUFF_41_Y, Y => AND2_140_Y);
    
    \MAJ3_SumB[21]\ : MAJ3
      port map(A => AND2_152_Y, B => \PP5[11]\, C => \E[4]\, Y
         => \SumB[21]\);
    
    \XOR2_PP1[5]\ : XOR2
      port map(A => MX2_32_Y, B => BUFF_12_Y, Y => \PP1[5]\);
    
    MAJ3_20 : MAJ3
      port map(A => MAJ3_28_Y, B => XOR3_4_Y, C => MAJ3_25_Y, Y
         => MAJ3_20_Y);
    
    AND2_151 : AND2
      port map(A => AND2_108_Y, B => AND2_20_Y, Y => AND2_151_Y);
    
    \MX2_PP5[12]\ : MX2
      port map(A => MX2_57_Y, B => AO1_36_Y, S => NOR2_11_Y, Y
         => \PP5[12]\);
    
    \XOR2_PP1[3]\ : XOR2
      port map(A => MX2_31_Y, B => BUFF_17_Y, Y => \PP1[3]\);
    
    \XOR2_PP2[5]\ : XOR2
      port map(A => MX2_29_Y, B => BUFF_29_Y, Y => \PP2[5]\);
    
    \XOR3_SumB[9]\ : XOR3
      port map(A => MAJ3_19_Y, B => XOR3_27_Y, C => XOR3_7_Y, Y
         => \SumB[9]\);
    
    \XOR2_PP2[7]\ : XOR2
      port map(A => MX2_46_Y, B => BUFF_29_Y, Y => \PP2[7]\);
    
    BUFF_2 : BUFF
      port map(A => DataB(5), Y => BUFF_2_Y);
    
    AND2_15 : AND2
      port map(A => AND2_34_Y, B => AND2_44_Y, Y => AND2_15_Y);
    
    AND2_87 : AND2
      port map(A => XOR2_36_Y, B => BUFF_36_Y, Y => AND2_87_Y);
    
    BUFF_28 : BUFF
      port map(A => DataA(0), Y => BUFF_28_Y);
    
    \XOR2_PP4[4]\ : XOR2
      port map(A => MX2_58_Y, B => BUFF_34_Y, Y => \PP4[4]\);
    
    AND2_75 : AND2
      port map(A => XOR2_41_Y, B => BUFF_1_Y, Y => AND2_75_Y);
    
    AND2_127 : AND2
      port map(A => XOR2_11_Y, B => BUFF_15_Y, Y => AND2_127_Y);
    
    AO1_24 : AO1
      port map(A => AND2_58_Y, B => AO1_0_Y, C => AO1_13_Y, Y => 
        AO1_24_Y);
    
    BUFF_0 : BUFF
      port map(A => DataB(1), Y => BUFF_0_Y);
    
    AND2_56 : AND2
      port map(A => DataB(0), B => BUFF_27_Y, Y => AND2_56_Y);
    
    \XOR2_PP2[9]\ : XOR2
      port map(A => MX2_66_Y, B => BUFF_2_Y, Y => \PP2[9]\);
    
    AND2_149 : AND2
      port map(A => XOR2_34_Y, B => BUFF_14_Y, Y => AND2_149_Y);
    
    XOR2_52 : XOR2
      port map(A => \SumA[5]\, B => \SumB[5]\, Y => XOR2_52_Y);
    
    AND2_113 : AND2
      port map(A => XOR2_2_Y, B => BUFF_16_Y, Y => AND2_113_Y);
    
    AO1_15 : AO1
      port map(A => XOR2_26_Y, B => AO1_55_Y, C => AND2_148_Y, Y
         => AO1_15_Y);
    
    XNOR2_6 : XNOR2
      port map(A => DataB(8), B => BUFF_30_Y, Y => XNOR2_6_Y);
    
    \MAJ3_SumA[13]\ : MAJ3
      port map(A => XOR3_0_Y, B => MAJ3_20_Y, C => XOR3_25_Y, Y
         => \SumA[13]\);
    
    AO1_51 : AO1
      port map(A => XOR2_44_Y, B => AO1_0_Y, C => AND2_155_Y, Y
         => AO1_51_Y);
    
    \MAJ3_SumA[6]\ : MAJ3
      port map(A => XOR3_11_Y, B => AND2_77_Y, C => \PP3[0]\, Y
         => \SumA[6]\);
    
    XOR3_12 : XOR3
      port map(A => XOR3_6_Y, B => MAJ3_10_Y, C => MAJ3_18_Y, Y
         => XOR3_12_Y);
    
    BUFF_46 : BUFF
      port map(A => DataB(11), Y => BUFF_46_Y);
    
    XOR3_4 : XOR3
      port map(A => \PP1[10]\, B => \PP0[12]\, C => \PP2[8]\, Y
         => XOR3_4_Y);
    
    XOR3_6 : XOR3
      port map(A => \PP1[9]\, B => \PP0[11]\, C => \PP2[7]\, Y
         => XOR3_6_Y);
    
    AND2_81 : AND2
      port map(A => \SumA[13]\, B => \SumB[13]\, Y => AND2_81_Y);
    
    MX2_46 : MX2
      port map(A => AND2_93_Y, B => BUFF_8_Y, S => NOR2_7_Y, Y
         => MX2_46_Y);
    
    AND2_20 : AND2
      port map(A => AND2_110_Y, B => AND2_24_Y, Y => AND2_20_Y);
    
    AND2_134 : AND2
      port map(A => XOR2_11_Y, B => BUFF_8_Y, Y => AND2_134_Y);
    
    MX2_35 : MX2
      port map(A => AND2_100_Y, B => BUFF_36_Y, S => NOR2_11_Y, Y
         => MX2_35_Y);
    
    AO1_29 : AO1
      port map(A => XOR2_57_Y, B => AO1_44_Y, C => AND2_2_Y, Y
         => AO1_29_Y);
    
    XOR2_56 : XOR2
      port map(A => \SumA[21]\, B => \SumB[21]\, Y => XOR2_56_Y);
    
    XNOR2_5 : XNOR2
      port map(A => DataB(4), B => BUFF_2_Y, Y => XNOR2_5_Y);
    
    MAJ3_4 : MAJ3
      port map(A => \PP5[9]\, B => \PP4[11]\, C => \E[3]\, Y => 
        MAJ3_4_Y);
    
    XOR3_16 : XOR3
      port map(A => \PP4[11]\, B => \E[3]\, C => \PP5[9]\, Y => 
        XOR3_16_Y);
    
    \XOR2_PP2[0]\ : XOR2
      port map(A => XOR2_22_Y, B => DataB(5), Y => \PP2[0]\);
    
    AO1_1 : AO1
      port map(A => AND2_144_Y, B => AO1_22_Y, C => AO1_30_Y, Y
         => AO1_1_Y);
    
    AND2_131 : AND2
      port map(A => XOR2_8_Y, B => BUFF_15_Y, Y => AND2_131_Y);
    
    \XOR2_PP4[11]\ : XOR2
      port map(A => MX2_61_Y, B => BUFF_6_Y, Y => \PP4[11]\);
    
    AO1_37 : AO1
      port map(A => AND2_20_Y, B => AO1_1_Y, C => AO1_56_Y, Y => 
        AO1_37_Y);
    
    MX2_2 : MX2
      port map(A => AND2_124_Y, B => BUFF_44_Y, S => NOR2_8_Y, Y
         => MX2_2_Y);
    
    \XOR2_PP4[9]\ : XOR2
      port map(A => MX2_51_Y, B => BUFF_6_Y, Y => \PP4[9]\);
    
    XNOR2_14 : XNOR2
      port map(A => DataB(2), B => BUFF_12_Y, Y => XNOR2_14_Y);
    
    AND2_13 : AND2
      port map(A => XOR2_12_Y, B => BUFF_11_Y, Y => AND2_13_Y);
    
    AND2_86 : AND2
      port map(A => XOR2_36_Y, B => BUFF_16_Y, Y => AND2_86_Y);
    
    AND2_73 : AND2
      port map(A => AND2_41_Y, B => AND2_65_Y, Y => AND2_73_Y);
    
    AND2_59 : AND2
      port map(A => AND2_119_Y, B => AND2_144_Y, Y => AND2_59_Y);
    
    AO1_40 : AO1
      port map(A => XOR2_46_Y, B => AO1_50_Y, C => AND2_94_Y, Y
         => AO1_40_Y);
    
    AO1_38 : AO1
      port map(A => XOR2_81_Y, B => AND2_148_Y, C => AND2_101_Y, 
        Y => AO1_38_Y);
    
    AO1_9 : AO1
      port map(A => XOR2_39_Y, B => OR3_3_Y, C => AND3_3_Y, Y => 
        AO1_9_Y);
    
    XOR2_11 : XOR2
      port map(A => BUFF_23_Y, B => DataB(2), Y => XOR2_11_Y);
    
    MX2_52 : MX2
      port map(A => AND2_97_Y, B => BUFF_1_Y, S => NOR2_0_Y, Y
         => MX2_52_Y);
    
    AND2_35 : AND2
      port map(A => \SumA[18]\, B => \SumB[18]\, Y => AND2_35_Y);
    
    AND2_108 : AND2
      port map(A => AND2_119_Y, B => AND2_144_Y, Y => AND2_108_Y);
    
    XOR2_48 : XOR2
      port map(A => \SumA[18]\, B => \SumB[18]\, Y => XOR2_48_Y);
    
    \XOR2_PP3[3]\ : XOR2
      port map(A => MX2_18_Y, B => BUFF_13_Y, Y => \PP3[3]\);
    
    XOR2_81 : XOR2
      port map(A => \SumA[11]\, B => \SumB[11]\, Y => XOR2_81_Y);
    
    \XOR2_PP3[8]\ : XOR2
      port map(A => MX2_39_Y, B => BUFF_4_Y, Y => \PP3[8]\);
    
    NOR2_9 : NOR2
      port map(A => XOR2_2_Y, B => XNOR2_4_Y, Y => NOR2_9_Y);
    
    \AOI1_E[1]\ : AOI1
      port map(A => XOR2_30_Y, B => OR3_1_Y, C => AND3_5_Y, Y => 
        \E[1]\);
    
    \XOR2_PP0[3]\ : XOR2
      port map(A => MX2_20_Y, B => BUFF_25_Y, Y => \PP0[3]\);
    
    BUFF_14 : BUFF
      port map(A => DataA(6), Y => BUFF_14_Y);
    
    \MAJ3_SumA[16]\ : MAJ3
      port map(A => XOR3_19_Y, B => MAJ3_6_Y, C => XOR3_29_Y, Y
         => \SumA[16]\);
    
    XOR3_24 : XOR3
      port map(A => \PP5[8]\, B => \PP4[10]\, C => XOR2_60_Y, Y
         => XOR3_24_Y);
    
    XOR2_5 : XOR2
      port map(A => \SumA[18]\, B => \SumB[18]\, Y => XOR2_5_Y);
    
    MAJ3_8 : MAJ3
      port map(A => XOR2_45_Y, B => AND2_26_Y, C => \PP5[4]\, Y
         => MAJ3_8_Y);
    
    \XOR2_PP4[6]\ : XOR2
      port map(A => MX2_12_Y, B => BUFF_30_Y, Y => \PP4[6]\);
    
    \XOR2_PP0[8]\ : XOR2
      port map(A => MX2_44_Y, B => BUFF_22_Y, Y => \PP0[8]\);
    
    XOR3_1 : XOR3
      port map(A => AND2_26_Y, B => \PP5[4]\, C => XOR2_45_Y, Y
         => XOR3_1_Y);
    
    AO1_14 : AO1
      port map(A => XOR2_71_Y, B => AO1_32_Y, C => AND2_116_Y, Y
         => AO1_14_Y);
    
    AO1_45 : AO1
      port map(A => XOR2_68_Y, B => AND2_11_Y, C => AND2_6_Y, Y
         => AO1_45_Y);
    
    AND2_155 : AND2
      port map(A => \SumA[14]\, B => \SumB[14]\, Y => AND2_155_Y);
    
    BUFF_24 : BUFF
      port map(A => DataA(2), Y => BUFF_24_Y);
    
    AND2_142 : AND2
      port map(A => AND2_108_Y, B => AND2_123_Y, Y => AND2_142_Y);
    
    \XOR2_PP0[7]\ : XOR2
      port map(A => MX2_54_Y, B => BUFF_22_Y, Y => \PP0[7]\);
    
    MX2_47 : MX2
      port map(A => BUFF_38_Y, B => XOR2_30_Y, S => XOR2_12_Y, Y
         => MX2_47_Y);
    
    AND2_50 : AND2
      port map(A => \SumA[16]\, B => \SumB[16]\, Y => AND2_50_Y);
    
    \XOR2_Mult[1]\ : XOR2
      port map(A => \PP0[1]\, B => \S[0]\, Y => Mult(1));
    
    MX2_24 : MX2
      port map(A => AND2_88_Y, B => BUFF_14_Y, S => NOR2_0_Y, Y
         => MX2_24_Y);
    
    \MAJ3_SumA[8]\ : MAJ3
      port map(A => XOR3_15_Y, B => MAJ3_11_Y, C => XOR3_22_Y, Y
         => \SumA[8]\);
    
    AND2_150 : AND2
      port map(A => AND2_119_Y, B => AND2_144_Y, Y => AND2_150_Y);
    
    MAJ3_24 : MAJ3
      port map(A => \PP3[10]\, B => \PP2[12]\, C => \VCC\, Y => 
        MAJ3_24_Y);
    
    MX2_59 : MX2
      port map(A => AND2_8_Y, B => BUFF_14_Y, S => NOR2_8_Y, Y
         => MX2_59_Y);
    
    AND2_89 : AND2
      port map(A => AND2_41_Y, B => AND2_15_Y, Y => AND2_89_Y);
    
    AND2_116 : AND2
      port map(A => \SumA[12]\, B => \SumB[12]\, Y => AND2_116_Y);
    
    MX2_3 : MX2
      port map(A => AND2_13_Y, B => BUFF_15_Y, S => NOR2_1_Y, Y
         => MX2_3_Y);
    
    AO1_26 : AO1
      port map(A => XOR2_10_Y, B => AO1_21_Y, C => AND2_130_Y, Y
         => AO1_26_Y);
    
    XOR2_74 : XOR2
      port map(A => \PP1[3]\, B => \PP0[5]\, Y => XOR2_74_Y);
    
    NOR2_11 : NOR2
      port map(A => XOR2_36_Y, B => XNOR2_1_Y, Y => NOR2_11_Y);
    
    AO1_19 : AO1
      port map(A => AND2_33_Y, B => AO1_54_Y, C => AO1_31_Y, Y
         => AO1_19_Y);
    
    XOR3_25 : XOR3
      port map(A => AND2_54_Y, B => \PP5[3]\, C => XOR2_65_Y, Y
         => XOR3_25_Y);
    
    AND2_42 : AND2
      port map(A => XOR2_20_Y, B => XOR2_15_Y, Y => AND2_42_Y);
    
    AND2_33 : AND2
      port map(A => AND2_110_Y, B => AND2_60_Y, Y => AND2_33_Y);
    
    AO1_4 : AO1
      port map(A => XOR2_35_Y, B => AND2_35_Y, C => AND2_114_Y, Y
         => AO1_4_Y);
    
    NOR2_10 : NOR2
      port map(A => XOR2_11_Y, B => XNOR2_14_Y, Y => NOR2_10_Y);
    
    MX2_12 : MX2
      port map(A => AND2_147_Y, B => BUFF_33_Y, S => NOR2_8_Y, Y
         => MX2_12_Y);
    
    AND2_2 : AND2
      port map(A => \SumA[20]\, B => \SumB[20]\, Y => AND2_2_Y);
    
    \XOR3_SumB[6]\ : XOR3
      port map(A => MAJ3_2_Y, B => XOR2_1_Y, C => XOR3_20_Y, Y
         => \SumB[6]\);
    
    BUFF_43 : BUFF
      port map(A => DataB(7), Y => BUFF_43_Y);
    
    AND2_62 : AND2
      port map(A => DataB(0), B => BUFF_7_Y, Y => AND2_62_Y);
    
    BUFF_41 : BUFF
      port map(A => DataA(11), Y => BUFF_41_Y);
    
    AND2_103 : AND2
      port map(A => AND2_128_Y, B => AND2_27_Y, Y => AND2_103_Y);
    
    \XOR2_PP5[0]\ : XOR2
      port map(A => XOR2_59_Y, B => DataB(11), Y => \PP5[0]\);
    
    XNOR2_1 : XNOR2
      port map(A => DataB(10), B => BUFF_20_Y, Y => XNOR2_1_Y);
    
    AND3_2 : AND3
      port map(A => DataB(3), B => DataB(4), C => DataB(5), Y => 
        AND3_2_Y);
    
    \MAJ3_SumA[11]\ : MAJ3
      port map(A => XOR3_12_Y, B => MAJ3_22_Y, C => XOR3_2_Y, Y
         => \SumA[11]\);
    
    BUFF_15 : BUFF
      port map(A => DataA(8), Y => BUFF_15_Y);
    
    BUFF_10 : BUFF
      port map(A => DataA(10), Y => BUFF_10_Y);
    
    XOR2_12 : XOR2
      port map(A => BUFF_23_Y, B => DataB(2), Y => XOR2_12_Y);
    
    NOR2_13 : NOR2
      port map(A => XOR2_73_Y, B => XNOR2_9_Y, Y => NOR2_13_Y);
    
    AND2_117 : AND2
      port map(A => \SumA[21]\, B => \SumB[21]\, Y => AND2_117_Y);
    
    AND2_135 : AND2
      port map(A => XOR2_2_Y, B => BUFF_36_Y, Y => AND2_135_Y);
    
    XOR2_64 : XOR2
      port map(A => \SumA[12]\, B => \SumB[12]\, Y => XOR2_64_Y);
    
    AND2_80 : AND2
      port map(A => AND2_108_Y, B => AND2_153_Y, Y => AND2_80_Y);
    
    XOR2_75 : XOR2
      port map(A => \S[1]\, B => \SumB[2]\, Y => XOR2_75_Y);
    
    \XOR2_Mult[5]\ : XOR2
      port map(A => XOR2_53_Y, B => AO1_42_Y, Y => Mult(5));
    
    BUFF_25 : BUFF
      port map(A => DataB(1), Y => BUFF_25_Y);
    
    BUFF_20 : BUFF
      port map(A => DataB(11), Y => BUFF_20_Y);
    
    XOR3_20 : XOR3
      port map(A => \PP3[1]\, B => \PP2[3]\, C => \S[3]\, Y => 
        XOR3_20_Y);
    
    MAJ3_12 : MAJ3
      port map(A => XOR2_66_Y, B => AND2_125_Y, C => \PP5[5]\, Y
         => MAJ3_12_Y);
    
    AND2_130 : AND2
      port map(A => \E[5]\, B => \SumB[22]\, Y => AND2_130_Y);
    
    MX2_34 : MX2
      port map(A => AND2_122_Y, B => BUFF_36_Y, S => NOR2_9_Y, Y
         => MX2_34_Y);
    
    AO1_31 : AO1
      port map(A => AND2_60_Y, B => AO1_57_Y, C => AO1_51_Y, Y
         => AO1_31_Y);
    
    \XOR2_Mult[7]\ : XOR2
      port map(A => XOR2_67_Y, B => AO1_33_Y, Y => Mult(7));
    
    \AND2_SumA[3]\ : AND2
      port map(A => \PP1[1]\, B => \PP0[3]\, Y => \SumA[3]\);
    
    MX2_19 : MX2
      port map(A => AND2_78_Y, B => BUFF_32_Y, S => NOR2_3_Y, Y
         => MX2_19_Y);
    
    \XOR2_SumA[21]\ : XOR2
      port map(A => \PP5[12]\, B => \VCC\, Y => \SumA[21]\);
    
    MX2_43 : MX2
      port map(A => AND2_146_Y, B => BUFF_5_Y, S => NOR2_3_Y, Y
         => MX2_43_Y);
    
    XOR3_30 : XOR3
      port map(A => AND2_141_Y, B => \PP5[2]\, C => XOR2_61_Y, Y
         => XOR3_30_Y);
    
    XOR2_16 : XOR2
      port map(A => BUFF_21_Y, B => DataB(6), Y => XOR2_16_Y);
    
    NOR2_7 : NOR2
      port map(A => XOR2_8_Y, B => XNOR2_8_Y, Y => NOR2_7_Y);
    
    \XOR2_PP4[2]\ : XOR2
      port map(A => MX2_71_Y, B => BUFF_34_Y, Y => \PP4[2]\);
    
    AND2_24 : AND2
      port map(A => XOR2_71_Y, B => XOR2_27_Y, Y => AND2_24_Y);
    
    AND2_48 : AND2
      port map(A => \SumA[9]\, B => \SumB[9]\, Y => AND2_48_Y);
    
    AO1_44 : AO1
      port map(A => AND2_136_Y, B => AO1_52_Y, C => AO1_4_Y, Y
         => AO1_44_Y);
    
    \XOR3_SumB[12]\ : XOR3
      port map(A => MAJ3_20_Y, B => XOR3_25_Y, C => XOR3_0_Y, Y
         => \SumB[12]\);
    
    \XOR2_Mult[18]\ : XOR2
      port map(A => XOR2_17_Y, B => AO1_12_Y, Y => Mult(18));
    
    \XOR3_SumB[3]\ : XOR3
      port map(A => \PP1[2]\, B => \PP0[4]\, C => \PP2[0]\, Y => 
        \SumB[3]\);
    
    AND2_139 : AND2
      port map(A => AND2_0_Y, B => XOR2_58_Y, Y => AND2_139_Y);
    
    \MAJ3_SumA[20]\ : MAJ3
      port map(A => MAJ3_4_Y, B => XOR2_21_Y, C => \PP5[10]\, Y
         => \SumA[20]\);
    
    MX2_20 : MX2
      port map(A => AND2_56_Y, B => BUFF_26_Y, S => AND2A_1_Y, Y
         => MX2_20_Y);
    
    AND2_68 : AND2
      port map(A => DataB(0), B => BUFF_26_Y, Y => AND2_68_Y);
    
    XOR2_65 : XOR2
      port map(A => \PP4[5]\, B => \PP3[7]\, Y => XOR2_65_Y);
    
    \XOR2_PP0[10]\ : XOR2
      port map(A => MX2_22_Y, B => BUFF_0_Y, Y => \PP0[10]\);
    
    \XOR3_SumB[8]\ : XOR3
      port map(A => MAJ3_14_Y, B => XOR3_18_Y, C => XOR3_9_Y, Y
         => \SumB[8]\);
    
    XOR2_70 : XOR2
      port map(A => \E[5]\, B => \SumB[22]\, Y => XOR2_70_Y);
    
    AO1_49 : AO1
      port map(A => AND2_42_Y, B => AO1_35_Y, C => AO1_55_Y, Y
         => AO1_49_Y);
    
    AO1_16 : AO1
      port map(A => AND2_126_Y, B => AO1_57_Y, C => AO1_24_Y, Y
         => AO1_16_Y);
    
    AND2_5 : AND2
      port map(A => XOR2_38_Y, B => BUFF_36_Y, Y => AND2_5_Y);
    
    AND2_92 : AND2
      port map(A => XOR2_16_Y, B => BUFF_9_Y, Y => AND2_92_Y);
    
    XOR2_28 : XOR2
      port map(A => \PP4[3]\, B => \PP3[5]\, Y => XOR2_28_Y);
    
    NOR2_6 : NOR2
      port map(A => XOR2_77_Y, B => XNOR2_10_Y, Y => NOR2_6_Y);
    
    \XOR2_PP5[10]\ : XOR2
      port map(A => MX2_35_Y, B => BUFF_20_Y, Y => \PP5[10]\);
    
    XOR2_38 : XOR2
      port map(A => BUFF_21_Y, B => DataB(6), Y => XOR2_38_Y);
    
    BUFF_32 : BUFF
      port map(A => DataA(1), Y => BUFF_32_Y);
    
    MX2_62 : MX2
      port map(A => AND2_111_Y, B => BUFF_33_Y, S => NOR2_6_Y, Y
         => MX2_62_Y);
    
    \XOR2_PP1[11]\ : XOR2
      port map(A => MX2_5_Y, B => BUFF_38_Y, Y => \PP1[11]\);
    
    AND2_148 : AND2
      port map(A => \SumA[10]\, B => \SumB[10]\, Y => AND2_148_Y);
    
    AND2_152 : AND2
      port map(A => \PP4[12]\, B => \VCC\, Y => AND2_152_Y);
    
    XOR2_1 : XOR2
      port map(A => \PP1[5]\, B => \PP0[7]\, Y => XOR2_1_Y);
    
    MX2_41 : MX2
      port map(A => AND2_57_Y, B => BUFF_1_Y, S => NOR2_8_Y, Y
         => MX2_41_Y);
    
    \XOR3_SumB[7]\ : XOR3
      port map(A => MAJ3_11_Y, B => XOR3_22_Y, C => XOR3_15_Y, Y
         => \SumB[7]\);
    
    XOR3_23 : XOR3
      port map(A => \PP1[12]\, B => EBAR, C => \PP2[10]\, Y => 
        XOR3_23_Y);
    
    \MAJ3_SumA[18]\ : MAJ3
      port map(A => XOR3_24_Y, B => MAJ3_27_Y, C => MAJ3_17_Y, Y
         => \SumA[18]\);
    
    \XOR2_PP1[7]\ : XOR2
      port map(A => MX2_37_Y, B => BUFF_12_Y, Y => \PP1[7]\);
    
    XOR2_8 : XOR2
      port map(A => BUFF_40_Y, B => DataB(4), Y => XOR2_8_Y);
    
    \MAJ3_SumA[5]\ : MAJ3
      port map(A => XOR2_74_Y, B => \S[2]\, C => \PP2[1]\, Y => 
        \SumA[5]\);
    
    MX2_26 : MX2
      port map(A => AND2_86_Y, B => BUFF_10_Y, S => NOR2_11_Y, Y
         => MX2_26_Y);
    
    XOR3_29 : XOR3
      port map(A => \PP5[6]\, B => \PP4[8]\, C => AND2_16_Y, Y
         => XOR3_29_Y);
    
    MX2_48 : MX2
      port map(A => AND2_115_Y, B => BUFF_27_Y, S => AND2A_1_Y, Y
         => MX2_48_Y);
    
    MX2_55 : MX2
      port map(A => AND2_95_Y, B => BUFF_14_Y, S => NOR2_6_Y, Y
         => MX2_55_Y);
    
    \XOR2_Mult[11]\ : XOR2
      port map(A => XOR2_76_Y, B => AO1_49_Y, Y => Mult(11));
    
    XOR2_60 : XOR2
      port map(A => \PP3[12]\, B => \VCC\, Y => XOR2_60_Y);
    
    AND2_106 : AND2
      port map(A => XOR2_8_Y, B => BUFF_45_Y, Y => AND2_106_Y);
    
    \XOR2_PP5[5]\ : XOR2
      port map(A => MX2_52_Y, B => BUFF_39_Y, Y => \PP5[5]\);
    
    AO1_50 : AO1
      port map(A => XOR2_52_Y, B => AND2_105_Y, C => AND2_61_Y, Y
         => AO1_50_Y);
    
    AND2_47 : AND2
      port map(A => XOR2_46_Y, B => XOR2_31_Y, Y => AND2_47_Y);
    
    \XOR2_Mult[9]\ : XOR2
      port map(A => XOR2_19_Y, B => AO1_35_Y, Y => Mult(9));
    
    \MAJ3_SumA[4]\ : MAJ3
      port map(A => \PP2[0]\, B => \PP1[2]\, C => \PP0[4]\, Y => 
        \SumA[4]\);
    
    MX2_30 : MX2
      port map(A => AND2_143_Y, B => BUFF_28_Y, S => NOR2_12_Y, Y
         => MX2_30_Y);
    
    XOR3_27 : XOR3
      port map(A => \PP4[2]\, B => \PP3[4]\, C => \PP5[0]\, Y => 
        XOR3_27_Y);
    
    AND2_54 : AND2
      port map(A => \PP4[4]\, B => \PP3[6]\, Y => AND2_54_Y);
    
    AO1_6 : AO1
      port map(A => XOR2_75_Y, B => AO1_39_Y, C => AND2_11_Y, Y
         => AO1_6_Y);
    
    XNOR2_10 : XNOR2
      port map(A => DataB(6), B => BUFF_4_Y, Y => XNOR2_10_Y);
    
    MX2_69 : MX2
      port map(A => BUFF_2_Y, B => XOR2_55_Y, S => XOR2_54_Y, Y
         => MX2_69_Y);
    
    \AND2_S[5]\ : AND2
      port map(A => XOR2_59_Y, B => DataB(11), Y => \S[5]\);
    
    \XOR2_PP4[5]\ : XOR2
      port map(A => MX2_41_Y, B => BUFF_30_Y, Y => \PP4[5]\);
    
    \XOR2_PP3[2]\ : XOR2
      port map(A => MX2_19_Y, B => BUFF_13_Y, Y => \PP3[2]\);
    
    AND2_67 : AND2
      port map(A => \PP1[5]\, B => \PP0[7]\, Y => AND2_67_Y);
    
    \XOR2_Mult[14]\ : XOR2
      port map(A => XOR2_9_Y, B => AO1_3_Y, Y => Mult(14));
    
    \XOR2_PP5[7]\ : XOR2
      port map(A => MX2_24_Y, B => BUFF_39_Y, Y => \PP5[7]\);
    
    AND3_5 : AND3
      port map(A => DataB(1), B => DataB(2), C => DataB(3), Y => 
        AND3_5_Y);
    
    AND2_8 : AND2
      port map(A => XOR2_23_Y, B => BUFF_44_Y, Y => AND2_8_Y);
    
    MAJ3_0 : MAJ3
      port map(A => XOR2_60_Y, B => \PP5[8]\, C => \PP4[10]\, Y
         => MAJ3_0_Y);
    
    XNOR2_11 : XNOR2
      port map(A => DataB(2), B => BUFF_17_Y, Y => XNOR2_11_Y);
    
    AND2_98 : AND2
      port map(A => XOR2_41_Y, B => BUFF_24_Y, Y => AND2_98_Y);
    
    NOR2_0 : NOR2
      port map(A => XOR2_34_Y, B => XNOR2_2_Y, Y => NOR2_0_Y);
    
    MAJ3_11 : MAJ3
      port map(A => \S[3]\, B => \PP3[1]\, C => \PP2[3]\, Y => 
        MAJ3_11_Y);
    
    XOR2_73 : XOR2
      port map(A => BUFF_40_Y, B => DataB(4), Y => XOR2_73_Y);
    
    AND2_41 : AND2
      port map(A => AND2_59_Y, B => AND2_43_Y, Y => AND2_41_Y);
    
    XOR2_79 : XOR2
      port map(A => \PP0[1]\, B => \S[0]\, Y => XOR2_79_Y);
    
    XOR2_44 : XOR2
      port map(A => \SumA[14]\, B => \SumB[14]\, Y => XOR2_44_Y);
    
    OR3_5 : OR3
      port map(A => DataB(7), B => DataB(8), C => DataB(9), Y => 
        OR3_5_Y);
    
    \AND2_S[1]\ : AND2
      port map(A => XOR2_69_Y, B => DataB(3), Y => \S[1]\);
    
    AO1_55 : AO1
      port map(A => XOR2_15_Y, B => AND2_112_Y, C => AND2_48_Y, Y
         => AO1_55_Y);
    
    AND2_132 : AND2
      port map(A => XOR2_43_Y, B => BUFF_5_Y, Y => AND2_132_Y);
    
    AND2_107 : AND2
      port map(A => AND2_37_Y, B => XOR2_46_Y, Y => AND2_107_Y);
    
    \XOR2_PP1[4]\ : XOR2
      port map(A => MX2_36_Y, B => BUFF_17_Y, Y => \PP1[4]\);
    
    BUFF_36 : BUFF
      port map(A => DataA(9), Y => BUFF_36_Y);
    
    AND2_61 : AND2
      port map(A => \SumA[5]\, B => \SumB[5]\, Y => AND2_61_Y);
    
    AND2_143 : AND2
      port map(A => XOR2_37_Y, B => BUFF_19_Y, Y => AND2_143_Y);
    
    AND3_3 : AND3
      port map(A => DataB(5), B => DataB(6), C => DataB(7), Y => 
        AND3_3_Y);
    
    XOR2_77 : XOR2
      port map(A => BUFF_21_Y, B => DataB(6), Y => XOR2_77_Y);
    
    AO1_46 : AO1
      port map(A => AND2_83_Y, B => AO1_1_Y, C => AO1_15_Y, Y => 
        AO1_46_Y);
    
    MAJ3_22 : MAJ3
      port map(A => MAJ3_9_Y, B => XOR3_14_Y, C => AND2_137_Y, Y
         => MAJ3_22_Y);
    
    MX2_36 : MX2
      port map(A => AND2_156_Y, B => BUFF_27_Y, S => NOR2_12_Y, Y
         => MX2_36_Y);
    
    MX2_15 : MX2
      port map(A => AND2_71_Y, B => BUFF_5_Y, S => NOR2_4_Y, Y
         => MX2_15_Y);
    
    AND2_25 : AND2
      port map(A => XOR2_41_Y, B => BUFF_32_Y, Y => AND2_25_Y);
    
    \AOI1_E[3]\ : AOI1
      port map(A => XOR2_39_Y, B => OR3_3_Y, C => AND3_3_Y, Y => 
        \E[3]\);
    
    \AND2_S[2]\ : AND2
      port map(A => XOR2_22_Y, B => DataB(5), Y => \S[2]\);
    
    XOR2_63 : XOR2
      port map(A => \SumA[4]\, B => \SumB[4]\, Y => XOR2_63_Y);
    
    AND2_46 : AND2
      port map(A => XOR2_54_Y, B => BUFF_41_Y, Y => AND2_46_Y);
    
    AND2_84 : AND2
      port map(A => \SumA[7]\, B => \SumB[7]\, Y => AND2_84_Y);
    
    NOR2_14 : NOR2
      port map(A => XOR2_38_Y, B => XNOR2_12_Y, Y => NOR2_14_Y);
    
    XOR2_69 : XOR2
      port map(A => AND2_53_Y, B => BUFF_17_Y, Y => XOR2_69_Y);
    
    MX2_27 : MX2
      port map(A => AND2_72_Y, B => BUFF_42_Y, S => AND2A_0_Y, Y
         => MX2_27_Y);
    
    AO1_5 : AO1
      port map(A => AND2_65_Y, B => AO1_27_Y, C => AO1_23_Y, Y
         => AO1_5_Y);
    
    AND2_124 : AND2
      port map(A => XOR2_23_Y, B => BUFF_18_Y, Y => AND2_124_Y);
    
    XOR2_45 : XOR2
      port map(A => \PP4[6]\, B => \PP3[8]\, Y => XOR2_45_Y);
    
    AND2_66 : AND2
      port map(A => AND2_103_Y, B => AND2_37_Y, Y => AND2_66_Y);
    
    AO1_8 : AO1
      port map(A => XOR2_31_Y, B => AND2_94_Y, C => AND2_84_Y, Y
         => AO1_8_Y);
    
    \XOR2_PP2[3]\ : XOR2
      port map(A => MX2_42_Y, B => BUFF_31_Y, Y => \PP2[3]\);
    
    \AOI1_E[5]\ : AOI1
      port map(A => XOR2_47_Y, B => OR3_2_Y, C => AND3_4_Y, Y => 
        \E[5]\);
    
    MX2_5 : MX2
      port map(A => AND2_140_Y, B => BUFF_42_Y, S => NOR2_1_Y, Y
         => MX2_5_Y);
    
    \XOR3_SumB[11]\ : XOR3
      port map(A => MAJ3_29_Y, B => XOR3_30_Y, C => XOR3_3_Y, Y
         => \SumB[11]\);
    
    XOR2_67 : XOR2
      port map(A => \SumA[6]\, B => \SumB[6]\, Y => XOR2_67_Y);
    
    XOR3_0 : XOR3
      port map(A => XOR3_21_Y, B => MAJ3_23_Y, C => MAJ3_3_Y, Y
         => XOR3_0_Y);
    
    AND2_121 : AND2
      port map(A => DataB(0), B => BUFF_15_Y, Y => AND2_121_Y);
    
    MX2_9 : MX2
      port map(A => AND2_76_Y, B => BUFF_11_Y, S => NOR2_1_Y, Y
         => MX2_9_Y);
    
    \XOR2_Mult[4]\ : XOR2
      port map(A => XOR2_24_Y, B => AO1_6_Y, Y => Mult(4));
    
    NOR2_4 : NOR2
      port map(A => XOR2_43_Y, B => XNOR2_3_Y, Y => NOR2_4_Y);
    
    AND2_97 : AND2
      port map(A => XOR2_34_Y, B => BUFF_33_Y, Y => AND2_97_Y);
    
    XOR3_5 : XOR3
      port map(A => AND2_125_Y, B => \PP5[5]\, C => XOR2_66_Y, Y
         => XOR3_5_Y);
    
    \XOR3_SumB[5]\ : XOR3
      port map(A => AND2_77_Y, B => \PP3[0]\, C => XOR3_11_Y, Y
         => \SumB[5]\);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.0.4
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LCLP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_MULT
-- LPM_HINT:XBOOTHMULT
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:D:/Vishak/MIcrosemi/Projects/MAC_32bit-Bias-ReLu-Pooling_bulk2by2_fix/MAC/smartgen\mult_libero
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:UM4X4M1NLPLV
-- SMARTGEN_PACKAGE:vq100
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTHA:12
-- WIDTHB:12
-- REPRESENTATION:SIGNED
-- CLK_EDGE:RISE
-- MAXPGEN:0
-- PIPES:0
-- INST_FA:1
-- HYBRID:0
-- DEBUG:0

-- _End_Comments_

