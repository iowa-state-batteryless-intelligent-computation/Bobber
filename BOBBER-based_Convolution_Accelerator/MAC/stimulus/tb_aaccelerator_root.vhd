----------------------------------------------------------------------

--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity root_spi is
end root_spi;

architecture behavioral of root_spi is

    constant SYSCLK_PERIOD : time := 50 ns; -- 20Mhz
    constant SPI_CLK: time := 500ns; -- 2MHZ spi 

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    signal s_i_MOSI: std_logic;
    signal s_o_MISO: std_logic;
    signal s_i_SCLK: std_logic;
    signal s_i_CS_N: std_logic;
    signal s_o_done: std_logic;
    signal s_o_CALC: std_logic;
    signal s_o_READ: std_logic;
    signal s_o_WRITE: std_logic;
    signal s_flashfreeze: std_logic := '1';
    signal s_boot: std_logic;
    component accelerator_root
        -- ports
        port( 
            -- Inputs
            PAD     : in  std_logic;
            i_CLK   : in  std_logic;
            i_CS_N  : in  std_logic;
            i_MOSI  : in  std_logic;
            i_RST   : in  std_logic;
            i_SCLK  : in  std_logic;
            -- Outputs
            o_CALC  : out std_logic;
            o_DONE  : out std_logic;
            o_MISO  : out std_logic;
            o_READ  : out std_logic;
            o_WRITE : out std_logic;
            o_boot  : out std_logic

        );
    end component;

    
          -- function to convert 8 bit data to spi signals --
    procedure SPI_SEND(
        data: in std_logic_vector(7 downto 0);
        clk: in time;
        signal s_MOSI: out std_logic;
        signal s_sclk: out std_logic;
        signal s_i_CS_N: out std_logic
    ) is
    begin
		s_i_CS_N <= '0';

        s_MOSI <= '0';
         ------ starting 8 cycles of SCLK --------
        s_MOSI <= data(7);
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(6);
        wait for clk; -- end of cycle (eoc) 1
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge    
        s_MOSI <= data(5);
        wait for clk; -- end of cycle (eoc) 2
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(4);
        wait for clk; -- end of cycle (eoc) 3
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(3);
        wait for clk; -- end of cycle (eoc) 4
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(2);
        wait for clk; -- end of cycle (eoc) 5
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(1);
        wait for clk; -- end of cycle (eoc) 6
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(0);
        wait for clk; -- end of cycle (eoc) 7
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        wait for clk; -- end of cycle (eoc) 8
 --       s_sclk <= '1'; -- R edge
 --       wait for clk; 
 --       s_sclk <= '0'; -- F edge
 --       wait for clk; -- end of cycle (eoc) 9?
        ------ END OF 8 CLOCK CYCLES ------

  --      wait for (SPI_CLK);
        s_i_CS_N <= '1';
                wait for ( SYSCLK_PERIOD * 2 );


    end SPI_SEND;


begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 2 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  acCylator_spi
    acCylator_spi_0 : accelerator_root
        -- port map
        port map( 
            -- Inputs
            PAD => s_flashfreeze,
            i_RST => NSYSRESET,
            i_CLK => SYSCLK,
            i_MOSI => s_i_MOSI,
            i_SCLK => s_i_SCLK,
            i_CS_N => s_i_CS_N,

            -- Outputs
            o_DONE =>  s_o_done,
            o_MISO =>  s_o_MISO,
            o_CALC =>  s_o_CALC,
            o_READ =>  s_o_READ,
            o_WRITE => s_o_WRITE,
            o_boot => s_boot
        );
    
    SPI_SIM: process 
    begin
    
        -- initialize values
        s_i_MOSI <= '0';
        s_i_SCLK <= '0';
        s_i_CS_N <= '1';

        wait for ( SYSCLK_PERIOD * 50 );
        
		-- cont_load_input
        SPI_SEND(x"96", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
        -- load data 0
        SPI_SEND(x"08", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"E3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"49", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"2F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"F6", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"19", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"26", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F1", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"DB", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"23", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

 -- load data 0
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"A7", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"59", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"4A", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"DE", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"A0", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"2F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"81", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"B4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		--wait until s_o_done = '1';
		
        SPI_SEND(x"34", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"38", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"FB", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"A3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"FD", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"11", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
        
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"FC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"8B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"30", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        
        SPI_SEND(x"49", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"25", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"D3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"A7", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
 -- load data 0
        
        SPI_SEND(x"19", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"20", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"53", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C5", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
       
        SPI_SEND(x"81", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"13", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"45", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"1C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- wait until s_o_done = '1';
		
		-- load data 1
        
        SPI_SEND(x"EC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"D6", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F0", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        

        -- load data 0
        SPI_SEND(x"C5", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"F6", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"DF", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"FD", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"0C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"EA", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"53", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"38", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"09", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E1", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"6E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

 -- load data 0
        SPI_SEND(x"7F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"D9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"8B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"A7", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"E2", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"58", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"D7", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"CF", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"92", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"17", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"0D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		--wait until s_o_done = '1';
		
        SPI_SEND(x"E4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"13", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"15", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"09", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"C8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"35", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"30", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
        
        SPI_SEND(x"1F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"EC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"81", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"10", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        
        SPI_SEND(x"50", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"0E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"FA", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"9B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
 -- load data 0
        
        SPI_SEND(x"1F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"27", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"0B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"EF", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
       
        SPI_SEND(x"FA", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"15", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"1E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- wait until s_o_done = '1';
		
		-- load data 1
        
        SPI_SEND(x"F2", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"08", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"31", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"3B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        

        
		-- load data 1
        SPI_SEND(x"20", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"2C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"36", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"4D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"5B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"15", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E1", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"03", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

 -- load data 0
        SPI_SEND(x"C1", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"B4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"E4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"D9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"93", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"93", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"81", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"CB", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"DB", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"10", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"22", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		--wait until s_o_done = '1';
		
        SPI_SEND(x"23", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"16", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"B9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"81", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"AA", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"9F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"E0", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"9D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
        
        SPI_SEND(x"E9", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"0B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F0", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        
        SPI_SEND(x"FC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"41", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"4C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
 -- load data 0
        
        SPI_SEND(x"F8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"29", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"68", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"36", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- load data 1
       
        SPI_SEND(x"0B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"EC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"29", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"10", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"1A", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"DA", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        --weights done

		-- cont_load_input

        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
        -- load data 0
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

-- load data 1
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
--
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);    

        
		-- wait until s_o_done = '1';
		
		-- buffer
         -- load data 0
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

		-- load data 1
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		-- wait until s_o_done = '1';
		
		-- load data 1
        SPI_SEND(x"D4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"5E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"39", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"F2", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

-- load data 1
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"1F", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"C8", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"17", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
--
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        SPI_SEND(x"F2", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
        SPI_SEND(x"BC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"23", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"A4", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"71", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"63", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"91", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

-- load data 1
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C2", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"61", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"8E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
--
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);  
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"C3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

-- load data 1
        SPI_SEND(x"7E", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"C3", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
		-- load weight 3
        SPI_SEND(x"46", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"7A", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"BB", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"80", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);      
  
        -- calc
        SPI_SEND(x"45", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"03", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
        
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- calc
       -- SPI_SEND(x"08", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        --SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
       -- SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
       -- SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        
        -- calc
        SPI_SEND(x"85", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"03", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- calc
       -- SPI_SEND(x"08", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        --SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
       -- SPI_SEND(x"01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
       -- SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
	
        -- calc
        SPI_SEND(x"C5", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"0C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"03", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        
         -- calc
        SPI_SEND(x"45", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"0D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        
         -- calc
        SPI_SEND(x"85", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"0D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        
         -- calc
        SPI_SEND(x"C5", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"0D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        
         -- calc
        SPI_SEND(x"45", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- wait until s_o_done = '1';
        
         -- calc
        SPI_SEND(x"85", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

  -- calc
        SPI_SEND(x"C5", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"0C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- wait until s_o_done = '1';

        
         -- calc
        SPI_SEND(x"05", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"13", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        SPI_SEND(x"45", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"19", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"2B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"64", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        
         -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"02", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

       
        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"04", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

        -- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"06", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';

        wait for SPI_CLK * 3;
    end process;

 

end behavioral;

