--------------------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity root_spi is
end root_spi;

architecture behavioral of root_spi is

    constant SYSCLK_PERIOD : time := 50 ns; -- 20MHZ
    constant SPI_CLK: time := 2000ns; -- 1MHZ spi 

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    signal s_i_MOSI: std_logic;
    signal s_o_MISO: std_logic;
    signal s_i_SCLK: std_logic;
    signal s_i_CS_N: std_logic;
    signal s_o_done: std_logic;
    signal s_o_CALC: std_logic;
    signal s_o_READ: std_logic;
    signal s_o_WRITE: std_logic;

    component accelerator_root
        -- ports
        port( 
            -- Inputs
            i_CLK   : in  std_logic;
            i_CS_N  : in  std_logic;
            i_MOSI  : in  std_logic;
            i_RST   : in  std_logic;
            i_SCLK  : in  std_logic;
            -- Outputs
            o_CALC  : out std_logic;
            o_DONE  : out std_logic;
            o_MISO  : out std_logic;
            o_READ  : out std_logic;
            o_WRITE : out std_logic

        );
    end component;

    
          -- function to convert 8 bit data to spi signals --
    procedure SPI_SEND(
        data: in std_logic_vector(7 downto 0);
        clk: in time;
        signal s_MOSI: out std_logic;
        signal s_sclk: out std_logic;
        signal s_i_CS_N: out std_logic
    ) is
    begin
		s_i_CS_N <= '0';

        s_MOSI <= '0';
         ------ starting 8 cycles of SCLK --------
        s_MOSI <= data(7);
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(6);
        wait for clk; -- end of cycle (eoc) 1
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge    
        s_MOSI <= data(5);
        wait for clk; -- end of cycle (eoc) 2
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(4);
        wait for clk; -- end of cycle (eoc) 3
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(3);
        wait for clk; -- end of cycle (eoc) 4
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(2);
        wait for clk; -- end of cycle (eoc) 5
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(1);
        wait for clk; -- end of cycle (eoc) 6
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        s_MOSI <= data(0);
        wait for clk; -- end of cycle (eoc) 7
        s_sclk <= '1'; -- R edge
        wait for clk; 
        s_sclk <= '0'; -- F edge
        wait for clk; -- end of cycle (eoc) 8
 --       s_sclk <= '1'; -- R edge
 --       wait for clk; 
 --       s_sclk <= '0'; -- F edge
 --       wait for clk; -- end of cycle (eoc) 9?
        ------ END OF 8 CLOCK CYCLES ------

  --      wait for (SPI_CLK);
        s_i_CS_N <= '1';
                wait for ( SYSCLK_PERIOD * 2 );


    end SPI_SEND;

    procedure INSTR_SEND (
        data: in std_logic_vector(31 downto 0);
        clk: in time;
        signal s_MOSI: out std_logic;
        signal s_sclk: out std_logic;
        signal s_i_CS_N: out std_logic
    )  is 
    begin
        SPI_SEND(data(7 downto 0), clk, s_MOSI, s_sclk, s_i_CS_N);
        SPI_SEND(data(15 downto 8), clk, s_MOSI, s_sclk, s_i_CS_N);  
        SPI_SEND(data(23 downto 16), clk, s_MOSI, s_sclk, s_i_CS_N); 
        SPI_SEND(data(31 downto 24), clk, s_MOSI, s_sclk, s_i_CS_N); 
    end procedure;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 2 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  acCylator_spi
    acCylator_spi_0 : accelerator_root
        -- port map
        port map( 
            -- Inputs
            i_RST => NSYSRESET,
            i_CLK => SYSCLK,
            i_MOSI => s_i_MOSI,
            i_SCLK => s_i_SCLK,
            i_CS_N => s_i_CS_N,

            -- Outputs
            o_DONE =>  s_o_done,
            o_MISO =>  s_o_MISO,
            o_CALC =>  s_o_CALC,
            o_READ =>  s_o_READ,
            o_WRITE => s_o_WRITE
        );
    
    SPI_SIM: process 
    begin
    
        -- initialize values
        s_i_MOSI <= '0';
        s_i_SCLK <= '0';
        s_i_CS_N <= '1';

        wait for ( SYSCLK_PERIOD * 2 );
        
		-- load 10 data (bank 11)
        INSTR_SEND(x"0000_6060", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6161", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6262", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6363", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6464", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6565", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6666", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6767", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6868", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_6969", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

        -- load 20 weights (bank 01)
        INSTR_SEND(x"0000_200a", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_210b", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_220c", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_230d", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_240e", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_250f", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2610", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2711", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2812", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2913", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 

        INSTR_SEND(x"0000_2A14", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2B15", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2C16", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2D17", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2E18", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_2F19", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_301A", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_311B", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_321C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(x"0000_331D", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
		

        -- 5 2x2 matrix-vector
        INSTR_SEND(b"1100_1100000_0100000_000000000000_00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(b"1100_1100010_0100100_000000000000_01", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(b"1100_1100100_0101000_000000000000_10", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(b"1100_1100110_0101100_000000000000_11", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        INSTR_SEND(b"1100_1101000_0110000_000000000000_00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 


		-- calc
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"0C", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"CC", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 0
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 1
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"20", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';
		
		-- read 2
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
		-- wait until s_o_done = '1';
		
		-- read 3
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"60", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);


		-- buffer
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"60", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N); 
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- buffer
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"00", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"60", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);
        SPI_SEND(x"40", SPI_CLK, s_i_MOSI, s_i_SCLK, s_i_CS_N);

		-- wait until s_o_done = '1';

        wait for SPI_CLK * 3;
    end process;

end behavioral;

