-------------------------------------------------------------------------

-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_barrelShifter_N is
    generic(gCLK_HPER   : time := 10 ns;
			N : integer := 32;
			A : integer := 5;
			SFT_WIDTH : integer := 5); --Generic of type integer for data width, default is 32
end tb_barrelShifter_N;

architecture behavior of tb_barrelShifter_N is

	signal CLK : std_logic := '0';
	constant cCLK_PER : time := gCLK_HPER*2; 
	
	-- barrelshifter_N Module
	component barrelshifter_N is
		generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
		port(
			 i_nLogic_arith : in std_logic;
			 i_shamt        : in std_logic_vector(SFT_WIDTH-1 downto 0);
			 i_A            : in std_logic_vector(N-1 downto 0);
			 o_F            : out std_logic_vector(N-1 downto 0);
			 i_nRight_Left 	:in std_logic
			
		);
	end component;

	-- Signals for test bench 
	signal s_i_nLogic_arith : std_logic;
	signal s_i_Shamt : std_logic_vector(SFT_WIDTH-1 downto 0);
	signal s_i_A : std_logic_vector(N-1 downto 0);
	signal s_o_F : std_logic_vector(N-1 downto 0);
	signal s_i_nRight_Left : std_logic;
	

	begin
		--This first process is to setup the clock for the test bench

		DUT0 : barrelshifter_N
			port MAP(
				i_nLogic_arith => s_i_nLogic_arith,
				i_Shamt => s_i_Shamt,
				i_A => s_i_A,
				i_nRight_Left => s_i_nRight_Left,
				o_F => s_o_F
				
			);


		-- Test Cases
		P_TEST_CASES : process
		begin
		
		-- Case 0: Expected F= 0xFF80_0000
			s_i_A <= X"F000_0000";
			s_i_Shamt <= "00101";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		
		-- Case 1: Expected F= 0x0000_00CA
			s_i_A <= X"0000_CAB0";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		-- Case 2: Expected F= 0xFEED_0000
			s_i_A <= X"FEED_0000";
			s_i_Shamt <= "00000";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		
		-- Case 3: Expected F= 0x0000_BEEF
			s_i_A <= X"00BEEF00";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		-- Case 4: Expected F= 0xFFAC_ADE0
			s_i_A <= X"FACADE00";
			s_i_Shamt <= "00100";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		
		-- Case 5: Expected F= 0000_0FAD
			s_i_A <= X"000FADED";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
			
		-- Case 6: Expected F= 0x0000_0000
			s_i_A <= X"0000_0000";
			s_i_Shamt <= "00100";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		
		-- Case 7: Expected F= 0x00CA_B000
			s_i_A <= X"0000_CAB0";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '1';
			wait for 50 ns;
		-- Case 8: Expected F= 0xf768_0000
			s_i_A <= X"FEED_0000";
			s_i_Shamt <= "00011";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '1';
			wait for 50 ns;
		
		-- Case 9: Expected F= 0xBEEF_0000
			s_i_A <= X"00BEEF00";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '1';
			wait for 50 ns;
		-- Case 10: Expected F= 0xFFAC_ADE0
			s_i_A <= X"FACA_DE00";
			s_i_Shamt <= "00100";
			s_i_nLogic_arith <= '1';
			s_i_nRight_Left  <= '0';
			wait for 50 ns;
		
		-- Case 11: Expected F= 0FAD_ED00
			s_i_A <= X"000FADED";
			s_i_Shamt <= "01000";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left  <= '1';
			wait for 50 ns;
		
		-- Case 12: Expected F = 0x0000001E
			s_i_A <= x"0000_000f";
			s_i_Shamt <= "00001";
			s_i_nLogic_arith <= '0';
			s_i_nRight_Left <= '1'; 
			wait for 50 ns; 

			
		end process;
end behavior;