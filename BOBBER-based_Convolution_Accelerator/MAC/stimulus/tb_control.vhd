----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_control is
end tb_control;

architecture behavioral of tb_control is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '1';
    signal NSYSRESET : std_logic := '0';

    component control
        -- ports
        port( 
            -- Inputs
            i_opcode : in std_logic_vector(3 downto 0);
            i_ADDRA : in std_logic_vector(4 downto 0);
            i_ADDRB : in std_logic_vector(4 downto 0);
            i_CLK : in std_logic;
            i_RST : in std_logic;

            -- Outputs
            o_ADDRA : out std_logic_vector(4 downto 0);
            o_ADDRB : out std_logic_vector(4 downto 0);
            o_RWA : out std_logic;
            o_RWB : out std_logic;
            o_load_weight : out std_logic;
            o_done : out std_logic;

            o_ps_ADDRA : out std_logic_vector(4 downto 0);
            o_ps_ADDRB : out std_logic_vector(4 downto 0);
            o_ps_RWA : out std_logic;
            o_ps_RWB : out std_logic;
            o_load_ps: out std_logic
        );
    end component;


        signal s_i_opcode : std_logic_vector(3 downto 0);
        signal s_i_ADDRA : std_logic_vector(4 downto 0);
        signal s_i_ADDRB : std_logic_vector(4 downto 0);

        -- Outputs
        signal s_o_ADDRA : std_logic_vector(4 downto 0);
        signal s_o_ADDRB : std_logic_vector(4 downto 0);
        signal s_o_RWA : std_logic;
        signal s_o_RWB : std_logic;
        signal s_o_load_weight : std_logic;
        signal s_o_done : std_logic;
        signal s_o_ps_ADDRA : std_logic_vector(4 downto 0);
        signal s_o_ps_ADDRB : std_logic_vector(4 downto 0);
        signal s_o_ps_RWA : std_logic;
        signal s_o_ps_RWB : std_logic;
        signal s_o_load_ps: std_logic;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 3 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  control
    control_0 : control
        -- port map
        port map( 
            -- Inputs
            i_opcode => s_i_opcode,
            i_ADDRA => s_i_ADDRA,
            i_ADDRB => s_i_ADDRB,
            i_CLK => SYSCLK,
            i_RST => NSYSRESET,

            -- Outputs
            o_ADDRA => s_o_ADDRA,
            o_ADDRB => s_o_ADDRB,
            o_RWA =>  s_o_RWA,
            o_RWB =>  s_o_RWB,
            o_load_weight =>  s_o_load_weight,
            o_done =>  s_o_done,
            
            o_ps_ADDRA => s_o_ps_ADDRA,
            o_ps_ADDRB => s_o_ps_ADDRB,
            o_ps_RWA => s_o_ps_RWA,
            o_ps_RWB => s_o_ps_RWB,
            o_load_ps => s_o_load_ps
            -- Inouts

        );

    P_TEST_CASES: process
    begin
        -- wait for 3 cycles (reset) and quarter cycle to simulate signal delay
        wait for ( SYSCLK_PERIOD * 3.25);
        -- instruction #1: load mem 
        s_i_opcode <= b"0000";
        s_i_ADDRA <= b"00100";
        s_i_ADDRB <= b"00000";
        wait for SYSCLK_PERIOD / 2;
        assert(s_o_ADDRA = b"00100" and s_o_RWA = '0' and s_o_RWB = '1') report "instr #1 success" severity note;
        wait for SYSCLK_PERIOD / 2;
        
        -- instruction #2: load mem
        s_i_ADDRA <= b"00101";
        s_i_ADDRB <= b"00001";
        wait for SYSCLK_PERIOD;

        -- instruction #2: load mem
        s_i_ADDRA <= b"00110";
        s_i_ADDRB <= b"00001";
        wait for SYSCLK_PERIOD;

        -- instruction #4: calculate
        s_i_opcode <= b"1100";
        s_i_ADDRA <= b"11000";
        s_i_ADDRB <= b"10000";
        wait for ( SYSCLK_PERIOD * 6);

        -- instruction #5: calculate
        s_i_opcode <= b"0000";
        s_i_ADDRA <= b"00100";
        s_i_ADDRB <= b"00000";
        wait for SYSCLK_PERIOD;

        -- instruction #6: calculate
        s_i_opcode <= b"1100";
        s_i_ADDRA <= b"11000";
        s_i_ADDRB <= b"10000";
        wait for ( SYSCLK_PERIOD * 3);

        -- instruction #7: load mem before calculate is done
        s_i_opcode <= b"0000";
        s_i_ADDRA <= b"00100";
        s_i_ADDRB <= b"00000";
        wait for SYSCLK_PERIOD*10;

    end process;
end behavioral;

