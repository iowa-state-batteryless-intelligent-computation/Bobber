----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_instruction_mult is
end tb_instruction_mult;

architecture behavioral of tb_instruction_mult is

    constant SYSCLK_PERIOD : time := 50 ns; -- 20MHZ

    component instruction_mult is 
    port (
        i_clk: in std_logic; -- system clock
        i_rst: in std_logic; -- reset, async, active high
        i_inst : IN  std_logic_vector(31 downto 0); -- instruction input
        i_spi_done : in std_logic;
        o_inst : OUT std_logic_vector(31 downto 0);  -- output instruction
        o_done : OUT std_logic -- instruction multiply done signal
    );
    end component;

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    signal s_i_inst : std_logic_vector(31 downto 0);
    signal s_o_inst : std_logic_vector(31 downto 0);    
    signal s_i_spi_done: std_logic;
    signal s_o_done : std_logic;

begin
    DUT: instruction_mult port map (
        i_clk => SYSCLK,
        i_rst => NSYSRESET,
        i_inst => s_i_inst,
        i_spi_done => s_i_spi_done,
        o_inst => s_o_inst,
        o_done => s_o_done
    );

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 2 );
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  
    P_TEST_CASES: process 
    begin 
        wait for ( SYSCLK_PERIOD * 3 );
        s_i_spi_done <= '1';
        s_i_inst <= b"0111_01000000000_00000000000_000000";
        wait for SYSCLK_PERIOD;

        s_i_inst <= b"1100_01000000000_00000000000_000000";
        wait for SYSCLK_PERIOD;

        s_i_inst <= b"0110_01000000000_00000000000_000101";
        wait for SYSCLK_PERIOD;
        s_i_spi_done <= '0';
        wait for SYSCLK_PERIOD*8*14;
        
        s_i_spi_done <= '1';
        s_i_inst <= b"0110_01000000000_00000000000_100000";
        wait for SYSCLK_PERIOD;
        s_i_spi_done <= '0';
        wait for SYSCLK_PERIOD*20*14;

    end process;

end behavioral;

