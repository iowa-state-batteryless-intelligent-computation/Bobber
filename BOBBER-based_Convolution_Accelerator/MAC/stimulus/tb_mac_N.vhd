--------------------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_mac_N is
    generic(N : integer := 32; D: integer := 8); -- input data is 8bits, output is 32 bits

end tb_mac_N;

architecture behavioral of tb_mac_N is


    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component mac_N
        -- ports
        port( 
             -- Inputs
            i_weights : in std_logic_vector(D-1 downto 0);
            i_load_weight : in std_logic;
            i_rst : in std_logic;
            i_clk : in std_logic;
            i_input : in std_logic_vector(D-1 downto 0);
            i_ps : in std_logic_vector(N-1 downto 0);

            -- Outputs
            o_weight : out std_logic_vector(D-1 downto 0);
            o_ps : out std_logic_vector(N-1 downto 0); 
            o_input : out std_logic_vector(D-1 downto 0) 
        );
    end component;

    signal s_i_weights: std_logic_vector(D-1 downto 0);
    signal s_i_load_weight: std_logic;
    signal s_i_input: std_logic_vector(D-1 downto 0);
    signal s_i_ps: std_logic_vector(N-1 downto 0);

    signal s_o_weight: std_logic_vector(D-1 downto 0);
    signal s_o_ps: std_logic_vector(N-1 downto 0);
    signal s_o_input: std_logic_vector(D-1 downto 0);

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  mac_N
    mac_N_0 : mac_N
        -- port map
        port map( 
            -- Inputs
            i_weights => s_i_weights,
            i_load_weight => s_i_load_weight,
            i_rst => NSYSRESET,
            i_clk => SYSCLK,
            i_input => s_i_input,
            i_ps => s_i_ps,

            -- Outputs
            o_weight => s_o_weight,
            o_ps => s_o_ps,
            o_input => s_o_input

            -- Inouts

        );
    
    P_TEST_CASES: process 
    begin 
        wait for ( SYSCLK_PERIOD * 10 );
       -- load weights in 
        s_i_weights <= x"12";
        s_i_load_weight <= '1';
        wait for SYSCLK_PERIOD;
        
       -- load another weight, verify it's being cycled out
        s_i_weights <= x"EE";
        wait for SYSCLK_PERIOD;

       -- load input i
        s_i_load_weight <= '0';
        s_i_input <= x"73";
        s_i_ps <= x"00000000";
        wait for SYSCLK_PERIOD;
        
       -- load input and ps
        s_i_input <= x"ee";
        s_i_ps <= x"00000314";
        wait for SYSCLK_PERIOD;

    end process;


end behavioral;

