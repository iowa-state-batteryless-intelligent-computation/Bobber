----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_memory_module is
end tb_memory_module;

architecture behavioral of tb_memory_module is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component memory_module
        -- ports
        port( 
            -- Inputs
            i_CLK : in std_logic;
            i_RST : in std_logic;
            i_WD_READ_ADDR_A : in std_logic_vector(6 downto 0);
            i_WD_READ_ADDR_B : in std_logic_vector(6 downto 0);
            i_WD_WRITE_ADDR_A : in std_logic_vector(6 downto 0);
            i_WD_WRITE_ADDR_B : in std_logic_vector(6 downto 0);
            i_WD_WRITE_DATA_A : in std_logic_vector(7 downto 0);
            i_WD_WRITE_DATA_B : in std_logic_vector(7 downto 0);
            i_WD_WRITE_EN_A : in std_logic;
            i_WD_WRITE_EN_B : in std_logic;
            i_PS_READ_ADDR_A : in std_logic_vector(6 downto 0);
            i_PS_READ_ADDR_B : in std_logic_vector(6 downto 0);
            i_PS_WRITE_ADDR_A : in std_logic_vector(6 downto 0);
            i_PS_WRITE_ADDR_B : in std_logic_vector(6 downto 0);
            i_PS_WRITE_DATA_A : in std_logic_vector(7 downto 0);
            i_PS_WRITE_DATA_B : in std_logic_vector(7 downto 0);
            i_PS_WRITE_EN_A : in std_logic;
            i_PS_WRITE_EN_B : in std_logic;

            -- Outputs
            o_WD_READ_DATA_A : out std_logic_vector(7 downto 0);
            o_WD_READ_DATA_B : out std_logic_vector(7 downto 0);
            o_PS_READ_DATA_A : out std_logic_vector(7 downto 0);
            o_PS_READ_DATA_B : out std_logic_vector(7 downto 0)

            -- Inouts

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  memory_module
    memory_module_0 : memory_module
        -- port map
        port map( 
            -- Inputs
            i_CLK => SYSCLK,
            i_RST => NSYSRESET,
            i_WD_READ_ADDR_A => (others=> '0'),
            i_WD_READ_ADDR_B => (others=> '0'),
            i_WD_WRITE_ADDR_A => (others=> '0'),
            i_WD_WRITE_ADDR_B => (others=> '0'),
            i_WD_WRITE_DATA_A => (others=> '0'),
            i_WD_WRITE_DATA_B => (others=> '0'),
            i_WD_WRITE_EN_A => '0',
            i_WD_WRITE_EN_B => '0',
            i_PS_READ_ADDR_A => (others=> '0'),
            i_PS_READ_ADDR_B => (others=> '0'),
            i_PS_WRITE_ADDR_A => (others=> '0'),
            i_PS_WRITE_ADDR_B => (others=> '0'),
            i_PS_WRITE_DATA_A => (others=> '0'),
            i_PS_WRITE_DATA_B => (others=> '0'),
            i_PS_WRITE_EN_A => '0',
            i_PS_WRITE_EN_B => '0',

            -- Outputs
            o_WD_READ_DATA_A => open,
            o_WD_READ_DATA_B => open,
            o_PS_READ_DATA_A => open,
            o_PS_READ_DATA_B => open

            -- Inouts

        );

end behavioral;

