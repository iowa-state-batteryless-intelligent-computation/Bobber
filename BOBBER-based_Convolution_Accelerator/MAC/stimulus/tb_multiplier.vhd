--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_multiplier is
    generic(gCLK_HPER : time := 50 ns);
end tb_multiplier;

architecture behavioral of tb_multiplier is
    constant cCLK_PER : time := gCLK_HPER * 2;

    component multiplier
        port(i_a    : in signed(6 downto 0);
             i_b    : in signed(6 downto 0);
             o_p    : out signed(13 downto 0));
    end component;

    signal s_CLK    : std_logic;
    signal s_a      : signed(6 downto 0);
    signal s_b      : signed(6 downto 0);
    signal s_p      : signed(13 downto 0);

    -- Pro tier testing signals
    signal s_testCount      : integer := 0;
    signal s_expected       : std_logic_vector(13 downto 0);
    signal test_diff        : std_logic_vector(13 downto 0);
    signal test_passed      : std_logic;

begin
    DUT: multiplier
    port map (i_a       => s_a,
              i_b       => s_b,
              o_p       => s_p);

    test_diff <= std_logic_vector(s_p) xor s_expected;
    test_passed <= std_logic_vector(s_p) ?= s_expected;

    P_CLK: process
    begin
        s_CLK <= '0';
        wait for gCLK_HPER;
        s_CLK <= '1';
        wait for gCLK_HPER;
        s_testCount <= s_testCount + 1;
    end process;

    P_TB: process
    begin
        -- Test 1
        s_a <= b"0000101";
        s_b <= b"0000110";
        s_expected <= b"00000000011110";
    	wait for cCLK_PER;

        -- Test 2
        s_a <= b"1111111";
        s_b <= b"1111111";
        s_expected <= b"00000000000001";
    	wait for cCLK_PER;

        -- Test 3
        s_a <= b"0111111";
        s_b <= b"0111111";
        s_expected <= b"00111110000001";
    	wait for cCLK_PER;
    	wait;
    end process;
end behavioral;
