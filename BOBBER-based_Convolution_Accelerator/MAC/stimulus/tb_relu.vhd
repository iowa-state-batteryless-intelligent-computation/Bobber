----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_relu is
end tb_relu;

architecture behavioral of tb_relu is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    signal s_i_D: std_logic_vector(7 downto 0);
    signal s_o_DATA: std_logic_vector(7 downto 0);

    component relu
        -- ports
        port( 
            -- Inputs
            i_D : in std_logic_vector(7 downto 0);

            -- Outputs
            o_DATA : out std_logic_vector(7 downto 0)

            -- Inouts

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

       

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  relu
    relu_0 : relu
        -- port map
        port map( 
            -- Inputs
            i_D => s_i_D,
            -- Outputs
            o_DATA => s_o_DATA
        );

     P_TEST_CASES: process
    begin
        -- wait for 3 cycles (reset) and quarter cycle to simulate signal delay
        wait for ( SYSCLK_PERIOD * 3.25);
        
        s_i_D <= x"07"; -- data > 0
        assert(s_o_data = x"07") report "instr #1 success" severity note;
        wait for SYSCLK_PERIOD;
        s_i_D <= x"91"; -- data < 0
        assert(s_o_data = x"00") report "instr #1 success" severity note;
        wait for SYSCLK_PERIOD;

    end process;

end behavioral;

