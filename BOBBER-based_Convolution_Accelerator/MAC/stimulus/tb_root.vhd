 --------------------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_root is
generic(N : integer := 32; D: integer := 8); -- input data is 8bits, output is 32 bits
end tb_root;

architecture behavioral of tb_root is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '1';
    signal NSYSRESET : std_logic := '0';

    component acCylator
    port (
        i_RST : in std_logic;
        i_CLK : in std_logic;
        i_inst: in std_logic_vector (N-1 downto 0);  -- instruction
        o_done: out std_logic;
        o_ps_0: out std_logic_vector (D-1 downto 0);
        o_ps_1: out std_logic_vector (D-1 downto 0);
        o_ps_rw: out std_logic;

        o_WRITE: out std_logic;
        o_READ: out std_logic;
        o_CALC: out std_logic
    );
    end component;

    signal s_i_instr: std_logic_vector(N-1 downto 0);
    signal s_o_done: std_logic;
    signal s_o_ps_0 : std_logic_vector( D-1 downto 0);
    signal s_o_ps_1: std_logic_vector( D-1 downto 0);
    signal s_o_ps_rw: std_logic;

    signal s_o_WRITE: std_logic;
    signal s_o_READ: std_logic;
    signal s_o_CALC: std_logic;


begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 2 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  accCylator
    acCylator_0 : acCylator
        -- port map
        port map( 
            -- Inputs
            i_RST => NSYSRESET,
            i_CLK => SYSCLK,
            i_inst => s_i_instr,

            -- Outputs
            o_done =>  s_o_done,
            o_ps_0 => s_o_ps_0,
            o_ps_1 => s_o_ps_1,
            o_ps_rw => open,

            o_WRITE => s_o_WRITE,
            o_READ => s_o_READ,
            o_CALC => s_o_CALC
            -- Inouts

        );

     P_TEST_CASES: process
    begin
        wait for ( SYSCLK_PERIOD * 2.25 );
        -- load data into memory
        -- format: opcode_DD_Address_Data
        s_i_instr <= b"0000_000000000_01000000000_10000000";  -- send data 0 - 0x24
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000000001_10000000"; -- send data 1 - 0x73
        wait for SYSCLK_PERIOD;

        s_i_instr <= b"0000_000000000_01000000010_10000000";  -- send bias 1 - 0x25
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000000011_10000000";  -- send bias 1 - 0x26
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000000100_00100111";  -- send bias 1 - 0x27
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000000101_00101000";  -- send bias 1 - 0x28
        wait for SYSCLK_PERIOD;

        s_i_instr <= b"0000_000000000_01000000110_01110110"; -- send bias 2 - 0x76
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000000111_01110111"; -- send bias 2 - 0x77
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000001000_01111000"; -- send bias 2 - 0x78
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_01000001001_01111001"; -- send bias 2 - 0x79
        wait for SYSCLK_PERIOD;

        -- load weight into memory
        -- 0 -> A, 1 -> B, 2 -> C, 3 -> D
        s_i_instr <= b"0000_000000000_10001111111_00000000"; -- weight 0 - 0x00
        wait for SYSCLK_PERIOD;   
        s_i_instr <= b"0000_000000000_10001111111_00101110"; -- weight 1 - 0x2E
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_10001111111_00001101"; -- weight 2 - 0x0D
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0000_000000000_10001111111_00111100"; -- weight 3 - 0x3C
        wait for SYSCLK_PERIOD;

        -- buffer instruction
        s_i_instr <= b"0000_000000000_10000000011_00111100";
        wait for SYSCLK_PERIOD;
        -- result without bias = 14AA, 1cc8
        -- result with bias    = 14CF, 1d3e

        -- call the calculation instruction
        -- format: opcode_d-address_w-address-dd-offset
        -- s_i_instr <= b"1110_01000000000_10000000000_000000";  --Piecewise 
        s_i_instr <= b"0111_01000000000_00000000000_000000";
        wait for (SYSCLK_PERIOD * 18);

        -- read data back
        s_i_instr <= b"0100_00000000100_00000000000000000"; -- read 1-0
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000101_00000000000000000"; -- read 1-1
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000110_00000000000000000"; -- read 1-2
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000111_00000000000000000"; -- read 1-3
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000000_00000000000000000"; -- read 0-0
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000001_00000000000000000"; -- read 0-1
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000010_00000000000000000"; -- read 0-2
        wait for SYSCLK_PERIOD;
        s_i_instr <= b"0100_00000000011_00000000000000000"; -- read 0-3
        wait for SYSCLK_PERIOD;

        
    end process;
end behavioral;

