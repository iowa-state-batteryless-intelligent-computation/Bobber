----------------------------------------------------------------------

--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_shift_reg_32 is
end tb_shift_reg_32;

architecture behavioral of tb_shift_reg_32 is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component shift_reg_spi_32
        -- ports
        port( 
            -- Inputs
            i_DATA : in std_logic_vector(31 downto 0);
            i_WE_P : in std_logic;
            i_CLK : in std_logic;
            i_RST : in std_logic;   
            i_SHIFT_EN: in std_logic;


            -- Outputs
            o_DATA : out std_logic_vector(7 downto 0)

            -- Inouts

        );
    end component;

    signal s_i_DATA: std_logic_vector(31 downto 0);
    signal s_i_WE_P: std_logic;
    signal s_o_DATA: std_logic_Vector(7 downto 0);
    signal s_i_SHIFT_EN: std_logic;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  shift_reg_spi_32
    shift_reg_spi_32_0 : shift_reg_spi_32
        -- port map
        port map( 
            -- Inputs
            i_DATA => s_i_DATA,
            i_WE_P => s_i_WE_P,
            i_CLK => SYSCLK,
            i_RST => NSYSRESET,
            i_SHIFT_EN => s_i_SHIFT_EN,

            -- Outputs
            o_DATA => s_o_DATA

            -- Inouts

        );

SPI_SIM: process 
    begin
        wait for (SYSCLK_PERIOD * 10);
        s_i_SHIFT_EN <= '0';
        wait for (SYSCLK_PERIOD * 5);

        s_i_WE_P <= '1';
        s_i_DATA <= x"ddccbbaa";
        s_i_SHIFT_EN <= '1';
        wait for (SYSCLK_PERIOD);
        s_i_SHIFT_EN<= '0';
        s_i_WE_P <= '0';

        wait for (SYSCLK_PERIOD * 10);  -- 1 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';

        wait for (SYSCLK_PERIOD * 10);  -- 2 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';

        wait for (SYSCLK_PERIOD * 10);  -- 3 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';

        wait for (SYSCLK_PERIOD * 10);  -- 4 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';

             wait for (SYSCLK_PERIOD * 10);  -- buffer


        s_i_WE_P <= '1';
        s_i_SHIFT_EN<= '1';
        s_i_DATA <= x"44332211";
        wait for (SYSCLK_PERIOD);
        s_i_SHIFT_EN<= '0';
        s_i_WE_P <= '0';

        wait for (SYSCLK_PERIOD * 10);  -- 1 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';


        wait for (SYSCLK_PERIOD * 10);  -- 2 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';
        
             wait for (SYSCLK_PERIOD * 10);  -- buffer

        s_i_WE_P <= '1';
        s_i_DATA <= x"87654321";
        wait for (SYSCLK_PERIOD);
        s_i_WE_P <= '0';
        wait for (SYSCLK_PERIOD * 4);

        wait for (SYSCLK_PERIOD * 10);  -- 1 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';


        wait for (SYSCLK_PERIOD * 10);  -- 2 of 4
        s_i_SHIFT_EN<= '1';
        wait for (SYSCLK_PERIOD * 3);
        s_i_SHIFT_EN<= '0';


    end process;

end behavioral;

