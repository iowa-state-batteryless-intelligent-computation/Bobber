----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_sys_array_2_2 is
generic(N : integer := 32; D: integer := 8); -- input data is 8bits, output is 32 bits
end tb_sys_array_2_2;

architecture behavioral of tb_sys_array_2_2 is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '1';
    signal NSYSRESET : std_logic := '0';

    component sys_array_2_2
        -- ports
        port( 
            -- Inputs
            i_weights_0 : in std_logic_vector(D-1 downto 0);
            i_weights_1 : in std_logic_vector (D-1 downto 0);
            i_input_0 : in std_logic_vector(D-1 downto 0);
            i_input_1 : in std_logic_vector(D-1 downto 0);
            i_ps_0 : in std_logic_vector(N-1 downto 0);
            i_ps_1 : in std_logic_vector(N-1 downto 0);
            i_load_weight : in std_logic;
            i_rst : in std_logic;
            i_clk : in std_logic;

            -- Outputs
            o_ps_0 : out std_logic_vector(N-1 downto 0);
            o_ps_1 : out std_logic_vector(N-1 downto 0)

            -- Inouts

        );
    end component;

    signal s_i_weights: std_logic_vector(D-1 downto 0);
    signal s_i_input_0: std_logic_vector(D-1 downto 0);
    signal s_i_input_1: std_logic_vector(D-1 downto 0);
    signal s_i_ps_0: std_logic_vector(N-1 downto 0);
    signal s_i_ps_1 : std_logic_vector(N-1 downto 0);
    signal s_i_load_weight : std_logic;
    -- Outputs
    signal s_o_ps_0 : std_logic_vector(N-1 downto 0);
    signal s_o_ps_1 : std_logic_vector(N-1 downto 0);

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  sys_array_2_2
    sys_array_2_2_0 : sys_array_2_2
        -- port map
        port map( 
            -- Inputs
            i_weights_0 => s_i_weights,
            i_weights_1 => s_i_weights,
            i_input_0 => s_i_input_0,
            i_input_1 => s_i_input_1,
            i_ps_0 => s_i_ps_0,
            i_ps_1 => s_i_ps_1,
            i_load_weight => s_i_load_weight,
            i_rst => NSYSRESET,
            i_clk => SYSCLK,

            -- Outputs
            o_ps_0 => s_o_ps_0,
            o_ps_1 => s_o_ps_1

        );

    P_TEST_CASES: process 
    begin 
        wait for ( SYSCLK_PERIOD * 10 );
        
        --loading weights in , this takes 2 cycles
        s_i_weights <= x"30";
        s_i_load_weight <= '1';
        wait for SYSCLK_PERIOD;
    
        s_i_weights <= x"12";
        s_i_ps_0 <= x"00112233";
        s_i_ps_1 <= x"33221100";
        wait for SYSCLK_PERIOD;

        --cycle 1 
        s_i_load_weight <= '0';
        s_i_input_0 <= x"1a";
        s_i_input_1 <= x"2b";
        wait for SYSCLK_PERIOD;

        --cycle 2
        s_i_input_0 <= x"bf";
        s_i_input_1 <= x"05";
        wait for SYSCLK_PERIOD;

        --cycle 3
        s_i_input_0 <= x"32";
        s_i_input_1 <= x"43";
        wait for SYSCLK_PERIOD;
    
        --cycle 4
        s_i_input_0 <= x"ef";
        s_i_input_1 <= x"5f";
        wait for SYSCLK_PERIOD;
    end process;
end behavioral;

