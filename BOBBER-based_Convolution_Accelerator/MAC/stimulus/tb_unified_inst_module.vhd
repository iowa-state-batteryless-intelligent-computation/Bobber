----------------------------------------------------------------------

--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity tb_unified_inst_module is
end tb_unified_inst_module;

architecture behavioral of tb_unified_inst_module is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';
    signal s_i_INST_IN_SPI: std_logic_vector(7 downto 0);
    signal s_i_SPI_OUT_VLD: std_logic;
    signal s_o_DONE: std_logic;
    signal s_o_INST: std_logic_vector(31 downto 0);

    component unified_inst_module
        -- ports
        port( 
            -- Inputs
            i_CLK : in std_logic;
            i_RST : in std_logic;
            i_INST_IN_SPI : in std_logic_vector(7 downto 0);
            i_SPI_OUT_VLD : in std_logic;

            -- Outputs
            o_DONE : out std_logic;
            o_INST : out std_logic_vector(31 downto 0)

        );
    end component;

    

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;


    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '1';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '0';
            wait;
        end if;
    
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  unified_inst_module
    unified_inst_module_0 : unified_inst_module
        -- port map
        port map( 
            -- Inputs
            i_CLK => SYSCLK,
            i_RST => NSYSRESET,
            i_INST_IN_SPI => s_i_INST_IN_SPI,
            i_SPI_OUT_VLD => s_i_SPI_OUT_VLD,

            -- Outputs
            o_DONE =>  s_o_DONE,
            o_INST => s_o_INST

            -- Inouts

        );

P_TEST_CASES: process
    begin
        wait for ( SYSCLK_PERIOD * 10 );
        s_i_INST_IN_SPI <= b"00_000101";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"00000000";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"0000000_0";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"0110_0100";
        s_i_SPI_OUT_VLD <= '1';

-- instruction 2
        wait for ( SYSCLK_PERIOD * 10 );
        s_i_INST_IN_SPI <= b"00_100001";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"00000000";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"0000000_0";
        s_i_SPI_OUT_VLD <= '1';

        wait for ( SYSCLK_PERIOD);
        s_i_INST_IN_SPI <= b"0110_0100";
        s_i_SPI_OUT_VLD <= '1';
        
end process;


end behavioral;

