add wave -position insertpoint  \
sim:/root_spi/NSYSRESET \
sim:/root_spi/SPI_CLK \
sim:/root_spi/SYSCLK \
sim:/root_spi/SYSCLK_PERIOD \
sim:/root_spi/s_i_CS_N \
sim:/root_spi/s_i_MOSI \
sim:/root_spi/s_i_SCLK \
sim:/root_spi/s_o_MISO \
sim:/root_spi/s_o_done
add wave -position insertpoint  \
sim:/root_spi/acCylator_spi_0/s_inst \
sim:/root_spi/acCylator_spi_0/s_mem_ps_out_a \
sim:/root_spi/acCylator_spi_0/s_ps_rw \
sim:/root_spi/acCylator_spi_0/s_ready \
sim:/root_spi/acCylator_spi_0/s_reg_upper_out \
sim:/root_spi/acCylator_spi_0/s_spi_out \
sim:/root_spi/acCylator_spi_0/s_spi_out_vld
add wave -position insertpoint  \
sim:/root_spi/acCylator_spi_0/spi_impl/bit_cnt \
sim:/root_spi/acCylator_spi_0/spi_impl/bit_cnt_max \
sim:/root_spi/acCylator_spi_0/spi_impl/data_shreg \
sim:/root_spi/acCylator_spi_0/spi_impl/last_bit_en \
sim:/root_spi/acCylator_spi_0/spi_impl/load_data_en \
sim:/root_spi/acCylator_spi_0/spi_impl/rx_data_vld \
sim:/root_spi/acCylator_spi_0/spi_impl/shreg_busy \
sim:/root_spi/acCylator_spi_0/spi_impl/slave_ready \
sim:/root_spi/acCylator_spi_0/spi_impl/spi_clk_fedge_en \
sim:/root_spi/acCylator_spi_0/spi_impl/spi_clk_redge_en \
sim:/root_spi/acCylator_spi_0/spi_impl/spi_clk_reg