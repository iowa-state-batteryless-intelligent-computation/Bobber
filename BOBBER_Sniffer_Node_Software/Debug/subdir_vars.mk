################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../msp430fr5994.ld 

C_SRCS += \
../dump_data.c \
../gpio.c \
../main.c \
../system_clock.c \
../timer.c \
../uart_in.c \
../uart_out.c 

C_DEPS += \
./dump_data.d \
./gpio.d \
./main.d \
./system_clock.d \
./timer.d \
./uart_in.d \
./uart_out.d 

OBJS += \
./dump_data.o \
./gpio.o \
./main.o \
./system_clock.o \
./timer.o \
./uart_in.o \
./uart_out.o 

OBJS__QUOTED += \
"dump_data.o" \
"gpio.o" \
"main.o" \
"system_clock.o" \
"timer.o" \
"uart_in.o" \
"uart_out.o" 

C_DEPS__QUOTED += \
"dump_data.d" \
"gpio.d" \
"main.d" \
"system_clock.d" \
"timer.d" \
"uart_in.d" \
"uart_out.d" 

C_SRCS__QUOTED += \
"../dump_data.c" \
"../gpio.c" \
"../main.c" \
"../system_clock.c" \
"../timer.c" \
"../uart_in.c" \
"../uart_out.c" 


