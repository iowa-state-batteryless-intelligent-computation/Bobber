# Sniffer node for BOBBER
In order to evaluate the system, we use a sniffer node(MSP430FR5994) which captures the inferences occuring in the Node Under Test(NUT). The NUT indicates when the device boots and when an inference is done through GPIO's and once an inference is done, the computed output result is sent using a single channel UART interface.
The sniffer node timestamps the gpio indications and channelizes the received computed inference output using the backchannel UART
# License
Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.