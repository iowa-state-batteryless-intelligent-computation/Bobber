/* Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "gpio.h"

void gpioConfig(){
// Configure GPIO
// Configure P1.0 and P1.1 for LED output
   P1OUT = 0; //P1.0 output it low
   P1DIR = 0xFF; //P1.0 is output


   P4OUT = 0; //P1.0 output it low
   P4DIR = 0xFF; //P1.0 is output

//   P2OUT  = 0; P2DIR  = 0xFF;              // USCI_A0 UART operation
   P2SEL0 &= ~(BIT0 | BIT1);
   P2SEL1 |= BIT0 | BIT1;

   P6SEL0 |= (BIT0 | BIT1);                // USCI_A3 UART operation
   P6SEL1 &= ~(BIT0 | BIT1);

   // Configure P1.4, P1.5 for Timer B 0.1, 0.2 capture
   P1DIR &=  ~(BIT4 | BIT5); //P1.4 and P1.5 is input
   P1OUT &= ~(BIT4 | BIT5); //P1.4 and P1.5 has a pull-down resistor
   P1REN |= (BIT4 | BIT5);

   P1SEL0 |=  (BIT4 | BIT5); //P1.4 and P1.5 alternate functions on TB0.2
   P1SEL1 &= ~(BIT4 | BIT5);

   P3DIR &=  ~(BIT4 | BIT5 | BIT6); //P3.4, P3.5, P3.6, and P3.7 is input
   P3OUT &= ~(BIT4 | BIT5 | BIT6); //P3.4, P3.5, P3.6, and P3.7 has a pull-down resistor
   P3REN |= (BIT4 | BIT5 | BIT6);
   P3SEL0 |=  (BIT4 | BIT5 | BIT6); //P3.4, P3.5, P3.6, and P3.7 alternate functions are TB0.3, TB0.4, TB0.5, and TB0.6
   P3SEL1 &= ~(BIT4 | BIT5 | BIT6);

// Configure P5.6 for button input
    P5DIR &= ~BIT6; //P5.6 is input
    P5OUT |= BIT6; //P5.6 has a pull-up resistor
    P5REN |= BIT6;

    // Disable the GPIO power-on default high-impedance mode to activate
       // previously configured port settings
   PM5CTL0 &= ~LOCKLPM5;

}
