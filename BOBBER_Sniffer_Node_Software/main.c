/* Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <msp430.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "gpio.h"
#include "system_clock.h"
#include "uart_in.h"
#include "uart_out.h"
#include "timer.h"

char readstr[40]={0};
//volatile unsigned char tmp_rxbuf[40] = {0};
uint16_t tmp_TB0CCR1=0, tmp_TB0CCR2=0, tmp_TB0CCR3 = 0, tmp_TB0CCR4=0,tmp_TB0CCR5 = 0;
uint16_t tmp_rxbuf =0, exp_count=0, total_exp_done=5,inf_done=0,boot=0,operator_nut=0,outrx=0,calc_done=0;
uint64_t num_compute=0,numcomm=0;

uint16_t ovf_count_TB = 0;
unsigned long p_off = 1;
static const long max_time = 135000;// 135 sec
static const int on_time = 500;     // 500 msec

int main(void)
{

    WDTCTL = WDTPW | WDTHOLD;               // Stop Watchdog

    system_clockConfig(); //configure the system clock
    // Configure P6.0,P6.1,P2.0,P2.1 to use as the UART channel
    uart_inConfig();
    uart_outConfig();
    gpioConfig(); //Configure the gpio's

    timerConfig();

    while(1) {
        if(~P5IN & BIT6) {
         uart_outTX("Experiment started: ");
         uart_outTX("\n");
                while(1){
                     P1OUT &= ~BIT0; // clear red led
                     P4OUT &= ~BIT3; // Turn ON the harvester, pulling the RESET low
                     __bis_SR_register(LPM0_bits | GIE); //enter LPM    // Enter LPM0, interrupts enabled

                     if(boot==1){
                         uart_outTX("boot: ");
                         sprintf(readstr, "%x", tmp_TB0CCR4);
                         uart_outTX(readstr);
                         uart_outTX(",");
                         sprintf(readstr, "%d", ovf_count_TB);
                         uart_outTX(readstr);
                         uart_outTX("\n");
                         tmp_TB0CCR4 = 0;
                         ovf_count_TB = 0;
                         boot=0;
                     }
                     if(operator_nut==1){
                          uart_outTX("Op: ");
                          sprintf(readstr, "%x", tmp_TB0CCR3);
                          uart_outTX(readstr);
                          uart_outTX(",");
                          sprintf(readstr, "%d", ovf_count_TB);
                          uart_outTX(readstr);
                          uart_outTX("\n");
                          operator_nut=0;
                          tmp_TB0CCR3=0;
                          ovf_count_TB=0;

                     }
                     if(inf_done==1){
                         while(~P1IN & BIT4);
                         uart_outTX(",");
                         sprintf(readstr, "%x", tmp_TB0CCR1);
                         uart_outTX(readstr);
                         uart_outTX(",");
                         sprintf(readstr, "%d", ovf_count_TB);
                         uart_outTX(readstr);
                         uart_outTX(",");
                         sprintf(readstr, "%ld", num_compute);
                         uart_outTX(readstr);
//                         uart_outTX(",");
//                         sprintf(readstr, "%ld", numcomm);
//                         uart_outTX(readstr);
                         uart_outTX("\n");
                         inf_done=0;
                         tmp_TB0CCR1 =0;
                         ovf_count_TB=0;
                         num_compute=0;
                         numcomm=0;
//                         uart_outTX("Experiment no:");
//                         sprintf(readstr, "%d\n", exp_count);
//                         uart_outTX(readstr);

                     }

                     if(exp_count==2*total_exp_done) {
                         uart_outTX("Experiment completed");
                         P1OUT |= BIT0;
                         total_exp_done=0;
                         while(1);
                     }

                }
           }
     }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=EUSCI_A3_VECTOR
__interrupt void USCI_A3_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(EUSCI_A3_VECTOR))) USCI_A3_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(UCA3IV, USCI_UART_UCTXCPTIFG))
    {
        case USCI_NONE: break;
        case USCI_UART_UCRXIFG:
            while(!(inf_done));
            UCA0TXBUF= UCA3RXBUF;
//            __no_operation();
            __bic_SR_register_on_exit(LPM0_bits);
            break;

        case USCI_UART_UCTXIFG: break;
        case USCI_UART_UCSTTIFG: break;
        case USCI_UART_UCTXCPTIFG: break;
        default: break;
    }
}


// TimerB Interrupt Vector (TBIV) handler
 #if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
 #pragma vector=TIMER0_B1_VECTOR
 __interrupt void TIMER0_B1_ISR(void)
 #elif defined(__GNUC__)
 void __attribute__ ((interrupt(TIMER0_B1_VECTOR))) TIMER0_B1_ISR (void)
 #else
 #error Compiler not supported!
 #endif
 {
     switch(__even_in_range(TB0IV, TBIV__TBIFG))
     {
         case TBIV__NONE:    break;          // No interrupt
         case TBIV__TBCCR1://P1.4
             exp_count++;
             inf_done +=1;
             tmp_TB0CCR1 = TBCCR1;
             __bic_SR_register_on_exit(LPM0_bits);
             break;
         case TBIV__TBCCR2://P1.5
             tmp_TB0CCR2 = TBCCR2;
              num_compute += 1;
              __bic_SR_register_on_exit(LPM0_bits);
             break;                           // TB0CCR2 interrupt
         case TBIV__TBCCR3://P3.4
             tmp_TB0CCR3 = TBCCR3;
             operator_nut=1;
             __bic_SR_register_on_exit(LPM0_bits);
             break;           // TB0CCR3 interrupt
         case TBIV__TBCCR4://P3.5
             tmp_TB0CCR4 = TBCCR4;
             boot=1;
             __bic_SR_register_on_exit(LPM0_bits);
             break;           // TB0CCR4 interrupt
         case TBIV__TBCCR5:
             tmp_TB0CCR5 = TBCCR5;
             numcomm+=1;
             __bic_SR_register_on_exit(LPM0_bits);
             break;           // TB0CCR5 interrupt
         case TBIV__TBCCR6:  break;           // TB0CCR6 interrupt
         case TBIV__TBIFG:
            ovf_count_TB++;
            break;                 // overflow
         default: break;
     }
 }
