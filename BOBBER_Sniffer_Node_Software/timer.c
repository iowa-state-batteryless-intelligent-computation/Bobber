/* Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <msp430.h>

void timerConfig(){
  // Configure timer B for timestamping the data
    TB0CCTL1 = CM__FALLING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL2 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL3 = CM__RISING | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL4 = CM__BOTH | CCIS_0 | SCS | CAP | CCIE;
    TB0CCTL5 = CM__FALLING | CCIS_0 | SCS | CAP | CCIE;


    TB0CTL = TBSSEL__SMCLK | MC__CONTINUOUS | TBCLR | TBIE;
}

