/* Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "uart_in.h"

void uart_inConfig(){
 // Configure USCI_A3 for UART mode
//for 8 MHz
    UCA3CTLW0 = UCSWRST;                      // Put eUSCI in reset
    UCA3CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
    UCA3BRW = 4;                             // 8000000/115200
    UCA3MCTLW |= UCOS16 | UCBRF_5 | 0x55;
    UCA3CTLW0 &= ~UCSWRST;
    UCA3IE |= UCRXIE;                       // Enable USCI_A3 RX interrupt

 //For 16 Mhz
//    UCA3CTLW0 = UCSWRST;                      // Put eUSCI in reset
//    UCA3CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
//    UCA3BRW = 8;                             // 8000000/115200
//    UCA3MCTLW |= UCOS16 | UCBRF_10 | 0xF7;
//    UCA3CTLW0 &= ~UCSWRST;
//    UCA3IE |= UCRXIE;                       // Enable USCI_A3 RX interrupt
}


