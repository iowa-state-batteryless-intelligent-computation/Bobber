/* Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "uart_out.h"

void uart_outTX(char * tx_data)                 // Define a function which accepts a character pointer to an array
{
    unsigned int i=0;
    while(tx_data[i])                        // Increment through array, look for null pointer (0) at end of string
    {
        while ((UCA0STATW & UCBUSY));        // Wait if line TX/RX module is busy with data
        UCA0TXBUF = tx_data[i];              // Send out element i of tx_data array on UART bus
        i++;                                 // Increment variable for array address
    }
}


void uart_outConfig() {
    // Set P2.0 and P2.1 for USCI_A0 UART operation

    // Set P2.0 and P2.1 for USCI_A0 UART operation
//       P2SEL0 &= ~(BIT0 | BIT1);
//       P2SEL1 |= BIT0 | BIT1;
//       UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
//       UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
//       UCA0BRW = 8;                             // 16000000/16/115200
//    //    UCA0BRW = 52;
//       UCA0MCTLW |= UCOS16 | UCBRF_10 | 0xF7;
//    //    UCA0MCTLW |= UCOS16 | UCBRF_1 | 0x49;
//       UCA0CTLW0 &= ~UCSWRST;


    UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
    UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK
    UCA0BRW = 4;                             // 8000000/115200
    UCA0MCTLW |= UCOS16 | UCBRF_5 | 0x55;
    UCA0CTLW0 &= ~UCSWRST;

}
