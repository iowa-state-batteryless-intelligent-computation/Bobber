
#ifndef SYSTEM_PERIPHERALS_PERIPHERALS_H_
#define SYSTEM_PERIPHERALS_PERIPHERALS_H_



#endif /* SYSTEM_PERIPHERALS_PERIPHERALS_H_ */

#include <stdint.h>
#include <msp430.h>
#include <stdio.h>
#include <string.h>


#define ADC_MONITOR_THRESHOLD   3.0
#define ADC_MONITOR_FREQUENCY   1000


#define MCLK_FREQUENCY          8000000
#define SMCLK_FREQUENCY         8000000



void initGpio(void);
void initGpio_ext_adc(void);
void initClocks(void);
void initClocks_8Mhz(void);
void initAdcMonitor(void);
void initAdcMonitor_ext(void);
void stopAdcMonitor(void);
void init_spi(void);
void initCompMonitor();


