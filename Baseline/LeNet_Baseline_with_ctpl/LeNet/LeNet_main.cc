
#include <LeNet/LeNet_input.h>
#include <LeNet/LeNet_model.h>
#include "tensorflow/lite/micro/kernels/micro_ops.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/testing/micro_test.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include "tensorflow/lite/version.h"
#include "uarttx.h"

#define extern_adc_en 0
#include <msp430.h>

#include <fram-utilities/ctpl/ctpl.h>
#include "system_peripherals/peripherals.h"

#define FILL_16 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
#define FILL_64 FILL_16,FILL_16,FILL_16,FILL_16
#define FILL_256 FILL_64,FILL_64,FILL_64,FILL_64
#define FILL_1024 FILL_256,FILL_256,FILL_256,FILL_256
#define FILL_4096 FILL_1024,FILL_1024,FILL_1024,FILL_1024


static void __attribute__((naked, section(".crt_0500run_preinit_array"),used))
_system_pre_init(void){

  ctpl_init();

}


TF_LITE_MICRO_TESTS_BEGIN
char readstr[40]={0};
uint16_t inf_out= 0;
uint32_t num_exp=100;
uint32_t j;
uint32_t numcount;

WDTCTL = WDTPW | WDTHOLD;



// Configure GPIO

if (!extern_adc_en){
    initGpio();
    uartConfig();

//    initClocks_8Mhz();
    initAdcMonitor();
}

else {
    initGpio_ext_adc();
    uartConfig();
//    initClocks_8Mhz();
    initAdcMonitor_ext();
}


 /* Enable global interrupts. */
//__bis_SR_register(LPM3_bits | GIE); // CPU off, enable interrupts

__enable_interrupt();

P1OUT |= BIT0;

TF_LITE_MICRO_TEST(TestInvoke) { //
  // Set up logging.
  tflite::MicroErrorReporter micro_error_reporter;
  tflite::ErrorReporter* error_reporter = &micro_error_reporter;

  // Map the model into a usable data structure. This doesn't involve any
  // copying or parsing, it's a very lightweight operation.
  const tflite::Model* model =
      ::tflite::GetModel(LeNet5_model);
  if (model->version() != TFLITE_SCHEMA_VERSION) {
    error_reporter->Report(
        "Model provided is schema version %d not equal "
        "to supported version %d.\n",
        model->version(), TFLITE_SCHEMA_VERSION);
  }

P1OUT &= ~BIT0;

  // Pull in only the operation implementations we need.
  // This relies on a complete list of all the ops needed by this graph.
  // An easier approach is to just use the AllOpsResolver, but this will
  // incur some penalty in code space for op implementations that are not
  // needed by this graph.
  //
static tflite::MicroMutableOpResolver micro_mutable_op_resolver;


micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_QUANTIZE,
                                                             tflite::ops::micro::Register_QUANTIZE());

          micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEPTHWISE_CONV_2D,
                                                                       tflite::ops::micro::Register_DEPTHWISE_CONV_2D(),3,3);

          micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_CONV_2D,
                                                                   tflite::ops::micro::Register_CONV_2D(), 3, 3);



          micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_AVERAGE_POOL_2D,
                                                                                tflite::ops::micro::Register_AVERAGE_POOL_2D(),2,2);



          micro_mutable_op_resolver.AddBuiltin(
                         tflite::BuiltinOperator_FULLY_CONNECTED,
                         tflite::ops::micro::Register_FULLY_CONNECTED(), 4, 4);


          micro_mutable_op_resolver.AddBuiltin(
                  tflite::BuiltinOperator_SOFTMAX,
                  tflite::ops::micro::Register_SOFTMAX(), 2, 2);

          micro_mutable_op_resolver.AddBuiltin(tflite::BuiltinOperator_DEQUANTIZE,
                                               tflite::ops::micro::Register_DEQUANTIZE(), 2, 2);



 // Create an area of memory to use for input, output, and intermediate arrays.
 const int tensor_arena_size = 10*1024;
__attribute__((section(".upper.text")))
         static uint8_t tensor_arena[tensor_arena_size] = {FILL_4096,FILL_4096,FILL_1024,FILL_1024};

// Build an interpreter to run the model with.
  tflite::MicroInterpreter interpreter(model, micro_mutable_op_resolver,
                                       tensor_arena, tensor_arena_size,
                                       error_reporter);

  interpreter.AllocateTensors();


  // Get information about the memory area to use for the model's input.
  TfLiteTensor* input = interpreter.input(0);

  // Make sure the input has the properties we expect.
    TF_LITE_MICRO_EXPECT_NE(nullptr, input);
    TF_LITE_MICRO_EXPECT_EQ(4, input->dims->size);
      TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[0]);
      TF_LITE_MICRO_EXPECT_EQ(28, input->dims->data[1]);
      TF_LITE_MICRO_EXPECT_EQ(28, input->dims->data[2]);
      TF_LITE_MICRO_EXPECT_EQ(1, input->dims->data[3]);
      TF_LITE_MICRO_EXPECT_EQ(kTfLiteFloat32, input->type);


  // Copy a spectrogram created from a .wav audio file of someone saying "Yes",
  // into the memory area used for the input.
      const float* yes_features_data = image_float32;
       for (int i = 0; i < (input->bytes)/4; ++i) {
         input->data.f[i] = yes_features_data[i];
       }
//int j =0;
  // Run the model on this input and make sure it succeeds.
 for(j=0; j< num_exp; j++){ // here i decides the number of iterations inference was run consecutively on same input to measure execution time
  TfLiteStatus invoke_status = interpreter.Invoke();
  if (invoke_status != kTfLiteOk) {
    error_reporter->Report("Invoke failed\n");
  }
  TF_LITE_MICRO_EXPECT_EQ(kTfLiteOk, invoke_status);
//  }

  TfLiteTensor* output = interpreter.output(0);
  TF_LITE_MICRO_EXPECT_EQ(2, output->dims->size);
  TF_LITE_MICRO_EXPECT_EQ(1, output->dims->data[0]);
  TF_LITE_MICRO_EXPECT_EQ(10, output->dims->data[1]);
  TF_LITE_MICRO_EXPECT_EQ(kTfLiteFloat32, output->type);

  P7OUT ^= BIT2;
  __delay_cycles(100);
  P7OUT ^= BIT2;
  uint8_t k=0;
  while(k<20)                        // Increment through array, look for null pointer (0) at end of string
      {
      inf_out = output->data.i16[k]; // Increment variable for array address
//      send_data(inf_out);
      sprintf(readstr, "%04x", (inf_out));
      uartTX(readstr);
      k++;
      }
      sprintf(readstr, ",%04ld", (numcount));
      uartTX(readstr);
      numcount =0;
      __delay_cycles(24000);
      P7OUT |= BIT2;
      __delay_cycles(500);
      P7OUT &= ~BIT2;

  //  yes_features_data = image_float32;
    for (int i = 0; i < (int)(input->bytes)/4; ++i) {
       input->data.f[i] = yes_features_data[i];
  }

 }


//
//  /**
//   * FOR BENCHMARKING - Set breakpoint at the line above.
//   * To see data: View Variables -> output  -> output -> data -> f -> right click -> display as array -> size = 10
//   *
//   * Expected result:
//   * 0. 0.0
//   * 1. 0.0
//   * 2. 0.0
//   * 3. 0.0
//   * 4. 0.0
//   * 5. 0.0
//   * 6. 0.0
//   * 7. 0.99609375
//   * 8. 0.0
//   * 9. 0.0
//   */
//

//#ifndef __MSP430__
//  clear_outputs();
//  for(int i = 0; i<=18; i++){
//      auto tensor = interpreter.context_.tensors[i];
//      output_tensor(i,tensor);
//  }
//#endif
}

TF_LITE_MICRO_TESTS_END


//#pragma vector = ADC12_VECTOR;
__attribute__((__interrupt__(ADC12_VECTOR)))
void ADC12_ISR(void)
{
    switch(__even_in_range(ADC12IV, ADC12IV_ADC12LOIFG)) {
        case ADC12IV_NONE:        break;        // Vector  0: No interrupt
        case ADC12IV_ADC12OVIFG:  break;        // Vector  2: ADC12MEMx Overflow
        case ADC12IV_ADC12TOVIFG: break;        // Vector  4: Conversion time overflow
        case ADC12IV_ADC12HIIFG:                // Vector  6: Window comparator high side
            /* Disable the high side and enable the low side interrupt. */
            ADC12IER2 &= ~ADC12HIIE;
            ADC12IER2 |= ADC12LOIE;
            ADC12IFGR2 &= ~ADC12LOIFG;
            break;
        case ADC12IV_ADC12LOIFG:                // Vector  8: Window comparator low side
            /* Stop the ADC monitor and enter device shutdown with 64ms timeout. */
//            stopAdcMonitor();
//            uartTX("Ei\n");
            ctpl_enterShutdown(CTPL_SHUTDOWN_TIMEOUT_64_MS);
            initGpio();
            P1OUT |= BIT0;
            uartConfig();


            /* Reinitialize the ADC monitor since the ADC state is not retained. */
//            initAdcMonitor();
            ADC12IER2 &= ~ADC12LOIE;
            ADC12IER2 |= ADC12HIIE;
            ADC12IFGR2 &= ~ADC12HIIFG;
            break;
        default: break;
    }
}
