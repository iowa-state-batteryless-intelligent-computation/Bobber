//#include "spi.h"
//
//char spi_send_data(char transmit) {
//    P8OUT &= ~BIT3;      // Pull SS bit low
//    P1OUT |= BIT0;       // light up LED
//
//    UCB1TXBUF = transmit; //send some data
//    while(0x1 & UCB1STATW);
//
//    while(!(0x1 & UCB1IFG)); //wait until received
//    char received_ch = UCB1RXBUF; // store received data
//
//    P8OUT |= BIT3;      // Set SS bit high
//    P1OUT &= ~BIT0;
//
//    return received_ch;
//}
//
//void init_spi() {
//    WDTCTL = WDTPW + WDTHOLD;    // Stop watchdog timer
//    P8DIR |= BIT3;               // Set P8.3 to output (SS)
//    P1DIR |= 0x01;
//
//    P5SEL1 = 0;                  // P5.2 = SCLK 5.1=MISO 5.0-=MOSI
//    P5SEL0 = BIT0 + BIT1 + BIT2;
//
//    PM5CTL0 &= ~LOCKLPM5; // enable pin outputs
//
//    P8OUT |= BIT3; // set SS to high
//    P8OUT &= ~BIT3;      // Pull SS bit low
//    P8OUT |= BIT3; // set SS to high
//    P1OUT &= ~BIT0;
//
//    // Set up GPIO ports, for data addressing
//    P3DIR |= BIT0 + BIT1 + BIT2; // 3.0 = addr0; 3.1 = addr1; 3.2 = weight_Ndata
//
//    UCB1CTLW0 |= UCSWRST;         // Hold USCI in reset for rest of the setup
//    //  UCB1CTLW0 |= UCCKPL + UCMST + UCSYNC + UCSSEL + UCCKPH + UCMSB ;     // 3 pin, 8 bit, master
//    UCB1CTLW0 |=  UCMST + UCSYNC + UCSSEL + UCMSB + UCCKPH;
//    UCB1CTLW0 |= UCSSEL_2;        // CLOCK = SMCLK
//    UCB1BRW = 0xFf;              // SPI CLK = SMCLK/2
//    UCB1CTLW0 &= ~UCSWRST;        // start USCI
//
//    __delay_cycles(10000);
//}
//
