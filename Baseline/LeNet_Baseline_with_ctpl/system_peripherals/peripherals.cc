
#include "peripherals.h"

void initGpio_ext_adc(void)
{
    /* Configure GPIO to default state */
    P1OUT  = 0; P1DIR  = 0xFF;
    P1SEL1 |= BIT0;                         // Configure P1.0 for ADC
    P1SEL0 |= BIT0;

//    P2OUT  = 0; P2DIR  = 0xFF;
//    P2SEL0 &= ~(BIT0 | BIT1);
//    P2SEL1 |= BIT0 | BIT1;

    P5SEL1 = 0;                  // P5.2 = SCLK 5.1=MISO 5.0-=MOSI
    P5SEL0 = BIT0 + BIT1 + BIT2;

//    P7OUT  &= ~BIT2;
//    P7DIR  |= BIT2;
//
//    P7OUT  &= ~BIT4;
//    P7DIR  |= BIT4;
//
//    P7OUT  |= BIT3;
//    P7DIR  &= ~BIT3;
//    PJOUT  = 0; PJDIR  = 0xFFFF;
    P7OUT  = 0; P7DIR  = 0xFF;
//    P5SEL1 = 0;                  // P5.2 = SCLK 5.1=MISO 5.0-=MOSI
//    P5SEL0 = BIT0 + BIT1 + BIT2;
    /* Disable the GPIO power-on default high-impedance mode. */
    PM5CTL0 &= ~LOCKLPM5;
}


void initGpio(void)
{
    /* Configure GPIO to default state */
    P1OUT  = 0; P1DIR  = 0xFF;

//    P2OUT  = 0; P2DIR  = 0xFF;
//    P2SEL0 &= ~(BIT0 | BIT1);
//    P2SEL1 |= BIT0 | BIT1;

//    P7OUT  &= ~BIT2;
//    P7DIR  |= BIT2;

//        P3OUT  &= ~BIT5;
//        P3DIR  |= BIT5;


//
//    P7OUT  &= ~BIT4;
//    P7DIR  |= BIT4;
//
//    P7DIR  &= ~BIT3;
//    P7OUT  &= ~BIT3;
    P7OUT  = 0; P7DIR  = 0xFF;


    P5SEL1 = 0;                  // P5.2 = SCLK 5.1=MISO 5.0-=MOSI
    P5SEL0 = BIT0 + BIT1 + BIT2;

//    PJOUT  = 0; PJDIR  = 0xFFFF;
    /* Disable the GPIO power-on default high-impedance mode. */
    PM5CTL0 &= ~LOCKLPM5;
}

void init_spi(void) {

    P7OUT |= BIT3; // set SS to high
    P7OUT &= ~BIT3;      // Pull SS bit low
    P7OUT |= BIT3; // set SS to high
//    P1OUT &= ~BIT0;

    // Set up GPIO ports, for data addressing
//    P3DIR |= BIT0 + BIT1 + BIT2; // 3.0 = addr0; 3.1 = addr1; 3.2 = weight_Ndata

    UCB1CTLW0 |= UCSWRST;         // Hold USCI in reset for rest of the setup
    //  UCB1CTLW0 |= UCCKPL + UCMST + UCSYNC + UCSSEL + UCCKPH + UCMSB ;     // 3 pin, 8 bit, master
    UCB1CTLW0 |=  UCMST + UCSYNC + UCSSEL + UCMSB + UCCKPH;
    UCB1CTLW0 |= UCSSEL_2;        // CLOCK = SMCLK
    UCB1BRW = 0x09;              // SPI CLK = SMCLK
    UCB1CTLW0 &= ~UCSWRST;        // start USCI
//    UCB1IE |= UCRXIE;
    __delay_cycles(100);
}

void initClocks(void)
{
    /* Clock System Setup, MCLK = SMCLK = DCO (1MHz), ACLK = VLOCLK */
    CSCTL0_H = CSKEY >> 8;
    CSCTL1 = DCOFSEL_0;
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;
    CSCTL0_H = 0;
}
void initClocks_8Mhz(void)
{
//setting frequency to 16 MHz
FRCTL0 = FRCTLPW | NWAITS_1;
// Clock System Setup
// Setting the frequency to 8MHz
CSCTL0_H = CSKEY_H;                     // Unlock CS registers
//CSCTL1 = DCOFSEL_0;                     // Set DCO to 1MHz
CSCTL1 = DCOFSEL_6 ;    // Set DCO to 8MHz
// Delay by ~10us to let DCO settle. 60 cycles = 20 cycles buffer + (10us / (1/4MHz))
__delay_cycles(60);
// Set SMCLK = MCLK = DCO, ACLK = VLOCLK
CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
// Per Device Errata set divider to 4 before changing frequency to
// prevent out of spec operation from overshoot transient
CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;   // Set all dividers to 1 for 16MHz operation
CSCTL0_H = 0;                           // Lock CS registers

//CSCTL3 = DIVA__4 | DIVS__4 | DIVM__4;   // Set all corresponding clk sources to divide by 4 for errata
//CSCTL1 = DCOFSEL_4 | DCORSEL;           // Set DCO to 16MHz
// Delay by ~10us to let DCO settle. 60 cycles = 20 cycles buffer + (10us / (1/4MHz))
//__delay_cycles(60);
//CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;   // Set all dividers to 1 for 16MHz operation
//CSCTL0_H = 0;                           // Lock CS registers
}


void initAdcMonitor_ext(void)
{
    /* Initialize timer for ADC trigger. */
    TA0CCR0 = (SMCLK_FREQUENCY/ADC_MONITOR_FREQUENCY);
    TA0CCR1 = TA0CCR0/2;
    TA0CCTL1 = OUTMOD_3;
    TA0CTL = TASSEL__SMCLK | MC__UP;


    /*
     * Initialize ADC12_B window comparator using the battery monitor.
     * The monitor will first enable the high side to the monitor voltage plus
     * 0.1v to make sure the voltage is sufficiently above the threshold. When
     * the high side is triggered the interrupt service routine will switch to
     * the low side and wait for the voltage to drop below the threshold. When
     * the voltage drops below the threshold voltage the device will invoke the
     * compute through power loss shutdown function to save the application
     * state and enter complete device shutdown.
     */

    while(REFCTL0 & REFGENBUSY);            // If ref generator busy, WAIT
    REFCTL0 |= REFVSEL_1 | REFON;           // Select internal ref = 2V
                                               // Internal Reference ON
    while(!(REFCTL0 & REFGENRDY));          // Wait for reference generator
       // Configure ADC12
   ADC12CTL0 = ADC12SHT0_2 | ADC12ON;
   ADC12CTL1 = ADC12SHS_1 | ADC12SSEL_0 | ADC12CONSEQ_2 | ADC12SHP;                   // ADCCLK = MODOSC; sampling timer
//   ADC12CTL2 |= ADC12RES_2;                // 12-bit conversion results
   ADC12MCTL0 |= ADC12INCH_0 | ADC12VRSEL_1 | ADC12WINC; // A0 ADC input select; Vref=1.2V

   ADC12HI = (uint16_t)(4096*((ADC_MONITOR_THRESHOLD+0.1)/2)/(2.0));
   ADC12LO = (uint16_t)(4096*(ADC_MONITOR_THRESHOLD/2)/(2.0));

   ADC12IFGR2 &= ~(ADC12HIIFG | ADC12LOIFG);
   ADC12IER2 = ADC12HIIE;
   ADC12CTL0 |= ADC12ENC;
}

void initAdcMonitor(void)
{
    /* Initialize timer for ADC trigger. */
    TA0CCR0 = (SMCLK_FREQUENCY/ADC_MONITOR_FREQUENCY);
    TA0CCR1 = TA0CCR0/2;
    TA0CCTL1 = OUTMOD_3;
    TA0CTL = TASSEL__SMCLK | MC__UP;

    /* Configure internal 2.0V reference. */
    while(REFCTL0 & REFGENBUSY);
    REFCTL0 |= REFVSEL_1 | REFON;
    while(!(REFCTL0 & REFGENRDY));

    /*
     * Initialize ADC12_B window comparator using the battery monitor.
     * The monitor will first enable the high side to the monitor voltage plus
     * 0.1v to make sure the voltage is sufficiently above the threshold. When
     * the high side is triggered the interrupt service routine will switch to
     * the low side and wait for the voltage to drop below the threshold. When
     * the voltage drops below the threshold voltage the device will invoke the
     * compute through power loss shutdown function to save the application
     * state and enter complete device shutdown.
     */
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON;
    ADC12CTL1 = ADC12SHS_1 | ADC12SSEL_0 | ADC12CONSEQ_2 | ADC12SHP;
    ADC12CTL3 = ADC12BATMAP;
    ADC12MCTL0 = ADC12INCH_31 | ADC12VRSEL_1 | ADC12WINC;
    ADC12HI = (uint16_t)(4096*((ADC_MONITOR_THRESHOLD+0.1)/2)/(2.0));
    ADC12LO = (uint16_t)(4096*(ADC_MONITOR_THRESHOLD/2)/(2.0));
    ADC12IFGR2 &= ~(ADC12HIIFG | ADC12LOIFG);
    ADC12IER2 = ADC12HIIE;
    ADC12CTL0 |= ADC12ENC;
}


void stopAdcMonitor(void)
{
    /* Stop the timer, reference and ADC. */
    TA0CTL &= ~MC_3;
    REFCTL0 &= ~REFON;
    ADC12CTL0 &= ~(ADC12ENC | ADC12ON);
}

void initCompMonitor()
{
    /* Setup COMP_E for P1.5 (C9) input with 1.5v reference. */
    /*
     * CEIPEN: Channel input enable for the V+ terminal of the comparator
     * CEIPSEL: Channel input selected for the V+ terminal of the comparator if CEIPEN is set to 1.
     */
    CECTL0 = CEIPEN | CEIPSEL_0;
    /*
     * CEPWRMD_1: Power mode = Normal mode
     * CEMRVS: This bit defines if the comparator output selects between VREF0 or VREF1
     */
    CECTL1 = CEPWRMD_1 | CEMRVS;
    /*
     * CEREFL: Reference voltage level, 2.0 V is selected
     * CERS: Referencesource.This bit define if the reference voltage is derived from VCC
     * or from the precise shared reference. Shared reference voltage applied to the resistor ladder
     * CERSEL: Reference select. This bit selects to which terminal the VCCREF is applied.
     * CEREF0: This register defines the tap of the resistor string while CEOUT = 0
     */
    CECTL2 = CEREFL_2 | CERS_2 | CERSEL | CEREF0_23;
    /* Enable input buffer for the pin of the selected port */
    CECTL3 = BIT0;
    /* Comparator_E Interrupt Control Register */
    CEINT = CEIIE;
    /* his bit turns the comparator on */
    CECTL1 |= CEON;
    /* Delay for the reference to settle */
    __delay_cycles(75);
}
