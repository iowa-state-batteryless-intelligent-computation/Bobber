# Release
All notable changes to the repository and the releases are documented in this file for reference.
## Overview

* [v0.1](#v0.1)
* [Rel1.0](#Rel1.0)
* [Template](#template)
* [License](#license)

## <a name="v0.1"></a> v0.1

* Added a README_pre_release.md file documenting the project overview to provide a permanent repo link for FPGA 2023 artifact evaluation form [README_pre_release](README_pre_release).

## <a name="Rel1.0"></a> Rel1.0

### Added
* Added all of BOBBER FPGA 2023 design files for artifact evaluation after paper acceptance decision.

## <a name="template"></a> Template

### Added
* New Hardware and Software design files

### Changed
* Changes to existing repository by adding new files

### Deprecated
* No files removed

### Removed
* No files removed

### Fixed
* No Fixes

### Security
* No changes

# License
Copyright 2022 The BOBBER Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.