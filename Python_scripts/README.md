# Collecting logs

1. To dump the UART logs to a file, a [script](Python_scripts/serial_reader.py) is used. Make any changes in the file if required (eg: different log file name).
2. Identify the COM port details of your Sniffer Node and run the below command in the folder containing the [script](Python_scripts/serial_reader.py). Note: Replace x in ttySx, with the COM port number of your sniffer. 

       python3 serial_reader.py -p /dev/ttySx -o both

# Regenerating results with the existing experimental data

- To generate the results reported in the paper with the existing experimental data, run the python scripts [Latency_Energy_plots](Python_scripts/ifpga_plots_rev0.1.py), [Power_overhead_plot](Python_scripts/bobber_energy_efficiency_w_poweroverhead.py) using

        python3 ifpga_plots_rev0.1.py
        python3 bobber_energy_efficiency_w_poweroverhead.py


### Note: No changes has to be done to the python script to regenerate results with the existing experimental data. All the experimental data are available in the Exp_data folder.  

# Regenerating results with new experimental data

- The python script only supports parsing a single inference experiment result. If you are running and collecting multiple experiments, the script must be modified by the user. 
- If plotting for 1 inference result, the collected experiment result should have a similar log structure as [here](Exp_data/11_03_2022/) for Intermittent power and as [here](Exp_data/06_20_2022/) for continuous power.
- Once the file structure is verified, the appropriate file names to be changed must be identified and manually replaced.
- Run the below script to generate the plots. 

        python3 ifpga_plots_rev0.1.py
        python3 bobber_energy_efficiency_w_poweroverhead.py
## Note:
- As experiments are rerun and the results are plotted, there will be a +/- 10% variation in latencies measured under Intermittent power which are expected variations. This would also show a +/- 10% variation in the energy plot. 
- All the values used in bobber_energy_efficiency_w_poweroverhead.py are physically measured results from the hardware and are note captured by the logs.
- The python script will be cleaned up and modified in the next releases.  

# License
Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.