import numpy as np
import os
from collections import deque
import matplotlib.pyplot as plt

class boober_characterization(): # A class to show how performance and energy efficiency varies with different oprtimizations
    def __init__(self,power:float, filename: str, cap_size:float, ontime: float, offtime: float):
        self.power=power
        self.filename=filename
        self.clk=8e06
        self.num_lines_skip=1
        self.cap_size=50e-03
        self.on_time=ontime
        self.off_time=offtime
        self.vmax=1.25
        self.vmin=1.02
        self.harvest_power=7.94e-03
        self.msp_depthwise_exec_cycles=2
        self.msp_conv_exec_cycles=6
        self.msp_other_compute_cycles=1
        self.bobber_depthwise_cycle=46
        self.bobber_conv_cycle=129
        self.bobber_other_compute_cycle=29        
        self.bobber_boot_time=155e-03
        self.msp_boot_time=8.64e-03
        
    def calc_power_from_ontime(self):
        self.power=(0.5*self.cap_size*(self.vmax**2-self.vmin**2))/self.on_time
        # print(f'Bobber power{self.power}')    
    def plot_power_breakdown(self):
        width=0.5
        reg_efficiency=0.85
        ldo_quiescent_current=330e-06
        bobber_1v5_Vout=1.5
        bobber_1v5_In=3.3

        # print("TBD")
        msp_wctpl_depth_conv_dynamic_power_w=4.66e-03
        msp_wctpl_other_compute_dynamic_power_w=4.36e-03
        msp_wctpl_static_power_w=8.63e-04

        bobber_fpga_dynamic_power_w=7.05e-03+7.47e-04
        bobber_fpga_static_power_w=4.05e-05+5.28e-06
        bobber_fpga_static_current=2.70e-05+0.0000016
        bobber_fpga_dynamic_current=4.7e-03+0.0002265

        bobber_msp_depth_conv_dynamic_power=7.75e-03
        bobber_dynamic_power_other_compute_total_w= 4.72e-03
        bobber_msp_depth_conv_spi_w=bobber_msp_depth_conv_dynamic_power-bobber_dynamic_power_other_compute_total_w
        bobber_1v5_reg_static=bobber_fpga_static_power_w/((bobber_fpga_static_power_w)/(bobber_1v5_In*(bobber_fpga_static_current+ldo_quiescent_current)))
        bobber_1v5_reg_dynamic=bobber_fpga_dynamic_power_w/((bobber_fpga_dynamic_power_w)/(bobber_1v5_In*(bobber_fpga_dynamic_current+ldo_quiescent_current)))
        bobber_dynamic_power_depth_conv_total_w=3.23e-02
        bobber_oscillator_dynamic_power=3.23e-02-2.89e-02
        bobber_1v5_reg_dynamic=bobber_1v5_reg_dynamic+(bobber_dynamic_power_depth_conv_total_w-(bobber_1v5_reg_dynamic+bobber_msp_depth_conv_spi_w+msp_wctpl_other_compute_dynamic_power_w+bobber_fpga_dynamic_power_w+bobber_oscillator_dynamic_power))
        

        bobber_potential_msp_depth_conv_dynamic_power=7.75e-03
        bobber_potential_msp_depth_conv_spi_w=bobber_potential_msp_depth_conv_dynamic_power-bobber_dynamic_power_other_compute_total_w
        bobber_potential_fpga_dynamic_power_w= 5.28e-03+7.47e-04
        bobber_potential_fpga_static_power_w= 1.53E-05+5.28e-06
        bobber_potential_oscillator_dynamic_power= 0.168e-03
        bobber_potential_1v5reg_input=bobber_fpga_dynamic_power_w/reg_efficiency
        bobber_potential_1v5reg=bobber_potential_1v5reg_input-bobber_fpga_dynamic_power_w
        bobber_potential_other=5.51e-04
        # print((bobber_msp_depth_conv_spi_w+bobber_fpga_dynamic_power_w))

        # print(bobber_potential_msp_depth_conv_spi_w+bobber_potential_fpga_dynamic_power_w+bobber_dynamic_power_other_compute_total_w+bobber_potential_1v5reg+bobber_potential_oscillator_dynamic_power)

        fig, (ax0)= plt.subplots(1,1)

        rects3 = ax0.bar(0,msp_wctpl_depth_conv_dynamic_power_w,width,color='gold', hatch="||",label='MSP Dynamic')

        rects1 = ax0.bar(0,0,width,bottom=msp_wctpl_depth_conv_dynamic_power_w,color='darkgreen',hatch="||",label='SPI')

                
        rects5 = ax0.bar(0,0,width,bottom=msp_wctpl_static_power_w+msp_wctpl_depth_conv_dynamic_power_w,color='lime',hatch="oo",label='FPGA dynamic')
                
        rects5 = ax0.bar(0,0,width,bottom=msp_wctpl_static_power_w+msp_wctpl_depth_conv_dynamic_power_w,color='red',hatch="++",label='Oscillator')

        rects5 = ax0.bar(0,0,width,bottom=msp_wctpl_static_power_w+msp_wctpl_depth_conv_dynamic_power_w,color='saddlebrown',hatch="..",label='1v5 LDO')



        rects4 = ax0.bar(1,bobber_dynamic_power_other_compute_total_w,width,color='gold', hatch="||")
       
        rects1 = ax0.bar(1,bobber_msp_depth_conv_spi_w,width,bottom=bobber_dynamic_power_other_compute_total_w,color='darkgreen',hatch="//")

        rects5 = ax0.bar(1,bobber_fpga_dynamic_power_w,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w,color='lime',hatch="oo")
                
        rects5 = ax0.bar(1,bobber_oscillator_dynamic_power,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w+bobber_fpga_dynamic_power_w,color='red',hatch="++")

        rects5 = ax0.bar(1,bobber_1v5_reg_dynamic,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w+bobber_fpga_dynamic_power_w+bobber_oscillator_dynamic_power,color='saddlebrown',hatch="..")


        # rects4 = ax0.bar(2,bobber_dynamic_power_other_compute_total_w,width,color='lightblue', hatch="..")
       
        # rects1 = ax0.bar(2,bobber_msp_depth_conv_spi_w,width,bottom=bobber_dynamic_power_other_compute_total_w,color='lightgreen',hatch="//")

        # rects5 = ax0.bar(2,bobber_potential_fpga_dynamic_power_w,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w,color='lightslategrey',hatch="oo")
                
        # rects5 = ax0.bar(2,bobber_potential_oscillator_dynamic_power,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w+bobber_potential_fpga_dynamic_power_w,color='tan',hatch="++")
        
        # rects5 = ax0.bar(2,bobber_potential_1v5reg,width,bottom=bobber_msp_depth_conv_spi_w+bobber_dynamic_power_other_compute_total_w+bobber_potential_fpga_dynamic_power_w+bobber_potential_oscillator_dynamic_power,color='cyan',hatch="||")


        ax0.set_xticks([0, 1], ['MSP430\nw-CTPL', 'BOBBER'],fontsize=15)
        ax0.set_ylabel('Power (w)',fontsize=16)
        ax0.legend(loc='upper left',ncol=1,fontsize=15)

        ax0.tick_params(axis='x', which='both', labelsize=13,width=3)
        ax0.grid()
        # ax2.set_yscale('log')
        ax0.set_xlim(-0.5,1.5)

        fig.set_size_inches(6,4)
        plt.savefig(f'power_comparison_rebuttal_v0.2.pdf', dpi=250, quality=95, format='pdf')
        plt.show()
    def plot_pie_chart(self):
        reg_efficiency=0.85
        ldo_quiescent_current=330e-06
        bobber_1v5_Vout=1.5
        bobber_1v5_In=3.3

        print("TBD")
        msp_wctpl_depth_conv_dynamic_power_w=4.66e-03
        msp_wctpl_other_compute_dynamic_power_w=4.36e-03
        msp_wctpl_static_power_w=8.63e-04

        bobber_fpga_dynamic_power_w=7.05e-03+7.47e-04
        bobber_fpga_static_power_w=4.05e-05+5.28e-06
        bobber_fpga_static_current=2.70e-05+0.0000016
        bobber_fpga_dynamic_current=4.7e-03+0.0002265

        bobber_msp_depth_conv_dynamic_power=7.75e-03
        bobber_dynamic_power_other_compute_total_w= 4.72e-03
        bobber_msp_depth_conv_spi_w=bobber_msp_depth_conv_dynamic_power-bobber_dynamic_power_other_compute_total_w
        bobber_1v5_reg_static=bobber_fpga_static_power_w/((bobber_fpga_static_power_w)/(bobber_1v5_In*(bobber_fpga_static_current+ldo_quiescent_current)))
        bobber_1v5_reg_dynamic=bobber_fpga_dynamic_power_w/((bobber_fpga_dynamic_power_w)/(bobber_1v5_In*(bobber_fpga_dynamic_current+ldo_quiescent_current)))
        bobber_dynamic_power_depth_conv_total_w=3.23e-02
        bobber_oscillator_dynamic_power=3.23e-02-2.89e-02
        bobber_1v5_reg_dynamic=bobber_1v5_reg_dynamic+(bobber_dynamic_power_depth_conv_total_w-(bobber_1v5_reg_dynamic+bobber_msp_depth_conv_spi_w+msp_wctpl_other_compute_dynamic_power_w+bobber_fpga_dynamic_power_w+bobber_oscillator_dynamic_power))

        labels_msp = 'MSP Dynamic', '', '', '', ''
        msp = np.asarray([msp_wctpl_depth_conv_dynamic_power_w,0,0,0,0])
        fracs_msp = msp/msp.sum(axis=0) * 100

        labels_bobber = 'MSP Dynamic', 'SPI', 'FPGA Dynamic', 'Oscillator', '1v5 LDO'
        bobber=np.asarray([bobber_dynamic_power_other_compute_total_w, bobber_msp_depth_conv_spi_w,bobber_fpga_dynamic_power_w,bobber_oscillator_dynamic_power,bobber_1v5_reg_dynamic])
        fracs_bobber = bobber/bobber.sum(axis=0) * 100

        # Setting figure colors
        cmap = plt.get_cmap("tab20c")
        outer_colors = cmap(np.arange(5)*4)
        inner_colors = cmap(np.array([1,3,5,7,9]))

        # Setting the size of the figure
        plt.figure(figsize=(8,6))

        # Plotting the outer pie
        p, tx, autotexts=plt.pie(fracs_bobber, labels = labels_bobber, 
            startangle=90, pctdistance =0.88 ,colors=outer_colors,
            autopct = '%1.1f%%', radius= 1.0, labeldistance=0.4,
            textprops ={ 'fontweight': 'bold','fontsize':15},
            wedgeprops = {'linewidth' : 3, 'edgecolor' : "w" } )

        for i, a in enumerate(autotexts):
            a.set_text("{} watt".format(bobber[i])[:6])

        # PLotting the inner pie
        p, tx, autotexts_msp=plt.pie(fracs_msp,labels=labels_msp,startangle=90, pctdistance =0.88,colors=inner_colors,
                autopct = '%1.1f%%',radius= 0.50,labeldistance=0.25,
            textprops ={'fontweight': 'bold' ,'fontsize':15}, 
            wedgeprops = {'linewidth' : 3, 'edgecolor' : "w" } )
        for i, a in enumerate(autotexts_msp):
            if(i<1):
                a.set_text("{} watt".format(msp[i])[:6])
        # Creating the donut shape for the pie
        centre_circle = plt.Circle((0,0), 0.25, fc='white')
        fig= plt.gcf()
        fig.gca().add_artist(centre_circle) # adding the centre circle

        # Plotting the pie 
        plt.axis('equal')  # equal aspect ratio
        plt.legend(loc='best', fontsize =15)
        plt.tight_layout()
        plt.savefig(f'power_comparison_rebuttal_v0.1_pie_chart.pdf', dpi=250, quality=95, format='pdf')

        plt.show()
    def parsedata(self):
        totalTime, timeAfterPrevTimeStamp = 0, 0
        prevRxTime=0
        totalinftime,timeafterprevInf=0,0
        previnftime=0
        num_cycles=0
        num_cycles_op=0
        delayfor_gpio= 12.5e-06
        totalcycleforinference=deque([])
        bootupdown= deque([])
        on_time,off_time =deque([]),deque([])
        output=deque([])
        timeEdgeRx= deque([])
        timeelapsedforinf=deque([])
        bootupdowntime =deque([])
        inference_time,operator_time,operator_time_allexp =deque([]),deque([]),deque([])
        classoutputs,compute_counter,communication_counter = deque([]),deque([]),deque([])
        num_cycles_single_op,num_cycle_op_allexp=deque([]),deque([])
        totalinftimestamp = deque([])
        totalinftimeallexp = deque([])
        file = open(self.filename, 'r')
        # Convert the file into a list
        fileList = list(file)[self.num_lines_skip:]
        for num,line in enumerate(fileList):
            stripedLine = line.rstrip("\n")
            # print(stripedLine)
            if("boot" in stripedLine):
                bootupdown.append(stripedLine)
                infline=line.split(':')[1]
                # print(infline)
                infdata=infline.split(',') #boot up data
                if(len(infdata[0])<6):   
                    timervalue = int(infdata[0],base=16)
                    overflowcount=abs(int(infdata[1]))
                    timeAfterPrevTimeStamp=self.calcTimeFromStamps(timervalue,overflowcount,self.clk)
                    previnftime += timeAfterPrevTimeStamp
                    num_cycles +=1   
                    num_cycles_op+=1  
            elif("Op" in stripedLine):
                # print(stripedLine)                            
                infline=line.split(':')[1]
                infdata=infline.split(',') #boot up data  
                timervalue = int(infdata[0],base=16)
                overflowcount=abs(int(infdata[1]))
                timeAfterPrevTimeStamp=self.calcTimeFromStamps(timervalue,overflowcount,self.clk)
                previnftime += timeAfterPrevTimeStamp
                operator_time.append(previnftime)
                num_cycles_single_op.append(np.round(num_cycles_op/2))
                num_cycles_op=0
            else:
                # print(stripedLine)
                output.append(stripedLine)
                if(previnftime!=0):
                    # print(stripedLine)
                    infdata=line.split(',') #boot up data
                    compute_counter.append(int(infdata[4]))
                    communication_counter.append(int(infdata[1]))
                    timervalue = int(infdata[2],base=16)
                    overflowcount=abs(int(infdata[3]))
                    timeAfterPrevTimeStamp=self.calcTimeFromStamps(timervalue,overflowcount,self.clk) 
                    previnftime += timeAfterPrevTimeStamp
                    totalinftimeallexp.append(previnftime)
                    totalcycleforinference.append(np.round(num_cycles)/2)
                operator_time_allexp.append(operator_time)
                num_cycle_op_allexp.append(num_cycles_single_op)
                operator_time=deque([])
                num_cycles_single_op =deque([])
                num_cycles=0
                previnftime=0

        totalinftimeallexp=np.asarray(totalinftimeallexp)
        operator_time_allexp=np.array(operator_time_allexp)
        compute_counter=np.array(compute_counter)
        totalcycleforinference=np.asarray(totalcycleforinference)
        communication_counter=np.array(communication_counter)
        num_cycle_op_allexp=np.asarray(num_cycle_op_allexp)
        # print(operator_time_allexp)
        # print(communication_counter)
        bootupdown=list(bootupdown)
        if not bootupdown:
             off_time.append(1)
             on_time.append(1)
        output=list(output)
        boot_index=0

        for num,line in enumerate(bootupdown): 
            # print(line)
            bootupdownline=line.split(':')[1]
            bootupdowndata=bootupdownline.split(',') #boot up data
            if(len(bootupdowndata[0])<6):
                timervalue = int(bootupdowndata[0],base=16)
                overflowcount=abs(int(bootupdowndata[1]))
                timeAfterPrevTimeStamp=self.calcTimeFromStamps(timervalue,overflowcount,self.clk)
                totalTime += timeAfterPrevTimeStamp
                # Calculate time difference from prev Power TX edge
                timeDiff = totalTime - prevRxTime
                bootupdowntime.append(timeDiff)
                prevRxTime = totalTime
                if(boot_index==0):
                    on_time.append(timeDiff)
                    boot_index=1
                else:
                    off_time.append(timeDiff)
                    boot_index=0
                # record time at each edge of Rx
                timeEdgeRx.append(totalTime)  #on time and off time is recorded in bootupdown time alternatively.
                                                  #bootupdowntime[0]- on time bootupdowntime[1] - off time    
                                                  #        
        bootupdowntime=np.asarray(bootupdowntime)
        on_time=np.asarray(on_time)
        off_time=np.asarray(off_time)
        timeEdgeRx=np.asarray(timeEdgeRx)

    def calcTimeFromStamps(self,timeStamp, ovfCount,clk):
        clkFreq = clk*1e06
        timerSize=16
        maxTimerCount = 2**timerSize
        timerResolution = 1/clkFreq
        overflowResolution = maxTimerCount/clkFreq
        time = (timerResolution * timeStamp) + (overflowResolution* ovfCount)     
        return time
def main():
    output_dir='output'
    # if(not os.path.isdir(output_dir)):
    #    os.mkdir(output_dir)
    # path=os.path.join(output_dir,'figures')
    # if(not os.path.isdir(path)):
    #     os.mkdir(path)
    expFiles=["./Exp_data/08_29_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_2_50mF_Dearlydie_2_Cearlydie_1.txt"]
    # for idx, f in enumerate(expFiles):
    Bobber_power=32e-03
    cap_size=50e-03
    on_time=4.5952
    off_time=1.7652
    bobber=boober_characterization(Bobber_power,expFiles,cap_size,on_time,off_time)
    bobber.calc_power_from_ontime()
    bobber.plot_power_breakdown()
    # bobber.plot_pie_chart()
if __name__ == "__main__":
    main()