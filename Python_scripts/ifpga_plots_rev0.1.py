
from cProfile import label
from matplotlib import colors, pyplot as plt
import numpy as np
import pickle
from collections import deque
import ctypes
import struct
from matplotlib.patches import Rectangle
from mpl_toolkits import mplot3d
# 000000000000000000000000000000000000000000000000000000003f7f0000000000000000 -> correct inference output

def calcTimeFromStamps(timeStamp, ovfCount,clk):
    clkFreq = clk*1e06
    timerSize=16
    maxTimerCount = 2**timerSize
    timerResolution = 1/clkFreq
    overflowResolution = maxTimerCount/clkFreq
    time = (timerResolution * timeStamp) + (overflowResolution* ovfCount)     
    return time

def hex_to_float(hexarray,len):
    floatarraytotalexp=deque([])
    floatarrayoneexp=deque([])
    hexarray=list(hexarray)
    # print(hexarray[0][1],len)
    for idx in range(len):
        for i in range(10):
            str1 = ''.join(hexarray[idx][i])
            floatval= struct.unpack('!f', bytes.fromhex(str1))[0]
            floatarrayoneexp.append(floatval)
        floatarraytotalexp.append(floatarrayoneexp)
        floatarrayoneexp=deque([])
    floatarraytotalexp=np.asarray(floatarraytotalexp)
    return floatarraytotalexp

def parsedata(filename,num_lines_skip,clk):
        totalTime, timeAfterPrevTimeStamp = 0, 0
        prevRxTime=0
        totalinftime,timeafterprevInf=0,0
        previnftime=0
        num_cycles=0
        num_cycles_op=0
        delayfor_gpio= 12.5e-06
        totalcycleforinference=deque([])
        bootupdown= deque([])
        on_time,off_time =deque([]),deque([])
        output=deque([])
        timeEdgeRx= deque([])
        timeelapsedforinf=deque([])
        bootupdowntime =deque([])
        inference_time,operator_time,operator_time_allexp =deque([]),deque([]),deque([])
        classoutputs,compute_counter,communication_counter = deque([]),deque([]),deque([])
        num_cycles_single_op,num_cycle_op_allexp=deque([]),deque([])
        totalinftimestamp = deque([])
        totalinftimeallexp = deque([])
        file = open(filename, 'r')
        # Convert the file into a list
        fileList = list(file)[num_lines_skip:]
        for num,line in enumerate(fileList):
            stripedLine = line.rstrip("\n")
            # print(stripedLine)
            if("boot" in stripedLine):
                bootupdown.append(stripedLine)
                infline=line.split(':')[1]
                # print(infline)
                infdata=infline.split(',') #boot up data
                if(len(infdata[0])<6):   
                    timervalue = int(infdata[0],base=16)
                    overflowcount=abs(int(infdata[1]))
                    timeAfterPrevTimeStamp=calcTimeFromStamps(timervalue,overflowcount,clk)
                    previnftime += timeAfterPrevTimeStamp
                    num_cycles +=1   
                    num_cycles_op+=1  
            elif("Op" in stripedLine):
                # print(stripedLine)                            
                infline=line.split(':')[1]
                infdata=infline.split(',') #boot up data  
                timervalue = int(infdata[0],base=16)
                overflowcount=abs(int(infdata[1]))
                timeAfterPrevTimeStamp=calcTimeFromStamps(timervalue,overflowcount,clk)
                previnftime += timeAfterPrevTimeStamp
                operator_time.append(previnftime)
                num_cycles_single_op.append(np.round(num_cycles_op/2))
                num_cycles_op=0

            else:
                # print(stripedLine)
                output.append(stripedLine)
                if(previnftime!=0):
                    # print(stripedLine)
                    infdata=line.split(',') #boot up data
                    compute_counter.append(int(infdata[4]))
                    communication_counter.append(int(infdata[1]))
                    timervalue = int(infdata[2],base=16)
                    overflowcount=abs(int(infdata[3]))
                    timeAfterPrevTimeStamp=calcTimeFromStamps(timervalue,overflowcount,clk) 
                    previnftime += timeAfterPrevTimeStamp
                    totalinftimeallexp.append(previnftime)
                    totalcycleforinference.append(np.round(num_cycles)/2)
                operator_time_allexp.append(operator_time)
                num_cycle_op_allexp.append(num_cycles_single_op)
                operator_time=deque([])
                num_cycles_single_op =deque([])
                num_cycles=0
                previnftime=0

        totalinftimeallexp=np.asarray(totalinftimeallexp)
        operator_time_allexp=np.array(operator_time_allexp)
        compute_counter=np.array(compute_counter)
        totalcycleforinference=np.asarray(totalcycleforinference)
        communication_counter=np.array(communication_counter)
        num_cycle_op_allexp=np.asarray(num_cycle_op_allexp)
        # print(operator_time_allexp)
        # print(communication_counter)
        bootupdown=list(bootupdown)
        if not bootupdown:
             off_time.append(1)
             on_time.append(1)
        output=list(output)
        boot_index=0

        for num,line in enumerate(bootupdown): 
            # print(line)
            bootupdownline=line.split(':')[1]
            bootupdowndata=bootupdownline.split(',') #boot up data
            if(len(bootupdowndata[0])<6):
                timervalue = int(bootupdowndata[0],base=16)
                overflowcount=abs(int(bootupdowndata[1]))
                timeAfterPrevTimeStamp=calcTimeFromStamps(timervalue,overflowcount,clk)
                totalTime += timeAfterPrevTimeStamp
                # Calculate time difference from prev Power TX edge
                timeDiff = totalTime - prevRxTime
                bootupdowntime.append(timeDiff)
                prevRxTime = totalTime
                if(boot_index==0):
                    on_time.append(timeDiff)
                    boot_index=1
                else:
                    off_time.append(timeDiff)
                    boot_index=0
                # record time at each edge of Rx
                timeEdgeRx.append(totalTime)  #on time and off time is recorded in bootupdown time alternatively.
                                                  #bootupdowntime[0]- on time bootupdowntime[1] - off time    
                                                  #        
        bootupdowntime=np.asarray(bootupdowntime)
        on_time=np.asarray(on_time)
        off_time=np.asarray(off_time)
#         print(bootdowntime)
        timeEdgeRx=np.asarray(timeEdgeRx)
        # print(timeEdgeRx)
        for num,line in enumerate(output):
            outputdata=line.split(',') #outputdata[0]-> Inference output, outputdata[2],outputdata[3] -> Inference time stamps
            if(len(outputdata[0])==80):
                classoutputs.append(outputdata[0])
                timervalue = int(outputdata[2],base=16)
                overflowcount=abs(int(outputdata[3]))
                timeafterprevInf=calcTimeFromStamps(timervalue,overflowcount,clk)
                totalinftime += timeafterprevInf
                timeDiff = totalinftime - previnftime
                inference_time.append(timeDiff)
                previnftime = totalinftime                
                totalinftimestamp.append(totalinftime)
        inference_time=np.asarray(inference_time)
        classoutputs=np.asarray(classoutputs)
        return on_time,off_time,classoutputs,totalinftimeallexp,totalcycleforinference,num_cycle_op_allexp,operator_time_allexp,compute_counter,communication_counter

def plot_box_plot_latencies_cont_power(data1,data2,data3,data4,numBoxes):
    lentoplot=20
    # positions=np.arange(1,numBoxes+1)
    positions=[1,2,3]
    medians = list(range(numBoxes))
    data1=data1[0:lentoplot].reshape(lentoplot,1)
    data2=data2[0*lentoplot:1*lentoplot].reshape(lentoplot,1)
    data3=data3[0*lentoplot:1*lentoplot].reshape(lentoplot,1)
    data4=data4[0*lentoplot:1*lentoplot].reshape(lentoplot,1)

    data_to_plot=np.hstack((data1,data2,data3,data4))

    top=np.max(data_to_plot)+30
    
    fig, ax = plt.subplots()
    params = {'figure.figsize': [5, 10],
            'legend.fontsize': 15}
    plt.rcParams.update(params)
    # plt.title(f"LENET Inference Latencies HELIA- \nObservation for 20 inferences-Continuous power" , fontsize=20)
    # plt.xlabel('Test', size=15)
    plt.ylabel('Latency(s)', size=20)

    ax.xaxis.set_major_locator(plt.MultipleLocator(10))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(5))

    ax.boxplot(data_to_plot,positions=positions,showfliers=False,showmeans=True)
    means = [np.mean(data) for data in data_to_plot.T]
    i=0
    for tick in range(numBoxes):
        if(i==0):
            ax.text(positions[tick],top -top*0.1, np.round(means[tick],decimals=2),horizontalalignment='center',color='green',label='Average',fontsize=20)
            i=1
        else:
            ax.text(positions[tick],top -top*0.1, np.round(means[tick],decimals=2),horizontalalignment='center',color='green',fontsize=20 )
    
    ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
               alpha=0.5)

    plt.xticks([1, 2, 3, 4], ['Baseline', 'Baseline \n w/ctpl' , 'HELP-BLIP' , 'HELP-BLIP \n Dtile-2 Ctile-2' ],fontsize=20)

    plt.legend(loc='best')
    # plt.xscale("log")
    # plt.yscale("log")
    
    plt.xlim(0.5,4.5)
    plt.ylim(0,top+10)

    fig.set_size_inches(12, 5)
    plt.tight_layout()
    plt.savefig(f'latency_cont_power_rev0.1.pdf', dpi=250, format='pdf')
    plt.show()

def plot_box_plot_latencies_intermittent_power(data1,data1ontime,data2,data2ontime,data3,data3ontime,data4,data4ontime,data5,data5ontime,data6,data6ontime,data7,data7ontime,data8,data8ontime,numBoxes):
    lentoplot=9
    # positions=np.arange(1,numBoxes+1)
    positions=[2,3,4,6,7,8]

    data1=data1[0:lentoplot].reshape(lentoplot,1)
    data2=data2[0:lentoplot].reshape(lentoplot,1)
    data3=data3[0:lentoplot].reshape(lentoplot,1)
    data4=data4[0:lentoplot].reshape(lentoplot,1)
    data5=data5[0:lentoplot].reshape(lentoplot,1)
    data6=data6[0:lentoplot].reshape(lentoplot,1)
    data7=data7[0:lentoplot].reshape(lentoplot,1)
    data8=data8[0:lentoplot].reshape(lentoplot,1)

    data_to_plot=np.hstack((data2,data3,data4,data6,data7,data8))
    numBoxes=len(data_to_plot.T)
    medians = list(range(numBoxes))

    top=np.max(data_to_plot)+30
  

    fig, ax = plt.subplots()
    params = {'figure.figsize': [5, 10],
            'legend.fontsize': 15}
    plt.rcParams.update(params)
    plt.title(f"Lenet Inference Latencies HELP-BLIP-\n Observation under Intermittent power" , fontsize=15)
    # plt.xlabel('Test', size=15)
    plt.ylabel('Latency(s)', size=15)

    ax.xaxis.set_major_locator(plt.MultipleLocator(10))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(5))

    ax.boxplot(data_to_plot,positions=positions,showfliers=False,showmeans=True)
    means = [np.mean(data) for data in data_to_plot.T]
    i=0
    for tick in range(numBoxes):
        if(i==0):
            ax.text(positions[tick],top +top*0.1, np.round(means[tick],decimals=2),horizontalalignment='center',color='green',label='Average',fontsize=12)
            i=1
        else:
            ax.text(positions[tick],top +top*0.1, np.round(means[tick],decimals=2),horizontalalignment='center',color='green',fontsize=12  )
    ax.text(1,0.95,'X',horizontalalignment='center',color='red',fontsize=15,label='Do not complete')
    ax.text(5,0.95,'X',horizontalalignment='center',color='red',fontsize=15)
    # ax.text(4,0.95,'X',horizontalalignment='center',color='red',fontsize=15)

    ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
               alpha=0.5)

    plt.xticks([1, 2, 3, 4,5,6,7,8], [f"Baseline \n ontime={np.round(np.mean(data2ontime),decimals=2)}s \n cap 144mF", 
                            #   f"Baseline w/ctpl\nontime={np.round(np.mean(data1ontime),decimals=2)}s \n cap 50mF",
                              f"Baseline w/ctpl\nontime={np.round(np.mean(data2ontime),decimals=2)}s \n cap 144mF",
                              f"Baseline w/ctpl\nontime={np.round(np.mean(data3ontime),decimals=2)}s \n cap 244mF",
                              f"Baseline w/ctpl\nontime={np.round(np.mean(data4ontime),decimals=2)}s \n cap 344mF",
                              f"HELP-BLIP w/ctpl \nontime={np.round(np.mean(data6ontime),decimals=2)}s \n cap 144mF",
                            #   f"HELP-BLIP,\n ontime={np.round(np.mean(data5ontime),decimals=2)}s \n cap 50mF \nDtile-7 Ctile-5",
                              f"HELP-BLIP,\n ontime={np.round(np.mean(data6ontime),decimals=2)}s \n cap 144mF\nDtile-7 Ctile-2",
                              f"HELP-BLIP,\n ontime={np.round(np.mean(data7ontime),decimals=2)}s \n cap 244mF\nDtile-4 Ctile-2",
                              f"HELP-BLIP,\n ontime={np.round(np.mean(data8ontime),decimals=2)}s \n cap 344mF\nDtile-2 Ctile-2"], fontsize=12)
    plt.legend(loc='best')
    # plt.xscale("log")
    # plt.yscale("log")
    
    plt.xlim(0.5,numBoxes+2.5)
    plt.ylim(0,top+100)

    fig.set_size_inches(15,5)
    plt.tight_layout()
    plt.savefig(f'latency_intermittent_powerrev0.1_144mF_244mF_344mF_v2.pdf', dpi=250, format='pdf')
    plt.show()

def numcyclesforinferenceplot_intermittent_power(data1,data1ontime,data2,data2ontime,data3,data3ontime,data4,data4ontime,data5,data5ontime,data6,data6ontime,data7,data7ontime,data8,data8ontime,numBoxes):
    lentoplot=3
    # positions=np.arange(1,numBoxes+1)
    positions=[2,3,4,5]

    data1=data1[0:lentoplot].reshape(lentoplot,1)
    data2=data2[0:lentoplot].reshape(lentoplot,1)
    data3=data3[0:lentoplot].reshape(lentoplot,1)
    data4=data4[0:lentoplot].reshape(lentoplot,1)
    data5=data5[0:lentoplot].reshape(lentoplot,1)
    data6=data6[0:lentoplot].reshape(lentoplot,1)
    data7=data7[0:lentoplot].reshape(lentoplot,1)
    data8=data8[0:lentoplot].reshape(lentoplot,1)

    data_to_plot=[np.mean(data5),np.mean(data6),np.mean(data7),np.mean(data8)]
    numBoxes=len(data_to_plot)
    medians = list(range(numBoxes))

    top=np.max(data_to_plot)+30

    fig, ax = plt.subplots()
    params = {'figure.figsize': [5, 10],
            'legend.fontsize': 15}
    plt.rcParams.update(params)

    # plt.xlabel('Test', size=15)
    plt.ylabel('Intermittent cycles', size=15)

    ax.xaxis.set_major_locator(plt.MultipleLocator(10))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(5))
    width=0.35
    i=0
    for num,tick in enumerate(positions):
        ax.bar(tick,data_to_plot[num],width,color='green',hatch="//")

    # ax.text(1,0.95,'X',horizontalalignment='center',color='red',fontsize=15,label='Do not complete')
    ax.plot(1,15,'X',color='red',ms=15,label='Do not complete')

    # ax.text(2,0.95,'X',horizontalalignment='center',color='red',fontsize=15)
    # ax.plot(5,10,'X',color='red',ms=15)
    ax.yaxis.grid(True, linestyle='-', which='major', color='lightgrey',
            alpha=0.5)
    plt.xticks([1, 2, 3, 4,5], [f"Baseline", 
                            #   f"Baseline w/ctpl\nontime={np.round(np.mean(data1ontime),decimals=2)}s \n cap 50mF",
                              f"HELP-BLIP,\n cap 50mF ",
                              f"HELP-BLIP \n cap 144mF",
                              f"HELP-BLIP,\n cap 244mF",
                              f"HELP-BLIP \n cap 344mF"], fontsize=9)
    plt.legend(loc='best',fontsize=15)
    # plt.xscale("log")
    # plt.yscale("log")
    
    plt.xlim(0.5,numBoxes+0.5)
    plt.ylim(0,top+100)

    fig.set_size_inches(7,3)
    plt.tight_layout()
    plt.savefig(f'numcycles_intermittent_power0.1_144mF_244mF_344mF_v1.pdf', dpi=250, format='pdf')
    plt.show()

def plot_energy_overhead( baseline_w_ctpl_II,baseline_w_ctpl_numcycles_II,baseline_w_ctpl_compute_counter_II,baseline_w_ctpl_comm_counter_II,baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II,baseline_w_ctpl_op_numcycles_II,
                          helpblip_intpower_II,helpblip_intpower_numcycles_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II,helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,helpblip_intpower_op_numcycles_II):

    spi_cycle_time=(1/(8e06/3))
    spi_cycle_power=0.00724
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=143e-03
    msp_boot_time=8.64e-03
    width=0.5
    y_min=0.001
    y_max=100
    depthwise_index=2
    conv_index=4
    cap_energy=0.03759
    fpga_boot_energy=2.32E-04
    fpga_boot_power=0.125

    save_activation_record=4.513e-06
    save_activation_record_energy=0.16113e-06
    restore_activation_record=7.2517e-06
    restore_activation_record_energy=2.389e-06
    # restore_activation_record_energy=0.16113e-06
    save_cpu_stack_time=611.375e-06
    save_cpu_stack_energy=(save_activation_record_energy/save_activation_record)*save_cpu_stack_time

    restore_cpu_stack_time=819.406e-06
    restore_cpu_stack_energy=(restore_activation_record_energy/restore_activation_record)*restore_cpu_stack_time

    # baseline_w_ctpl_II=np.mean(baseline_w_ctpl_II,axis=0)
    helpblip_intpower_II=np.mean(helpblip_intpower_II,axis=0)

    # baseline_w_ctpl_numcycles_II=np.mean(baseline_w_ctpl_numcycles_II,axis=0)
    helpblip_intpower_numcycles_II=np.mean(helpblip_intpower_numcycles_II,axis=0)

    # baseline_w_ctpl_op_numcycles_II=np.mean(baseline_w_ctpl_op_numcycles_II,axis=0)
    helpblip_intpower_op_numcycles_II=np.mean(helpblip_intpower_op_numcycles_II,axis=0)

    # print(helpblip_intpower_ontime_II)
    # baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II=np.mean(baseline_w_ctpl_ontime_II),np.mean(baseline_w_ctpl_offtime_II)
    helpblip_intpower_ontime_II,helpblip_intpower_offtime_II=np.mean(helpblip_intpower_ontime_II),np.mean(helpblip_intpower_offtime_II)

    # baseline_wctpl_power=cap_energy/baseline_w_ctpl_ontime_II

    helpblip_power=5.84e-02
    
    # print(f'power{helpblip_power}')
    helpblip_intpower_boot_overhead_II=(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[conv_index])*fpga_boot_time
    helpblip_intpower_fpgacomm_Dtile_2_Ctile_5=np.mean(helpblip_intpower_commcounter_II)*((7*spi_cycle_time)+(spi_cycle_time/2))

    helpblip_intpower_fpgacomm_II=np.mean(helpblip_intpower_commcounter_II)*((7*spi_cycle_time)+(spi_cycle_time/2))

    # baseline_wctpl_save_stack_overhead=save_cpu_stack_energy*baseline_w_ctpl_numcycles_II
    # baseline_wctpl_restore_stack_overhead=restore_cpu_stack_energy*baseline_w_ctpl_numcycles_II
    # baseline_wctpl_save_activation_record_overhead=0
    # baseline_wctpl_restore_activation_record_overhead=0
    # baseline_wctpl_datamovement=0
    # baseline_wctpl_depthwise=baseline_w_ctpl_op_numcycles_II[depthwise_index]*cap_energy
    # baseline_wctpl_conv=baseline_w_ctpl_op_numcycles_II[conv_index]*cap_energy
    # baseline_boot_overhead=0
    # baseline_wctpl_overall=baseline_wctpl_power*baseline_w_ctpl_II[9]
    

    helpblip_save_stack_overhead=save_cpu_stack_energy*helpblip_intpower_numcycles_II
    helpblip_restore_stack_overhead=restore_cpu_stack_energy*helpblip_intpower_numcycles_II
    helpblip_save_activation_record_overhead=save_activation_record_energy*(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[conv_index])
    helpblip_restore_activation_record_overhead=restore_activation_record_energy*(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[conv_index])
    helpblip_datamovement=helpblip_intpower_fpgacomm_II*spi_cycle_power
    # helpblip_boot_overhead_depthwise=fpga_boot_power*fpga_boot_time*(helpblip_intpower_op_numcycles_II[depthwise_index])
    # helpblip_boot_overhead_conv=fpga_boot_power*fpga_boot_time*(helpblip_intpower_op_numcycles_II[conv_index])
    helpblip_boot_overhead_depthwise=fpga_boot_energy*(helpblip_intpower_op_numcycles_II[depthwise_index])
    helpblip_boot_overhead_conv=fpga_boot_energy*(helpblip_intpower_op_numcycles_II[conv_index])
    # print(helpblip_intpower_op_numcycles_II[depthwise_index],helpblip_intpower_op_numcycles_II[conv_index])
    helpblip_boot_overhead=helpblip_boot_overhead_depthwise+helpblip_boot_overhead_conv
    
    helpblip_depthwise=(helpblip_intpower_op_numcycles_II[depthwise_index]*cap_energy)-helpblip_boot_overhead_depthwise
    
    helpblip_conv=(helpblip_intpower_op_numcycles_II[conv_index]*cap_energy)-helpblip_boot_overhead_conv
    helpblip_overall=helpblip_save_stack_overhead+helpblip_restore_stack_overhead+helpblip_save_activation_record_overhead+helpblip_restore_activation_record_overhead+helpblip_datamovement+helpblip_boot_overhead+helpblip_depthwise+helpblip_conv

    # helpblip_overall=helpblip_power*helpblip_intpower_II[9]

    fig,ax1=plt.subplots()

    # rects1 = ax1.bar(0,baseline_wctpl_save_stack_overhead,width,color='mediumorchid',hatch="//",label='Save stack')
    # rects2 = ax1.bar(0.5,baseline_wctpl_restore_stack_overhead,width,color='lightcoral',hatch="**",label='Restore stack')
    # rects3 = ax1.bar(1,baseline_wctpl_save_activation_record_overhead,width,color='lightblue', hatch="..",label='Save activation record')
    # rects4 = ax1.bar(1.5,baseline_wctpl_restore_activation_record_overhead,width,color='lightgreen',hatch="\\",label='Restore activation record')
    # rects12 = ax1.bar(2,baseline_wctpl_datamovement,width,color='lightslategrey',hatch="oo",label='Datamovement')
    # rects9 = ax1.bar(2.5,baseline_wctpl_depthwise,width,color='orange',hatch="||",label='Live-Depthwise')
    # rects10 = ax1.bar(3,baseline_wctpl_conv,width,color='purple',hatch="++",label='Live-Conv')
    # rects11 = ax1.bar(3.5,baseline_boot_overhead,width,color='navy', hatch="xx",label='Boot')
    # rects12 = ax1.bar(4,baseline_wctpl_overall,width,color='cyan',hatch="--",label='Overall')

    rects1 = ax1.bar(0,0,width,color='mediumorchid',hatch="//",label='Save Stack')
    rects2 = ax1.bar(0.5,0,width,color='lightcoral',hatch="**",label='Restore Stack')
    rects3 = ax1.bar(1,0,width,color='lightblue', hatch="..",label='Save Activation Record')
    rects4 = ax1.bar(1.5,0,width,color='lightgreen',hatch="\\",label='Restore Activation Record')
    rects11 = ax1.bar(3.5,0,width,color='navy', hatch="xx",label='Boot')

    rects12 = ax1.bar(2,0,width,color='lightslategrey',hatch="oo",label='Data Movement')
    rects9 = ax1.bar(2.5,0,width,color='orange',hatch="||",label='Depthwise')
    rects10 = ax1.bar(3,0,width,color='purple',hatch="++",label='Conv')
    rects12 = ax1.bar(4,0,width,color='cyan',hatch="--",label='Overall')
    

    # ax1.plot([],[],'X',color='red',ms=15,label='NA')
    # ax1.plot(1,5e-06,'X',color='red',ms=15)
    # ax1.plot(1.5,5e-06,'X',color='red',ms=15)
    # ax1.plot(2,5e-06,'X',color='red',ms=15)
    # ax1.plot(3.5,5e-06,'X',color='red',ms=15)

    # print("overhead",helpblip_overall/baseline_wctpl_overall)
    rects1 = ax1.bar(0,helpblip_save_stack_overhead,width,color='mediumorchid',hatch="//")
    rects2 = ax1.bar(0.5,helpblip_restore_stack_overhead,width,color='lightcoral',hatch="**")
    rects3 = ax1.bar(1,helpblip_save_activation_record_overhead,width,color='lightblue', hatch="..")
    rects4 = ax1.bar(1.5,helpblip_restore_activation_record_overhead,width,color='lightgreen',hatch="\\")
    rects11 = ax1.bar(2,helpblip_boot_overhead,width,color='navy', hatch="xx")
    rects12 = ax1.bar(2.5,helpblip_datamovement,width,color='lightslategrey',hatch="oo")
    rects9 = ax1.bar(3,helpblip_depthwise,width,color='orange',hatch="||")
    rects10 = ax1.bar(3.5,helpblip_conv,width,color='purple',hatch="++")
    rects12 = ax1.bar(4,helpblip_overall,width,color='cyan',hatch="--")
    # print(helpblip_restore_activation_record_overhead+helpblip_restore_stack_overhead+helpblip_boot_overhead/helpblip_overall)
    ax1.set_ylabel('Energy (J)',fontsize=30)
    ax1.set_xticks([2], ['BOBBER'],fontsize=25)
    ax1.text(1,-3.8,'Intermittent power 144mF',horizontalalignment='center', color='black',fontsize=18)
    # print(helpblip_restore_stack_overhead,helpblip_boot_overhead,helpblip_overall)
    ax1.legend(loc='upper left', bbox_to_anchor=(-0.15, 1.18),ncol=5,fontsize=10)
    ax1.set_yscale('log')
    ax1.set_xlim(-0.5,4.5)
    ax1.grid()
    ax1.tick_params(axis='x', which='both', labelsize=25,width=3)
    ax1.tick_params(axis='y', which='both', labelsize=12,width=1)
    
    fig.set_size_inches(8.5, 4)
    plt.savefig(f'energy_overhead_rel0.2_rebuttal.pdf', dpi=250, quality=95, format='pdf')
    plt.show()
    
def plot_comparebaseline_stacked_latencies_cont_power(baseline,heliaopt1,heliaopt2):

    baseline=np.mean(baseline,axis=0)
    heliaopt1=np.mean(heliaopt1,axis=0)
    heliaopt2=np.mean(heliaopt2,axis=0)

    x=np.linspace(1,len(baseline),num=len(baseline))
    width=0.25
    j=0
    fig, ax = plt.subplots()
    params = {'figure.figsize': [5, 12],
                'legend.fontsize': 12}
    plt.rcParams.update(params)
    for i in range(len(x)-1):
        if(j==0):
            ax.bar(x[i]-width,baseline[i+1]-baseline[i],width,color='lightcoral', hatch="//",label='Baseline')
            plt.text(x[i]-width,baseline[i+1]-baseline[i],np.round(baseline[i+1]-baseline[i],decimals=2), ha = 'center',rotation=45)
            ax.bar(x[i],heliaopt1[i+1]-heliaopt1[i],width,color='lightblue', hatch="--",label='Baseline w/ctpl')
            plt.text(x[i],heliaopt1[i+1]-heliaopt1[i],np.round(heliaopt1[i+1]-heliaopt1[i],decimals=2),ha = 'center',rotation=45)
            ax.bar(x[i]+width,heliaopt2[i+1]-heliaopt2[i],width,color='lightgreen',hatch="//",label='HELP-BLIP')
            plt.text(x[i]+width,heliaopt2[i+1]-heliaopt2[i],np.round(heliaopt2[i+1]-heliaopt2[i],decimals=2),ha = 'center',rotation=45)

            j=1
        else:
            ax.bar(x[i]-width,baseline[i+1]-baseline[i],width,color='lightcoral', hatch="//")
            plt.text(x[i]-width,baseline[i+1]-baseline[i],np.round(baseline[i+1]-baseline[i],decimals=2), ha = 'center',rotation=45)
            ax.bar(x[i],heliaopt1[i+1]-heliaopt1[i],width,color='lightblue', hatch="--")
            plt.text(x[i],heliaopt1[i+1]-heliaopt1[i],np.round(heliaopt1[i+1]-heliaopt1[i],decimals=2),ha = 'center',rotation=45)
            ax.bar(x[i]+width,heliaopt2[i+1]-heliaopt2[i],width,color='lightgreen',hatch="\\")
            plt.text(x[i]+width,heliaopt2[i+1]-heliaopt2[i],np.round(heliaopt2[i+1]-heliaopt2[i],decimals=2),ha = 'center',rotation=45)


    ax.set_ylabel('Inference Latency (s)',fontsize=15)
    plt.xticks([1,2,3,4,5,6,7,8,9], ['Quant', 'Depthwise', 'Pool', 'Conv', 'pool', 'Conv', 'FC','FC', 'Softmax'],fontsize=10,rotation=20),
    ax.legend(ncol=1,fontsize=12,loc='best')
    ax.yaxis.set_major_locator(plt.MultipleLocator(2))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(1))
    # ax.set_yscale('log')
    ax.set_xlim(0,9.5)
    # plt.subplots_adjust(right=0.85)
    fig.tight_layout()
    fig.set_size_inches(10, 4.8)
    plt.savefig(f'baselinsvsheliarev0.1.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_computation_scheduling_344mf(  
                                inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1,
                                inftime_Dtile2_Ctile2,op_Dtile2_Ctile2,comp_Dtile2_Ctile2,comm_Dtile2_Ctile2, 
                                helpblip_intpower,helpblip_intpower_numcycles,helpblip_intpower_compute_counter,helpblip_intpower_commcounter,helpblip_intpower_ontime,helpblip_intpower_offtime,helpblip_intpower_op_numcycles,
                                inftime_Dtile2_Ctile5,op_Dtile2_Ctile5,comp_Dtile2_Ctile5,comm_Dtile2_Ctile5, 
                                helpblip_intpower_II,helpblip_intpower_numcycles_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II,helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,helpblip_intpower_op_numcycles_II,
                                inftime_Dtile2_Ctile10,op_Dtile2_Ctile10,comp_Dtile2_Ctile10,comm_Dtile2_Ctile10, 
                                helpblip_intpower_III,helpblip_intpower_numcycles_III,helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III,helpblip_intpower_ontime_III,helpblip_intpower_offtime_III,helpblip_intpower_op_numcycles_III,
                                inftime_Dtile4_Ctile2,op_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2, 
                                helpblip_intpower_IV,helpblip_intpower_numcycles_IV,helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV,helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV,helpblip_intpower_op_numcycles_IV,
                                inftime_Dtile4_Ctile5,op_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5,
                                helpblip_intpower_V,helpblip_intpower_numcycles_V,helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V,helpblip_intpower_ontime_V,helpblip_intpower_offtime_V,helpblip_intpower_op_numcycles_V,
                                inftime_Dtile4_Ctile10,op_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10,
                                helpblip_intpower_VI,helpblip_intpower_numcycles_VI,helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI,helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI,helpblip_intpower_op_numcycles_VI,
                                inftime_Dtile7_Ctile2,op_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2,
                                helpblip_intpower_VII,helpblip_intpower_numcycles_VII,helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII,helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII,helpblip_intpower_op_numcycles_VII,
                                inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
                                helpblip_intpower_VIII,helpblip_intpower_numcycles_VIII,helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII,helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII,helpblip_intpower_op_numcycles_VIII,
                                inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
                                helpblip_intpower_IX,helpblip_intpower_numcycles_IX,helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX,helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX,helpblip_intpower_op_numcycles_IX,
                                inftime_Dtile14_Ctile2,op_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2,
                                helpblip_intpower_X,helpblip_intpower_numcycles_X,helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X,helpblip_intpower_ontime_X,helpblip_intpower_offtime_X,helpblip_intpower_op_numcycles_X,
                                inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
                                helpblip_intpower_XI,helpblip_intpower_numcycles_XI,helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI,helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI,helpblip_intpower_op_numcycles_XI,
                                inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
                                helpblip_intpower_XII,helpblip_intpower_numcycles_XII,helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII,helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII,helpblip_intpower_op_numcycles_XII,
                                inftime_Dtile28_Ctile2,op_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2,
                                helpblip_intpower_XIII,helpblip_intpower_numcycles_XIII,helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII,helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII,helpblip_intpower_op_numcycles_XIII,
                                inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
                                helpblip_intpower_XIV,helpblip_intpower_numcycles_XIV,helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV,helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV,helpblip_intpower_op_numcycles_XIV,
                                inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
                                helpblip_intpower_XV,helpblip_intpower_numcycles_XV,helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV,helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV,helpblip_intpower_op_numcycles_XV):
    
    spi_cycle_time=(1/(8e06/3))
    spi_cycle_power=0.00724
    fpga_clock=1/20e06
   
    dtile=[4,16,49,196,784]
    # dtile=[4,16,49]
    ctile=[4,25,100]
    X=np.arange(len(dtile)*len(ctile))
    cap_size=344e-03
    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=130e-03+8.64e-03
    fpga_boot_energy=2.32E-04
    cap_energy=0.03759-fpga_boot_energy
    mean_power_system = 55e-03
    # msp_boot_time=5e-03
    depthwise_index=2
    conv_index=4
    label=['Knob-1', 'Knob-2','Knob-3','Knob-4','Knob-5', 'Knob-6','Knob-7','Knob-8',
           'Knob-9', 'Knob-10','Knob-11','Knob-12','Knob-13', 'Knob-14','Knob-15']
    
    
    helpblip_intpower_numcycles=np.mean(helpblip_intpower_numcycles,axis=0)
    helpblip_intpower_numcycles_II=np.mean(helpblip_intpower_numcycles_II,axis=0)
    helpblip_intpower_numcycles_III=np.mean(helpblip_intpower_numcycles_III,axis=0)
    helpblip_intpower_numcycles_IV=np.mean(helpblip_intpower_numcycles_IV,axis=0)
    helpblip_intpower_numcycles_V=np.mean(helpblip_intpower_numcycles_V,axis=0)
    helpblip_intpower_numcycles_VI=np.mean(helpblip_intpower_numcycles_VI,axis=0)
    helpblip_intpower_numcycles_VII=np.mean(helpblip_intpower_numcycles_VII,axis=0)
    helpblip_intpower_numcycles_VIII=np.mean(helpblip_intpower_numcycles_VIII,axis=0)
    helpblip_intpower_numcycles_IX=np.mean(helpblip_intpower_numcycles_IX,axis=0)
    helpblip_intpower_numcycles_X=np.mean(helpblip_intpower_numcycles_X,axis=0)
    helpblip_intpower_numcycles_XI=np.mean(helpblip_intpower_numcycles_XI,axis=0)
    helpblip_intpower_numcycles_XII=np.mean(helpblip_intpower_numcycles_XII,axis=0)
    helpblip_intpower_numcycles_XIII=np.mean(helpblip_intpower_numcycles_XIII,axis=0)
    helpblip_intpower_numcycles_XIV=np.mean(helpblip_intpower_numcycles_XIV,axis=0)
    helpblip_intpower_numcycles_XV=np.mean(helpblip_intpower_numcycles_XV,axis=0)

    helpblip_intpower_ontime,helpblip_intpower_offtime=np.mean(helpblip_intpower_ontime),np.mean(helpblip_intpower_offtime)
    helpblip_intpower_ontime_II,helpblip_intpower_offtime_II=np.mean(helpblip_intpower_ontime_II),np.mean(helpblip_intpower_offtime_II)
    helpblip_intpower_ontime_III,helpblip_intpower_offtime_III=np.mean(helpblip_intpower_ontime_III),np.mean(helpblip_intpower_offtime_III)
    helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV=np.mean(helpblip_intpower_ontime_IV),np.mean(helpblip_intpower_offtime_IV)
    helpblip_intpower_ontime_V,helpblip_intpower_offtime_V=np.mean(helpblip_intpower_ontime_V),np.mean(helpblip_intpower_offtime_V)
    helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI=np.mean(helpblip_intpower_ontime_VI),np.mean(helpblip_intpower_offtime_VI)
    helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII=np.mean(helpblip_intpower_ontime_VII),np.mean(helpblip_intpower_offtime_VII)
    helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII=np.mean(helpblip_intpower_ontime_VIII),np.mean(helpblip_intpower_offtime_VIII)
    helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX=np.mean(helpblip_intpower_ontime_IX),np.mean(helpblip_intpower_offtime_IX)
    helpblip_intpower_ontime_X,helpblip_intpower_offtime_X=np.mean(helpblip_intpower_ontime_X),np.mean(helpblip_intpower_offtime_X)
    helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI=np.mean(helpblip_intpower_ontime_XI),np.mean(helpblip_intpower_offtime_XI)
    helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII=np.mean(helpblip_intpower_ontime_XII),np.mean(helpblip_intpower_offtime_XII)
    helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII=np.mean(helpblip_intpower_ontime_XIII),np.mean(helpblip_intpower_offtime_XIII)
    helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV=np.mean(helpblip_intpower_ontime_XIV),np.mean(helpblip_intpower_offtime_XIV)
    helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV=np.mean(helpblip_intpower_ontime_XV),np.mean(helpblip_intpower_offtime_XV)

    mean_power_Dtile_1_Ctile_1=cap_energy/helpblip_intpower_ontime
    mean_power_Dtile_2_Ctile_2=cap_energy/helpblip_intpower_ontime
    mean_power_Dtile_2_Ctile_5=cap_energy/helpblip_intpower_ontime_II
    mean_power_Dtile_2_Ctile_10=cap_energy/helpblip_intpower_ontime_III
    mean_power_Dtile_4_Ctile_2=cap_energy/helpblip_intpower_ontime_IV
    mean_power_Dtile_4_Ctile_5=cap_energy/helpblip_intpower_ontime_V
    mean_power_Dtile_4_Ctile_10=cap_energy/helpblip_intpower_ontime_VI
    mean_power_Dtile_7_Ctile_2=cap_energy/helpblip_intpower_ontime_VII
    mean_power_Dtile_7_Ctile_5=cap_energy/helpblip_intpower_ontime_VIII
    mean_power_Dtile_7_Ctile_10=cap_energy/helpblip_intpower_ontime_IX
    mean_power_Dtile_14_Ctile_2=cap_energy/helpblip_intpower_ontime_X
    mean_power_Dtile_14_Ctile_5=cap_energy/helpblip_intpower_ontime_XI
    mean_power_Dtile_14_Ctile_10=cap_energy/helpblip_intpower_ontime_XII
    mean_power_Dtile_28_Ctile_2=cap_energy/helpblip_intpower_ontime_XIII
    mean_power_Dtile_28_Ctile_5=cap_energy/helpblip_intpower_ontime_XIV
    mean_power_Dtile_28_Ctile_10=cap_energy/helpblip_intpower_ontime_XV

    
    ###############continuous power calculations###################

    inftime_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1=np.mean(inftime_Dtile1_Ctile1),np.mean(comp_Dtile1_Ctile1),np.mean(comm_Dtile1_Ctile1)
    inftime_Dtile2_Ctile2,comp_Dtile2_Ctile2,comm_Dtile2_Ctile2=np.mean(inftime_Dtile2_Ctile2),np.mean(comp_Dtile2_Ctile2),np.mean(comm_Dtile2_Ctile2)
    inftime_Dtile2_Ctile5,comp_Dtile2_Ctile5,comm_Dtile2_Ctile5=np.mean(inftime_Dtile2_Ctile5),np.mean(comp_Dtile2_Ctile5),np.mean(comm_Dtile2_Ctile5)
    inftime_Dtile2_Ctile10,comp_Dtile2_Ctile10,comm_Dtile2_Ctile10=np.mean(inftime_Dtile2_Ctile10),np.mean(comp_Dtile2_Ctile10),np.mean(comm_Dtile2_Ctile10)
    inftime_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2=np.mean(inftime_Dtile4_Ctile2),np.mean(comp_Dtile4_Ctile2),np.mean(comm_Dtile4_Ctile2) 
    inftime_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5=np.mean(inftime_Dtile4_Ctile5),np.mean(comp_Dtile4_Ctile5),np.mean(comm_Dtile4_Ctile5) 
    inftime_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10=np.mean(inftime_Dtile4_Ctile10),np.mean(comp_Dtile4_Ctile10),np.mean(comm_Dtile4_Ctile10)
    inftime_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2=np.mean(inftime_Dtile7_Ctile2),np.mean(comp_Dtile7_Ctile2),np.mean(comm_Dtile7_Ctile2)
    inftime_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5=np.mean(inftime_Dtile7_Ctile5),np.mean(comp_Dtile7_Ctile5),np.mean(comm_Dtile7_Ctile5)
    inftime_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10=np.mean(inftime_Dtile7_Ctile10),np.mean(comp_Dtile7_Ctile10),np.mean(comm_Dtile7_Ctile10)
    inftime_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2=np.mean(inftime_Dtile14_Ctile2),np.mean(comp_Dtile14_Ctile2),np.mean(comm_Dtile14_Ctile2)
    inftime_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5=np.mean(inftime_Dtile14_Ctile5),np.mean(comp_Dtile14_Ctile5),np.mean(comm_Dtile14_Ctile5)
    inftime_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10=np.mean(inftime_Dtile14_Ctile10),np.mean(comp_Dtile14_Ctile10),np.mean(comm_Dtile14_Ctile10)
    inftime_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2=np.mean(inftime_Dtile28_Ctile2),np.mean(comp_Dtile28_Ctile2),np.mean(comm_Dtile28_Ctile2)
    inftime_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5=np.mean(inftime_Dtile28_Ctile5),np.mean(comp_Dtile28_Ctile5),np.mean(comm_Dtile28_Ctile5)
    inftime_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10=np.mean(inftime_Dtile28_Ctile10),np.mean(comp_Dtile28_Ctile10),np.mean(comm_Dtile28_Ctile10)

    ####mean power#######
    # mean_power_Dtile_1_Ctile_1=42.8458e-03
    # mean_power_Dtile_2_Ctile_2=43.2055e-03
    # mean_power_Dtile_2_Ctile_5=42.0467e-03
    # mean_power_Dtile_2_Ctile_10=43.0679e-03
    # mean_power_Dtile_4_Ctile_2=42.609e-03
    # mean_power_Dtile_4_Ctile_5=43.4253e-03
    # mean_power_Dtile_4_Ctile_10=42.3665e-03
    # mean_power_Dtile_7_Ctile_2=42.1503e-03
    # mean_power_Dtile_7_Ctile_5=43.8843e-03
    # mean_power_Dtile_7_Ctile_10=43.8646e-03
    # mean_power_Dtile_14_Ctile_2=4.7559e-03
    # mean_power_Dtile_14_Ctile_5=42e-03
    # mean_power_Dtile_14_Ctile_10=42e-03
    # mean_power_Dtile_28_Ctile_2=42e-03
    # mean_power_Dtile_28_Ctile_5=42e-03
    # mean_power_Dtile_28_Ctile_10=42e-03

    ########datamovementtime##########

    comm_time_Dtile1_Ctile1=comm_Dtile1_Ctile1*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile2_Ctile2=comm_Dtile2_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile2_Ctile5=comm_Dtile2_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile2_Ctile10=comm_Dtile2_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile2=comm_Dtile4_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile5=comm_Dtile4_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile10=comm_Dtile4_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile2=comm_Dtile7_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile5=comm_Dtile7_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile10=comm_Dtile7_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile2=comm_Dtile14_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile5=comm_Dtile14_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile10=comm_Dtile14_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile2=comm_Dtile28_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile5=comm_Dtile28_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile10=comm_Dtile28_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))


    ##########energy data movement#############

    energy_lost_dm_Dtile2_Ctile2=(comm_time_Dtile2_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_2
    energy_lost_dm_Dtile2_Ctile5=(comm_time_Dtile2_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_5
    energy_lost_dm_Dtile2_Ctile10=(comm_time_Dtile2_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_10
    energy_lost_dm_Dtile4_Ctile2=(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_4_Ctile_2
    energy_lost_dm_Dtile4_Ctile5=(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_4_Ctile_5
    energy_lost_dm_Dtile4_Ctile10=(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_4_Ctile_10
    energy_lost_dm_Dtile7_Ctile2=(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_7_Ctile_2
    energy_lost_dm_Dtile7_Ctile5=(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_7_Ctile_5
    energy_lost_dm_Dtile7_Ctile10=(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_7_Ctile_10
    energy_lost_dm_Dtile14_Ctile2=(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_14_Ctile_2
    energy_lost_dm_Dtile14_Ctile5=(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_14_Ctile_5
    energy_lost_dm_Dtile14_Ctile10=(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_14_Ctile_10
    energy_lost_dm_Dtile28_Ctile2=(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_28_Ctile_2
    energy_lost_dm_Dtile28_Ctile5=(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_28_Ctile_5
    energy_lost_dm_Dtile28_Ctile10=(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_28_Ctile_10


    ########Redundant computation##################
    energy_lost_rc_Dtile2_Ctile2=((inftime_Dtile2_Ctile2-comm_time_Dtile2_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_2
    energy_lost_rc_Dtile2_Ctile5=((inftime_Dtile2_Ctile5-comm_time_Dtile2_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_5
    energy_lost_rc_Dtile2_Ctile10=((inftime_Dtile2_Ctile10-comm_time_Dtile2_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_10
    energy_lost_rc_Dtile4_Ctile2=((inftime_Dtile4_Ctile2-comm_time_Dtile4_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_2
    energy_lost_rc_Dtile4_Ctile5=((inftime_Dtile4_Ctile5-comm_time_Dtile4_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_5
    energy_lost_rc_Dtile4_Ctile10=((inftime_Dtile4_Ctile10-comm_time_Dtile4_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_10
    energy_lost_rc_Dtile7_Ctile2=((inftime_Dtile7_Ctile2-comm_time_Dtile7_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_2
    energy_lost_rc_Dtile7_Ctile5=((inftime_Dtile7_Ctile5-comm_time_Dtile7_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_5
    energy_lost_rc_Dtile7_Ctile10=((inftime_Dtile7_Ctile10-comm_time_Dtile7_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_10
    energy_lost_rc_Dtile14_Ctile2=((inftime_Dtile14_Ctile2-comm_time_Dtile14_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_2
    energy_lost_rc_Dtile14_Ctile5=((inftime_Dtile14_Ctile5-comm_time_Dtile14_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_5
    energy_lost_rc_Dtile14_Ctile10=((inftime_Dtile14_Ctile10-comm_time_Dtile14_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_10
    energy_lost_rc_Dtile28_Ctile2=((inftime_Dtile28_Ctile2-comm_time_Dtile28_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_2
    energy_lost_rc_Dtile28_Ctile5=((inftime_Dtile28_Ctile5-comm_time_Dtile28_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_5
    energy_lost_rc_Dtile28_Ctile10=((inftime_Dtile28_Ctile10-comm_time_Dtile28_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_10

    ##############Intermittent power calculations##################

    helpblip_intpower_compute_counter,helpblip_intpower_commcounter=np.mean(helpblip_intpower_compute_counter),np.mean(helpblip_intpower_commcounter)
    helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II=np.mean(helpblip_intpower_compute_counter_II),np.mean(helpblip_intpower_commcounter_II)
    helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III=np.mean(helpblip_intpower_compute_counter_III),np.mean(helpblip_intpower_commcounter_III)
    helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV=np.mean(helpblip_intpower_compute_counter_IV),np.mean(helpblip_intpower_commcounter_IV)
    helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V=np.mean(helpblip_intpower_compute_counter_V),np.mean(helpblip_intpower_commcounter_V)
    helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI=np.mean(helpblip_intpower_compute_counter_VI),np.mean(helpblip_intpower_commcounter_VI)
    helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII=np.mean(helpblip_intpower_compute_counter_VII),np.mean(helpblip_intpower_commcounter_VII)
    helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII=np.mean(helpblip_intpower_compute_counter_VIII),np.mean(helpblip_intpower_commcounter_VIII)
    helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX=np.mean(helpblip_intpower_compute_counter_IX),np.mean(helpblip_intpower_commcounter_IX)
    helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X=np.mean(helpblip_intpower_compute_counter_X),np.mean(helpblip_intpower_commcounter_X)
    helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI=np.mean(helpblip_intpower_compute_counter_XI),np.mean(helpblip_intpower_commcounter_XI)
    helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII=np.mean(helpblip_intpower_compute_counter_XII),np.mean(helpblip_intpower_commcounter_XII)
    helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII=np.mean(helpblip_intpower_compute_counter_XIII),np.mean(helpblip_intpower_commcounter_XIII)
    helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV=np.mean(helpblip_intpower_compute_counter_XIV),np.mean(helpblip_intpower_commcounter_XIV)
    helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV=np.mean(helpblip_intpower_compute_counter_XV),np.mean(helpblip_intpower_commcounter_XV)


    helpblip_intpower=np.mean(helpblip_intpower,axis=0)
    helpblip_intpower_II=np.mean(helpblip_intpower_II,axis=0)
    helpblip_intpower_III=np.mean(helpblip_intpower_III,axis=0)
    helpblip_intpower_IV=np.mean(helpblip_intpower_IV,axis=0)
    helpblip_intpower_V=np.mean(helpblip_intpower_V,axis=0)
    helpblip_intpower_VI=np.mean(helpblip_intpower_VI,axis=0)
    helpblip_intpower_VII=np.mean(helpblip_intpower_VII,axis=0)
    helpblip_intpower_VIII=np.mean(helpblip_intpower_VIII,axis=0)
    helpblip_intpower_IX=np.mean(helpblip_intpower_IX,axis=0)
    helpblip_intpower_X=np.mean(helpblip_intpower_X,axis=0)
    helpblip_intpower_XI=np.mean(helpblip_intpower_XI,axis=0)
    helpblip_intpower_XII=np.mean(helpblip_intpower_XII,axis=0)
    helpblip_intpower_XIII=np.mean(helpblip_intpower_XIII,axis=0)
    helpblip_intpower_XIV=np.mean(helpblip_intpower_XIV,axis=0)
    helpblip_intpower_XV=np.mean(helpblip_intpower_XV,axis=0)


    helpblip_intpower_op_numcycles=np.mean(helpblip_intpower_op_numcycles,axis=0)
    helpblip_intpower_op_numcycles_II=np.mean(helpblip_intpower_op_numcycles_II,axis=0)
    helpblip_intpower_op_numcycles_III=np.mean(helpblip_intpower_op_numcycles_III,axis=0)
    helpblip_intpower_op_numcycles_IV=np.mean(helpblip_intpower_op_numcycles_IV,axis=0)
    helpblip_intpower_op_numcycles_V=np.mean(helpblip_intpower_op_numcycles_V,axis=0)
    helpblip_intpower_op_numcycles_VI=np.mean(helpblip_intpower_op_numcycles_VI,axis=0)
    helpblip_intpower_op_numcycles_VII=np.mean(helpblip_intpower_op_numcycles_VII,axis=0)
    helpblip_intpower_op_numcycles_VIII=np.mean(helpblip_intpower_op_numcycles_VIII,axis=0)
    helpblip_intpower_op_numcycles_IX=np.mean(helpblip_intpower_op_numcycles_IX,axis=0)
    helpblip_intpower_op_numcycles_X=np.mean(helpblip_intpower_op_numcycles_X,axis=0)
    helpblip_intpower_op_numcycles_XI=np.mean(helpblip_intpower_op_numcycles_XI,axis=0)
    helpblip_intpower_op_numcycles_XII=np.mean(helpblip_intpower_op_numcycles_XII,axis=0)
    helpblip_intpower_op_numcycles_XIII=np.mean(helpblip_intpower_op_numcycles_XIII,axis=0)
    helpblip_intpower_op_numcycles_XIV=np.mean(helpblip_intpower_op_numcycles_XIV,axis=0)
    helpblip_intpower_op_numcycles_XV=np.mean(helpblip_intpower_op_numcycles_XV,axis=0)


    helpblip_intpower_boot_overhead_Dtile_2_Ctile_2=(helpblip_intpower_op_numcycles[depthwise_index]+helpblip_intpower_op_numcycles[conv_index])*fpga_boot_time*mean_power_Dtile_2_Ctile_2
    helpblip_intpower_boot_overhead_Dtile_2_Ctile_5=(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[depthwise_index])*fpga_boot_time*mean_power_Dtile_2_Ctile_5
    helpblip_intpower_boot_overhead_Dtile_2_Ctile_10=(helpblip_intpower_op_numcycles_III[depthwise_index]+helpblip_intpower_op_numcycles_III[depthwise_index])*fpga_boot_time*mean_power_Dtile_2_Ctile_10
    helpblip_intpower_boot_overhead_Dtile_4_Ctile_2=(helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV)*fpga_boot_time*mean_power_Dtile_4_Ctile_2
    helpblip_intpower_boot_overhead_Dtile_4_Ctile_5=(helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V)*fpga_boot_time*mean_power_Dtile_4_Ctile_5
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_2=(helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI)*fpga_boot_time*mean_power_Dtile_4_Ctile_10
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_5=(helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII)*fpga_boot_time*mean_power_Dtile_7_Ctile_2
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_10=(helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII)*fpga_boot_time*mean_power_Dtile_7_Ctile_5
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_10=(helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX)*fpga_boot_time*mean_power_Dtile_7_Ctile_10
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_2=(helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X)*fpga_boot_time*mean_power_Dtile_14_Ctile_2
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_5=(helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI)*fpga_boot_time*mean_power_Dtile_14_Ctile_5
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_10=(helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII)*fpga_boot_time*mean_power_Dtile_14_Ctile_10
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_2=(helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII)*fpga_boot_time*mean_power_Dtile_28_Ctile_2
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_5=(helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV)*fpga_boot_time*mean_power_Dtile_28_Ctile_5
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_10=(helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV)*fpga_boot_time*mean_power_Dtile_28_Ctile_10



    helpblip_intpower_fpgacomm_Dtile_2_Ctile_2=helpblip_intpower_commcounter*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_2_Ctile_5=helpblip_intpower_commcounter_II*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_2_Ctile_10=helpblip_intpower_commcounter_III*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_2=helpblip_intpower_commcounter_IV*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_5=helpblip_intpower_commcounter_V*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_10=helpblip_intpower_commcounter_VI*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_2=helpblip_intpower_commcounter_VII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_5=helpblip_intpower_commcounter_VIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_10=helpblip_intpower_commcounter_IX*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_2=helpblip_intpower_commcounter_X*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_5=helpblip_intpower_commcounter_XI*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_10=helpblip_intpower_commcounter_XII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_2=helpblip_intpower_commcounter_XIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_5=helpblip_intpower_commcounter_XIV*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_10=helpblip_intpower_commcounter_XV*((7*spi_cycle_time)+(spi_cycle_time/2))

    energy_lost_redm_Dtile2_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_2_Ctile_2-(comm_time_Dtile2_Ctile2-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_2
    energy_lost_redm_Dtile2_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_2_Ctile_5-(comm_time_Dtile2_Ctile5-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_5 
    energy_lost_redm_Dtile2_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_2_Ctile_10-(comm_time_Dtile2_Ctile10-comm_time_Dtile1_Ctile1))*mean_power_Dtile_2_Ctile_10 
    energy_lost_redm_Dtile4_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2-(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_2 
    energy_lost_redm_Dtile4_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5-(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_5 
    energy_lost_redm_Dtile4_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10-(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1))*mean_power_Dtile_4_Ctile_10 
    energy_lost_redm_Dtile7_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2-(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_2 
    energy_lost_redm_Dtile7_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5-(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_5 
    energy_lost_redm_Dtile7_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10-(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1))*mean_power_Dtile_7_Ctile_10 
    energy_lost_redm_Dtile14_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2-(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_2 
    energy_lost_redm_Dtile14_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5-(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_5 
    energy_lost_redm_Dtile14_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10-(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1))*mean_power_Dtile_14_Ctile_10
    energy_lost_redm_Dtile28_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2-(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_2 
    energy_lost_redm_Dtile28_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5-(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_5 
    energy_lost_redm_Dtile28_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10-(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1))*mean_power_Dtile_28_Ctile_10


    # print(energy_lost_redm_Dtile14_Ctile10_intpower)
    ############prepare data to plot ###################
   
    energy_lost_dm=[energy_lost_dm_Dtile28_Ctile10,energy_lost_dm_Dtile14_Ctile10,energy_lost_dm_Dtile7_Ctile10,energy_lost_dm_Dtile4_Ctile10,energy_lost_dm_Dtile2_Ctile10,
                              energy_lost_dm_Dtile28_Ctile5,energy_lost_dm_Dtile28_Ctile2,energy_lost_dm_Dtile14_Ctile5,energy_lost_dm_Dtile7_Ctile5,energy_lost_dm_Dtile4_Ctile5,
                              energy_lost_dm_Dtile2_Ctile5,energy_lost_dm_Dtile14_Ctile2,energy_lost_dm_Dtile7_Ctile2, energy_lost_dm_Dtile4_Ctile2,energy_lost_dm_Dtile2_Ctile2]
    energy_lost_rc=[energy_lost_rc_Dtile28_Ctile10,energy_lost_rc_Dtile14_Ctile10,energy_lost_rc_Dtile7_Ctile10,energy_lost_rc_Dtile4_Ctile10,energy_lost_rc_Dtile2_Ctile10,
                              energy_lost_rc_Dtile28_Ctile5,energy_lost_rc_Dtile28_Ctile2,energy_lost_rc_Dtile14_Ctile5,energy_lost_rc_Dtile7_Ctile5,energy_lost_rc_Dtile4_Ctile5,
                              energy_lost_rc_Dtile2_Ctile5,energy_lost_rc_Dtile14_Ctile2,energy_lost_rc_Dtile7_Ctile2, energy_lost_rc_Dtile4_Ctile2,energy_lost_rc_Dtile2_Ctile2]

    energy_lost_redm=[energy_lost_redm_Dtile28_Ctile10_intpower,energy_lost_redm_Dtile14_Ctile10_intpower,energy_lost_redm_Dtile7_Ctile10_intpower,energy_lost_redm_Dtile4_Ctile10_intpower,energy_lost_redm_Dtile2_Ctile10_intpower,
                              energy_lost_redm_Dtile28_Ctile5_intpower,energy_lost_redm_Dtile28_Ctile2_intpower,energy_lost_redm_Dtile14_Ctile5_intpower,energy_lost_redm_Dtile7_Ctile5_intpower,energy_lost_redm_Dtile4_Ctile5_intpower,
                              energy_lost_redm_Dtile2_Ctile5_intpower,energy_lost_redm_Dtile14_Ctile2_intpower,energy_lost_redm_Dtile7_Ctile2_intpower, energy_lost_redm_Dtile4_Ctile2_intpower,energy_lost_redm_Dtile2_Ctile2_intpower]
    fig,ax=plt.subplots()
    ax.plot(X,energy_lost_dm,color='Violet',marker='X',linewidth=2,ms=10,label='Data Movement')
    ax.plot(X,energy_lost_rc,color='Green',marker='v',linewidth=2,ms=10,label='Redundant computation')
    ax.plot(X,energy_lost_redm,color='red',marker='o',linewidth=2,ms=10,ls='--',label='Re-data movement\n Intermittent power')
    # print(energy_lost_dm)

    ax.set_xticks(np.arange(len(dtile)*len(ctile)), labels=label)
    ax.tick_params(axis='both', which='both', labelsize=11)

    ax.grid()
    ax.legend(loc='best',bbox_to_anchor=(1,1.16),ncol=3,fontsize=15)
    ax.set_yscale('log')
    ax.set_ylabel('Energy lost per inference (J)',fontsize=20)
    ax.set_xlabel('Optimization knob',fontsize=20)
    fig.set_size_inches(13,6.2)
    plt.savefig(f'computation_schedule.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_computation_scheduling_144mF(inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1, 
                                inftime_Dtile4_Ctile2,op_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2, 
                                helpblip_intpower_IV,helpblip_intpower_numcycles_IV,helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV,helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV,helpblip_intpower_op_numcycles_IV,
                                helpblip_intpower_IV_noearly_die,helpblip_intpower_numcycles_IV_noearly_die,helpblip_intpower_compute_counter_IV_noearly_die,helpblip_intpower_commcounter_IV_noearly_die,helpblip_intpower_ontime_IV_noearly_die,helpblip_intpower_offtime_IV_noearly_die,helpblip_intpower_op_numcycles_IV_noearly_die,

                                inftime_Dtile4_Ctile5,op_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5,
                                helpblip_intpower_V,helpblip_intpower_numcycles_V,helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V,helpblip_intpower_ontime_V,helpblip_intpower_offtime_V,helpblip_intpower_op_numcycles_V,
                                helpblip_intpower_V_noearly_die,helpblip_intpower_numcycles_V_noearly_die,helpblip_intpower_compute_counter_V_noearly_die,helpblip_intpower_commcounter_V_noearly_die,helpblip_intpower_ontime_V_noearly_die,helpblip_intpower_offtime_V_noearly_die,helpblip_intpower_op_numcycles_V_noearly_die,

                                inftime_Dtile4_Ctile10,op_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10,
                                helpblip_intpower_VI,helpblip_intpower_numcycles_VI,helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI,helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI,helpblip_intpower_op_numcycles_VI,
                                helpblip_intpower_VI_noearly_die,helpblip_intpower_numcycles_VI_noearly_die,helpblip_intpower_compute_counter_VI_noearly_die,helpblip_intpower_commcounter_VI_noearly_die,helpblip_intpower_ontime_VI_noearly_die,helpblip_intpower_offtime_VI_noearly_die,helpblip_intpower_op_numcycles_VI_noearly_die,

                                inftime_Dtile7_Ctile2,op_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2,
                                helpblip_intpower_VII,helpblip_intpower_numcycles_VII,helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII,helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII,helpblip_intpower_op_numcycles_VII,
                                helpblip_intpower_VII_noearly_die,helpblip_intpower_numcycles_VII_noearly_die,helpblip_intpower_compute_counter_VII_noearly_die,helpblip_intpower_commcounter_VII_noearly_die,helpblip_intpower_ontime_VII_noearly_die,helpblip_intpower_offtime_VII_noearly_die,helpblip_intpower_op_numcycles_VII_noearly_die,

                                inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
                                helpblip_intpower_VIII,helpblip_intpower_numcycles_VIII,helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII,helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII,helpblip_intpower_op_numcycles_VIII,
                                helpblip_intpower_VIII_noearly_die,helpblip_intpower_numcycles_VIII_noearly_die,helpblip_intpower_compute_counter_VIII_noearly_die,helpblip_intpower_commcounter_VIII_noearly_die,helpblip_intpower_ontime_VIII_noearly_die,helpblip_intpower_offtime_VIII_noearly_die,helpblip_intpower_op_numcycles_VIII_noearly_die,

                                inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
                                helpblip_intpower_IX,helpblip_intpower_numcycles_IX,helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX,helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX,helpblip_intpower_op_numcycles_IX,
                                helpblip_intpower_IX_noearly_die,helpblip_intpower_numcycles_IX_noearly_die,helpblip_intpower_compute_counter_IX_noearly_die,helpblip_intpower_commcounter_IX_noearly_die,helpblip_intpower_ontime_IX_noearly_die,helpblip_intpower_offtime_IX_noearly_die,helpblip_intpower_op_numcycles_IX_noearly_die,

                                inftime_Dtile14_Ctile2,op_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2,
                                helpblip_intpower_X,helpblip_intpower_numcycles_X,helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X,helpblip_intpower_ontime_X,helpblip_intpower_offtime_X,helpblip_intpower_op_numcycles_X,
                                helpblip_intpower_X_noearly_die,helpblip_intpower_numcycles_X_noearly_die,helpblip_intpower_compute_counter_X_noearly_die,helpblip_intpower_commcounter_X_noearly_die,helpblip_intpower_ontime_X_noearly_die,helpblip_intpower_offtime_X_noearly_die,helpblip_intpower_op_numcycles_X_noearly_die,

                                inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
                                helpblip_intpower_XI,helpblip_intpower_numcycles_XI,helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI,helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI,helpblip_intpower_op_numcycles_XI,
                                helpblip_intpower_XI_noearly_die,helpblip_intpower_numcycles_XI_noearly_die,helpblip_intpower_compute_counter_XI_noearly_die,helpblip_intpower_commcounter_XI_noearly_die,helpblip_intpower_ontime_XI_noearly_die,helpblip_intpower_offtime_XI_noearly_die,helpblip_intpower_op_numcycles_XI_noearly_die,

                                inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
                                helpblip_intpower_XII,helpblip_intpower_numcycles_XII,helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII,helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII,helpblip_intpower_op_numcycles_XII,
                                helpblip_intpower_XII_noearly_die,helpblip_intpower_numcycles_XII_noearly_die,helpblip_intpower_compute_counter_XII_noearly_die,helpblip_intpower_commcounter_XII_noearly_die,helpblip_intpower_ontime_XII_noearly_die,helpblip_intpower_offtime_XII_noearly_die,helpblip_intpower_op_numcycles_XII_noearly_die,

                                inftime_Dtile28_Ctile2,op_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2,
                                helpblip_intpower_XIII,helpblip_intpower_numcycles_XIII,helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII,helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII,helpblip_intpower_op_numcycles_XIII,
                                helpblip_intpower_XIII_noearly_die,helpblip_intpower_numcycles_XIII_noearly_die,helpblip_intpower_compute_counter_XIII_noearly_die,helpblip_intpower_commcounter_XIII_noearly_die,helpblip_intpower_ontime_XIII_noearly_die,helpblip_intpower_offtime_XIII_noearly_die,helpblip_intpower_op_numcycles_XIII_noearly_die,

                                inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
                                helpblip_intpower_XIV,helpblip_intpower_numcycles_XIV,helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV,helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV,helpblip_intpower_op_numcycles_XIV,
                                helpblip_intpower_XIV_noearly_die,helpblip_intpower_numcycles_XIV_noearly_die,helpblip_intpower_compute_counter_XIV_noearly_die,helpblip_intpower_commcounter_XIV_noearly_die,helpblip_intpower_ontime_XIV_noearly_die,helpblip_intpower_offtime_XIV_noearly_die,helpblip_intpower_op_numcycles_XIV_noearly_die,

                                inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
                                helpblip_intpower_XV,helpblip_intpower_numcycles_XV,helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV,helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV,helpblip_intpower_op_numcycles_XV,
                                helpblip_intpower_XV_noearly_die,helpblip_intpower_numcycles_XV_noearly_die,helpblip_intpower_compute_counter_XV_noearly_die,helpblip_intpower_commcounter_XV_noearly_die,helpblip_intpower_ontime_XV_noearly_die,helpblip_intpower_offtime_XV_noearly_die,helpblip_intpower_op_numcycles_XV_noearly_die):
    
    spi_cycle_time=(1/(8e06/3))
    spi_cycle_power=0.00724
    fpga_clock=1/20e06
   
    dtile=[16,49,196,784]
    # dtile=[4,16,49]
    ctile=[4,25,100]
    X=np.arange(len(dtile)*len(ctile))
    cap_size=144e-03
    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=130e-03+8.64e-03
    fpga_boot_energy=2.32E-04
    cap_energy=0.03759-fpga_boot_energy
    mean_system_power=56.8e-03
    # msp_boot_time=5e-03
    depthwise_index=2
    conv_index=4
    label=['Knob-1', 'Knob-2','Knob-3','Knob-4','Knob-5', 'Knob-6','Knob-7','Knob-8',
           'Knob-9', 'Knob-10','Knob-11','Knob-12','Knob-13', 'Knob-14','Knob-15']
    
    
    

    op_Dtile1_Ctile1=np.mean(op_Dtile1_Ctile1,axis=0)
    op_Dtile4_Ctile2=np.mean(op_Dtile4_Ctile2,axis=0)
    op_Dtile4_Ctile5=np.mean(op_Dtile4_Ctile5,axis=0)
    op_Dtile4_Ctile10=np.mean(op_Dtile4_Ctile10,axis=0)
    op_Dtile7_Ctile2=np.mean(op_Dtile7_Ctile2,axis=0)
    op_Dtile7_Ctile5=np.mean(op_Dtile7_Ctile5,axis=0)
    op_Dtile7_Ctile10=np.mean(op_Dtile7_Ctile10,axis=0)
    op_Dtile14_Ctile2=np.mean(op_Dtile14_Ctile2,axis=0)
    op_Dtile14_Ctile5=np.mean(op_Dtile14_Ctile5,axis=0)
    op_Dtile14_Ctile10=np.mean(op_Dtile14_Ctile10,axis=0)
    op_Dtile28_Ctile2=np.mean(op_Dtile28_Ctile2,axis=0)
    op_Dtile28_Ctile5=np.mean(op_Dtile28_Ctile5,axis=0)
    op_Dtile28_Ctile10=np.mean(op_Dtile28_Ctile10,axis=0)



    helpblip_intpower_numcycles_IV=np.mean(helpblip_intpower_numcycles_IV,axis=0)
    helpblip_intpower_numcycles_V=np.mean(helpblip_intpower_numcycles_V,axis=0)
    helpblip_intpower_numcycles_VI=np.mean(helpblip_intpower_numcycles_VI,axis=0)
    helpblip_intpower_numcycles_VII=np.mean(helpblip_intpower_numcycles_VII,axis=0)
    helpblip_intpower_numcycles_VIII=np.mean(helpblip_intpower_numcycles_VIII,axis=0)
    helpblip_intpower_numcycles_IX=np.mean(helpblip_intpower_numcycles_IX,axis=0)
    helpblip_intpower_numcycles_X=np.mean(helpblip_intpower_numcycles_X,axis=0)
    helpblip_intpower_numcycles_XI=np.mean(helpblip_intpower_numcycles_XI,axis=0)
    helpblip_intpower_numcycles_XII=np.mean(helpblip_intpower_numcycles_XII,axis=0)
    helpblip_intpower_numcycles_XIII=np.mean(helpblip_intpower_numcycles_XIII,axis=0)
    helpblip_intpower_numcycles_XIV=np.mean(helpblip_intpower_numcycles_XIV,axis=0)
    helpblip_intpower_numcycles_XV=np.mean(helpblip_intpower_numcycles_XV,axis=0)

    #No early die data##########

    helpblip_intpower_numcycles_IV_noearly_die=np.mean(helpblip_intpower_numcycles_IV_noearly_die,axis=0)
    helpblip_intpower_numcycles_V_noearly_die=np.mean(helpblip_intpower_numcycles_V_noearly_die,axis=0)
    helpblip_intpower_numcycles_VI_noearly_die=np.mean(helpblip_intpower_numcycles_VI_noearly_die,axis=0)
    helpblip_intpower_numcycles_VII_noearly_die=np.mean(helpblip_intpower_numcycles_VII_noearly_die,axis=0)
    helpblip_intpower_numcycles_VIII_noearly_die=np.mean(helpblip_intpower_numcycles_VIII_noearly_die,axis=0)
    helpblip_intpower_numcycles_IX_noearly_die=np.mean(helpblip_intpower_numcycles_IX_noearly_die,axis=0)
    helpblip_intpower_numcycles_X_noearly_die=np.mean(helpblip_intpower_numcycles_X_noearly_die,axis=0)
    helpblip_intpower_numcycles_XI_noearly_die=np.mean(helpblip_intpower_numcycles_XI_noearly_die,axis=0)
    helpblip_intpower_numcycles_XII_noearly_die=np.mean(helpblip_intpower_numcycles_XII_noearly_die,axis=0)
    helpblip_intpower_numcycles_XIII_noearly_die=np.mean(helpblip_intpower_numcycles_XIII_noearly_die,axis=0)
    helpblip_intpower_numcycles_XIV_noearly_die=np.mean(helpblip_intpower_numcycles_XIV_noearly_die,axis=0)
    helpblip_intpower_numcycles_XV_noearly_die=np.mean(helpblip_intpower_numcycles_XV_noearly_die,axis=0)


    helpblip_intpower_ontime_IV_noearly_die,helpblip_intpower_offtime_IV_noearly_die=np.mean(helpblip_intpower_ontime_IV_noearly_die),np.mean(helpblip_intpower_offtime_IV_noearly_die)
    helpblip_intpower_ontime_V_noearly_die,helpblip_intpower_offtime_V_noearly_die=np.mean(helpblip_intpower_ontime_V_noearly_die),np.mean(helpblip_intpower_offtime_V_noearly_die)
    helpblip_intpower_ontime_VI_noearly_die,helpblip_intpower_offtime_VI_noearly_die=np.mean(helpblip_intpower_ontime_VI_noearly_die),np.mean(helpblip_intpower_offtime_VI_noearly_die)
    helpblip_intpower_ontime_VII_noearly_die,helpblip_intpower_offtime_VII_noearly_die=np.mean(helpblip_intpower_ontime_VII_noearly_die),np.mean(helpblip_intpower_offtime_VII_noearly_die)
    helpblip_intpower_ontime_VIII_noearly_die,helpblip_intpower_offtime_VIII_noearly_die=np.mean(helpblip_intpower_ontime_VIII_noearly_die),np.mean(helpblip_intpower_offtime_VIII_noearly_die)
    helpblip_intpower_ontime_IX_noearly_die,helpblip_intpower_offtime_IX_noearly_die=np.mean(helpblip_intpower_ontime_IX_noearly_die),np.mean(helpblip_intpower_offtime_IX_noearly_die)
    helpblip_intpower_ontime_X_noearly_die,helpblip_intpower_offtime_X_noearly_die=np.mean(helpblip_intpower_ontime_X_noearly_die),np.mean(helpblip_intpower_offtime_X_noearly_die)
    helpblip_intpower_ontime_XI_noearly_die,helpblip_intpower_offtime_XI_noearly_die=np.mean(helpblip_intpower_ontime_XI_noearly_die),np.mean(helpblip_intpower_offtime_XI_noearly_die)
    helpblip_intpower_ontime_XII_noearly_die,helpblip_intpower_offtime_XII_noearly_die=np.mean(helpblip_intpower_ontime_XII_noearly_die),np.mean(helpblip_intpower_offtime_XII_noearly_die)
    helpblip_intpower_ontime_XIII_noearly_die,helpblip_intpower_offtime_XIII_noearly_die=np.mean(helpblip_intpower_ontime_XIII_noearly_die),np.mean(helpblip_intpower_offtime_XIII_noearly_die)
    helpblip_intpower_ontime_XIV_noearly_die,helpblip_intpower_offtime_XIV_noearly_die=np.mean(helpblip_intpower_ontime_XIV_noearly_die),np.mean(helpblip_intpower_offtime_XIV_noearly_die)
    helpblip_intpower_ontime_XV_noearly_die,helpblip_intpower_offtime_XV_noearly_die=np.mean(helpblip_intpower_ontime_XV_noearly_die),np.mean(helpblip_intpower_offtime_XV_noearly_die)
    
    helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV=np.mean(helpblip_intpower_ontime_IV),np.mean(helpblip_intpower_offtime_IV)
    helpblip_intpower_ontime_V,helpblip_intpower_offtime_V=np.mean(helpblip_intpower_ontime_V),np.mean(helpblip_intpower_offtime_V)
    helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI=np.mean(helpblip_intpower_ontime_VI),np.mean(helpblip_intpower_offtime_VI)
    helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII=np.mean(helpblip_intpower_ontime_VII),np.mean(helpblip_intpower_offtime_VII)
    helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII=np.mean(helpblip_intpower_ontime_VIII),np.mean(helpblip_intpower_offtime_VIII)
    helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX=np.mean(helpblip_intpower_ontime_IX),np.mean(helpblip_intpower_offtime_IX)
    helpblip_intpower_ontime_X,helpblip_intpower_offtime_X=np.mean(helpblip_intpower_ontime_X),np.mean(helpblip_intpower_offtime_X)
    helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI=np.mean(helpblip_intpower_ontime_XI),np.mean(helpblip_intpower_offtime_XI)
    helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII=np.mean(helpblip_intpower_ontime_XII),np.mean(helpblip_intpower_offtime_XII)
    helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII=np.mean(helpblip_intpower_ontime_XIII),np.mean(helpblip_intpower_offtime_XIII)
    helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV=np.mean(helpblip_intpower_ontime_XIV),np.mean(helpblip_intpower_offtime_XIV)
    helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV=np.mean(helpblip_intpower_ontime_XV),np.mean(helpblip_intpower_offtime_XV)
    
    # print(helpblip_intpower_ontime_V)

    # mean_power_Dtile_4_Ctile_2=cap_energy/helpblip_intpower_ontime_IV
    # mean_power_Dtile_4_Ctile_5=cap_energy/helpblip_intpower_ontime_V
    # mean_power_Dtile_4_Ctile_10=cap_energy/helpblip_intpower_ontime_VI
    # mean_power_Dtile_7_Ctile_2=cap_energy/helpblip_intpower_ontime_VII
    # mean_power_Dtile_7_Ctile_5=cap_energy/helpblip_intpower_ontime_VIII
    # mean_power_Dtile_7_Ctile_10=cap_energy/helpblip_intpower_ontime_IX
    # mean_power_Dtile_14_Ctile_2=cap_energy/helpblip_intpower_ontime_X
    # mean_power_Dtile_14_Ctile_5=cap_energy/helpblip_intpower_ontime_XI
    # mean_power_Dtile_14_Ctile_10=cap_energy/helpblip_intpower_ontime_XII
    # mean_power_Dtile_28_Ctile_2=cap_energy/helpblip_intpower_ontime_XIII
    # mean_power_Dtile_28_Ctile_5=cap_energy/helpblip_intpower_ontime_XIV
    # mean_power_Dtile_28_Ctile_10=cap_energy/helpblip_intpower_ontime_XV

    
    ###############continuous power calculations###################

    inftime_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1=np.mean(inftime_Dtile1_Ctile1),np.mean(comp_Dtile1_Ctile1),np.mean(comm_Dtile1_Ctile1) 
    inftime_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2=np.mean(inftime_Dtile4_Ctile2),np.mean(comp_Dtile4_Ctile2),np.mean(comm_Dtile4_Ctile2) 
    inftime_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5=np.mean(inftime_Dtile4_Ctile5),np.mean(comp_Dtile4_Ctile5),np.mean(comm_Dtile4_Ctile5) 
    inftime_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10=np.mean(inftime_Dtile4_Ctile10),np.mean(comp_Dtile4_Ctile10),np.mean(comm_Dtile4_Ctile10)
    inftime_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2=np.mean(inftime_Dtile7_Ctile2),np.mean(comp_Dtile7_Ctile2),np.mean(comm_Dtile7_Ctile2)
    inftime_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5=np.mean(inftime_Dtile7_Ctile5),np.mean(comp_Dtile7_Ctile5),np.mean(comm_Dtile7_Ctile5)
    inftime_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10=np.mean(inftime_Dtile7_Ctile10),np.mean(comp_Dtile7_Ctile10),np.mean(comm_Dtile7_Ctile10)
    inftime_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2=np.mean(inftime_Dtile14_Ctile2),np.mean(comp_Dtile14_Ctile2),np.mean(comm_Dtile14_Ctile2)
    inftime_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5=np.mean(inftime_Dtile14_Ctile5),np.mean(comp_Dtile14_Ctile5),np.mean(comm_Dtile14_Ctile5)
    inftime_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10=np.mean(inftime_Dtile14_Ctile10),np.mean(comp_Dtile14_Ctile10),np.mean(comm_Dtile14_Ctile10)
    inftime_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2=np.mean(inftime_Dtile28_Ctile2),np.mean(comp_Dtile28_Ctile2),np.mean(comm_Dtile28_Ctile2)
    inftime_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5=np.mean(inftime_Dtile28_Ctile5),np.mean(comp_Dtile28_Ctile5),np.mean(comm_Dtile28_Ctile5)
    inftime_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10=np.mean(inftime_Dtile28_Ctile10),np.mean(comp_Dtile28_Ctile10),np.mean(comm_Dtile28_Ctile10)

    ####mean power#######
    # mean_power_Dtile_1_Ctile_1=42.8458e-03
    # mean_power_Dtile_2_Ctile_2=43.2055e-03
    # mean_power_Dtile_2_Ctile_5=42.0467e-03
    # mean_power_Dtile_2_Ctile_10=43.0679e-03
    # mean_power_Dtile_4_Ctile_2=42.609e-03
    # mean_power_Dtile_4_Ctile_5=43.4253e-03
    # mean_power_Dtile_4_Ctile_10=42.3665e-03
    # mean_power_Dtile_7_Ctile_2=42.1503e-03
    # mean_power_Dtile_7_Ctile_5=43.8843e-03
    # mean_power_Dtile_7_Ctile_10=43.8646e-03
    # mean_power_Dtile_14_Ctile_2=4.7559e-03
    # mean_power_Dtile_14_Ctile_5=42e-03
    # mean_power_Dtile_14_Ctile_10=42e-03
    # mean_power_Dtile_28_Ctile_2=42e-03
    # mean_power_Dtile_28_Ctile_5=42e-03
    # mean_power_Dtile_28_Ctile_10=42e-03

    ########datamovementtime##########
    comm_time_Dtile1_Ctile1=comm_Dtile1_Ctile1*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile2=comm_Dtile4_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile5=comm_Dtile4_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile4_Ctile10=comm_Dtile4_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile2=comm_Dtile7_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile5=comm_Dtile7_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile10=comm_Dtile7_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile2=comm_Dtile14_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile5=comm_Dtile14_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile10=comm_Dtile14_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile2=comm_Dtile28_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile5=comm_Dtile28_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile10=comm_Dtile28_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))


    ##########energy data movement#############

    # energy_lost_dm_Dtile2_Ctile2=(comm_time_Dtile2_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_2
    # energy_lost_dm_Dtile2_Ctile5=(comm_time_Dtile2_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_5
    # energy_lost_dm_Dtile2_Ctile10=(comm_time_Dtile2_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_10
    energy_lost_dm_Dtile4_Ctile2=(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile4_Ctile5=(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile4_Ctile10=(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile7_Ctile2=(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile7_Ctile5=(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile7_Ctile10=(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile14_Ctile2=(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile14_Ctile5=(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile14_Ctile10=(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile28_Ctile2=(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile28_Ctile5=(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile28_Ctile10=(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power


    ########Redundant computation##################
    # energy_lost_rc_Dtile4_Ctile2=((inftime_Dtile4_Ctile2-comm_time_Dtile4_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5=((inftime_Dtile4_Ctile5-comm_time_Dtile4_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10=((inftime_Dtile4_Ctile10-comm_time_Dtile4_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2=((inftime_Dtile7_Ctile2-comm_time_Dtile7_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5=((inftime_Dtile7_Ctile5-comm_time_Dtile7_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10=((inftime_Dtile7_Ctile10-comm_time_Dtile7_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2=((inftime_Dtile14_Ctile2-comm_time_Dtile14_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5=((inftime_Dtile14_Ctile5-comm_time_Dtile14_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10=((inftime_Dtile14_Ctile10-comm_time_Dtile14_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2=((inftime_Dtile28_Ctile2-comm_time_Dtile28_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5=((inftime_Dtile28_Ctile5-comm_time_Dtile28_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10=((inftime_Dtile28_Ctile10-comm_time_Dtile28_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power

    energy_lost_rc_Dtile4_Ctile2=((op_Dtile4_Ctile2[conv_index]-comm_time_Dtile4_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile4_Ctile5=((op_Dtile4_Ctile5[conv_index]-comm_time_Dtile4_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile4_Ctile10=((op_Dtile4_Ctile10[conv_index]-comm_time_Dtile4_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile7_Ctile2=((op_Dtile7_Ctile2[conv_index]-comm_time_Dtile7_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5=((op_Dtile7_Ctile5[conv_index]-comm_time_Dtile7_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10=((op_Dtile7_Ctile10[conv_index]-comm_time_Dtile7_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile14_Ctile2=((op_Dtile14_Ctile2[conv_index]-comm_time_Dtile14_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5=((op_Dtile14_Ctile5[conv_index]-comm_time_Dtile14_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10=((op_Dtile14_Ctile10[conv_index]-comm_time_Dtile14_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile28_Ctile2=((op_Dtile28_Ctile2[conv_index]-comm_time_Dtile28_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5=((op_Dtile28_Ctile5[conv_index]-comm_time_Dtile28_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10=((op_Dtile28_Ctile10[conv_index]-comm_time_Dtile28_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power


    ##############Intermittent power calculations##################


    helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV=np.mean(helpblip_intpower_compute_counter_IV),np.mean(helpblip_intpower_commcounter_IV)
    helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V=np.mean(helpblip_intpower_compute_counter_V),np.mean(helpblip_intpower_commcounter_V)
    helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI=np.mean(helpblip_intpower_compute_counter_VI),np.mean(helpblip_intpower_commcounter_VI)
    helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII=np.mean(helpblip_intpower_compute_counter_VII),np.mean(helpblip_intpower_commcounter_VII)
    helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII=np.mean(helpblip_intpower_compute_counter_VIII),np.mean(helpblip_intpower_commcounter_VIII)
    helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX=np.mean(helpblip_intpower_compute_counter_IX),np.mean(helpblip_intpower_commcounter_IX)
    helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X=np.mean(helpblip_intpower_compute_counter_X),np.mean(helpblip_intpower_commcounter_X)
    helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI=np.mean(helpblip_intpower_compute_counter_XI),np.mean(helpblip_intpower_commcounter_XI)
    helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII=np.mean(helpblip_intpower_compute_counter_XII),np.mean(helpblip_intpower_commcounter_XII)
    helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII=np.mean(helpblip_intpower_compute_counter_XIII),np.mean(helpblip_intpower_commcounter_XIII)
    helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV=np.mean(helpblip_intpower_compute_counter_XIV),np.mean(helpblip_intpower_commcounter_XIV)
    helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV=np.mean(helpblip_intpower_compute_counter_XV),np.mean(helpblip_intpower_commcounter_XV)

#####No early die data ################
    helpblip_intpower_compute_counter_IV_noearly_die,helpblip_intpower_commcounter_IV_noearly_die=np.mean(helpblip_intpower_compute_counter_IV_noearly_die),np.mean(helpblip_intpower_commcounter_IV_noearly_die)
    helpblip_intpower_compute_counter_V_noearly_die,helpblip_intpower_commcounter_V_noearly_die=np.mean(helpblip_intpower_compute_counter_V_noearly_die),np.mean(helpblip_intpower_commcounter_V_noearly_die)
    helpblip_intpower_compute_counter_VI_noearly_die,helpblip_intpower_commcounter_VI_noearly_die=np.mean(helpblip_intpower_compute_counter_VI_noearly_die),np.mean(helpblip_intpower_commcounter_VI_noearly_die)
    helpblip_intpower_compute_counter_VII_noearly_die,helpblip_intpower_commcounter_VII_noearly_die=np.mean(helpblip_intpower_compute_counter_VII_noearly_die),np.mean(helpblip_intpower_commcounter_VII_noearly_die)
    helpblip_intpower_compute_counter_VIII_noearly_die,helpblip_intpower_commcounter_VIII_noearly_die=np.mean(helpblip_intpower_compute_counter_VIII_noearly_die),np.mean(helpblip_intpower_commcounter_VIII_noearly_die)
    helpblip_intpower_compute_counter_IX_noearly_die,helpblip_intpower_commcounter_IX_noearly_die=np.mean(helpblip_intpower_compute_counter_IX_noearly_die),np.mean(helpblip_intpower_commcounter_IX_noearly_die)
    helpblip_intpower_compute_counter_X_noearly_die,helpblip_intpower_commcounter_X_noearly_die=np.mean(helpblip_intpower_compute_counter_X_noearly_die),np.mean(helpblip_intpower_commcounter_X_noearly_die)
    helpblip_intpower_compute_counter_XI_noearly_die,helpblip_intpower_commcounter_XI_noearly_die=np.mean(helpblip_intpower_compute_counter_XI_noearly_die),np.mean(helpblip_intpower_commcounter_XI_noearly_die)
    helpblip_intpower_compute_counter_XII_noearly_die,helpblip_intpower_commcounter_XII_noearly_die=np.mean(helpblip_intpower_compute_counter_XII_noearly_die),np.mean(helpblip_intpower_commcounter_XII_noearly_die)
    helpblip_intpower_compute_counter_XIII_noearly_die,helpblip_intpower_commcounter_XIII_noearly_die=np.mean(helpblip_intpower_compute_counter_XIII_noearly_die),np.mean(helpblip_intpower_commcounter_XIII_noearly_die)
    helpblip_intpower_compute_counter_XIV_noearly_die,helpblip_intpower_commcounter_XIV_noearly_die=np.mean(helpblip_intpower_compute_counter_XIV_noearly_die),np.mean(helpblip_intpower_commcounter_XIV_noearly_die)
    helpblip_intpower_compute_counter_XV_noearly_die,helpblip_intpower_commcounter_XV_noearly_die=np.mean(helpblip_intpower_compute_counter_XV_noearly_die),np.mean(helpblip_intpower_commcounter_XV_noearly_die)


    helpblip_intpower_IV=np.mean(helpblip_intpower_IV,axis=0)
    helpblip_intpower_V=np.mean(helpblip_intpower_V,axis=0)
    helpblip_intpower_VI=np.mean(helpblip_intpower_VI,axis=0)
    helpblip_intpower_VII=np.mean(helpblip_intpower_VII,axis=0)
    helpblip_intpower_VIII=np.mean(helpblip_intpower_VIII,axis=0)
    helpblip_intpower_IX=np.mean(helpblip_intpower_IX,axis=0)
    helpblip_intpower_X=np.mean(helpblip_intpower_X,axis=0)
    helpblip_intpower_XI=np.mean(helpblip_intpower_XI,axis=0)
    helpblip_intpower_XII=np.mean(helpblip_intpower_XII,axis=0)
    helpblip_intpower_XIII=np.mean(helpblip_intpower_XIII,axis=0)
    helpblip_intpower_XIV=np.mean(helpblip_intpower_XIV,axis=0)
    helpblip_intpower_XV=np.mean(helpblip_intpower_XV,axis=0)



    helpblip_intpower_IV_noearly_die=np.mean(helpblip_intpower_IV_noearly_die,axis=0)
    helpblip_intpower_V_noearly_die=np.mean(helpblip_intpower_V_noearly_die,axis=0)
    helpblip_intpower_VI_noearly_die=np.mean(helpblip_intpower_VI_noearly_die,axis=0)
    helpblip_intpower_VII_noearly_die=np.mean(helpblip_intpower_VII_noearly_die,axis=0)
    helpblip_intpower_VIII_noearly_die=np.mean(helpblip_intpower_VIII_noearly_die,axis=0)
    helpblip_intpower_IX_noearly_die=np.mean(helpblip_intpower_IX_noearly_die,axis=0)
    helpblip_intpower_X_noearly_die=np.mean(helpblip_intpower_X_noearly_die,axis=0)
    helpblip_intpower_XI_noearly_die=np.mean(helpblip_intpower_XI_noearly_die,axis=0)
    helpblip_intpower_XII_noearly_die=np.mean(helpblip_intpower_XII_noearly_die,axis=0)
    helpblip_intpower_XIII_noearly_die=np.mean(helpblip_intpower_XIII_noearly_die,axis=0)
    helpblip_intpower_XIV_noearly_die=np.mean(helpblip_intpower_XIV_noearly_die,axis=0)
    helpblip_intpower_XV_noearly_die=np.mean(helpblip_intpower_XV_noearly_die,axis=0)


    helpblip_intpower_op_numcycles_IV=np.mean(helpblip_intpower_op_numcycles_IV,axis=0)
    helpblip_intpower_op_numcycles_V=np.mean(helpblip_intpower_op_numcycles_V,axis=0)
    helpblip_intpower_op_numcycles_VI=np.mean(helpblip_intpower_op_numcycles_VI,axis=0)
    helpblip_intpower_op_numcycles_VII=np.mean(helpblip_intpower_op_numcycles_VII,axis=0)
    helpblip_intpower_op_numcycles_VIII=np.mean(helpblip_intpower_op_numcycles_VIII,axis=0)
    helpblip_intpower_op_numcycles_IX=np.mean(helpblip_intpower_op_numcycles_IX,axis=0)
    helpblip_intpower_op_numcycles_X=np.mean(helpblip_intpower_op_numcycles_X,axis=0)
    helpblip_intpower_op_numcycles_XI=np.mean(helpblip_intpower_op_numcycles_XI,axis=0)
    helpblip_intpower_op_numcycles_XII=np.mean(helpblip_intpower_op_numcycles_XII,axis=0)
    helpblip_intpower_op_numcycles_XIII=np.mean(helpblip_intpower_op_numcycles_XIII,axis=0)
    helpblip_intpower_op_numcycles_XIV=np.mean(helpblip_intpower_op_numcycles_XIV,axis=0)    
    helpblip_intpower_op_numcycles_XV=np.mean(helpblip_intpower_op_numcycles_XV,axis=0)

    helpblip_intpower_op_numcycles_IV_noearly_die=np.mean(helpblip_intpower_op_numcycles_IV_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_V_noearly_die=np.mean(helpblip_intpower_op_numcycles_V_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_VI_noearly_die=np.mean(helpblip_intpower_op_numcycles_VI_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_VII_noearly_die=np.mean(helpblip_intpower_op_numcycles_VII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_VIII_noearly_die=np.mean(helpblip_intpower_op_numcycles_VIII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_IX_noearly_die=np.mean(helpblip_intpower_op_numcycles_IX_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_X_noearly_die=np.mean(helpblip_intpower_op_numcycles_X_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XI_noearly_die=np.mean(helpblip_intpower_op_numcycles_XI_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XII_noearly_die=np.mean(helpblip_intpower_op_numcycles_XII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XIII_noearly_die=np.mean(helpblip_intpower_op_numcycles_XIII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XIV_noearly_die=np.mean(helpblip_intpower_op_numcycles_XIV_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XV_noearly_die=np.mean(helpblip_intpower_op_numcycles_XV_noearly_die,axis=0)

    # print(f'cycles{helpblip_intpower_op_numcycles_IV}')
    helpblip_intpower_boot_overhead_Dtile_4_Ctile_2=((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_4_Ctile_5=((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_4_Ctile_10=((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_2=((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_5=((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_10=((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_2=((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_5=((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_10=((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_2=((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_5=((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_10=((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*fpga_boot_energy)




    helpblip_intpower_fpgacomm_Dtile_4_Ctile_2=helpblip_intpower_commcounter_IV*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_5=helpblip_intpower_commcounter_V*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_10=helpblip_intpower_commcounter_VI*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_2=helpblip_intpower_commcounter_VII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_5=helpblip_intpower_commcounter_VIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_10=helpblip_intpower_commcounter_IX*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_2=helpblip_intpower_commcounter_X*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_5=helpblip_intpower_commcounter_XI*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_10=helpblip_intpower_commcounter_XII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_2=helpblip_intpower_commcounter_XIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_5=helpblip_intpower_commcounter_XIV*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_10=helpblip_intpower_commcounter_XV*((7*spi_cycle_time)+(spi_cycle_time/2))



    helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die=helpblip_intpower_commcounter_IV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die=helpblip_intpower_commcounter_V_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die=helpblip_intpower_commcounter_VI_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die=helpblip_intpower_commcounter_VII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die=helpblip_intpower_commcounter_VIII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die=helpblip_intpower_commcounter_IX_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die=helpblip_intpower_commcounter_X_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die=helpblip_intpower_commcounter_XI_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die=helpblip_intpower_commcounter_XII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die=helpblip_intpower_commcounter_XIII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die=helpblip_intpower_commcounter_XIV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die=helpblip_intpower_commcounter_XV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))



    # energy_lost_redm_Dtile4_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2-(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5-(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10-(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2-(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5-(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10-(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2-(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5-(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10-(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2-(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5-(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10-(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power

    energy_lost_redm_Dtile4_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2-(comm_time_Dtile4_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile4_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5-(comm_time_Dtile4_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile4_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10-(comm_time_Dtile4_Ctile10))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2-(comm_time_Dtile7_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5-(comm_time_Dtile7_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10-(comm_time_Dtile7_Ctile10))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2-(comm_time_Dtile14_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5-(comm_time_Dtile14_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10-(comm_time_Dtile14_Ctile10))*mean_system_power
    energy_lost_redm_Dtile28_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2-(comm_time_Dtile28_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5-(comm_time_Dtile28_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10-(comm_time_Dtile28_Ctile10))*mean_system_power



    # energy_lost_redm_Dtile4_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die-(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die-(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die-(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die-(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die-(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die-(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die-(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die-(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die-(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die-(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die-(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die-(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power

    energy_lost_redm_Dtile4_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die-(comm_time_Dtile4_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile4_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die-(comm_time_Dtile4_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile4_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die-(comm_time_Dtile4_Ctile10))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die-(comm_time_Dtile7_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die-(comm_time_Dtile7_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die-(comm_time_Dtile7_Ctile10))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die-(comm_time_Dtile14_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die-(comm_time_Dtile14_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die-(comm_time_Dtile14_Ctile10))*mean_system_power
    energy_lost_redm_Dtile28_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die-(comm_time_Dtile28_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die-(comm_time_Dtile28_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die-(comm_time_Dtile28_Ctile10))*mean_system_power


    # print(f'conv{op_Dtile4_Ctile2[conv_index],conv_index}')

    # energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power

#### Recomputation cost$
    energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power

####Recom loss per cycle######

    # energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])
    # energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])
    # energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])
    # energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])
    # energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])
    # energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])
    # energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])
    # energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])
    # energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])
    # energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])
    # energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])
    # energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])


    # energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IV_noearly_die[conv_index])*helpblip_intpower_ontime_IV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die)-((op_Dtile4_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_V_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_V_noearly_die[conv_index])*helpblip_intpower_ontime_V_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die)-((op_Dtile4_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VI_noearly_die[conv_index])*helpblip_intpower_ontime_VI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die)-((op_Dtile4_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VII_noearly_die[conv_index])*helpblip_intpower_ontime_VII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die)-((op_Dtile7_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VIII_noearly_die[conv_index])*helpblip_intpower_ontime_VIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die)-((op_Dtile7_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IX_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IX_noearly_die[conv_index])*helpblip_intpower_ontime_IX_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die)-((op_Dtile7_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_X_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_X_noearly_die[conv_index])*helpblip_intpower_ontime_X_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die)-((op_Dtile14_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XI_noearly_die[conv_index])*helpblip_intpower_ontime_XI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die)-((op_Dtile14_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XII_noearly_die[conv_index])*helpblip_intpower_ontime_XII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die)-((op_Dtile14_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIII_noearly_die[conv_index])*helpblip_intpower_ontime_XIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die)-((op_Dtile28_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIV_noearly_die[conv_index])*helpblip_intpower_ontime_XIV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die)-((op_Dtile28_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XV_noearly_die[conv_index])*helpblip_intpower_ontime_XV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die)-((op_Dtile28_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # print(op_Dtile4_Ctile2)
    energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IV_noearly_die[conv_index])*helpblip_intpower_ontime_IV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power
    # print(energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die)
    energy_lost_rc_Dtile4_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_V_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_V_noearly_die[conv_index])*helpblip_intpower_ontime_V_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile4_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VI_noearly_die[conv_index])*helpblip_intpower_ontime_VI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VII_noearly_die[conv_index])*helpblip_intpower_ontime_VII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VIII_noearly_die[conv_index])*helpblip_intpower_ontime_VIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IX_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IX_noearly_die[conv_index])*helpblip_intpower_ontime_IX_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_X_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_X_noearly_die[conv_index])*helpblip_intpower_ontime_X_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XI_noearly_die[conv_index])*helpblip_intpower_ontime_XI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XII_noearly_die[conv_index])*helpblip_intpower_ontime_XII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIII_noearly_die[conv_index])*helpblip_intpower_ontime_XIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIV_noearly_die[conv_index])*helpblip_intpower_ontime_XIV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XV_noearly_die[conv_index])*helpblip_intpower_ontime_XV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power


    ############prepare data to plot ###################
   
    # energy_lost_dm=[energy_lost_dm_Dtile28_Ctile10,energy_lost_dm_Dtile28_Ctile5,energy_lost_dm_Dtile28_Ctile2,energy_lost_dm_Dtile14_Ctile10,energy_lost_dm_Dtile14_Ctile5,
    #                 energy_lost_dm_Dtile14_Ctile2,energy_lost_dm_Dtile7_Ctile10,energy_lost_dm_Dtile4_Ctile10,
    #                 energy_lost_dm_Dtile7_Ctile5,energy_lost_dm_Dtile7_Ctile2,energy_lost_dm_Dtile4_Ctile5,energy_lost_dm_Dtile4_Ctile2]
    # energy_lost_dm=np.array(energy_lost_dm)
    
    # print(f'Re DM{energy_lost_dm}')
    # print(f'sorted Re DM{-np.sort(-energy_lost_dm)}')

    energy_lost_dm=[energy_lost_dm_Dtile28_Ctile10,energy_lost_dm_Dtile14_Ctile10,energy_lost_dm_Dtile7_Ctile10,energy_lost_dm_Dtile4_Ctile10,energy_lost_dm_Dtile28_Ctile5,
                    energy_lost_dm_Dtile28_Ctile2,energy_lost_dm_Dtile14_Ctile5,energy_lost_dm_Dtile7_Ctile5,energy_lost_dm_Dtile4_Ctile5,
                    energy_lost_dm_Dtile14_Ctile2,energy_lost_dm_Dtile7_Ctile2,energy_lost_dm_Dtile4_Ctile2]
                
    energy_lost_rc=[energy_lost_rc_Dtile28_Ctile10,energy_lost_rc_Dtile14_Ctile10,energy_lost_rc_Dtile7_Ctile10,energy_lost_rc_Dtile4_Ctile10,energy_lost_rc_Dtile28_Ctile5,
                    energy_lost_rc_Dtile28_Ctile2,energy_lost_rc_Dtile14_Ctile5,energy_lost_rc_Dtile7_Ctile5,
                    energy_lost_rc_Dtile4_Ctile5, energy_lost_rc_Dtile14_Ctile2,energy_lost_rc_Dtile7_Ctile2,energy_lost_rc_Dtile4_Ctile2]
    energy_lost_rc=np.array(energy_lost_rc)

    energy_lost_redm=[energy_lost_redm_Dtile28_Ctile10_intpower,energy_lost_redm_Dtile14_Ctile10_intpower, energy_lost_redm_Dtile7_Ctile10_intpower,energy_lost_redm_Dtile4_Ctile10_intpower,energy_lost_redm_Dtile28_Ctile5_intpower,
                      energy_lost_redm_Dtile28_Ctile2_intpower,energy_lost_redm_Dtile14_Ctile5_intpower,energy_lost_redm_Dtile7_Ctile5_intpower,
                      energy_lost_redm_Dtile4_Ctile5_intpower,energy_lost_redm_Dtile14_Ctile2_intpower, energy_lost_redm_Dtile7_Ctile2_intpower,energy_lost_redm_Dtile4_Ctile2_intpower]
    energy_lost_redm=np.array(energy_lost_redm)

    energy_lost_redm_noearly_die=[energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile4_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die,
                              energy_lost_redm_Dtile28_Ctile2_intpower_noearly_die,energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die,energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die,
                              energy_lost_redm_Dtile4_Ctile5_intpower_noearly_die,energy_lost_redm_Dtile14_Ctile2_intpower_noearly_die,energy_lost_redm_Dtile7_Ctile2_intpower_noearly_die,energy_lost_redm_Dtile4_Ctile2_intpower_noearly_die]
    energy_lost_redm_noearly_die=np.array(energy_lost_redm_noearly_die)
    
    energy_lost_rc_intpower=[energy_lost_rc_Dtile28_Ctile10_intpower,energy_lost_rc_Dtile14_Ctile10_intpower,energy_lost_rc_Dtile7_Ctile10_intpower,energy_lost_rc_Dtile4_Ctile10_intpower,energy_lost_rc_Dtile28_Ctile5_intpower,
                              energy_lost_rc_Dtile28_Ctile2_intpower, energy_lost_rc_Dtile14_Ctile5_intpower,energy_lost_rc_Dtile7_Ctile5_intpower,
                              energy_lost_rc_Dtile4_Ctile5_intpower,energy_lost_rc_Dtile14_Ctile2_intpower,energy_lost_rc_Dtile7_Ctile2_intpower,energy_lost_rc_Dtile4_Ctile2_intpower]
    
    energy_lost_rc_intpower=np.array(energy_lost_rc_intpower)
    
    energy_lost_rc_intpower_noearly_die=[energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die,energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die,energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die,energy_lost_rc_Dtile4_Ctile10_intpower_noearly_die,
                              energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die,energy_lost_rc_Dtile28_Ctile2_intpower_noearly_die,energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die,energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die,
                              energy_lost_rc_Dtile4_Ctile5_intpower_noearly_die,energy_lost_rc_Dtile14_Ctile2_intpower_noearly_die,energy_lost_rc_Dtile7_Ctile2_intpower_noearly_die, energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die]
    energy_lost_rc_intpower_noearly_die=np.array(energy_lost_rc_intpower_noearly_die)
    
    energy_lost_boot=[helpblip_intpower_boot_overhead_Dtile_28_Ctile_10,helpblip_intpower_boot_overhead_Dtile_14_Ctile_10,helpblip_intpower_boot_overhead_Dtile_7_Ctile_10,helpblip_intpower_boot_overhead_Dtile_4_Ctile_10,
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_5,helpblip_intpower_boot_overhead_Dtile_28_Ctile_2,helpblip_intpower_boot_overhead_Dtile_14_Ctile_5,
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_5,helpblip_intpower_boot_overhead_Dtile_4_Ctile_5,helpblip_intpower_boot_overhead_Dtile_14_Ctile_2,helpblip_intpower_boot_overhead_Dtile_7_Ctile_2,helpblip_intpower_boot_overhead_Dtile_4_Ctile_2]
    energy_lost_boot=np.array(energy_lost_boot)
    
    # print(energy_lost_boot)
    fig,ax0=plt.subplots()

    # print(energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die)

    ax0.plot(X,energy_lost_dm,color='Violet',marker='x',linewidth=2,ms=12,label='Required Redundant Data Movement')
    # ax0.plot(X,energy_lost_redm_noearly_die+energy_lost_rc_intpower_noearly_die,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data and Recomputation')
    ax0.plot(X,energy_lost_redm+energy_lost_rc_intpower,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data and Recomputation')
    # ax0.plot(X,energy_lost_redm,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data')
    # ax0.plot(X,energy_lost_rc_intpower,color='brown',marker='o',linewidth=2,ms=12,label='Recomputation')


    ax0.plot(X,energy_lost_boot,color='blue',marker='*',linewidth=2,ms=12,label='Boot')
    # ax0.plot(X,energy_lost_dm+energy_lost_rc+energy_lost_rc_intpower_noearly_die+energy_lost_redm_noearly_die+energy_lost_boot,color='black',marker='D',linewidth=2,ms=12,ls='-.',label='Total')
    ax0.plot(X,energy_lost_dm+energy_lost_rc+energy_lost_rc_intpower+energy_lost_redm+energy_lost_boot,color='black',marker='d',linewidth=2,ms=12,ls='-.',label='Total')

    ax0.plot(X,energy_lost_rc,color='Green',marker='v',linewidth=2,ms=12,label='Required Redundant\nComputation')
    
    # ax0.plot(X,np.sort(energy_lost_redm_noearly_die+energy_lost_rc_intpower_noearly_die),color='blue',marker='*',linewidth=2,ms=12,label='Re-data movement and Re-computation \n early die')
    
    ax0.set_xticks([0,5.5,11], labels=['Minimum', 'Medium' , 'Maximum'],fontsize=35)
    ax0.tick_params(axis='both', which='both', labelsize=12,width=2)

    ax0.grid()
    ax0.legend(loc='upper left',bbox_to_anchor=(-0.14,1.17),ncol=3,fontsize=12)
    ax0.set_yscale('log')
    ax0.set_ylabel('Energy Cost per Inference (J)',fontsize=20)
    ax0.set_xlabel('Tiling Knob',loc='center',fontsize=20,labelpad=0.1)
    ax0.tick_params(axis='x', which='major', labelsize=15,width=3)
# ,bbox_to_anchor=(1.08,1.17)

    fig.set_size_inches(8.1,5.7)
    plt.savefig(f'computation_schedule_144mF_rev1.02.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_computation_scheduling_50mF(inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1, 
                               
                                inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
                                helpblip_intpower_VIII,helpblip_intpower_numcycles_VIII,helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII,helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII,helpblip_intpower_op_numcycles_VIII,
                                helpblip_intpower_VIII_noearly_die,helpblip_intpower_numcycles_VIII_noearly_die,helpblip_intpower_compute_counter_VIII_noearly_die,helpblip_intpower_commcounter_VIII_noearly_die,helpblip_intpower_ontime_VIII_noearly_die,helpblip_intpower_offtime_VIII_noearly_die,helpblip_intpower_op_numcycles_VIII_noearly_die,

                                inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
                                helpblip_intpower_IX,helpblip_intpower_numcycles_IX,helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX,helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX,helpblip_intpower_op_numcycles_IX,
                                helpblip_intpower_IX_noearly_die,helpblip_intpower_numcycles_IX_noearly_die,helpblip_intpower_compute_counter_IX_noearly_die,helpblip_intpower_commcounter_IX_noearly_die,helpblip_intpower_ontime_IX_noearly_die,helpblip_intpower_offtime_IX_noearly_die,helpblip_intpower_op_numcycles_IX_noearly_die,

                                inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
                                helpblip_intpower_XI,helpblip_intpower_numcycles_XI,helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI,helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI,helpblip_intpower_op_numcycles_XI,
                                helpblip_intpower_XI_noearly_die,helpblip_intpower_numcycles_XI_noearly_die,helpblip_intpower_compute_counter_XI_noearly_die,helpblip_intpower_commcounter_XI_noearly_die,helpblip_intpower_ontime_XI_noearly_die,helpblip_intpower_offtime_XI_noearly_die,helpblip_intpower_op_numcycles_XI_noearly_die,

                                inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
                                helpblip_intpower_XII,helpblip_intpower_numcycles_XII,helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII,helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII,helpblip_intpower_op_numcycles_XII,
                                helpblip_intpower_XII_noearly_die,helpblip_intpower_numcycles_XII_noearly_die,helpblip_intpower_compute_counter_XII_noearly_die,helpblip_intpower_commcounter_XII_noearly_die,helpblip_intpower_ontime_XII_noearly_die,helpblip_intpower_offtime_XII_noearly_die,helpblip_intpower_op_numcycles_XII_noearly_die,

                                inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
                                helpblip_intpower_XIV,helpblip_intpower_numcycles_XIV,helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV,helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV,helpblip_intpower_op_numcycles_XIV,
                                helpblip_intpower_XIV_noearly_die,helpblip_intpower_numcycles_XIV_noearly_die,helpblip_intpower_compute_counter_XIV_noearly_die,helpblip_intpower_commcounter_XIV_noearly_die,helpblip_intpower_ontime_XIV_noearly_die,helpblip_intpower_offtime_XIV_noearly_die,helpblip_intpower_op_numcycles_XIV_noearly_die,

                                inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
                                helpblip_intpower_XV,helpblip_intpower_numcycles_XV,helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV,helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV,helpblip_intpower_op_numcycles_XV,
                                helpblip_intpower_XV_noearly_die,helpblip_intpower_numcycles_XV_noearly_die,helpblip_intpower_compute_counter_XV_noearly_die,helpblip_intpower_commcounter_XV_noearly_die,helpblip_intpower_ontime_XV_noearly_die,helpblip_intpower_offtime_XV_noearly_die,helpblip_intpower_op_numcycles_XV_noearly_die):
    
    spi_cycle_time=(1/(8e06/3))
    spi_cycle_power=0.00724
    fpga_clock=1/20e06
   
    dtile=[49,196,784]
    # dtile=[4,16,49]
    ctile=[25,100]
    X=np.arange(len(dtile)*len(ctile))
    cap_size=144e-03
    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=140e-03+8.64e-03
    fpga_boot_energy=2.32E-04
    cap_energy=0.03759-fpga_boot_energy
    mean_system_power=32.8e-03
    # msp_boot_time=5e-03
    depthwise_index=2
    conv_index=4
    label=['Knob-1', 'Knob-2','Knob-3','Knob-4','Knob-5', 'Knob-6','Knob-7','Knob-8',
           'Knob-9', 'Knob-10','Knob-11','Knob-12','Knob-13', 'Knob-14','Knob-15']
    
    
    

    op_Dtile1_Ctile1=np.mean(op_Dtile1_Ctile1,axis=0)
    op_Dtile7_Ctile5=np.mean(op_Dtile7_Ctile5,axis=0)
    op_Dtile7_Ctile10=np.mean(op_Dtile7_Ctile10,axis=0)
    op_Dtile14_Ctile5=np.mean(op_Dtile14_Ctile5,axis=0)
    op_Dtile14_Ctile10=np.mean(op_Dtile14_Ctile10,axis=0)
    op_Dtile28_Ctile5=np.mean(op_Dtile28_Ctile5,axis=0)
    op_Dtile28_Ctile10=np.mean(op_Dtile28_Ctile10,axis=0)



    # helpblip_intpower_numcycles_IV=np.mean(helpblip_intpower_numcycles_IV,axis=0)
    # helpblip_intpower_numcycles_V=np.mean(helpblip_intpower_numcycles_V,axis=0)
    # helpblip_intpower_numcycles_VI=np.mean(helpblip_intpower_numcycles_VI,axis=0)
    # helpblip_intpower_numcycles_VII=np.mean(helpblip_intpower_numcycles_VII,axis=0)
    helpblip_intpower_numcycles_VIII=np.mean(helpblip_intpower_numcycles_VIII,axis=0)
    helpblip_intpower_numcycles_IX=np.mean(helpblip_intpower_numcycles_IX,axis=0)
    # helpblip_intpower_numcycles_X=np.mean(helpblip_intpower_numcycles_X,axis=0)
    helpblip_intpower_numcycles_XI=np.mean(helpblip_intpower_numcycles_XI,axis=0)
    helpblip_intpower_numcycles_XII=np.mean(helpblip_intpower_numcycles_XII,axis=0)
    # helpblip_intpower_numcycles_XIII=np.mean(helpblip_intpower_numcycles_XIII,axis=0)
    helpblip_intpower_numcycles_XIV=np.mean(helpblip_intpower_numcycles_XIV,axis=0)
    helpblip_intpower_numcycles_XV=np.mean(helpblip_intpower_numcycles_XV,axis=0)

    #No early die data##########

    # helpblip_intpower_numcycles_IV_noearly_die=np.mean(helpblip_intpower_numcycles_IV_noearly_die,axis=0)
    # helpblip_intpower_numcycles_V_noearly_die=np.mean(helpblip_intpower_numcycles_V_noearly_die,axis=0)
    # helpblip_intpower_numcycles_VI_noearly_die=np.mean(helpblip_intpower_numcycles_VI_noearly_die,axis=0)
    # helpblip_intpower_numcycles_VII_noearly_die=np.mean(helpblip_intpower_numcycles_VII_noearly_die,axis=0)
    helpblip_intpower_numcycles_VIII_noearly_die=np.mean(helpblip_intpower_numcycles_VIII_noearly_die,axis=0)
    helpblip_intpower_numcycles_IX_noearly_die=np.mean(helpblip_intpower_numcycles_IX_noearly_die,axis=0)
    # helpblip_intpower_numcycles_X_noearly_die=np.mean(helpblip_intpower_numcycles_X_noearly_die,axis=0)
    helpblip_intpower_numcycles_XI_noearly_die=np.mean(helpblip_intpower_numcycles_XI_noearly_die,axis=0)
    helpblip_intpower_numcycles_XII_noearly_die=np.mean(helpblip_intpower_numcycles_XII_noearly_die,axis=0)
    # helpblip_intpower_numcycles_XIII_noearly_die=np.mean(helpblip_intpower_numcycles_XIII_noearly_die,axis=0)
    helpblip_intpower_numcycles_XIV_noearly_die=np.mean(helpblip_intpower_numcycles_XIV_noearly_die,axis=0)
    helpblip_intpower_numcycles_XV_noearly_die=np.mean(helpblip_intpower_numcycles_XV_noearly_die,axis=0)


    # helpblip_intpower_ontime_IV_noearly_die,helpblip_intpower_offtime_IV_noearly_die=np.mean(helpblip_intpower_ontime_IV_noearly_die),np.mean(helpblip_intpower_offtime_IV_noearly_die)
    # helpblip_intpower_ontime_V_noearly_die,helpblip_intpower_offtime_V_noearly_die=np.mean(helpblip_intpower_ontime_V_noearly_die),np.mean(helpblip_intpower_offtime_V_noearly_die)
    # helpblip_intpower_ontime_VI_noearly_die,helpblip_intpower_offtime_VI_noearly_die=np.mean(helpblip_intpower_ontime_VI_noearly_die),np.mean(helpblip_intpower_offtime_VI_noearly_die)
    # helpblip_intpower_ontime_VII_noearly_die,helpblip_intpower_offtime_VII_noearly_die=np.mean(helpblip_intpower_ontime_VII_noearly_die),np.mean(helpblip_intpower_offtime_VII_noearly_die)
    helpblip_intpower_ontime_VIII_noearly_die,helpblip_intpower_offtime_VIII_noearly_die=np.mean(helpblip_intpower_ontime_VIII_noearly_die),np.mean(helpblip_intpower_offtime_VIII_noearly_die)
    helpblip_intpower_ontime_IX_noearly_die,helpblip_intpower_offtime_IX_noearly_die=np.mean(helpblip_intpower_ontime_IX_noearly_die),np.mean(helpblip_intpower_offtime_IX_noearly_die)
    # helpblip_intpower_ontime_X_noearly_die,helpblip_intpower_offtime_X_noearly_die=np.mean(helpblip_intpower_ontime_X_noearly_die),np.mean(helpblip_intpower_offtime_X_noearly_die)
    helpblip_intpower_ontime_XI_noearly_die,helpblip_intpower_offtime_XI_noearly_die=np.mean(helpblip_intpower_ontime_XI_noearly_die),np.mean(helpblip_intpower_offtime_XI_noearly_die)
    helpblip_intpower_ontime_XII_noearly_die,helpblip_intpower_offtime_XII_noearly_die=np.mean(helpblip_intpower_ontime_XII_noearly_die),np.mean(helpblip_intpower_offtime_XII_noearly_die)
    # helpblip_intpower_ontime_XIII_noearly_die,helpblip_intpower_offtime_XIII_noearly_die=np.mean(helpblip_intpower_ontime_XIII_noearly_die),np.mean(helpblip_intpower_offtime_XIII_noearly_die)
    helpblip_intpower_ontime_XIV_noearly_die,helpblip_intpower_offtime_XIV_noearly_die=np.mean(helpblip_intpower_ontime_XIV_noearly_die),np.mean(helpblip_intpower_offtime_XIV_noearly_die)
    helpblip_intpower_ontime_XV_noearly_die,helpblip_intpower_offtime_XV_noearly_die=np.mean(helpblip_intpower_ontime_XV_noearly_die),np.mean(helpblip_intpower_offtime_XV_noearly_die)
    
    # helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV=np.mean(helpblip_intpower_ontime_IV),np.mean(helpblip_intpower_offtime_IV)
    # helpblip_intpower_ontime_V,helpblip_intpower_offtime_V=np.mean(helpblip_intpower_ontime_V),np.mean(helpblip_intpower_offtime_V)
    # helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI=np.mean(helpblip_intpower_ontime_VI),np.mean(helpblip_intpower_offtime_VI)
    # helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII=np.mean(helpblip_intpower_ontime_VII),np.mean(helpblip_intpower_offtime_VII)
    helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII=np.mean(helpblip_intpower_ontime_VIII),np.mean(helpblip_intpower_offtime_VIII)
    helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX=np.mean(helpblip_intpower_ontime_IX),np.mean(helpblip_intpower_offtime_IX)
    # helpblip_intpower_ontime_X,helpblip_intpower_offtime_X=np.mean(helpblip_intpower_ontime_X),np.mean(helpblip_intpower_offtime_X)
    helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI=np.mean(helpblip_intpower_ontime_XI),np.mean(helpblip_intpower_offtime_XI)
    helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII=np.mean(helpblip_intpower_ontime_XII),np.mean(helpblip_intpower_offtime_XII)
    # helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII=np.mean(helpblip_intpower_ontime_XIII),np.mean(helpblip_intpower_offtime_XIII)
    helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV=np.mean(helpblip_intpower_ontime_XIV),np.mean(helpblip_intpower_offtime_XIV)
    helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV=np.mean(helpblip_intpower_ontime_XV),np.mean(helpblip_intpower_offtime_XV)
    
    # print(helpblip_intpower_ontime_V)

    # mean_power_Dtile_4_Ctile_2=cap_energy/helpblip_intpower_ontime_IV
    # mean_power_Dtile_4_Ctile_5=cap_energy/helpblip_intpower_ontime_V
    # mean_power_Dtile_4_Ctile_10=cap_energy/helpblip_intpower_ontime_VI
    # mean_power_Dtile_7_Ctile_2=cap_energy/helpblip_intpower_ontime_VII
    # mean_power_Dtile_7_Ctile_5=cap_energy/helpblip_intpower_ontime_VIII
    # mean_power_Dtile_7_Ctile_10=cap_energy/helpblip_intpower_ontime_IX
    # mean_power_Dtile_14_Ctile_2=cap_energy/helpblip_intpower_ontime_X
    # mean_power_Dtile_14_Ctile_5=cap_energy/helpblip_intpower_ontime_XI
    # mean_power_Dtile_14_Ctile_10=cap_energy/helpblip_intpower_ontime_XII
    # mean_power_Dtile_28_Ctile_2=cap_energy/helpblip_intpower_ontime_XIII
    # mean_power_Dtile_28_Ctile_5=cap_energy/helpblip_intpower_ontime_XIV
    # mean_power_Dtile_28_Ctile_10=cap_energy/helpblip_intpower_ontime_XV

    
    ###############continuous power calculations###################

    inftime_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1=np.mean(inftime_Dtile1_Ctile1),np.mean(comp_Dtile1_Ctile1),np.mean(comm_Dtile1_Ctile1) 
    # inftime_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2=np.mean(inftime_Dtile4_Ctile2),np.mean(comp_Dtile4_Ctile2),np.mean(comm_Dtile4_Ctile2) 
    # inftime_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5=np.mean(inftime_Dtile4_Ctile5),np.mean(comp_Dtile4_Ctile5),np.mean(comm_Dtile4_Ctile5) 
    # inftime_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10=np.mean(inftime_Dtile4_Ctile10),np.mean(comp_Dtile4_Ctile10),np.mean(comm_Dtile4_Ctile10)
    # inftime_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2=np.mean(inftime_Dtile7_Ctile2),np.mean(comp_Dtile7_Ctile2),np.mean(comm_Dtile7_Ctile2)
    inftime_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5=np.mean(inftime_Dtile7_Ctile5),np.mean(comp_Dtile7_Ctile5),np.mean(comm_Dtile7_Ctile5)
    inftime_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10=np.mean(inftime_Dtile7_Ctile10),np.mean(comp_Dtile7_Ctile10),np.mean(comm_Dtile7_Ctile10)
    # inftime_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2=np.mean(inftime_Dtile14_Ctile2),np.mean(comp_Dtile14_Ctile2),np.mean(comm_Dtile14_Ctile2)
    inftime_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5=np.mean(inftime_Dtile14_Ctile5),np.mean(comp_Dtile14_Ctile5),np.mean(comm_Dtile14_Ctile5)
    inftime_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10=np.mean(inftime_Dtile14_Ctile10),np.mean(comp_Dtile14_Ctile10),np.mean(comm_Dtile14_Ctile10)
    # inftime_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2=np.mean(inftime_Dtile28_Ctile2),np.mean(comp_Dtile28_Ctile2),np.mean(comm_Dtile28_Ctile2)
    inftime_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5=np.mean(inftime_Dtile28_Ctile5),np.mean(comp_Dtile28_Ctile5),np.mean(comm_Dtile28_Ctile5)
    inftime_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10=np.mean(inftime_Dtile28_Ctile10),np.mean(comp_Dtile28_Ctile10),np.mean(comm_Dtile28_Ctile10)
    ####mean power#######
    # mean_power_Dtile_1_Ctile_1=42.8458e-03
    # mean_power_Dtile_2_Ctile_2=43.2055e-03
    # mean_power_Dtile_2_Ctile_5=42.0467e-03
    # mean_power_Dtile_2_Ctile_10=43.0679e-03
    # mean_power_Dtile_4_Ctile_2=42.609e-03
    # mean_power_Dtile_4_Ctile_5=43.4253e-03
    # mean_power_Dtile_4_Ctile_10=42.3665e-03
    # mean_power_Dtile_7_Ctile_2=42.1503e-03
    # mean_power_Dtile_7_Ctile_5=43.8843e-03
    # mean_power_Dtile_7_Ctile_10=43.8646e-03
    # mean_power_Dtile_14_Ctile_2=4.7559e-03
    # mean_power_Dtile_14_Ctile_5=42e-03
    # mean_power_Dtile_14_Ctile_10=42e-03
    # mean_power_Dtile_28_Ctile_2=42e-03
    # mean_power_Dtile_28_Ctile_5=42e-03
    # mean_power_Dtile_28_Ctile_10=42e-03

    ########datamovementtime##########
    comm_time_Dtile1_Ctile1=comm_Dtile1_Ctile1*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile4_Ctile2=comm_Dtile4_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile4_Ctile5=comm_Dtile4_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile4_Ctile10=comm_Dtile4_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile7_Ctile2=comm_Dtile7_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile5=comm_Dtile7_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile7_Ctile10=comm_Dtile7_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile14_Ctile2=comm_Dtile14_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile5=comm_Dtile14_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile14_Ctile10=comm_Dtile14_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))
    # comm_time_Dtile28_Ctile2=comm_Dtile28_Ctile2*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile5=comm_Dtile28_Ctile5*((7*spi_cycle_time)+(spi_cycle_time/2))
    comm_time_Dtile28_Ctile10=comm_Dtile28_Ctile10*((7*spi_cycle_time)+(spi_cycle_time/2))


    ##########energy data movement#############

    # energy_lost_dm_Dtile2_Ctile2=(comm_time_Dtile2_Ctile2-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_2
    # energy_lost_dm_Dtile2_Ctile5=(comm_time_Dtile2_Ctile5-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_5
    # energy_lost_dm_Dtile2_Ctile10=(comm_time_Dtile2_Ctile10-comm_time_Dtile1_Ctile1)*mean_power_Dtile_2_Ctile_10
    # energy_lost_dm_Dtile4_Ctile2=(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    # energy_lost_dm_Dtile4_Ctile5=(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    # energy_lost_dm_Dtile4_Ctile10=(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    # energy_lost_dm_Dtile7_Ctile2=(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile7_Ctile5=(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile7_Ctile10=(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    # energy_lost_dm_Dtile14_Ctile2=(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile14_Ctile5=(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile14_Ctile10=(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    # energy_lost_dm_Dtile28_Ctile2=(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile28_Ctile5=(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1)*mean_system_power
    energy_lost_dm_Dtile28_Ctile10=(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1)*mean_system_power
    # print(f'comm{comm_time_Dtile1_Ctile1}')    


    ########Redundant computation##################
    # energy_lost_rc_Dtile4_Ctile2=((inftime_Dtile4_Ctile2-comm_time_Dtile4_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5=((inftime_Dtile4_Ctile5-comm_time_Dtile4_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10=((inftime_Dtile4_Ctile10-comm_time_Dtile4_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2=((inftime_Dtile7_Ctile2-comm_time_Dtile7_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5=((inftime_Dtile7_Ctile5-comm_time_Dtile7_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10=((inftime_Dtile7_Ctile10-comm_time_Dtile7_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2=((inftime_Dtile14_Ctile2-comm_time_Dtile14_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5=((inftime_Dtile14_Ctile5-comm_time_Dtile14_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10=((inftime_Dtile14_Ctile10-comm_time_Dtile14_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2=((inftime_Dtile28_Ctile2-comm_time_Dtile28_Ctile2)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5=((inftime_Dtile28_Ctile5-comm_time_Dtile28_Ctile5)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10=((inftime_Dtile28_Ctile10-comm_time_Dtile28_Ctile10)-(inftime_Dtile1_Ctile1-comm_time_Dtile1_Ctile1))*mean_system_power

    # energy_lost_rc_Dtile4_Ctile2=((op_Dtile4_Ctile2[conv_index]-comm_time_Dtile4_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5=((op_Dtile4_Ctile5[conv_index]-comm_time_Dtile4_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10=((op_Dtile4_Ctile10[conv_index]-comm_time_Dtile4_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2=((op_Dtile7_Ctile2[conv_index]-comm_time_Dtile7_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5=((op_Dtile7_Ctile5[conv_index]-comm_time_Dtile7_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10=((op_Dtile7_Ctile10[conv_index]-comm_time_Dtile7_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2=((op_Dtile14_Ctile2[conv_index]-comm_time_Dtile14_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5=((op_Dtile14_Ctile5[conv_index]-comm_time_Dtile14_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10=((op_Dtile14_Ctile10[conv_index]-comm_time_Dtile14_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2=((op_Dtile28_Ctile2[conv_index]-comm_time_Dtile28_Ctile2)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5=((op_Dtile28_Ctile5[conv_index]-comm_time_Dtile28_Ctile5)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10=((op_Dtile28_Ctile10[conv_index]-comm_time_Dtile28_Ctile10)-(op_Dtile1_Ctile1[conv_index]-comm_time_Dtile1_Ctile1))*mean_system_power


    ##############Intermittent power calculations##################


    # helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV=np.mean(helpblip_intpower_compute_counter_IV),np.mean(helpblip_intpower_commcounter_IV)
    # helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V=np.mean(helpblip_intpower_compute_counter_V),np.mean(helpblip_intpower_commcounter_V)
    # helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI=np.mean(helpblip_intpower_compute_counter_VI),np.mean(helpblip_intpower_commcounter_VI)
    # helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII=np.mean(helpblip_intpower_compute_counter_VII),np.mean(helpblip_intpower_commcounter_VII)
    helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII=np.mean(helpblip_intpower_compute_counter_VIII),np.mean(helpblip_intpower_commcounter_VIII)
    helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX=np.mean(helpblip_intpower_compute_counter_IX),np.mean(helpblip_intpower_commcounter_IX)
    # helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X=np.mean(helpblip_intpower_compute_counter_X),np.mean(helpblip_intpower_commcounter_X)
    helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI=np.mean(helpblip_intpower_compute_counter_XI),np.mean(helpblip_intpower_commcounter_XI)
    helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII=np.mean(helpblip_intpower_compute_counter_XII),np.mean(helpblip_intpower_commcounter_XII)
    # helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII=np.mean(helpblip_intpower_compute_counter_XIII),np.mean(helpblip_intpower_commcounter_XIII)
    helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV=np.mean(helpblip_intpower_compute_counter_XIV),np.mean(helpblip_intpower_commcounter_XIV)
    helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV=np.mean(helpblip_intpower_compute_counter_XV),np.mean(helpblip_intpower_commcounter_XV)

#####No early die data ################
    # helpblip_intpower_compute_counter_IV_noearly_die,helpblip_intpower_commcounter_IV_noearly_die=np.mean(helpblip_intpower_compute_counter_IV_noearly_die),np.mean(helpblip_intpower_commcounter_IV_noearly_die)
    # helpblip_intpower_compute_counter_V_noearly_die,helpblip_intpower_commcounter_V_noearly_die=np.mean(helpblip_intpower_compute_counter_V_noearly_die),np.mean(helpblip_intpower_commcounter_V_noearly_die)
    # helpblip_intpower_compute_counter_VI_noearly_die,helpblip_intpower_commcounter_VI_noearly_die=np.mean(helpblip_intpower_compute_counter_VI_noearly_die),np.mean(helpblip_intpower_commcounter_VI_noearly_die)
    # helpblip_intpower_compute_counter_VII_noearly_die,helpblip_intpower_commcounter_VII_noearly_die=np.mean(helpblip_intpower_compute_counter_VII_noearly_die),np.mean(helpblip_intpower_commcounter_VII_noearly_die)
    helpblip_intpower_compute_counter_VIII_noearly_die,helpblip_intpower_commcounter_VIII_noearly_die=np.mean(helpblip_intpower_compute_counter_VIII_noearly_die),np.mean(helpblip_intpower_commcounter_VIII_noearly_die)
    helpblip_intpower_compute_counter_IX_noearly_die,helpblip_intpower_commcounter_IX_noearly_die=np.mean(helpblip_intpower_compute_counter_IX_noearly_die),np.mean(helpblip_intpower_commcounter_IX_noearly_die)
    # helpblip_intpower_compute_counter_X_noearly_die,helpblip_intpower_commcounter_X_noearly_die=np.mean(helpblip_intpower_compute_counter_X_noearly_die),np.mean(helpblip_intpower_commcounter_X_noearly_die)
    helpblip_intpower_compute_counter_XI_noearly_die,helpblip_intpower_commcounter_XI_noearly_die=np.mean(helpblip_intpower_compute_counter_XI_noearly_die),np.mean(helpblip_intpower_commcounter_XI_noearly_die)
    helpblip_intpower_compute_counter_XII_noearly_die,helpblip_intpower_commcounter_XII_noearly_die=np.mean(helpblip_intpower_compute_counter_XII_noearly_die),np.mean(helpblip_intpower_commcounter_XII_noearly_die)
    # helpblip_intpower_compute_counter_XIII_noearly_die,helpblip_intpower_commcounter_XIII_noearly_die=np.mean(helpblip_intpower_compute_counter_XIII_noearly_die),np.mean(helpblip_intpower_commcounter_XIII_noearly_die)
    helpblip_intpower_compute_counter_XIV_noearly_die,helpblip_intpower_commcounter_XIV_noearly_die=np.mean(helpblip_intpower_compute_counter_XIV_noearly_die),np.mean(helpblip_intpower_commcounter_XIV_noearly_die)
    helpblip_intpower_compute_counter_XV_noearly_die,helpblip_intpower_commcounter_XV_noearly_die=np.mean(helpblip_intpower_compute_counter_XV_noearly_die),np.mean(helpblip_intpower_commcounter_XV_noearly_die)


    # helpblip_intpower_IV=np.mean(helpblip_intpower_IV,axis=0)
    # helpblip_intpower_V=np.mean(helpblip_intpower_V,axis=0)
    # helpblip_intpower_VI=np.mean(helpblip_intpower_VI,axis=0)
    # helpblip_intpower_VII=np.mean(helpblip_intpower_VII,axis=0)
    helpblip_intpower_VIII=np.mean(helpblip_intpower_VIII,axis=0)
    helpblip_intpower_IX=np.mean(helpblip_intpower_IX,axis=0)
    # helpblip_intpower_X=np.mean(helpblip_intpower_X,axis=0)
    helpblip_intpower_XI=np.mean(helpblip_intpower_XI,axis=0)
    helpblip_intpower_XII=np.mean(helpblip_intpower_XII,axis=0)
    # helpblip_intpower_XIII=np.mean(helpblip_intpower_XIII,axis=0)
    helpblip_intpower_XIV=np.mean(helpblip_intpower_XIV,axis=0)
    helpblip_intpower_XV=np.mean(helpblip_intpower_XV,axis=0)



    # helpblip_intpower_IV_noearly_die=np.mean(helpblip_intpower_IV_noearly_die,axis=0)
    # helpblip_intpower_V_noearly_die=np.mean(helpblip_intpower_V_noearly_die,axis=0)
    # helpblip_intpower_VI_noearly_die=np.mean(helpblip_intpower_VI_noearly_die,axis=0)
    # helpblip_intpower_VII_noearly_die=np.mean(helpblip_intpower_VII_noearly_die,axis=0)
    helpblip_intpower_VIII_noearly_die=np.mean(helpblip_intpower_VIII_noearly_die,axis=0)
    helpblip_intpower_IX_noearly_die=np.mean(helpblip_intpower_IX_noearly_die,axis=0)
    # helpblip_intpower_X_noearly_die=np.mean(helpblip_intpower_X_noearly_die,axis=0)
    helpblip_intpower_XI_noearly_die=np.mean(helpblip_intpower_XI_noearly_die,axis=0)
    helpblip_intpower_XII_noearly_die=np.mean(helpblip_intpower_XII_noearly_die,axis=0)
    # helpblip_intpower_XIII_noearly_die=np.mean(helpblip_intpower_XIII_noearly_die,axis=0)
    helpblip_intpower_XIV_noearly_die=np.mean(helpblip_intpower_XIV_noearly_die,axis=0)
    helpblip_intpower_XV_noearly_die=np.mean(helpblip_intpower_XV_noearly_die,axis=0)


    # helpblip_intpower_op_numcycles_IV=np.mean(helpblip_intpower_op_numcycles_IV,axis=0)
    # helpblip_intpower_op_numcycles_V=np.mean(helpblip_intpower_op_numcycles_V,axis=0)
    # helpblip_intpower_op_numcycles_VI=np.mean(helpblip_intpower_op_numcycles_VI,axis=0)
    # helpblip_intpower_op_numcycles_VII=np.mean(helpblip_intpower_op_numcycles_VII,axis=0)
    helpblip_intpower_op_numcycles_VIII=np.mean(helpblip_intpower_op_numcycles_VIII,axis=0)
    helpblip_intpower_op_numcycles_IX=np.mean(helpblip_intpower_op_numcycles_IX,axis=0)
    # helpblip_intpower_op_numcycles_X=np.mean(helpblip_intpower_op_numcycles_X,axis=0)
    helpblip_intpower_op_numcycles_XI=np.mean(helpblip_intpower_op_numcycles_XI,axis=0)
    helpblip_intpower_op_numcycles_XII=np.mean(helpblip_intpower_op_numcycles_XII,axis=0)
    # helpblip_intpower_op_numcycles_XIII=np.mean(helpblip_intpower_op_numcycles_XIII,axis=0)
    helpblip_intpower_op_numcycles_XIV=np.mean(helpblip_intpower_op_numcycles_XIV,axis=0)    
    helpblip_intpower_op_numcycles_XV=np.mean(helpblip_intpower_op_numcycles_XV,axis=0)

    # helpblip_intpower_op_numcycles_IV_noearly_die=np.mean(helpblip_intpower_op_numcycles_IV_noearly_die,axis=0)
    # helpblip_intpower_op_numcycles_V_noearly_die=np.mean(helpblip_intpower_op_numcycles_V_noearly_die,axis=0)
    # helpblip_intpower_op_numcycles_VI_noearly_die=np.mean(helpblip_intpower_op_numcycles_VI_noearly_die,axis=0)
    # helpblip_intpower_op_numcycles_VII_noearly_die=np.mean(helpblip_intpower_op_numcycles_VII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_VIII_noearly_die=np.mean(helpblip_intpower_op_numcycles_VIII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_IX_noearly_die=np.mean(helpblip_intpower_op_numcycles_IX_noearly_die,axis=0)
    # helpblip_intpower_op_numcycles_X_noearly_die=np.mean(helpblip_intpower_op_numcycles_X_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XI_noearly_die=np.mean(helpblip_intpower_op_numcycles_XI_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XII_noearly_die=np.mean(helpblip_intpower_op_numcycles_XII_noearly_die,axis=0)
    # helpblip_intpower_op_numcycles_XIII_noearly_die=np.mean(helpblip_intpower_op_numcycles_XIII_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XIV_noearly_die=np.mean(helpblip_intpower_op_numcycles_XIV_noearly_die,axis=0)
    helpblip_intpower_op_numcycles_XV_noearly_die=np.mean(helpblip_intpower_op_numcycles_XV_noearly_die,axis=0)

    # print(f'cycles{helpblip_intpower_op_numcycles_IV}')
    # helpblip_intpower_boot_overhead_Dtile_4_Ctile_2=((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*fpga_boot_energy)
    # helpblip_intpower_boot_overhead_Dtile_4_Ctile_5=((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*fpga_boot_energy)
    # helpblip_intpower_boot_overhead_Dtile_4_Ctile_10=((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*fpga_boot_energy)
    # helpblip_intpower_boot_overhead_Dtile_7_Ctile_2=((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_5=((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_10=((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*fpga_boot_energy)
    # helpblip_intpower_boot_overhead_Dtile_14_Ctile_2=((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_5=((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_14_Ctile_10=((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*fpga_boot_energy)
    # helpblip_intpower_boot_overhead_Dtile_28_Ctile_2=((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_5=((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*fpga_boot_energy)
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_10=((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*fpga_boot_energy)




    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_2=helpblip_intpower_commcounter_IV*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_5=helpblip_intpower_commcounter_V*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_10=helpblip_intpower_commcounter_VI*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_7_Ctile_2=helpblip_intpower_commcounter_VII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_5=helpblip_intpower_commcounter_VIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_10=helpblip_intpower_commcounter_IX*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_14_Ctile_2=helpblip_intpower_commcounter_X*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_5=helpblip_intpower_commcounter_XI*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_10=helpblip_intpower_commcounter_XII*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_28_Ctile_2=helpblip_intpower_commcounter_XIII*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_5=helpblip_intpower_commcounter_XIV*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_10=helpblip_intpower_commcounter_XV*((7*spi_cycle_time)+(spi_cycle_time/2))



    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die=helpblip_intpower_commcounter_IV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die=helpblip_intpower_commcounter_V_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die=helpblip_intpower_commcounter_VI_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die=helpblip_intpower_commcounter_VII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die=helpblip_intpower_commcounter_VIII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die=helpblip_intpower_commcounter_IX_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die=helpblip_intpower_commcounter_X_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die=helpblip_intpower_commcounter_XI_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die=helpblip_intpower_commcounter_XII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    # helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die=helpblip_intpower_commcounter_XIII_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die=helpblip_intpower_commcounter_XIV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die=helpblip_intpower_commcounter_XV_noearly_die*((7*spi_cycle_time)+(spi_cycle_time/2))



    # energy_lost_redm_Dtile4_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2-(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5-(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10-(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2-(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5-(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10-(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2-(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5-(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10-(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2-(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5-(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10-(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power

    # energy_lost_redm_Dtile4_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2-(comm_time_Dtile4_Ctile2))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5-(comm_time_Dtile4_Ctile5))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10-(comm_time_Dtile4_Ctile10))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2-(comm_time_Dtile7_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5-(comm_time_Dtile7_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10-(comm_time_Dtile7_Ctile10))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2-(comm_time_Dtile14_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5-(comm_time_Dtile14_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10-(comm_time_Dtile14_Ctile10))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2-(comm_time_Dtile28_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile5_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5-(comm_time_Dtile28_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile10_intpower=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10-(comm_time_Dtile28_Ctile10))*mean_system_power



    # energy_lost_redm_Dtile4_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die-(comm_time_Dtile4_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die-(comm_time_Dtile4_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die-(comm_time_Dtile4_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die-(comm_time_Dtile7_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die-(comm_time_Dtile7_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die-(comm_time_Dtile7_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die-(comm_time_Dtile14_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die-(comm_time_Dtile14_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die-(comm_time_Dtile14_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die-(comm_time_Dtile28_Ctile2-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die-(comm_time_Dtile28_Ctile5-comm_time_Dtile1_Ctile1))*mean_system_power 
    # energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die-(comm_time_Dtile28_Ctile10-comm_time_Dtile1_Ctile1))*mean_system_power

    # energy_lost_redm_Dtile4_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die-(comm_time_Dtile4_Ctile2))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die-(comm_time_Dtile4_Ctile5))*mean_system_power 
    # energy_lost_redm_Dtile4_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die-(comm_time_Dtile4_Ctile10))*mean_system_power 
    # energy_lost_redm_Dtile7_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die-(comm_time_Dtile7_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die-(comm_time_Dtile7_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die-(comm_time_Dtile7_Ctile10))*mean_system_power 
    # energy_lost_redm_Dtile14_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die-(comm_time_Dtile14_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die-(comm_time_Dtile14_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die-(comm_time_Dtile14_Ctile10))*mean_system_power
    # energy_lost_redm_Dtile28_Ctile2_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die-(comm_time_Dtile28_Ctile2))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die-(comm_time_Dtile28_Ctile5))*mean_system_power 
    energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die=(helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die-(comm_time_Dtile28_Ctile10))*mean_system_power


    # print(f'conv{op_Dtile4_Ctile2[conv_index],conv_index}')

    # energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power

#### Recomputation cost$
    # energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power
    # print(helpblip_intpower_ontime_XIV)
####Recom loss per cycle######

    # energy_lost_rc_Dtile4_Ctile2_intpower=((((helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])*helpblip_intpower_ontime_IV)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_IV[depthwise_index]+helpblip_intpower_op_numcycles_IV[conv_index])
    # energy_lost_rc_Dtile4_Ctile5_intpower=((((helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])*helpblip_intpower_ontime_V)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_V[depthwise_index]+helpblip_intpower_op_numcycles_V[conv_index])
    # energy_lost_rc_Dtile4_Ctile10_intpower=((((helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])*helpblip_intpower_ontime_VI)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_VI[depthwise_index]+helpblip_intpower_op_numcycles_VI[conv_index])
    # energy_lost_rc_Dtile7_Ctile2_intpower=((((helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])*helpblip_intpower_ontime_VII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_VII[depthwise_index]+helpblip_intpower_op_numcycles_VII[conv_index])
    # energy_lost_rc_Dtile7_Ctile5_intpower=((((helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])*helpblip_intpower_ontime_VIII)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_VIII[depthwise_index]+helpblip_intpower_op_numcycles_VIII[conv_index])
    # energy_lost_rc_Dtile7_Ctile10_intpower=((((helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])*helpblip_intpower_ontime_IX)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_IX[depthwise_index]+helpblip_intpower_op_numcycles_IX[conv_index])
    # energy_lost_rc_Dtile14_Ctile2_intpower=((((helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])*helpblip_intpower_ontime_X)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_X[depthwise_index]+helpblip_intpower_op_numcycles_X[conv_index])
    # energy_lost_rc_Dtile14_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])*helpblip_intpower_ontime_XI)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_XI[depthwise_index]+helpblip_intpower_op_numcycles_XI[conv_index])
    # energy_lost_rc_Dtile14_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])*helpblip_intpower_ontime_XII)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_XII[depthwise_index]+helpblip_intpower_op_numcycles_XII[conv_index])
    # energy_lost_rc_Dtile28_Ctile2_intpower=((((helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])*helpblip_intpower_ontime_XIII)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power/(helpblip_intpower_op_numcycles_XIII[depthwise_index]+helpblip_intpower_op_numcycles_XIII[conv_index])
    # energy_lost_rc_Dtile28_Ctile5_intpower=((((helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])*helpblip_intpower_ontime_XIV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power/(helpblip_intpower_op_numcycles_XIV[depthwise_index]+helpblip_intpower_op_numcycles_XIV[conv_index])
    # energy_lost_rc_Dtile28_Ctile10_intpower=((((helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])*helpblip_intpower_ontime_XV)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power/(helpblip_intpower_op_numcycles_XV[depthwise_index]+helpblip_intpower_op_numcycles_XV[conv_index])


    # energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IV_noearly_die[conv_index])*helpblip_intpower_ontime_IV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die)-((op_Dtile4_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_V_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_V_noearly_die[conv_index])*helpblip_intpower_ontime_V_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die)-((op_Dtile4_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VI_noearly_die[conv_index])*helpblip_intpower_ontime_VI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die)-((op_Dtile4_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VII_noearly_die[conv_index])*helpblip_intpower_ontime_VII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die)-((op_Dtile7_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VIII_noearly_die[conv_index])*helpblip_intpower_ontime_VIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die)-((op_Dtile7_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IX_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IX_noearly_die[conv_index])*helpblip_intpower_ontime_IX_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die)-((op_Dtile7_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_X_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_X_noearly_die[conv_index])*helpblip_intpower_ontime_X_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die)-((op_Dtile14_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XI_noearly_die[conv_index])*helpblip_intpower_ontime_XI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die)-((op_Dtile14_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XII_noearly_die[conv_index])*helpblip_intpower_ontime_XII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die)-((op_Dtile14_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIII_noearly_die[conv_index])*helpblip_intpower_ontime_XIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die)-((op_Dtile28_Ctile2[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIV_noearly_die[conv_index])*helpblip_intpower_ontime_XIV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die)-((op_Dtile28_Ctile5[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XV_noearly_die[conv_index])*helpblip_intpower_ontime_XV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die)-((op_Dtile28_Ctile10[conv_index])-comm_time_Dtile1_Ctile1))*mean_system_power
    # print(op_Dtile4_Ctile2)
    # energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IV_noearly_die[conv_index])*helpblip_intpower_ontime_IV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_2_noearly_die)-((op_Dtile4_Ctile2[conv_index]-2.98e-01-comm_time_Dtile4_Ctile2)))*mean_system_power
    # print(energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die)
    # energy_lost_rc_Dtile4_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_V_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_V_noearly_die[conv_index])*helpblip_intpower_ontime_V_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_5_noearly_die)-((op_Dtile4_Ctile5[conv_index]-2.98e-01-comm_time_Dtile4_Ctile5)))*mean_system_power
    # energy_lost_rc_Dtile4_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VI_noearly_die[conv_index])*helpblip_intpower_ontime_VI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_4_Ctile_10_noearly_die)-((op_Dtile4_Ctile10[conv_index]-2.98e-01-comm_time_Dtile4_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile7_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VII_noearly_die[conv_index])*helpblip_intpower_ontime_VII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_2_noearly_die)-((op_Dtile7_Ctile2[conv_index]-2.98e-01-comm_time_Dtile7_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_VIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_VIII_noearly_die[conv_index])*helpblip_intpower_ontime_VIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_5_noearly_die)-((op_Dtile7_Ctile5[conv_index]-2.98e-01-comm_time_Dtile7_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_IX_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_IX_noearly_die[conv_index])*helpblip_intpower_ontime_IX_noearly_die)-helpblip_intpower_fpgacomm_Dtile_7_Ctile_10_noearly_die)-((op_Dtile7_Ctile10[conv_index]-2.98e-01-comm_time_Dtile7_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile14_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_X_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_X_noearly_die[conv_index])*helpblip_intpower_ontime_X_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_2_noearly_die)-((op_Dtile14_Ctile2[conv_index]-2.98e-01-comm_time_Dtile14_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XI_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XI_noearly_die[conv_index])*helpblip_intpower_ontime_XI_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_5_noearly_die)-((op_Dtile14_Ctile5[conv_index]-2.98e-01-comm_time_Dtile14_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XII_noearly_die[conv_index])*helpblip_intpower_ontime_XII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_14_Ctile_10_noearly_die)-((op_Dtile14_Ctile10[conv_index]-2.98e-01-comm_time_Dtile14_Ctile10)))*mean_system_power
    # energy_lost_rc_Dtile28_Ctile2_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIII_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIII_noearly_die[conv_index])*helpblip_intpower_ontime_XIII_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_2_noearly_die)-((op_Dtile28_Ctile2[conv_index]-2.98e-01-comm_time_Dtile28_Ctile2)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XIV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XIV_noearly_die[conv_index])*helpblip_intpower_ontime_XIV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_5_noearly_die)-((op_Dtile28_Ctile5[conv_index]-2.98e-01-comm_time_Dtile28_Ctile5)))*mean_system_power
    energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die=((((helpblip_intpower_op_numcycles_XV_noearly_die[depthwise_index]+helpblip_intpower_op_numcycles_XV_noearly_die[conv_index])*helpblip_intpower_ontime_XV_noearly_die)-helpblip_intpower_fpgacomm_Dtile_28_Ctile_10_noearly_die)-((op_Dtile28_Ctile10[conv_index]-2.98e-01-comm_time_Dtile28_Ctile10)))*mean_system_power


    ############prepare data to plot ###################
   
    # energy_lost_dm=[energy_lost_dm_Dtile28_Ctile10,energy_lost_dm_Dtile28_Ctile5,energy_lost_dm_Dtile28_Ctile2,energy_lost_dm_Dtile14_Ctile10,energy_lost_dm_Dtile14_Ctile5,
    #                 energy_lost_dm_Dtile14_Ctile2,energy_lost_dm_Dtile7_Ctile10,energy_lost_dm_Dtile4_Ctile10,
    #                 energy_lost_dm_Dtile7_Ctile5,energy_lost_dm_Dtile7_Ctile2,energy_lost_dm_Dtile4_Ctile5,energy_lost_dm_Dtile4_Ctile2]
    # energy_lost_dm=np.array(energy_lost_dm)
    
    # print(f'Re DM{energy_lost_dm}')
    # print(f'sorted Re DM{-np.sort(-energy_lost_dm)}')

    energy_lost_dm=[energy_lost_dm_Dtile28_Ctile10,energy_lost_dm_Dtile14_Ctile10,energy_lost_dm_Dtile7_Ctile10,energy_lost_dm_Dtile28_Ctile5,
                    energy_lost_dm_Dtile14_Ctile5,energy_lost_dm_Dtile7_Ctile5,
                    ]
    # print(energy_lost_dm_Dtile28_Ctile10)            
    energy_lost_rc=[energy_lost_rc_Dtile28_Ctile10,energy_lost_rc_Dtile14_Ctile10,energy_lost_rc_Dtile7_Ctile10,energy_lost_rc_Dtile28_Ctile5,
                    energy_lost_rc_Dtile14_Ctile5,energy_lost_rc_Dtile7_Ctile5,
                     ]
    energy_lost_rc=np.array(energy_lost_rc)
    energy_lost_redm=[energy_lost_redm_Dtile28_Ctile10_intpower,energy_lost_redm_Dtile14_Ctile10_intpower, energy_lost_redm_Dtile7_Ctile10_intpower,energy_lost_redm_Dtile28_Ctile5_intpower,
                      energy_lost_redm_Dtile14_Ctile5_intpower,energy_lost_redm_Dtile7_Ctile5_intpower,
                       ]
    energy_lost_redm=np.array(energy_lost_redm)

    energy_lost_redm_noearly_die=[energy_lost_redm_Dtile28_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile14_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile7_Ctile10_intpower_noearly_die,energy_lost_redm_Dtile28_Ctile5_intpower_noearly_die,
                              energy_lost_redm_Dtile14_Ctile5_intpower_noearly_die,energy_lost_redm_Dtile7_Ctile5_intpower_noearly_die,
                              ]
    energy_lost_redm_noearly_die=np.array(energy_lost_redm_noearly_die)
    
    energy_lost_rc_intpower=[energy_lost_rc_Dtile28_Ctile10_intpower,energy_lost_rc_Dtile14_Ctile10_intpower,energy_lost_rc_Dtile7_Ctile10_intpower,energy_lost_rc_Dtile28_Ctile5_intpower,
                               energy_lost_rc_Dtile14_Ctile5_intpower,energy_lost_rc_Dtile7_Ctile5_intpower,
                              ]
    # print(f'bug{energy_lost_rc_intpower}')

    energy_lost_rc_intpower=np.array(energy_lost_rc_intpower)
    
    energy_lost_rc_intpower_noearly_die=[energy_lost_rc_Dtile28_Ctile10_intpower_noearly_die,energy_lost_rc_Dtile14_Ctile10_intpower_noearly_die,energy_lost_rc_Dtile7_Ctile10_intpower_noearly_die,
                              energy_lost_rc_Dtile28_Ctile5_intpower_noearly_die,energy_lost_rc_Dtile14_Ctile5_intpower_noearly_die,energy_lost_rc_Dtile7_Ctile5_intpower_noearly_die,
                                ]                
    energy_lost_rc_intpower_noearly_die=np.array(energy_lost_rc_intpower_noearly_die)
    
    energy_lost_boot=[helpblip_intpower_boot_overhead_Dtile_28_Ctile_10,helpblip_intpower_boot_overhead_Dtile_14_Ctile_10,helpblip_intpower_boot_overhead_Dtile_7_Ctile_10,
    helpblip_intpower_boot_overhead_Dtile_28_Ctile_5,helpblip_intpower_boot_overhead_Dtile_14_Ctile_5,
    helpblip_intpower_boot_overhead_Dtile_7_Ctile_5]
    energy_lost_boot=np.array(energy_lost_boot)
    # print(f'bug{energy_lost_rc_intpower}')

    # print(energy_lost_boot)
    fig,ax0=plt.subplots()

    # print(energy_lost_rc_Dtile4_Ctile2_intpower_noearly_die)

    ax0.plot(X,energy_lost_dm,color='Violet',marker='x',linewidth=2,ms=12,label='Required Redundant Data Movement')
    # ax0.plot(X,energy_lost_redm_noearly_die+energy_lost_rc_intpower_noearly_die,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data and Recomputation')
    ax0.plot(X,energy_lost_redm+energy_lost_rc_intpower,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data and Recomputation')
    # ax0.plot(X,energy_lost_redm,color='red',marker='o',linewidth=2,ms=12,label='Resent Volatile Data')
    # ax0.plot(X,energy_lost_rc_intpower,color='brown',marker='o',linewidth=2,ms=12,label='Recomputation')


    ax0.plot(X,energy_lost_boot,color='blue',marker='*',linewidth=2,ms=12,label='Boot')
    # ax0.plot(X,energy_lost_dm+energy_lost_rc+energy_lost_rc_intpower_noearly_die+energy_lost_redm_noearly_die+energy_lost_boot,color='black',marker='D',linewidth=2,ms=12,ls='-.',label='Total')
    ax0.plot(X,energy_lost_dm+energy_lost_rc+energy_lost_rc_intpower+energy_lost_redm+energy_lost_boot,color='black',marker='d',linewidth=2,ms=12,ls='-.',label='Total')

    ax0.plot(X,energy_lost_rc,color='Green',marker='v',linewidth=2,ms=12,label='Required Redundant\nComputation')
    
    # ax0.plot(X,np.sort(energy_lost_redm_noearly_die+energy_lost_rc_intpower_noearly_die),color='blue',marker='*',linewidth=2,ms=12,label='Re-data movement and Re-computation \n early die')
    
    ax0.set_xticks([0,2.5,5], labels=['Minimum', 'Medium' , 'Maximum'],fontsize=35)
    ax0.tick_params(axis='both', which='both', labelsize=12,width=2)

    ax0.grid()
    ax0.legend(loc='upper left',bbox_to_anchor=(-0.14,1.2),ncol=3,fontsize=12)
    ax0.set_yscale('log')
    ax0.set_ylabel('Energy Cost per Inference (J)',fontsize=18)
    ax0.set_xlabel('Tiling Knob',loc='center',fontsize=18,labelpad=0.1)
    ax0.tick_params(axis='x', which='major', labelsize=15,width=3)
# ,bbox_to_anchor=(1.08,1.17)

    fig.set_size_inches(8.1,5)
    plt.savefig(f'computation_schedule_50mF_rev1.02.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_ifpga_stacked_rev0_1(baseline,basline_numcycles,baseline_compute_counter,baseline_comm_counter,baseline_ontime,baseline_offtime,baseline_op_numcycles,
                           baseline_w_ctpl,baseline_w_ctpl_numcycles,baseline_w_ctpl_compute_counter,baseline_w_ctpl_comm_counter,baseline_w_ctpl_ontime,baseline_w_ctpl_offtime,baseline_w_ctpl_op_numcycles,
                           baseline_w_ctpl_II,baseline_w_ctpl_numcycles_II,baseline_w_ctpl_compute_counter_II,baseline_w_ctpl_comm_counter_II,baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II,baseline_w_ctpl_op_numcycles_II,
                           baseline_w_ctpl_III,baseline_w_ctpl_numcycles_III,baseline_w_ctpl_compute_counter_III,baseline_w_ctpl_comm_counter_III,baseline_w_ctpl_ontime_III,baseline_w_ctpl_offtime_III,baseline_w_ctpl_op_numcycles_III,
                           helpblip,helpblip_numcycles,helpblip_compute_counter,helpblip_comm_counter,helpblip_ontime,helpblip_offtime,helpblip_num_opcycles,dtile,ctile,
                           helpblip_intpower,helpblip_intpower_numcycles,helpblip_intpower_compute_counter,helpblip_intpower_commcounter,helpblip_intpower_ontime,helpblip_intpower_offtime,helpblip_intpower_op_numcycles,dtile_intpower,ctile_intpower,
                           helpblip_intpower_II,helpblip_intpower_numcycles_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II,helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,helpblip_intpower_op_numcycles_II,dtile_intpower_II,ctile_intpower_II,
                           helpblip_intpower_III,helpblip_intpower_numcycles_III,helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III,helpblip_intpower_ontime_III,helpblip_intpower_offtime_III,helpblip_intpower_op_numcycles_III,dtile_intpower_III,ctile_intpower_III):

    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=155e-03
    msp_boot_time=8.64e-03
    width=0.5
    y_min=0.001
    y_max=100
    depthwise_index=2
    conv_index=4
    baseline_mspcompute=0
    baseline_w_ctpl_mspcompute=0
    helpblip_mspcompute=0
    helpblip_intpower_mspcompute=0
    helpblip_intpower_II_mspcompute=0
    helpblip_intpower_III_mspcompute=0


    baseline=np.mean(baseline,axis=0)
    baseline_w_ctpl=np.mean(baseline_w_ctpl,axis=0)
    baseline_w_ctpl_II=np.mean(baseline_w_ctpl_II,axis=0)
    baseline_w_ctpl_III=np.mean(baseline_w_ctpl_III,axis=0)
    helpblip=np.mean(helpblip,axis=0)
    helpblip_intpower=np.mean(helpblip_intpower,axis=0)
    helpblip_intpower_II=np.mean(helpblip_intpower_II,axis=0)
    helpblip_intpower_III=np.mean(helpblip_intpower_III,axis=0)


    basline_numcycles=np.mean(basline_numcycles,axis=0)
    baseline_w_ctpl_numcycles=np.mean(baseline_w_ctpl_numcycles,axis=0)
    baseline_w_ctpl_numcycles_II=np.mean(baseline_w_ctpl_numcycles_II,axis=0)
    baseline_w_ctpl_numcycles_III=np.mean(baseline_w_ctpl_numcycles_III,axis=0)
    helpblip_numcycles=np.mean(helpblip_numcycles,axis=0)
    helpblip_intpower_numcycles=np.mean(helpblip_intpower_numcycles,axis=0)
    helpblip_intpower_numcycles_II=np.mean(helpblip_intpower_numcycles_II,axis=0)
    helpblip_intpower_numcycles_III=np.mean(helpblip_intpower_numcycles_III,axis=0)

    baseline_op_numcycles=np.mean(baseline_op_numcycles,axis=0)
    baseline_w_ctpl_op_numcycles=np.mean(baseline_w_ctpl_op_numcycles,axis=0)
    baseline_w_ctpl_op_numcycles_II=np.mean(baseline_w_ctpl_op_numcycles_II,axis=0)
    baseline_w_ctpl_op_numcycles_III=np.mean(baseline_w_ctpl_op_numcycles_III,axis=0)
    helpblip_num_opcycles=np.mean(helpblip_num_opcycles,axis=0)
    helpblip_intpower_op_numcycles=np.mean(helpblip_intpower_op_numcycles,axis=0)
    helpblip_intpower_op_numcycles_II=np.mean(helpblip_intpower_op_numcycles_II,axis=0)
    helpblip_intpower_op_numcycles_III=np.mean(helpblip_intpower_op_numcycles_III,axis=0)

    # print(f'onitme:{helpblip_intpower_ontime}')
    # print(f'msp w ctpl{np.max(baseline_w_ctpl_ontime)}\t {np.max(baseline_w_ctpl_offtime)}\t {baseline_w_ctpl_numcycles}\t {baseline_w_ctpl_op_numcycles}')
 
    baseline_ontime,baseline_offtime=np.mean(baseline_ontime),np.mean(baseline_offtime)
    baseline_w_ctpl_ontime,baseline_w_ctpl_offtime=np.mean(baseline_w_ctpl_ontime),np.mean(baseline_w_ctpl_offtime)
    baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II=np.mean(baseline_w_ctpl_ontime_II),np.mean(baseline_w_ctpl_offtime_II)
    baseline_w_ctpl_ontime_III,baseline_w_ctpl_offtime_III=np.mean(baseline_w_ctpl_ontime_III),np.mean(baseline_w_ctpl_offtime_III)
    helpblip_ontime,helpblip_offtime= np.mean(helpblip_ontime),np.mean(helpblip_offtime)
    helpblip_intpower_ontime,helpblip_intpower_offtime=np.mean(helpblip_intpower_ontime),np.mean(helpblip_intpower_offtime)
    helpblip_intpower_ontime_II,helpblip_intpower_offtime_II=np.mean(helpblip_intpower_ontime_II),np.mean(helpblip_intpower_offtime_II)
    helpblip_intpower_ontime_III,helpblip_intpower_offtime_III=np.mean(helpblip_intpower_ontime_III),np.mean(helpblip_intpower_offtime_III)


    baseline_boot_overhead=msp_boot_time
    baseline_w_ctpl_boot_overhead=baseline_w_ctpl_numcycles*msp_boot_time
    baseline_w_ctpl_boot_overhead_II=baseline_w_ctpl_numcycles_II*msp_boot_time
    baseline_w_ctpl_boot_overhead_III=baseline_w_ctpl_numcycles_III*msp_boot_time
    helpblip_boot_overhead=fpga_boot_time+msp_boot_time
    helpblip_intpower_boot_overhead=(helpblip_intpower_op_numcycles[depthwise_index]+helpblip_intpower_op_numcycles[conv_index])*fpga_boot_time
    # helpblip_intpower_boot_overhead_II=(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[conv_index])*fpga_boot_time
    helpblip_intpower_boot_overhead_II=fpga_boot_time+msp_boot_time
    helpblip_intpower_boot_overhead_III=(helpblip_intpower_op_numcycles_III[depthwise_index]+helpblip_intpower_op_numcycles_III[conv_index])*fpga_boot_time

    baseline_fpgacompute=np.mean(baseline_compute_counter)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute=np.mean(baseline_w_ctpl_compute_counter)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute_II=np.mean(baseline_w_ctpl_compute_counter_II)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute_III=np.mean(baseline_w_ctpl_compute_counter_III)*fpga_mac_cycle*fpga_clock
    helpblip_fpgacompute=np.mean(helpblip_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute=np.mean(helpblip_intpower_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute_II=np.mean(helpblip_intpower_compute_counter_II)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute_III=np.mean(helpblip_intpower_compute_counter_III)*fpga_mac_cycle*fpga_clock


    baseline_fpgacomm=np.mean(baseline_comm_counter)*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm=np.mean(baseline_w_ctpl_comm_counter)*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm_II=np.mean(baseline_w_ctpl_comm_counter_II)*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm_III=np.mean(baseline_w_ctpl_comm_counter_III)*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_fpgacomm=np.mean(helpblip_comm_counter)*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm=np.mean(helpblip_intpower_commcounter)*((7*spi_cycle_time)+(spi_cycle_time/2))
    # print(helpblip_comm_counter,helpblip_intpower_commcounter)
    helpblip_intpower_fpgacomm_II=np.mean(helpblip_intpower_commcounter_II)*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_III=np.mean(helpblip_intpower_commcounter_III)*((7*spi_cycle_time)+(spi_cycle_time/2))


    baseline_deadperiod=baseline_offtime*basline_numcycles
    baseline_w_ctpl_deadperiod=baseline_w_ctpl_offtime*baseline_w_ctpl_numcycles
    baseline_w_ctpl_deadperiod_II=baseline_w_ctpl_offtime_II*baseline_w_ctpl_numcycles_II
    baseline_w_ctpl_deadperiod_III=baseline_w_ctpl_offtime_III*baseline_w_ctpl_numcycles_III
    helpblip_deadperiod=helpblip_offtime*helpblip_numcycles
    helpblip_intpower_deadperiod=helpblip_intpower_offtime*helpblip_intpower_numcycles
    helpblip_intpower_deadperiod_II=helpblip_intpower_offtime_II*helpblip_intpower_numcycles_II
    helpblip_intpower_deadperiod_III=helpblip_intpower_offtime_III*helpblip_intpower_numcycles_III


    baseline_depthwise=baseline[depthwise_index]-baseline[depthwise_index-1]
    # baseline_w_ctpl_depthwise=(baseline_w_ctpl[depthwise_index]-baseline_w_ctpl[depthwise_index-1])-(baseline_w_ctpl_offtime*baseline_w_ctpl_op_numcycles[depthwise_index])
    # baseline_w_ctpl_depthwise_II=(baseline_w_ctpl_II[depthwise_index]-baseline_w_ctpl_II[depthwise_index-1])-(baseline_w_ctpl_offtime_II*baseline_w_ctpl_op_numcycles_II[depthwise_index])
    # baseline_w_ctpl_depthwise_III=(baseline_w_ctpl_III[depthwise_index]-baseline_w_ctpl_III[depthwise_index-1])-(baseline_w_ctpl_offtime_III*baseline_w_ctpl_op_numcycles_III[depthwise_index])

    baseline_w_ctpl_depthwise=baseline_w_ctpl_ontime*baseline_w_ctpl_op_numcycles[depthwise_index]

    baseline_w_ctpl_depthwise_II=baseline_w_ctpl_II[depthwise_index]-baseline_w_ctpl_II[depthwise_index-1]
    baseline_w_ctpl_depthwise_III=baseline_w_ctpl_ontime_III*baseline_w_ctpl_op_numcycles_III[depthwise_index]

    # print(helpblip_intpower_op_numcycles)
    helpblip_depthwise=helpblip[depthwise_index]-helpblip[depthwise_index-1]
    helpblip_intpower_depthwise=helpblip_intpower_ontime*helpblip_intpower_op_numcycles[depthwise_index]
    
    # helpblip_intpower_depthwise_II=helpblip_intpower_ontime_II*helpblip_intpower_op_numcycles_II[depthwise_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_II[depthwise_index])
    helpblip_intpower_depthwise_II=helpblip_intpower_II[depthwise_index]-helpblip_intpower_II[depthwise_index-1]
    # helpblip_intpower_depthwise_III=helpblip_intpower_ontime_III*helpblip_intpower_op_numcycles_III[depthwise_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_III[depthwise_index])
    helpblip_intpower_depthwise_III=helpblip_intpower_III[depthwise_index]-helpblip_intpower_III[depthwise_index-1]

    baseline_conv=baseline[conv_index]-baseline[conv_index-1]
    baseline_w_ctpl_conv=baseline_w_ctpl_ontime*baseline_w_ctpl_op_numcycles[conv_index]
    baseline_w_ctpl_conv_II=baseline_w_ctpl_II[conv_index]-baseline_w_ctpl_II[conv_index-1]
    baseline_w_ctpl_conv_III=baseline_w_ctpl_ontime_III*baseline_w_ctpl_op_numcycles_III[conv_index]

    # baseline_w_ctpl_conv=(baseline_w_ctpl[conv_index]-baseline_w_ctpl[conv_index-1])-(baseline_w_ctpl_offtime*baseline_w_ctpl_op_numcycles[conv_index])
    # baseline_w_ctpl_conv_II=(baseline_w_ctpl_II[conv_index]-baseline_w_ctpl_II[conv_index-1])-(baseline_w_ctpl_offtime_II*baseline_w_ctpl_op_numcycles_II[conv_index])
    # baseline_w_ctpl_conv_III=(baseline_w_ctpl_III[conv_index]-baseline_w_ctpl_III[conv_index-1])-(baseline_w_ctpl_offtime_III*baseline_w_ctpl_op_numcycles_III[conv_index])
    helpblip_conv=helpblip[conv_index]-helpblip[conv_index-1]
    helpblip_intpower_conv=helpblip_intpower_ontime*helpblip_intpower_op_numcycles[conv_index]
    # helpblip_intpower_conv_II=helpblip_intpower_ontime_II*helpblip_intpower_op_numcycles_II[conv_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_II[conv_index])
    helpblip_intpower_conv_II=helpblip_intpower_II[conv_index]-helpblip_intpower_II[conv_index-1]
    helpblip_intpower_conv_III=helpblip_intpower_III[conv_index]-helpblip_intpower_III[conv_index-1]

    # print(helpblip_intpower_conv_II)
    # helpblip_intpower_conv_III=helpblip_intpower_ontime_III*helpblip_intpower_op_numcycles_III[conv_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_III[conv_index])

    # helpblip_intpower_conv=(helpblip_intpower[conv_index]-helpblip_intpower[conv_index-1])-(helpblip_intpower_offtime*helpblip_intpower_op_numcycles[conv_index])
    # helpblip_intpower_conv_II=(helpblip_intpower_II[conv_index]-helpblip_intpower_II[conv_index-1])-(helpblip_intpower_offtime_II*helpblip_intpower_op_numcycles_II[conv_index])
    # helpblip_intpower_conv_III=(helpblip_intpower_III[conv_index]-helpblip_intpower_III[conv_index-1])-(helpblip_intpower_offtime_III*helpblip_intpower_op_numcycles_III[conv_index])
    # print(helpblip_intpower_op_numcycles,baseline_w_ctpl_op_numcycles)
    # print(f'depthwise+conv:{baseline_depthwise,helpblip_depthwise,helpblip_intpower_depthwise_II,helpblip_intpower_depthwise,baseline_conv,helpblip_conv,helpblip_intpower_conv_II,helpblip_intpower_conv}')

    baseline_interface_overhead = (baseline_depthwise+baseline_conv)-(baseline_depthwise+baseline_conv)
    baseline_w_ctpl_interface_overhead = (baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)-(baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)

    helpblip_interface_overhead = (helpblip_depthwise+helpblip_conv)-(helpblip_fpgacomm)
    
    helpblip_intpower_interface_overhead = (helpblip_intpower_depthwise+helpblip_intpower_conv)-(helpblip_intpower_fpgacomm)
    helpblip_intpower_interface_overhead_II = (helpblip_intpower_depthwise_II+helpblip_intpower_conv_II)-(helpblip_intpower_fpgacomm_II)


    baseline_total =baseline[len(baseline)-1]   
    baseline_w_ctpl_total= baseline_w_ctpl[len(baseline_w_ctpl)-1]
    baseline_w_ctpl_total_II= baseline_w_ctpl_II[len(baseline_w_ctpl_II)-1]
    baseline_w_ctpl_total_III= baseline_w_ctpl_III[len(baseline_w_ctpl_III)-1]
    helpblip_total= helpblip[len(helpblip)-1]
    helpblip_intpower_total=helpblip_intpower[len(helpblip_intpower)-1]
    helpblip_intpower_total_II=helpblip_intpower_II[len(helpblip_intpower_II)-1]
    helpblip_intpower_total_III=helpblip_intpower_III[len(helpblip_intpower_III)-1]

    baseline_mspcompute =baseline[len(baseline)-1]
    baseline_w_ctpl_mspcompute= baseline_w_ctpl[len(baseline_w_ctpl)-1]

    baseline_w_ctpl_mspcompute_II= baseline_w_ctpl_II[len(baseline_w_ctpl_II)-1]
    baseline_w_ctpl_mspcompute_III= baseline_w_ctpl_III[len(baseline_w_ctpl_III)-1]
    helpblip_mspcompute= helpblip[len(helpblip)-1]
    helpblip_intpower_mspcompute=helpblip_intpower[len(helpblip_intpower)-1]
    # print(helpblip_intpower_mspcompute,helpblip_intpower_boot_overhead,helpblip_intpower_deadperiod)
    helpblip_intpower_mspcompute_II=helpblip_intpower_II[len(helpblip_intpower_II)-1]
    helpblip_intpower_mspcompute_III=helpblip_intpower_III[len(helpblip_intpower_III)-1]

    baseline_mspcompute=baseline_mspcompute-baseline_boot_overhead-baseline_deadperiod-baseline_depthwise-baseline_conv
    
    baseline_w_ctpl_mspcompute=baseline_w_ctpl[len(baseline_w_ctpl)-1] - baseline_w_ctpl[conv_index+1] - (baseline_w_ctpl_op_numcycles[5]+baseline_w_ctpl_op_numcycles[6]+baseline_w_ctpl_op_numcycles[7]+baseline_w_ctpl_op_numcycles[8]+baseline_w_ctpl_op_numcycles[9])*baseline_w_ctpl_offtime

    baseline_w_ctpl_mspcompute_II=baseline_w_ctpl_mspcompute_II-baseline_w_ctpl_boot_overhead_II-baseline_w_ctpl_deadperiod_II-baseline_w_ctpl_depthwise_II-baseline_w_ctpl_conv_II
    # baseline_w_ctpl_mspcompute_III=baseline_w_ctpl_mspcompute_III-baseline_w_ctpl_boot_overhead_III-baseline_w_ctpl_deadperiod_III-baseline_w_ctpl_depthwise_III-baseline_w_ctpl_conv_III
    # print(baseline_w_ctpl_boot_overhead_II,baseline_w_ctpl_deadperiod_II,baseline_w_ctpl_depthwise_II,baseline_w_ctpl_conv_II)
    helpblip_mspcompute=helpblip_mspcompute-helpblip_boot_overhead-helpblip_deadperiod-helpblip_depthwise-helpblip_conv
    helpblip_intpower_mspcompute=helpblip_intpower[len(baseline_w_ctpl)-1] - helpblip_intpower[conv_index+1] - (helpblip_intpower_op_numcycles[5]+helpblip_intpower_op_numcycles[6]+helpblip_intpower_op_numcycles[7]+helpblip_intpower_op_numcycles[8]+helpblip_intpower_op_numcycles[9])*helpblip_intpower_offtime
    helpblip_intpower_mspcompute_II=helpblip_intpower_mspcompute_II-helpblip_intpower_deadperiod_II-helpblip_intpower_depthwise_II-helpblip_intpower_conv_II
    
    helpblip_intpower_mspcompute_III=helpblip_intpower_mspcompute_III-helpblip_intpower_deadperiod_III-helpblip_intpower_depthwise_III-helpblip_intpower_conv_III
    

######system analysis for potential improvement#######

    cap_size =50e-03
    Vmax=1.25
    Vmin=1.02
    cap_energy_one_cycle=0.5*cap_size*(Vmax**2-Vmin**2) ## cap energy per cycle
    # print(f'power{cap_energy_one_cycle/baseline_w_ctpl_ontime}')
    cap_energy_one_cycle_bobber=cap_energy_one_cycle-(fpga_boot_time*5.93e-03)
    cap_energy_one_cycle_msp=cap_energy_one_cycle-(msp_boot_time*2.68e-03)
    
    # print(cap_energy_one_cycle_bobber/(fpga_boot_time*8.31e-02))
    reg_efficiency=0.85
    ldo_quiescent_current=330e-06
    bobber_1v5_Vout=1.5
    bobber_1v5_In=3.3

    # print("TBD")
    msp_wctpl_depth_conv_dynamic_power_w=4.66e-03
    msp_wctpl_other_compute_dynamic_power_w=4.36e-03
    msp_wctpl_static_power_w=8.63e-04

    bobber_fpga_dynamic_power_w=7.05e-03+7.47e-04
    bobber_fpga_static_power_w=4.05e-05+5.28e-06
    bobber_fpga_static_current=2.70e-05+0.0000016
    bobber_fpga_dynamic_current=4.7e-03+0.0002265

    bobber_msp_depth_conv_dynamic_power=7.75e-03
    bobber_dynamic_power_other_compute_total_w= 4.72e-03
    bobber_msp_depth_conv_spi_w=bobber_msp_depth_conv_dynamic_power-bobber_dynamic_power_other_compute_total_w
    bobber_1v5_reg_static=bobber_fpga_static_power_w/((bobber_fpga_static_power_w)/(bobber_1v5_In*(bobber_fpga_static_current+ldo_quiescent_current)))
    bobber_1v5_reg_dynamic=bobber_fpga_dynamic_power_w/((bobber_fpga_dynamic_power_w)/(bobber_1v5_In*(bobber_fpga_dynamic_current+ldo_quiescent_current)))
    bobber_dynamic_power_depth_conv_total_w=3.23e-02
    bobber_oscillator_dynamic_power=3.23e-02-2.89e-02
    bobber_1v5_reg_dynamic=bobber_1v5_reg_dynamic+(bobber_dynamic_power_depth_conv_total_w-(bobber_1v5_reg_dynamic+bobber_msp_depth_conv_spi_w+msp_wctpl_other_compute_dynamic_power_w+bobber_fpga_dynamic_power_w+bobber_oscillator_dynamic_power))
    


    bobber_potential_msp_depth_conv_dynamic_power=7.75e-03
    bobber_potential_msp_depth_conv_spi_w=bobber_potential_msp_depth_conv_dynamic_power-bobber_dynamic_power_other_compute_total_w
    bobber_potential_fpga_dynamic_power_w= 5.28e-03+7.47e-04
    bobber_potential_fpga_static_power_w= 1.53E-05+5.28e-06
    bobber_potential_oscillator_dynamic_power= 1e-03
    bobber_potential_1v5reg_input=bobber_fpga_dynamic_power_w/reg_efficiency
    bobber_potential_1v5reg=bobber_potential_1v5reg_input-bobber_fpga_dynamic_power_w
    bobber_potential_other=5.51e-04
    bobber_potential_total=bobber_potential_msp_depth_conv_dynamic_power+bobber_potential_fpga_dynamic_power_w+bobber_potential_oscillator_dynamic_power+bobber_potential_1v5reg
    # print(0.75*(cap_energy_one_cycle_bobber/bobber_dynamic_power_depth_conv_total_w))
    # print((cap_energy_one_cycle/msp_wctpl_depth_conv_dynamic_power_w))
    msp_runtime_energy=msp_wctpl_other_compute_dynamic_power_w*(baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)
    # Bobber_pot_energy_depwthwise_conv=(helpblip_intpower_depthwise_II+helpblip_intpower_conv_II)*(bobber_potential_msp_depth_conv_dynamic_power+bobber_potential_msp_depth_conv_spi_w+bobber_potential_fpga_dynamic_power_w+bobber_potential_oscillator_dynamic_power+bobber_potential_1v5reg+bobber_potential_other)
    Bobber_pot_energy_depwthwise_conv=(helpblip_intpower_depthwise_III+helpblip_intpower_conv_III)*(bobber_dynamic_power_other_compute_total_w+bobber_potential_msp_depth_conv_spi_w+bobber_potential_fpga_dynamic_power_w+bobber_potential_oscillator_dynamic_power+bobber_potential_1v5reg)
    Bobber_pot_charge_cycles=Bobber_pot_energy_depwthwise_conv/cap_energy_one_cycle_bobber
    Bobber_pot_boot_overhead=Bobber_pot_charge_cycles*fpga_boot_time
    Bobber_pot_recharge_time=helpblip_intpower_offtime*Bobber_pot_charge_cycles

    Bobber_pot_ontime=0.75*(cap_energy_one_cycle_bobber/bobber_potential_total)

    Bobber_pot_fpga_comm=helpblip_intpower_fpgacomm_II    
    Bobber_pot_depth_conv_compute=Bobber_pot_charge_cycles*Bobber_pot_ontime

    # print(f'Bobber pot cycles {Bobber_pot_energy_depwthwise_conv/cap_energy_one_cycle_bobber}, time{helpblip_intpower_depthwise_III+helpblip_intpower_conv_III},{msp_runtime_energy/cap_energy_one_cycle_msp},time{baseline_w_ctpl_depthwise+baseline_w_ctpl_conv} ')
    Bobber_pot_inf_time_other_compute=helpblip_intpower_mspcompute_II
    # print(bobber_potential_total,0.75*(cap_energy_one_cycle_bobber/bobber_potential_total))


# - (baseline_w_ctpl_op_numcycles[5]+baseline_w_ctpl_op_numcycles[6]+baseline_w_ctpl_op_numcycles[7]+baseline_w_ctpl_op_numcycles[8]+baseline_w_ctpl_op_numcycles[9])*baseline_w_ctpl_offtime
    # print(f'MSP430{baseline_mspcompute}{baseline}')
    # print(f'MSP430 w ctpl{baseline_w_ctpl_mspcompute_II}{baseline_w_ctpl_II}')
    
    # print(f'Base accel{helpblip_mspcompute}{helpblip}')
    # print(f'Bobber{helpblip_intpower_mspcompute_II}{helpblip_intpower_II}')
    # print(f'MSP430 w ctpl int power{baseline_w_ctpl_mspcompute}{baseline_w_ctpl}')

    # print(f'Bobber int{helpblip_intpower_boot_overhead} {helpblip_intpower[9]}')
    
    
    
    # print(baseline_interface_overhead)
    fig, (ax1,ax2)= plt.subplots(1,2,gridspec_kw={'width_ratios':[1,1],'height_ratios': [0.1]})
    rects1 = ax1.bar(0,baseline_boot_overhead,width,color='mediumorchid',hatch="//",label='Boot Overhead')
    
    rects3 = ax1.bar(0,baseline_fpgacomm,width,bottom=baseline_boot_overhead,color='lightblue', hatch="..",label='Depthwise + Conv Data Movement')

    rects2 = ax1.bar(0,(baseline_depthwise+baseline_conv)-baseline_fpgacomm,width,bottom=baseline_boot_overhead+baseline_fpgacomm,color='lightcoral',hatch="**",label='Depthwise + Conv Compute')
    
    rects4 = ax1.bar(0,baseline_mspcompute,width,bottom=(baseline_depthwise+baseline_conv)-baseline_fpgacomm+baseline_fpgacomm+baseline_boot_overhead,color='lightgreen',hatch="\\",label='Other Compute')
    
    rects5 = ax1.bar(0,baseline_deadperiod,width,bottom=(baseline_depthwise+baseline_conv)-baseline_fpgacomm+baseline_fpgacomm+baseline_boot_overhead+baseline_mspcompute,color='lightslategrey',hatch="oo",label='Recharging')
    
    # ax1.bar(0,0,color='cyan',hatch="--",label='Overall')

    rects1 = ax1.bar(1,baseline_w_ctpl_boot_overhead_II,width,color='mediumorchid',hatch="//")
    
    rects3 = ax1.bar(1,baseline_w_ctpl_fpgacomm_II,width,bottom=baseline_w_ctpl_boot_overhead_II,color='lightblue', hatch="..")

    rects2 = ax1.bar(1,(baseline_w_ctpl_depthwise_II+baseline_w_ctpl_conv_II)-baseline_w_ctpl_fpgacomm_II,width,bottom=baseline_w_ctpl_boot_overhead_II+baseline_w_ctpl_fpgacomm_II,color='lightcoral',hatch="**")
    
    rects4 = ax1.bar(1,baseline_w_ctpl_mspcompute_II,width,bottom=(baseline_w_ctpl_depthwise_II+baseline_w_ctpl_conv_II)-baseline_w_ctpl_fpgacomm_II+baseline_w_ctpl_fpgacomm_II+baseline_w_ctpl_boot_overhead_II,color='lightgreen',hatch="\\")
    
    rects5 = ax1.bar(1,baseline_w_ctpl_deadperiod_II,width,bottom=(baseline_w_ctpl_depthwise_II+baseline_w_ctpl_conv_II)-baseline_w_ctpl_fpgacomm_II+baseline_w_ctpl_fpgacomm_II+baseline_w_ctpl_boot_overhead_II+baseline_w_ctpl_mspcompute_II,color='lightslategrey',hatch="oo")



    rects1 = ax1.bar(2,helpblip_boot_overhead,width,color='mediumorchid',hatch="//")

    rects3 = ax1.bar(2,helpblip_fpgacomm,width,bottom=helpblip_boot_overhead,color='lightblue', hatch="..")

    rects2 = ax1.bar(2,helpblip_interface_overhead,width,bottom=helpblip_boot_overhead+helpblip_fpgacomm,color='lightcoral',hatch="**")
            
    rects4 = ax1.bar(2,helpblip_mspcompute,width,bottom=helpblip_boot_overhead+helpblip_fpgacomm+helpblip_interface_overhead,color='lightgreen',hatch="\\")
    
    rects5 = ax1.bar(2,helpblip_deadperiod,width,bottom=helpblip_boot_overhead+helpblip_fpgacomm+helpblip_interface_overhead+helpblip_mspcompute,color='lightslategrey',hatch="oo")


    rects1 = ax1.bar(3,helpblip_intpower_boot_overhead_II,width,color='mediumorchid',hatch="//")

    rects3 = ax1.bar(3,helpblip_intpower_fpgacomm_II,width,bottom=helpblip_intpower_boot_overhead_II,color='lightblue', hatch="..")

    rects2 = ax1.bar(3,helpblip_intpower_interface_overhead_II,width,bottom=helpblip_intpower_boot_overhead_II+helpblip_intpower_fpgacomm_II,color='lightcoral',hatch="**")
            
    rects4 = ax1.bar(3,helpblip_intpower_mspcompute_II,width,bottom=helpblip_intpower_boot_overhead_II+helpblip_intpower_fpgacomm_II+helpblip_intpower_interface_overhead_II,color='lightgreen',hatch="\\")
    
    rects5 = ax1.bar(3,helpblip_intpower_deadperiod_II,width,bottom=helpblip_intpower_boot_overhead_II+helpblip_intpower_fpgacomm_II+helpblip_intpower_interface_overhead_II+helpblip_intpower_mspcompute_II,color='lightslategrey',hatch="oo")

    # print(baseline[9]/helpblip_intpower_II[9])

    # print(baseline,helpblip_intpower_II)
    # print("\n",helpblip_fpgacomm,helpblip_intpower_fpgacomm_II,helpblip_interface_overhead,helpblip_intpower_interface_overhead_II)

    ax1.set_ylabel('Time (s)',fontsize=30)
    ax1.set_xticks([0, 1 , 2, 3], ['MSP430','MSP430\nw-CTPL','Baseline\nAccel', 'BOBBER'],fontsize=12)
    ax1.plot([],[],'X',color='red',ms=15,label='Cannot Complete')

    ax1.text(1,-6.3,'(a) Continuous power',horizontalalignment='center', color='black',fontsize=15)
    
    ax1.legend(loc='upper left', bbox_to_anchor=(-0.345, 1.16),ncol=3,fontsize=13)
    ax1.set_xlim(-0.5,3.5)
    ax1.grid()
    ax1.tick_params(axis='both', which='both', labelsize=12,width=3)
    # ax1.minorticks_on()
    # ax1.set_ylim(0,50)
    # ax1.set_yscale('log')
    ax2.plot(0,10.5,'X',color='red',ms=25)

    ax2.plot(1,10.5,'X',color='red',ms=25)

    rects1 = ax2.bar(2,baseline_w_ctpl_boot_overhead,width,color='mediumorchid',hatch="//",label='Boot Overhead')
    
    rects3 = ax2.bar(2,baseline_w_ctpl_fpgacomm,width,bottom=baseline_w_ctpl_boot_overhead,color='lightblue', hatch="..",label='Depthwise + Conv Data Movement')

    rects2 = ax2.bar(2,(baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)-baseline_w_ctpl_fpgacomm,width,bottom=baseline_w_ctpl_boot_overhead+baseline_w_ctpl_fpgacomm,color='lightcoral',hatch="**",label='Depthwise + Conv Compute')
    
    rects4 = ax2.bar(2,baseline_w_ctpl_mspcompute,width,bottom=(baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)-baseline_w_ctpl_fpgacomm+baseline_w_ctpl_fpgacomm+baseline_w_ctpl_boot_overhead,color='lightgreen',hatch="\\",label='Other Compute')
    
    rects5 = ax2.bar(2,baseline_w_ctpl_deadperiod,width,bottom=(baseline_w_ctpl_depthwise+baseline_w_ctpl_conv)-baseline_w_ctpl_fpgacomm+baseline_w_ctpl_fpgacomm+baseline_w_ctpl_boot_overhead+baseline_w_ctpl_mspcompute,color='lightslategrey',hatch="oo",label='Recharging')

    helpblip_intpower_deadperiod=helpblip_intpower[9]-[helpblip_intpower_boot_overhead+helpblip_intpower_fpgacomm+helpblip_intpower_interface_overhead+helpblip_intpower_mspcompute]
    rects1 = ax2.bar(3,helpblip_intpower_boot_overhead,width,color='mediumorchid',hatch="//")

    rects3 = ax2.bar(3,helpblip_intpower_fpgacomm,width,bottom=helpblip_intpower_boot_overhead,color='lightblue', hatch="..")

    rects2 = ax2.bar(3,helpblip_intpower_interface_overhead,width,bottom=helpblip_intpower_boot_overhead+helpblip_intpower_fpgacomm,color='lightcoral',hatch="**")
            
    rects4 = ax2.bar(3,helpblip_intpower_mspcompute,width,bottom=helpblip_intpower_boot_overhead+helpblip_intpower_fpgacomm+helpblip_intpower_interface_overhead,color='lightgreen',hatch="\\")
    
    rects5 = ax2.bar(3,helpblip_intpower_deadperiod,width,bottom=helpblip_intpower_boot_overhead+helpblip_intpower_fpgacomm+helpblip_intpower_interface_overhead+helpblip_intpower_mspcompute,color='lightslategrey',hatch="oo")


    # rects1 = ax2.bar(4,Bobber_pot_boot_overhead,width,color='mediumorchid',hatch="//")

    # rects3 = ax2.bar(4,Bobber_pot_fpga_comm,width,bottom=Bobber_pot_boot_overhead,color='lightblue', hatch="..")

    # rects2 = ax2.bar(4,Bobber_pot_depth_conv_compute,width,bottom=Bobber_pot_boot_overhead+Bobber_pot_fpga_comm,color='lightcoral',hatch="**")
            
    # rects4 = ax2.bar(4,helpblip_intpower_mspcompute,width,bottom=Bobber_pot_boot_overhead+Bobber_pot_fpga_comm+Bobber_pot_depth_conv_compute,color='lightgreen',hatch="\\")
    
    # rects5 = ax2.bar(4,Bobber_pot_recharge_time,width,bottom=Bobber_pot_boot_overhead+Bobber_pot_fpga_comm+Bobber_pot_depth_conv_compute+helpblip_intpower_mspcompute,color='lightslategrey',hatch="oo")
    # print((helpblip_intpower_deadperiod))
    # print()
    # print("\n",helpblip_fpgacomm,helpblip_intpower_fpgacomm_II,helpblip_intpower_fpgacomm,helpblip_interface_overhead,helpblip_intpower_interface_overhead_II,helpblip_intpower_interface_overhead)

    # ax2.bar(1,helpblip_intpower_boot_overhead,width,color='mediumorchid',hatch="//")

    # ax2.bar(1.5,helpblip_intpower_fpgacomm,width,color='lightblue', hatch="..")

    # ax2.bar(2,helpblip_intpower_interface_overhead,width,color='lightcoral',hatch="**")
            
    # ax2.bar(2.5,helpblip_intpower_mspcompute,width,color='lightgreen',hatch="\\")
    
    # ax2.bar(3,helpblip_intpower_deadperiod,width,color='lightslategrey',hatch="oo")

    # ax2.bar(3.5,helpblip_intpower_total,width,color='cyan',hatch="--",)

    # print(helpblip_intpower_boot_overhead,helpblip_intpower_fpgacomm,helpblip_intpower_interface_overhead,helpblip_intpower_mspcompute,helpblip_intpower_deadperiod,helpblip_intpower_total)
    ax2.set_xticks([0, 1 , 2 , 3], ['MSP430','Baseline\nAccel','MSP430\nw-CTPL', 'BOBBER'],fontsize=12)
    ax2.text(1.5,-80.6,'(b) Intermittent power',horizontalalignment='center', color='black',fontsize=15)
    ax2.tick_params(axis='both', which='both', labelsize=12,width=3)
    ax2.grid()
    # ax2.set_yscale('log')
    ax2.set_xlim(-0.5,3.5)
    # ax2.minorticks_on()

# #####################################################################

    # ax3.plot(0,3,'X',color='red',ms=15)

    # rects1 = ax3.bar(1,baseline_w_ctpl_boot_overhead_II,width,color='mediumorchid',hatch="//")
    # rects2 = ax3.bar(1.5,baseline_w_ctpl_depthwise_II,width,color='lightcoral',hatch="**")
    # rects3 = ax3.bar(2,baseline_w_ctpl_conv_II,width,color='lightblue', hatch="..")
    # rects4 = ax3.bar(2.5,baseline_w_ctpl_mspcompute_II,width,color='lightgreen',hatch="\\")
    # rects5 = ax3.bar(3,baseline_w_ctpl_deadperiod_II,width,color='lightslategrey',hatch="oo")
    # rects6 = ax3.bar(3.5,baseline_w_ctpl_total_II,width,color='cyan',hatch="--")

    # ax3.plot(4.5,3,'X',color='red',ms=15)

    # rects9 = ax3.bar(5.5,helpblip_intpower_boot_overhead_II,width,color='mediumorchid',hatch="//")
    # rects10 = ax3.bar(6,helpblip_intpower_depthwise_II,width,color='lightcoral',hatch="**")
    # rects11 = ax3.bar(6.5,helpblip_intpower_conv_II,width,color='lightblue', hatch="..")
    # rects12 = ax3.bar(7,helpblip_intpower_mspcompute_II,width,color='lightgreen',hatch="\\")
    # rects13 = ax3.bar(7.5,helpblip_intpower_deadperiod_II,width,color='lightslategrey',hatch="oo")
    # rects13 = ax3.bar(8,helpblip_intpower_total_II,width,color='cyan',hatch="--")

    # ax3.plot(9,3,'X',color='red',ms=15)

    # rects1 = ax3.bar(10,baseline_w_ctpl_boot_overhead_III,width,color='mediumorchid',hatch="//")
    # rects2 = ax3.bar(10.5,baseline_w_ctpl_depthwise_III,width,color='lightcoral',hatch="**")
    # rects3 = ax3.bar(11,baseline_w_ctpl_conv_III,width,color='lightblue', hatch="..")
    # rects4 = ax3.bar(11.5,baseline_w_ctpl_mspcompute_III,width,color='lightgreen',hatch="\\")
    # rects5 = ax3.bar(12,baseline_w_ctpl_deadperiod_III,width,color='lightslategrey',hatch="oo")
    # rects5 = ax3.bar(12.5,baseline_w_ctpl_total_III,width,color='cyan',hatch="--")

    # ax3.plot(13.5,3,'X',color='red',ms=15)

    # rects9 = ax3.bar(14.5,helpblip_intpower_boot_overhead_III,width,color='mediumorchid',hatch="//")
    # rects10 = ax3.bar(15,helpblip_intpower_depthwise_III,width,color='lightcoral',hatch="**")
    # rects11 = ax3.bar(15.5,helpblip_intpower_conv_III,width,color='lightblue', hatch="..")
    # rects12 = ax3.bar(16,helpblip_intpower_mspcompute_III,width,color='lightgreen',hatch="\\")
    # rects13 = ax3.bar(16.5,helpblip_intpower_deadperiod_III,width,color='lightslategrey',hatch="oo")
    # rects13 = ax3.bar(17,helpblip_intpower_total_III,width,color='cyan',hatch="--")

    # ax3.set_xticks([0, 2.25, 4.5, 6.75, 9, 11.25, 13.5, 15.75], ['Baseline', 'Baseline\n w/ctpl','HELP-BLIP \n w/ctpl','HELP-BLIP','Baseline', 'Baseline\n w/ctpl','HELP-BLIP \n w/ctpl','HELP-BLIP'],fontsize=15)
    # ax3.text(3,-25.5,'c) Intermittent power 144mF',horizontalalignment='center', color='black',fontsize=18)
    # ax3.text(11.5,-25.5,'d) Intermittent power 344mF',horizontalalignment='center', color='black',fontsize=18)
    # ax3.set_xlim(-1,18)
    # ax3.tick_params(axis='both', which='both', labelsize=13,width=3)
    # ax3.grid()
    # ax3.minorticks_on()

    fig.set_size_inches(8.8,5.9)
    plt.savefig(f'latency_ifpga_rev1.2_rebuttal.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_ifpga_bar_stacked(baseline,basline_numcycles,baseline_compute_counter,baseline_comm_counter,baseline_ontime,baseline_offtime,baseline_op_numcycles,
                           baseline_w_ctpl,baseline_w_ctpl_numcycles,baseline_w_ctpl_compute_counter,baseline_w_ctpl_comm_counter,baseline_w_ctpl_ontime,baseline_w_ctpl_offtime,baseline_w_ctpl_op_numcycles,
                           baseline_w_ctpl_II,baseline_w_ctpl_numcycles_II,baseline_w_ctpl_compute_counter_II,baseline_w_ctpl_comm_counter_II,baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II,baseline_w_ctpl_op_numcycles_II,
                           baseline_w_ctpl_III,baseline_w_ctpl_numcycles_III,baseline_w_ctpl_compute_counter_III,baseline_w_ctpl_comm_counter_III,baseline_w_ctpl_ontime_III,baseline_w_ctpl_offtime_III,baseline_w_ctpl_op_numcycles_III,
                           helpblip,helpblip_numcycles,helpblip_compute_counter,helpblip_comm_counter,helpblip_ontime,helpblip_offtime,helpblip_num_opcycles,dtile,ctile,
                           helpblip_intpower,helpblip_intpower_numcycles,helpblip_intpower_compute_counter,helpblip_intpower_commcounter,helpblip_intpower_ontime,helpblip_intpower_offtime,helpblip_intpower_op_numcycles,dtile_intpower,ctile_intpower,
                           helpblip_intpower_II,helpblip_intpower_numcycles_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II,helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,helpblip_intpower_op_numcycles_II,dtile_intpower_II,ctile_intpower_II,
                           helpblip_intpower_III,helpblip_intpower_numcycles_III,helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III,helpblip_intpower_ontime_III,helpblip_intpower_offtime_III,helpblip_intpower_op_numcycles_III,dtile_intpower_III,ctile_intpower_III): 
    
    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=127.96e-03
    msp_boot_time=5e-03
    width=0.5
    y_min=0.001
    y_max=100
    depthwise_index=2
    conv_index=4
    baseline_mspcompute=0
    baseline_w_ctpl_mspcompute=0
    helpblip_mspcompute=0
    helpblip_intpower_mspcompute=0
    helpblip_intpower_II_mspcompute=0
    helpblip_intpower_III_mspcompute=0


    baseline=np.mean(baseline,axis=0)
    baseline_w_ctpl=np.mean(baseline_w_ctpl,axis=0)
    baseline_w_ctpl_II=np.mean(baseline_w_ctpl_II,axis=0)
    baseline_w_ctpl_III=np.mean(baseline_w_ctpl_III,axis=0)
    helpblip=np.mean(helpblip,axis=0)
    helpblip_intpower=np.mean(helpblip_intpower,axis=0)
    helpblip_intpower_II=np.mean(helpblip_intpower_II,axis=0)
    helpblip_intpower_III=np.mean(helpblip_intpower_III,axis=0)


    basline_numcycles=np.mean(basline_numcycles,axis=0)
    baseline_w_ctpl_numcycles=np.mean(baseline_w_ctpl_numcycles,axis=0)
    baseline_w_ctpl_numcycles_II=np.mean(baseline_w_ctpl_numcycles_II,axis=0)
    baseline_w_ctpl_numcycles_III=np.mean(baseline_w_ctpl_numcycles_III,axis=0)
    helpblip_numcycles=np.mean(helpblip_numcycles,axis=0)
    helpblip_intpower_numcycles=np.mean(helpblip_intpower_numcycles,axis=0)
    helpblip_intpower_numcycles_II=np.mean(helpblip_intpower_numcycles_II,axis=0)
    helpblip_intpower_numcycles_III=np.mean(helpblip_intpower_numcycles_III,axis=0)

    baseline_op_numcycles=np.mean(baseline_op_numcycles,axis=0)
    baseline_w_ctpl_op_numcycles=np.mean(baseline_w_ctpl_op_numcycles,axis=0)
    baseline_w_ctpl_op_numcycles_II=np.mean(baseline_w_ctpl_op_numcycles_II,axis=0)
    baseline_w_ctpl_op_numcycles_III=np.mean(baseline_w_ctpl_op_numcycles_III,axis=0)
    helpblip_num_opcycles=np.mean(helpblip_num_opcycles,axis=0)
    helpblip_intpower_op_numcycles=np.mean(helpblip_intpower_op_numcycles,axis=0)
    helpblip_intpower_op_numcycles_II=np.mean(helpblip_intpower_op_numcycles_II,axis=0)
    helpblip_intpower_op_numcycles_III=np.mean(helpblip_intpower_op_numcycles_III,axis=0)


    baseline_ontime,baseline_offtime=np.mean(baseline_ontime),np.mean(baseline_offtime)
    baseline_w_ctpl_ontime,baseline_w_ctpl_offtime=np.mean(baseline_w_ctpl_ontime),np.mean(baseline_w_ctpl_offtime)
    baseline_w_ctpl_ontime_II,baseline_w_ctpl_offtime_II=np.mean(baseline_w_ctpl_ontime_II),np.mean(baseline_w_ctpl_offtime_II)
    baseline_w_ctpl_ontime_III,baseline_w_ctpl_offtime_III=np.mean(baseline_w_ctpl_ontime_III),np.mean(baseline_w_ctpl_offtime_III)
    helpblip_ontime,helpblip_offtime= np.mean(helpblip_ontime),np.mean(helpblip_offtime)
    helpblip_intpower_ontime,helpblip_intpower_offtime=np.mean(helpblip_intpower_ontime),np.mean(helpblip_intpower_offtime)
    helpblip_intpower_ontime_II,helpblip_intpower_offtime_II=np.mean(helpblip_intpower_ontime_II),np.mean(helpblip_intpower_offtime_II)
    helpblip_intpower_ontime_III,helpblip_intpower_offtime_III=np.mean(helpblip_intpower_ontime_III),np.mean(helpblip_intpower_offtime_III)


    baseline_boot_overhead=msp_boot_time
    baseline_w_ctpl_boot_overhead=baseline_w_ctpl_numcycles*msp_boot_time
    baseline_w_ctpl_boot_overhead_II=baseline_w_ctpl_numcycles_II*msp_boot_time
    baseline_w_ctpl_boot_overhead_III=baseline_w_ctpl_numcycles_III*msp_boot_time
    helpblip_boot_overhead=fpga_boot_time+msp_boot_time
    helpblip_intpower_boot_overhead=(helpblip_intpower_op_numcycles[depthwise_index]+helpblip_intpower_op_numcycles[conv_index])*fpga_boot_time
    helpblip_intpower_boot_overhead_II=(helpblip_intpower_op_numcycles_II[depthwise_index]+helpblip_intpower_op_numcycles_II[conv_index])*fpga_boot_time
    helpblip_intpower_boot_overhead_III=(helpblip_intpower_op_numcycles_III[depthwise_index]+helpblip_intpower_op_numcycles_III[conv_index])*fpga_boot_time


    baseline_fpgacompute=np.mean(baseline_compute_counter)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute=np.mean(baseline_w_ctpl_compute_counter)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute_II=np.mean(baseline_w_ctpl_compute_counter_II)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute_III=np.mean(baseline_w_ctpl_compute_counter_III)*fpga_mac_cycle*fpga_clock
    helpblip_fpgacompute=np.mean(helpblip_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute=np.mean(helpblip_intpower_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute_II=np.mean(helpblip_intpower_compute_counter_II)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute_III=np.mean(helpblip_intpower_compute_counter_III)*fpga_mac_cycle*fpga_clock


    baseline_fpgacomm=baseline_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm=baseline_w_ctpl_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm_II=baseline_w_ctpl_comm_counter_II*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm_III=baseline_w_ctpl_comm_counter_III*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_fpgacomm=helpblip_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm=helpblip_intpower_commcounter*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_II=helpblip_intpower_commcounter_II*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm_III=helpblip_intpower_commcounter_III*((7*spi_cycle_time)+(spi_cycle_time/2))


    baseline_deadperiod=baseline_offtime*basline_numcycles
    baseline_w_ctpl_deadperiod=baseline_w_ctpl_offtime*baseline_w_ctpl_numcycles
    baseline_w_ctpl_deadperiod_II=baseline_w_ctpl_offtime_II*baseline_w_ctpl_numcycles_II
    baseline_w_ctpl_deadperiod_III=baseline_w_ctpl_offtime_III*baseline_w_ctpl_numcycles_III
    helpblip_deadperiod=helpblip_offtime*helpblip_numcycles
    helpblip_intpower_deadperiod=helpblip_intpower_offtime*helpblip_intpower_numcycles
    helpblip_intpower_deadperiod_II=helpblip_intpower_offtime_II*helpblip_intpower_numcycles_II
    helpblip_intpower_deadperiod_III=helpblip_intpower_offtime_III*helpblip_intpower_numcycles_III


    baseline_depthwise=baseline[depthwise_index]-baseline[depthwise_index-1]
    # baseline_w_ctpl_depthwise=(baseline_w_ctpl[depthwise_index]-baseline_w_ctpl[depthwise_index-1])-(baseline_w_ctpl_offtime*baseline_w_ctpl_op_numcycles[depthwise_index])
    # baseline_w_ctpl_depthwise_II=(baseline_w_ctpl_II[depthwise_index]-baseline_w_ctpl_II[depthwise_index-1])-(baseline_w_ctpl_offtime_II*baseline_w_ctpl_op_numcycles_II[depthwise_index])
    # baseline_w_ctpl_depthwise_III=(baseline_w_ctpl_III[depthwise_index]-baseline_w_ctpl_III[depthwise_index-1])-(baseline_w_ctpl_offtime_III*baseline_w_ctpl_op_numcycles_III[depthwise_index])

    baseline_w_ctpl_depthwise=baseline_w_ctpl_ontime*baseline_w_ctpl_op_numcycles[depthwise_index]
    baseline_w_ctpl_depthwise_II=baseline_w_ctpl_ontime_II*baseline_w_ctpl_op_numcycles_II[depthwise_index]
    baseline_w_ctpl_depthwise_III=baseline_w_ctpl_ontime_III*baseline_w_ctpl_op_numcycles_III[depthwise_index]


    helpblip_depthwise=helpblip[depthwise_index]-helpblip[depthwise_index-1]
    helpblip_intpower_depthwise=helpblip_intpower_ontime*helpblip_intpower_op_numcycles[depthwise_index]-(fpga_boot_time*helpblip_intpower_op_numcycles[depthwise_index])
    helpblip_intpower_depthwise_II=helpblip_intpower_ontime_II*helpblip_intpower_op_numcycles_II[depthwise_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_II[depthwise_index])
    helpblip_intpower_depthwise_III=helpblip_intpower_ontime_III*helpblip_intpower_op_numcycles_III[depthwise_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_III[depthwise_index])


    baseline_conv=baseline[conv_index]-baseline[conv_index-1]
    baseline_w_ctpl_conv=baseline_w_ctpl_ontime*baseline_w_ctpl_op_numcycles[conv_index]
    baseline_w_ctpl_conv_II=baseline_w_ctpl_ontime_II*baseline_w_ctpl_op_numcycles_II[conv_index]
    baseline_w_ctpl_conv_III=baseline_w_ctpl_ontime_III*baseline_w_ctpl_op_numcycles_III[conv_index]

    # baseline_w_ctpl_conv=(baseline_w_ctpl[conv_index]-baseline_w_ctpl[conv_index-1])-(baseline_w_ctpl_offtime*baseline_w_ctpl_op_numcycles[conv_index])
    # baseline_w_ctpl_conv_II=(baseline_w_ctpl_II[conv_index]-baseline_w_ctpl_II[conv_index-1])-(baseline_w_ctpl_offtime_II*baseline_w_ctpl_op_numcycles_II[conv_index])
    # baseline_w_ctpl_conv_III=(baseline_w_ctpl_III[conv_index]-baseline_w_ctpl_III[conv_index-1])-(baseline_w_ctpl_offtime_III*baseline_w_ctpl_op_numcycles_III[conv_index])
    helpblip_conv=helpblip[conv_index]-helpblip[conv_index-1]
    helpblip_intpower_conv=helpblip_intpower_ontime*helpblip_intpower_op_numcycles[conv_index]-(fpga_boot_time*helpblip_intpower_op_numcycles[conv_index])
    helpblip_intpower_conv_II=helpblip_intpower_ontime_II*helpblip_intpower_op_numcycles_II[conv_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_II[conv_index])
    # print(helpblip_intpower_conv_II)
    helpblip_intpower_conv_III=helpblip_intpower_ontime_III*helpblip_intpower_op_numcycles_III[conv_index]-(fpga_boot_time*helpblip_intpower_op_numcycles_III[conv_index])
    # helpblip_intpower_conv=(helpblip_intpower[conv_index]-helpblip_intpower[conv_index-1])-(helpblip_intpower_offtime*helpblip_intpower_op_numcycles[conv_index])
    # helpblip_intpower_conv_II=(helpblip_intpower_II[conv_index]-helpblip_intpower_II[conv_index-1])-(helpblip_intpower_offtime_II*helpblip_intpower_op_numcycles_II[conv_index])
    # helpblip_intpower_conv_III=(helpblip_intpower_III[conv_index]-helpblip_intpower_III[conv_index-1])-(helpblip_intpower_offtime_III*helpblip_intpower_op_numcycles_III[conv_index])
    # print(helpblip_intpower_op_numcycles,baseline_w_ctpl_op_numcycles)


    baseline_total =baseline[len(baseline)-1]   
    baseline_w_ctpl_total= baseline_w_ctpl[len(baseline_w_ctpl)-1]
    baseline_w_ctpl_total_II= baseline_w_ctpl_II[len(baseline_w_ctpl_II)-1]
    baseline_w_ctpl_total_III= baseline_w_ctpl_III[len(baseline_w_ctpl_III)-1]
    helpblip_total= helpblip[len(helpblip)-1]
    helpblip_intpower_total=helpblip_intpower[len(helpblip_intpower)-1]
    helpblip_intpower_total_II=helpblip_intpower_II[len(helpblip_intpower_II)-1]
    helpblip_intpower_total_III=helpblip_intpower_III[len(helpblip_intpower_III)-1]

    baseline_mspcompute =baseline[len(baseline)-1]
    baseline_w_ctpl_mspcompute= baseline_w_ctpl[len(baseline_w_ctpl)-1]
    baseline_w_ctpl_mspcompute_II= baseline_w_ctpl_II[len(baseline_w_ctpl_II)-1]
    baseline_w_ctpl_mspcompute_III= baseline_w_ctpl_III[len(baseline_w_ctpl_III)-1]
    helpblip_mspcompute= helpblip[len(helpblip)-1]
    helpblip_intpower_mspcompute=helpblip_intpower[len(helpblip_intpower)-1]
    helpblip_intpower_mspcompute_II=helpblip_intpower_II[len(helpblip_intpower_II)-1]
    helpblip_intpower_mspcompute_III=helpblip_intpower_III[len(helpblip_intpower_III)-1]

    baseline_mspcompute=baseline_mspcompute-baseline_boot_overhead-baseline_deadperiod-baseline_depthwise-baseline_conv
    baseline_w_ctpl_mspcompute=baseline_w_ctpl_mspcompute-baseline_w_ctpl_boot_overhead-baseline_w_ctpl_deadperiod-baseline_w_ctpl_depthwise-baseline_w_ctpl_conv
    baseline_w_ctpl_mspcompute_II=baseline_w_ctpl_mspcompute_II-baseline_w_ctpl_boot_overhead_II-baseline_w_ctpl_deadperiod_II-baseline_w_ctpl_depthwise_II-baseline_w_ctpl_conv_II
    baseline_w_ctpl_mspcompute_III=baseline_w_ctpl_mspcompute_III-baseline_w_ctpl_boot_overhead_III-baseline_w_ctpl_deadperiod_III-baseline_w_ctpl_depthwise_III-baseline_w_ctpl_conv_III
    helpblip_mspcompute=helpblip_mspcompute-helpblip_boot_overhead-helpblip_deadperiod-helpblip_depthwise-helpblip_conv
    helpblip_intpower_mspcompute=helpblip_intpower_mspcompute-helpblip_intpower_deadperiod-helpblip_intpower_depthwise-helpblip_intpower_conv
    helpblip_intpower_mspcompute_II=helpblip_intpower_mspcompute_II-helpblip_intpower_deadperiod_II-helpblip_intpower_depthwise_II-helpblip_intpower_conv_II
    helpblip_intpower_mspcompute_III=helpblip_intpower_mspcompute_III-helpblip_intpower_deadperiod_III-helpblip_intpower_depthwise_III-helpblip_intpower_conv_III
    
    fig, (ax1,ax2,ax3)= plt.subplots(1,3,gridspec_kw={'width_ratios': [0.35, 0.97, 1.68]})
    rects1 = ax1.bar(-0.75,baseline_boot_overhead,width,color='mediumorchid',hatch="//",label='Boot overhead')
    rects2 = ax1.bar(-0.25,baseline_depthwise,width,color='lightcoral',hatch="**",label='Live-Depthwise')
    rects3 = ax1.bar(0.25,baseline_conv,width,color='lightblue', hatch="..",label='Live-Conv')
    rects4 = ax1.bar(0.75,baseline_mspcompute,width,color='lightgreen',hatch="\\",label='Live-Other')
    rects5 = ax1.bar(0.75,baseline_deadperiod,width,bottom=baseline_mspcompute,color='lightslategrey',hatch="oo",label='Dead-period')
    ax1.bar(0.75,0,color='cyan',hatch="--",label='Overall')

    rects9 = ax1.bar(1.5,helpblip_boot_overhead,width,color='mediumorchid',hatch="//")
    rects10 = ax1.bar(2,helpblip_depthwise,width,color='lightcoral',hatch="**")
    rects11 = ax1.bar(2.5,helpblip_conv,width,color='lightblue', hatch="..")
    rects12 = ax1.bar(3,helpblip_mspcompute,width,color='lightgreen',hatch="\\")

    ax1.set_ylabel('Time (s)',fontsize=30)
    ax1.set_xticks([0.125, 2.25], ['Baseline','HELP-BLIP'],fontsize=25)
    ax1.plot([],[],'X',color='red',ms=15,label='Do not complete')

    ax1.text(1,-3.8,'a) Continuous power',horizontalalignment='center', color='black',fontsize=18)
    
    ax1.legend(loc='upper left', bbox_to_anchor=(0.2, 1.15),ncol=7,fontsize=15)
    ax1.set_xlim(-1,3.5)
    ax1.grid()
    ax1.tick_params(axis='both', which='both', labelsize=13,width=3)
    ax1.minorticks_on()
    # ax1.set_ylim(y_min,y_max)
    # ax1.set_yscale('log')
    ax2.plot(0,10.5,'X',color='red',ms=15)

    rects1 = ax2.bar(1,baseline_w_ctpl_boot_overhead,width,color='mediumorchid',hatch="//")
    rects2 = ax2.bar(1.5,baseline_w_ctpl_depthwise,width,color='lightcoral',hatch="**")
    rects3 = ax2.bar(2,baseline_w_ctpl_conv,width,color='lightblue', hatch="..")
    rects4 = ax2.bar(2.5,baseline_w_ctpl_mspcompute,width,color='lightgreen',hatch="\\")
    rects5 = ax2.bar(3,baseline_w_ctpl_deadperiod,width,color='lightslategrey',hatch="oo")
    rects6 = ax2.bar(3.5,baseline_w_ctpl_total,width,color='cyan',hatch="--")

    ax2.plot(4.5,10.5,'X',color='red',ms=15)

    rects9 = ax2.bar(5.5,helpblip_intpower_boot_overhead,width,color='mediumorchid',hatch="//")
    rects10 = ax2.bar(6,helpblip_intpower_depthwise,width,color='lightcoral',hatch="**")
    rects11 = ax2.bar(6.5,helpblip_intpower_conv,width,color='lightblue', hatch="..")
    rects12 = ax2.bar(7,helpblip_intpower_mspcompute,width,color='lightgreen',hatch="\\")
    rects13 = ax2.bar(7.5,helpblip_intpower_deadperiod,width,color='lightslategrey',hatch="oo")
    rects14 = ax2.bar(8,helpblip_intpower_total,width,color='cyan',hatch="--")


    ax2.set_xticks([0, 2.25, 4.5, 6.75], ['Baseline', 'Baseline\n w/ctpl','HELP-BLIP \n w/ctpl','HELP-BLIP'],fontsize=15)
    ax2.text(4,-95,'b) Intermittent power 50mF',horizontalalignment='center', color='black',fontsize=18)
    ax2.tick_params(axis='both', which='both', labelsize=13,width=3)
    ax2.grid()

    ax2.set_xlim(-0.5,8.5)
    ax2.minorticks_on()

# #####################################################################

    ax3.plot(0,3,'X',color='red',ms=15)

    rects1 = ax3.bar(1,baseline_w_ctpl_boot_overhead_II,width,color='mediumorchid',hatch="//")
    rects2 = ax3.bar(1.5,baseline_w_ctpl_depthwise_II,width,color='lightcoral',hatch="**")
    rects3 = ax3.bar(2,baseline_w_ctpl_conv_II,width,color='lightblue', hatch="..")
    rects4 = ax3.bar(2.5,baseline_w_ctpl_mspcompute_II,width,color='lightgreen',hatch="\\")
    rects5 = ax3.bar(3,baseline_w_ctpl_deadperiod_II,width,color='lightslategrey',hatch="oo")
    rects6 = ax3.bar(3.5,baseline_w_ctpl_total_II,width,color='cyan',hatch="--")

    ax3.plot(4.5,3,'X',color='red',ms=15)

    rects9 = ax3.bar(5.5,helpblip_intpower_boot_overhead_II,width,color='mediumorchid',hatch="//")
    rects10 = ax3.bar(6,helpblip_intpower_depthwise_II,width,color='lightcoral',hatch="**")
    rects11 = ax3.bar(6.5,helpblip_intpower_conv_II,width,color='lightblue', hatch="..")
    rects12 = ax3.bar(7,helpblip_intpower_mspcompute_II,width,color='lightgreen',hatch="\\")
    rects13 = ax3.bar(7.5,helpblip_intpower_deadperiod_II,width,color='lightslategrey',hatch="oo")
    rects13 = ax3.bar(8,helpblip_intpower_total_II,width,color='cyan',hatch="--")

    ax3.plot(9,3,'X',color='red',ms=15)

    rects1 = ax3.bar(10,baseline_w_ctpl_boot_overhead_III,width,color='mediumorchid',hatch="//")
    rects2 = ax3.bar(10.5,baseline_w_ctpl_depthwise_III,width,color='lightcoral',hatch="**")
    rects3 = ax3.bar(11,baseline_w_ctpl_conv_III,width,color='lightblue', hatch="..")
    rects4 = ax3.bar(11.5,baseline_w_ctpl_mspcompute_III,width,color='lightgreen',hatch="\\")
    rects5 = ax3.bar(12,baseline_w_ctpl_deadperiod_III,width,color='lightslategrey',hatch="oo")
    rects5 = ax3.bar(12.5,baseline_w_ctpl_total_III,width,color='cyan',hatch="--")

    ax3.plot(13.5,3,'X',color='red',ms=15)

    rects9 = ax3.bar(14.5,helpblip_intpower_boot_overhead_III,width,color='mediumorchid',hatch="//")
    rects10 = ax3.bar(15,helpblip_intpower_depthwise_III,width,color='lightcoral',hatch="**")
    rects11 = ax3.bar(15.5,helpblip_intpower_conv_III,width,color='lightblue', hatch="..")
    rects12 = ax3.bar(16,helpblip_intpower_mspcompute_III,width,color='lightgreen',hatch="\\")
    rects13 = ax3.bar(16.5,helpblip_intpower_deadperiod_III,width,color='lightslategrey',hatch="oo")
    rects13 = ax3.bar(17,helpblip_intpower_total_III,width,color='cyan',hatch="--")

    ax3.set_xticks([0, 2.25, 4.5, 6.75, 9, 11.25, 13.5, 15.75], ['Baseline', 'Baseline\n w/ctpl','HELP-BLIP \n w/ctpl','HELP-BLIP','Baseline', 'Baseline\n w/ctpl','HELP-BLIP \n w/ctpl','HELP-BLIP'],fontsize=15)
    ax3.text(3,-25.5,'c) Intermittent power 144mF',horizontalalignment='center', color='black',fontsize=18)
    ax3.text(11.5,-25.5,'d) Intermittent power 344mF',horizontalalignment='center', color='black',fontsize=18)
    ax3.set_xlim(-1,18)
    ax3.tick_params(axis='both', which='both', labelsize=13,width=3)
    ax3.grid()
    ax3.minorticks_on()

    fig.set_size_inches(25, 6.25)
    plt.savefig(f'latency_ifpga.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_comparebaseline_stacked_latencies_intermittent_power(baseline,basline_numcycles,baseline_compute_counter,baseline_comm_counter,baseline_ontime,baseline_offtime,
                                                              baseline_w_ctpl,baseline_w_ctpl_numcycles,baseline_w_ctpl_compute_counter,baseline_w_ctpl_comm_counter,baseline_w_ctpl_ontime,baseline_w_ctpl_offtime,
                                                              helpblip,helpblip_numcycles,helpblip_compute_counter,helpblip_comm_counter,helpblip_ontime,helpblip_offtime,
                                                              helpblip_intpower,helpblip_intpower_numcycles,helpblip_intpower_compute_counter,helpblip_intpower_commcounter,helpblip_intpower_ontime,helpblip_intpower_offtime,dtile,ctile):
    spi_cycle_time=(1/(8e06/3))
    fpga_clock=1/20e06
    fpga_mac_cycle=11
    fpga_boot_time=130e-03
    msp_boot_time=5e-03

    baseline_mspcompute=0
    baseline_w_ctpl_mspcompute=0
    helpblip_mspcompute=0
    helpblip_intpower_mspcompute=0

    baseline=np.mean(baseline,axis=0)
    baseline_w_ctpl=np.mean(baseline_w_ctpl,axis=0)
    helpblip=np.mean(helpblip,axis=0)
    helpblip_intpower=np.mean(helpblip_intpower,axis=0)

    basline_numcycles=np.mean(basline_numcycles,axis=0)
    baseline_w_ctpl_numcycles=np.mean(baseline_w_ctpl_numcycles,axis=0)
    helpblip_numcycles=np.mean(helpblip_numcycles,axis=0)
    helpblip_intpower_numcycles=np.mean(helpblip_intpower_numcycles,axis=0)


    baseline_ontime,baseline_offtime=np.mean(baseline_ontime),np.mean(baseline_offtime)
    baseline_w_ctpl_ontime,baseline_w_ctpl_offtime=np.mean(baseline_w_ctpl_ontime),np.mean(baseline_w_ctpl_offtime)
    helpblip_ontime,helpblip_offtime= np.mean(helpblip_ontime),np.mean(helpblip_offtime)
    helpblip_intpower_ontime,helpblip_intpower_offtime=np.mean(helpblip_intpower_ontime),np.mean(helpblip_intpower_offtime)

    baseline_boot_overhead=basline_numcycles*msp_boot_time
    baseline_w_ctpl_boot_overhead=baseline_w_ctpl_numcycles*msp_boot_time
    helpblip_boot_overhead=helpblip_numcycles*fpga_boot_time
    helpblip_intpower_boot_overhead=helpblip_intpower_numcycles*fpga_boot_time

    baseline_fpgacompute=np.mean(baseline_compute_counter)*fpga_mac_cycle*fpga_clock
    baseline_w_ctpl_fpgacompute=np.mean(baseline_w_ctpl_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_fpgacompute=np.mean(helpblip_compute_counter)*fpga_mac_cycle*fpga_clock
    helpblip_intpower_fpgacompute=np.mean(helpblip_intpower_compute_counter)*fpga_mac_cycle*fpga_clock

    baseline_fpgacomm=baseline_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    baseline_w_ctpl_fpgacomm=baseline_w_ctpl_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_fpgacomm=helpblip_comm_counter*((7*spi_cycle_time)+(spi_cycle_time/2))
    helpblip_intpower_fpgacomm=helpblip_intpower_commcounter*((7*spi_cycle_time)+(spi_cycle_time/2))

    baseline_deadperiod=baseline_offtime*basline_numcycles
    baseline_w_ctpl_deadperiod=baseline_w_ctpl_offtime*baseline_w_ctpl_numcycles
    helpblip_deadperiod=helpblip_offtime*helpblip_numcycles
    helpblip_intpower_deadperiod=helpblip_intpower_offtime*helpblip_intpower_numcycles


    baseline_mspcompute =baseline[len(baseline)-1]
    baseline_w_ctpl_mspcompute= baseline_w_ctpl[len(baseline_w_ctpl)-1]
    helpblip_mspcompute= helpblip[len(helpblip)-1]
    helpblip_intpower_mspcompute=helpblip_intpower[len(helpblip_intpower)-1]

    baseline_mspcompute=baseline_mspcompute-baseline_boot_overhead-baseline_fpgacompute-baseline_fpgacomm-baseline_deadperiod
    baseline_w_ctpl_mspcompute=baseline_w_ctpl_mspcompute-baseline_w_ctpl_boot_overhead-baseline_w_ctpl_fpgacompute-baseline_w_ctpl_fpgacomm-baseline_w_ctpl_deadperiod
    helpblip_mspcompute=helpblip_mspcompute-helpblip_boot_overhead-helpblip_fpgacompute-helpblip_fpgacomm-helpblip_deadperiod
    helpblip_intpower_mspcompute=helpblip_intpower_mspcompute-helpblip_intpower_boot_overhead-helpblip_intpower_fpgacompute-helpblip_intpower_fpgacomm-helpblip_intpower_deadperiod




    min_ylim=1e-02


    x=np.linspace(1,len(baseline),num=len(baseline))
    width=0.05
    j=0
    labels=['Conv2D']
    x = np.arange(len(labels))  # the label locations
    num_xtics=len(x)-1
    width = 0.2 # the width of the bars

    fig, ax = plt.subplots()
    params = {'figure.figsize': [20, 9],
                'legend.fontsize': 12}
    plt.rcParams.update(params)
    
    rects1 = ax.bar(0-(2*width),baseline_fpgacompute,width,color='lightcoral',hatch="**",label='FPGA compute')
    rects2 = ax.bar(0-(1*width),baseline_fpgacomm,width,color='lightblue', hatch="..",label='Data movement')
    rects3 = ax.bar(0,baseline_mspcompute,width,color='lightgreen',hatch="\\",label='MSP compute')
    rects4 = ax.bar(0+(1*width),baseline_boot_overhead,width,color='mediumorchid',hatch="//",label='Boot overhead')
    rects5 = ax.bar(0+(2*width),baseline_deadperiod,width,color='lightslategrey',hatch="oo",label='Dead recharging')

    ax.text(1,min_ylim,'X',horizontalalignment='center',color='red',fontsize=35)

    rects6 = ax.bar(2-(2*width),baseline_w_ctpl_fpgacompute,width,color='lightcoral',hatch="**")
    rects7 = ax.bar(2-(1*width),baseline_w_ctpl_fpgacomm,width,color='lightblue', hatch="..")
    rects8 = ax.bar(2,baseline_w_ctpl_mspcompute,width,color='lightgreen',hatch="\\")
    rects9 = ax.bar(2+(1*width),baseline_w_ctpl_boot_overhead,width,color='mediumorchid',hatch="//")
    rects10 = ax.bar(2+(2*width),baseline_w_ctpl_deadperiod,width,color='lightslategrey',hatch="oo")


    rects11 = ax.bar(3-(2*width),helpblip_fpgacompute,width,color='lightcoral',hatch="**")
    rects12 = ax.bar(3-width,helpblip_fpgacomm,width,color='lightblue', hatch="..")
    rects13 = ax.bar(3,helpblip_mspcompute,width,color='lightgreen',hatch="\\")
    rects14 = ax.bar(3+width,helpblip_boot_overhead,width,color='mediumorchid',hatch="//")
    rects15 = ax.bar(3+(2*width),helpblip_deadperiod,width,color='lightslategrey',hatch="oo")

    ax.text(4,min_ylim,'X',horizontalalignment='center',color='red',fontsize=35)

    rects16 = ax.bar(5-(2*width),helpblip_intpower_fpgacompute,width,color='lightcoral',hatch="**")
    rects17 = ax.bar(5-width,helpblip_intpower_fpgacomm,width,color='lightblue', hatch="..")
    rects18 = ax.bar(5,helpblip_intpower_mspcompute,width,color='lightgreen',hatch="\\")
    rects19 = ax.bar(5+width,helpblip_intpower_boot_overhead,width,color='mediumorchid',hatch="//")
    rects20 = ax.bar(5+(2*width),helpblip_intpower_deadperiod,width,color='lightslategrey',hatch="oo")

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Time (s)',fontsize=20)
    plt.xticks([0, 1, 2, 3, 4, 5], ['Baseline\n cont power', 'Baseline\nint. power', 'Baseline\n w/ctpl\nint. power','HELP-BLIP\n cont power','HELP-BLIP\n w/ctpl\nint. power',f"HELP-BLIP\n int power \n Dtile{dtile} Ctile{ctile}"],fontsize=20)
    ax.legend(ncol=3,fontsize=12,loc='best')

    # ax.bar_label(rects1, padding=3,fontsize=20)
    # ax.bar_label(rects2, padding=3,fontsize=20)
    # # ax.bar_label(rects3, padding=3,fontsize=12)
    # # ax.bar_label(rects3, padding=-8,label_type='center',fontsize=20)
    # # ax.bar_label(rects4, padding=3,fontsize=20)
    # # ax.bar_label(rects5, padding=-8,label_type='center',fontsize=12)
    # # ax.bar_label(rects6, padding=3,fontsize=12)
    # # ax.bar_label(rects7, padding=-8,label_type='center',fontsize=12)
    # # ax.bar_label(rects8, padding=3,fontsize=12)
    # # ax.bar_label(rects9, padding=-8,label_type='center',fontsize=12)
    # # ax.bar_label(rects10, padding=3,fontsize=12)
    # ax.bar_label(rects11, padding=-8,label_type='center',fontsize=20)
    # ax.bar_label(rects12, padding=3,fontsize=20)
    # # ax.bar_label(rects13, padding=-8,label_type='center',fontsize=20)
    # # ax.bar_label(rects14, padding=3,fontsize=20)
    # # ax.bar_label(rects13, padding=3,rotation=90)
    ax.set_aspect('auto')
    ax.set_yscale('log')
    ax.set_ylim(min_ylim,150)
    plt.subplots_adjust(right=0.85)
    # fig.tight_layout()
    fig.set_size_inches(22, 9)
    plt.savefig(f'baselinsvsheliarev0.3_int_power.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_compare_perf_vs_tile_size(inf_1_1,inf_1_2,inf_1_5,inf_1_10,
                                   inf_2_1,inf_2_2,inf_2_5,inf_2_10,
                                   inf_4_1,inf_4_2,inf_4_5,inf_4_10,
                                   inf_7_1,inf_7_2,inf_7_5,inf_7_10,
                                   inf_14_1,inf_14_2,inf_14_5,inf_14_10,
                                   inf_28_1,inf_28_2,inf_28_5,inf_28_10):
    dtile=[1,4,16,49,196,784]
    ctile=[1,4,25,100]

    inf_1_1,inf_1_2,inf_1_5,inf_1_10=np.mean(inf_1_1,axis=0),np.mean(inf_1_2,axis=0),np.mean(inf_1_5,axis=0),np.mean(inf_1_10,axis=0)
    inf_2_1,inf_2_2,inf_2_5,inf_2_10=np.mean(inf_2_1,axis=0),np.mean(inf_2_2,axis=0),np.mean(inf_2_5,axis=0),np.mean(inf_2_10,axis=0)
    inf_4_1,inf_4_2,inf_4_5,inf_4_10=np.mean(inf_4_1,axis=0),np.mean(inf_4_2,axis=0),np.mean(inf_4_5,axis=0),np.mean(inf_4_10,axis=0)
    inf_7_1,inf_7_2,inf_7_5,inf_7_10=np.mean(inf_7_1,axis=0),np.mean(inf_7_2,axis=0),np.mean(inf_7_5,axis=0),np.mean(inf_7_10,axis=0)
    inf_14_1,inf_14_2,inf_14_5,inf_14_10=np.mean(inf_14_1,axis=0),np.mean(inf_14_2,axis=0),np.mean(inf_14_5,axis=0),np.mean(inf_14_10,axis=0)
    inf_28_1,inf_28_2,inf_28_5,inf_28_10=np.mean(inf_28_1,axis=0),np.mean(inf_28_2,axis=0),np.mean(inf_28_5,axis=0),np.mean(inf_28_10,axis=0)

    inf_mat=[[inf_1_1,inf_2_1,inf_4_1,inf_7_1,inf_14_1,inf_28_1],
             [inf_1_2,inf_2_2,inf_4_2,inf_7_2,inf_14_2,inf_28_2],
             [inf_1_5,inf_2_5,inf_4_5,inf_7_5,inf_14_5,inf_28_5],
             [inf_1_10,inf_2_10,inf_4_10,inf_7_10,inf_14_10,inf_28_10]]

    dtile=np.array(dtile)
    ctile=np.array(ctile)
    inf_mat=np.array(inf_mat)

    # for row,tile_row in enumerate (ctile):
    #     for col,tile_column in enumerate(dtile):
    fig, ax = plt.subplots()
    im = ax.imshow(inf_mat)

        # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(dtile)), labels=dtile,fontsize='15')
    ax.set_yticks(np.arange(len(ctile)), labels=ctile,fontsize='15')

    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("Latency (s)", rotation=-90, va="bottom",fontsize='20')
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(ctile)):
        for j in range(len(dtile)):
            text = ax.text(j, i, np.int(inf_mat[i, j]),rotation=45,fontweight='bold',
                        ha="center", va="center", color="w",fontsize='15')

    ax.set_xlabel("# of Depthwise Tiles",fontsize='15',labelpad=0.01)
    ax.set_ylabel("# of Conv Tiles",fontsize='15',labelpad=0.001)
    ax.set_title("Continuous power",va="center",fontsize='20')
    fig.set_size_inches(5.5,4)

    # fig.tight_layout()

    plt.savefig(f'perf_tilesize_2d_cont_power.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_compare_perf_vs_tile_size_144mF(inf_4_2,inf_4_5,inf_4_10,
                                         inf_7_2,inf_7_5,inf_7_10,
                                         inf_14_2,inf_14_5,inf_14_10,
                                         inf_28_2,inf_28_5,inf_28_10,dtile,ctile):


    dtile_xlabel=[1,4,16,49,196,784]
    ctile_xlabel=[1,4,25,100]
    inf_1_1,inf_1_2,inf_1_5,inf_1_10=0,0,0,0
    inf_2_1,inf_2_2,inf_2_5,inf_2_10=0,0,0,0
    inf_4_1,inf_4_2,inf_4_5,inf_4_10=0,np.mean(inf_4_2,axis=0),np.mean(inf_4_5,axis=0),np.mean(inf_4_10,axis=0)
    inf_7_1,inf_7_2,inf_7_5,inf_7_10=0,np.mean(inf_7_2,axis=0),np.mean(inf_7_5,axis=0),np.mean(inf_7_10,axis=0)
    inf_14_1,inf_14_2,inf_14_5,inf_14_10=0,np.mean(inf_14_2,axis=0),np.mean(inf_14_5,axis=0),np.mean(inf_14_10,axis=0)
    inf_28_1,inf_28_2,inf_28_5,inf_28_10=0,np.mean(inf_28_2,axis=0),np.mean(inf_28_5,axis=0),np.mean(inf_28_10,axis=0)

    inf_mat=[[inf_1_1,inf_2_1,inf_4_1,inf_7_1,inf_14_1,inf_28_1],
             [inf_1_2,inf_2_2,inf_4_2,inf_7_2,inf_14_2,inf_28_2],
             [inf_1_5,inf_2_5,inf_4_5,inf_7_5,inf_14_5,inf_28_5],
             [inf_1_10,inf_2_10,inf_4_10,inf_7_10,inf_14_10,inf_28_10]]

    dtile=np.array(dtile)
    ctile=np.array(ctile)
    inf_mat=np.array(inf_mat)

    # for row,tile_row in enumerate (ctile):
    #     for col,tile_column in enumerate(dtile):
    fig, ax = plt.subplots()
    im = ax.imshow(inf_mat)

        # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(dtile_xlabel)), labels=dtile_xlabel,fontsize='15')
    ax.set_yticks(np.arange(len(ctile_xlabel)), labels=ctile_xlabel,fontsize='15')

    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("Latency (s)", rotation=-90, va="bottom",fontsize='20')
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(ctile_xlabel)):
        for j in range(len(dtile_xlabel)):
            text = ax.text(j, i, np.int(inf_mat[i, j]),rotation=45,fontweight='bold',
                        ha="center", va="center", color="w",fontsize='15')
    for i in range(len(ctile_xlabel)):
        for j in range(len(dtile_xlabel)):
            if(inf_mat[i, j]==0):
                ax.plot(j, i,'X',color='red',ms=25)

    ax.set_xlabel("# of Depthwise Tiles",fontsize='15',labelpad=0.01)
    ax.set_ylabel("# of Conv Tiles",fontsize='15',labelpad=0.001)
    ax.set_title("Intermittent power",va="center",fontsize='20')

    # ax.set_title("Inference latencies for different \n depthwise and convolutional tile sizes",va="center",fontsize='25')
    fig.set_size_inches(5.5,4)

    # fig.tight_layout()

    plt.savefig(f'perf_tilesize_2d_int_power_144mF.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def plot_compare_perf_vs_tile_size_50mF(
                                         inf_7_5,inf_7_10,
                                         inf_14_5,inf_14_10,
                                         inf_28_5,inf_28_10,dtile,ctile):


    dtile_xlabel=[1,4,16,49,196,784]
    ctile_xlabel=[1,4,25,100]
    inf_1_1,inf_1_2,inf_1_5,inf_1_10=0,0,0,0
    inf_2_1,inf_2_2,inf_2_5,inf_2_10=0,0,0,0
    inf_4_1,inf_4_2,inf_4_5,inf_4_10=0,0,0,0
    inf_7_1,inf_7_2,inf_7_5,inf_7_10=0,0,np.mean(inf_7_5,axis=0),np.mean(inf_7_10,axis=0)
    inf_14_1,inf_14_2,inf_14_5,inf_14_10=0,0,np.mean(inf_14_5,axis=0),np.mean(inf_14_10,axis=0)
    inf_28_1,inf_28_2,inf_28_5,inf_28_10=0,0,np.mean(inf_28_5,axis=0),np.mean(inf_28_10,axis=0)

    inf_mat=[[inf_1_1,inf_2_1,inf_4_1,inf_7_1,inf_14_1,inf_28_1],
             [inf_1_2,inf_2_2,inf_4_2,inf_7_2,inf_14_2,inf_28_2],
             [inf_1_5,inf_2_5,inf_4_5,inf_7_5,inf_14_5,inf_28_5],
             [inf_1_10,inf_2_10,inf_4_10,inf_7_10,inf_14_10,inf_28_10]]

    dtile=np.array(dtile)
    ctile=np.array(ctile)
    inf_mat=np.array(inf_mat)

    # for row,tile_row in enumerate (ctile):
    #     for col,tile_column in enumerate(dtile):
    fig, ax = plt.subplots()
    im = ax.imshow(inf_mat)

        # Show all ticks and label them with the respective list entries
    ax.set_xticks(np.arange(len(dtile_xlabel)), labels=dtile_xlabel,fontsize='15')
    ax.set_yticks(np.arange(len(ctile_xlabel)), labels=ctile_xlabel,fontsize='15')

    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("Latency (s)", rotation=-90, va="bottom",fontsize='20')
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(ctile_xlabel)):
        for j in range(len(dtile_xlabel)):
            text = ax.text(j, i, np.int(inf_mat[i, j]),rotation=45,fontweight='bold',
                        ha="center", va="center", color="w",fontsize='15')
    for i in range(len(ctile_xlabel)):
        for j in range(len(dtile_xlabel)):
            if(inf_mat[i, j]==0):
                ax.plot(j, i,'X',color='red',ms=25)

    ax.set_xlabel("# of Depthwise Tiles",fontsize='15',labelpad=0.01)
    ax.set_ylabel("# of Conv Tiles",fontsize='15',labelpad=0.001)
    ax.set_title("Intermittent power",va="center",fontsize='20')

    # ax.set_title("Inference latencies for different \n depthwise and convolutional tile sizes",va="center",fontsize='25')
    fig.set_size_inches(5.5,4)

    # fig.tight_layout()

    plt.savefig(f'perf_tilesize_2d_int_power_50mF_rebuttal.pdf', dpi=250, quality=95, format='pdf')
    plt.show()

def main():
    plot='true'
     ##### Baseline Continuous power data#########
    expFiles=["./Exp_data/06_08_2022/Baseline_Lenet5_cont_power_8Mhz.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline,offtime_baseline,classoutputs_baseline,totalinftimeallexp_baseline,totalcycleforinference_baseline,num_cycle_op_allexp_baseline,operator_time_allexp_baseline,compute_counter_baseline,communication_counter_baseline=parsedata(f,1,8)
    expFiles=["./Exp_data/06_08_2022/Baseline_Lenet5_wctpl_cont_power_8Mhz.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl,offtime_baseline_wctpl,classoutputs_baseline_wctpl,totalinftimeallexp_baseline_wctpl,totalcycleforinference_baseline_wctpl,num_cycle_op_allexp_baseline_wctpl,operator_time_allexp_baseline_wctpl,compute_counter_baseline_wctpl,communication_counter_baseline_wctpl=parsedata(f,1,8)
    # print(totalinftimeallexp_baseline_wctpl)
    expFiles=["./Exp_data/06_08_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_Dtile_4_Ctile_4,offtime_ifpga_Dtile_4_Ctile_4,classoutputs_ifpga_Dtile_4_Ctile_4,totalinftimeallexp_ifpga_Dtile_4_Ctile_4,totalcycleforinference_ifpga_Dtile_4_Ctile_4,num_cycle_op_allexp_ifpga_Dtile_4_Ctile_4,operator_time_allexp_ifpga_Dtile_4_Ctile_4,compute_counter_ifpga_Dtile_4_Ctile_4,communication_counter_ifpga_Dtile_4_Ctile_4=parsedata(f,1,8)    
 
      ##### Bas accel Continuous power data#########

    expFiles=["./Exp_data/06_08_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga,offtime_ifpga,classoutputs_ifpga,totalinftimeallexp_ifpga,totalcycleforinference_ifpga,num_cycle_op_allexp_ifpga,operator_time_allexp_ifpga,compute_counter_ifpga,communication_counter_ifpga=parsedata(f,1,8)    
    # print(f'Op time {operator_time_allexp_ifpga}')

    expFiles=["./Exp_data/06_08_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz.txt"]          
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile1_Ctile1,tcyc,ncycop,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1= parsedata(f,1,8)
   
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_1_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile1_Ctile2,tcyc,ncycop,op_Dtile1_Ctile2,comp_Dtile1_Ctile2,comm_Dtile1_Ctile2= parsedata(f,1,8)    
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_1_conv_tile_5.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile1_Ctile5,tcyc,ncycop,op_Dtile1_Ctile5,comp_Dtile1_Ctile5,comm_Dtile1_Ctile5= parsedata(f,1,8)  

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_1_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile1_Ctile10,tcyc,ncycop,op_Dtile1_Ctile10,comp_Dtile1_Ctile10,comm_Dtile1_Ctile10= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_1.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile2_Ctile1,tcyc,ncycop,op_Dtile2_Ctile1,comp_Dtile2_Ctile1,comm_Dtile2_Ctile1= parsedata(f,1,8)
                                                                                                                           
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile2_Ctile2,off_Dtile2_Ctile2,classop_Dtile2_Ctile2,inftime_Dtile2_Ctile2,tcyc_Dtile2_Ctile2,ncycop_Dtile2_Ctile2,op_Dtile2_Ctile2,comp_Dtile2_Ctile2,comm_Dtile2_Ctile2= parsedata(f,1,8)
    # print(op)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_5.txt"]
    
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile2_Ctile5,tcyc,ncycop,op_Dtile2_Ctile5,comp_Dtile2_Ctile5,comm_Dtile2_Ctile5= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile2_Ctile10,tcyc,ncycop,op_Dtile2_Ctile10,comp_Dtile2_Ctile10,comm_Dtile2_Ctile10= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_1.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile4_Ctile1,tcyc,ncycop,op_Dtile4_Ctile1,comp_Dtile4_Ctile1,comm_Dtile4_Ctile1= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile2,off_Dtile4_Ctile2,classop_Dtile4_Ctile2,inftime_Dtile4_Ctile2,tcyc_Dtile4_Ctile2,ncycop_Dtile4_Ctile2,op_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2= parsedata(f,1,8)
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_5.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile4_Ctile5,tcyc,ncycop,op_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile4_Ctile10,tcyc,ncycop,op_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10= parsedata(f,1,8)
                                                                                                                          
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_1.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile7_Ctile1,tcyc,ncycop,op_Dtile7_Ctile1,comp_Dtile7_Ctile1,comm_Dtile7_Ctile1= parsedata(f,1,8)
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile7_Ctile2,tcyc,ncycop,op_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_5.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile5,off_Dtile7_Ctile5,classop_Dtile7_Ctile5,inftime_Dtile7_Ctile5,tcyc_Dtile7_Ctile5,ncycop_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5= parsedata(f,1,8)
    
    # print(op)
    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile7_Ctile10,tcyc,ncycop,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_1.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile14_Ctile1,tcyc,ncycop,op_Dtile14_Ctile1,comp_Dtile14_Ctile1,comm_Dtile14_Ctile1= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile14_Ctile2,tcyc,ncycop,op_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_5.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile14_Ctile5,tcyc,ncycop,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile14_Ctile10,tcyc,ncycop,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_1.txt"]      
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile28_Ctile1,tcyc,ncycop,op_Dtile28_Ctile1,comp_Dtile28_Ctile1,comm_Dtile28_Ctile1= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_2.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile28_Ctile2,tcyc,ncycop,op_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_5.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile28_Ctile5,tcyc,ncycop,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5= parsedata(f,1,8)

    expFiles=["./Exp_data/06_20_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_10.txt"]
    for idx, f in enumerate(expFiles):
        on,off,classop,inftime_Dtile28_Ctile10,tcyc,ncycop,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10= parsedata(f,1,8)


    # if(plot=='true'):    
        # plot_box_plot_latencies_cont_power(totalinftimeallexp_baseline,totalinftimeallexp_baseline_wctpl,totalinftimeallexp_ifpga,totalinftimeallexp_ifpga_Dtile_4_Ctile_4,4)
        # plot_comparebaseline_stacked_latencies_cont_power(operator_time_allexp_baseline,operator_time_allexp_baseline_wctpl,operator_time_allexp_ifpga)
    plot_compare_perf_vs_tile_size(inftime_Dtile1_Ctile1,inftime_Dtile1_Ctile2,inftime_Dtile1_Ctile5,inftime_Dtile1_Ctile10,
                                    inftime_Dtile2_Ctile1,inftime_Dtile2_Ctile2,inftime_Dtile2_Ctile5,inftime_Dtile2_Ctile10,
                                    inftime_Dtile4_Ctile1,inftime_Dtile4_Ctile2,inftime_Dtile4_Ctile5,inftime_Dtile4_Ctile10,
                                    inftime_Dtile7_Ctile1,inftime_Dtile7_Ctile2,inftime_Dtile7_Ctile5,inftime_Dtile7_Ctile10,                                                                           
                                    inftime_Dtile14_Ctile1,inftime_Dtile14_Ctile2,inftime_Dtile14_Ctile5,inftime_Dtile14_Ctile10,
                                    inftime_Dtile28_Ctile1,inftime_Dtile28_Ctile2,inftime_Dtile28_Ctile5,inftime_Dtile28_Ctile10)

    # Intermittent power data
#     expFiles=["./Exp_data/08_29_2022/Baseline_Lenet5_wctpl_FR_intermittent_power_8Mhz_wo_1_5_3_3reg_50mF.txt"]
#     for idx, f in enumerate(expFiles):
#         ontime_baseline_wctpl,offtime_baseline_wctpl,classoutputs_baseline_wctpl,totalinftimeallexp_baseline_wctpl,totalcycleforinference_baseline_wctpl,num_cycle_op_allexp_baseline_wctpl,operator_time_allexp_baseline_wctpl,compute_counter_baseline_wctpl,communication_counter_baseline_wctpl=parsedata(f,1,8)

### MSP430 w CTPL intermittent power data###
    expFiles=["./Exp_data/08_29_2022/Baseline_Lenet5_wctpl_FR_intermittent_power_8Mhz_wo_1_5_3_3reg_50mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_II,offtime_baseline_wctpl_II,classoutputs_baseline_wctpl_II,totalinftimeallexp_baseline_wctpl_II,totalcycleforinference_baseline_wctpl_II,num_cycle_op_allexp_baseline_wctpl_II,operator_time_allexp_baseline_wctpl_II,compute_counter_baseline_wctpl_II,communication_counter_baseline_wctpl_II=parsedata(f,1,8)
    # print(f'Baseline w ctpl{np.mean(ontime_baseline_wctpl_II)}{operator_time_allexp_baseline_wctpl_II}')
    
    expFiles=["./Exp_data/06_19_2022/Baseline_Lenet5_wctpl_intermittent_power_8Mhz_w1_5reg_144mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_144mF,offtime_baseline_wctpl_144mF,classoutputs_baseline_wctpl_144mF,totalinftimeallexp_baseline_wctpl_144mF,totalcycleforinference_baseline_wctpl_144mF,num_cycle_op_allexp_baseline_wctpl_144mF,operator_time_allexp_baseline_wctpl_144mF,compute_counter_baseline_wctpl_144mF,communication_counter_baseline_wctpl_144mF=parsedata(f,1,8)


    expFiles=["./Exp_data/08_29_2022/Baseline_Lenet5_wctpl_FR_intermittent_power_8Mhz_wo_1_5_3_3reg_50mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_144mF_II,offtime_baseline_wctpl_144mF_II,classoutputs_baseline_wctpl_144mF_II,totalinftimeallexp_baseline_wctpl_144mF_II,totalcycleforinference_baseline_wctpl_144mF_II,num_cycle_op_allexp_baseline_wctpl_144mF_II,operator_time_allexp_baseline_wctpl_144mF_II,compute_counter_baseline_wctpl_144mF_II,communication_counter_baseline_wctpl_144mF_II=parsedata(f,1,8)

    # print(f'ontime{ontime_baseline_wctpl_144mF_II}')

    # print(num_cycle_op_allexp_baseline_wctpl_144mF_II)
    # print(ontime_baseline_wctpl_144mF_II)
    expFiles=["./Exp_data/06_19_2022/Baseline_Lenet5_wctpl_intermittent_power_8Mhz_w1_5reg_244mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_244mF,offtime_baseline_wctpl_244mF,classoutputs_baseline_wctpl_244mF,totalinftimeallexp_baseline_wctpl_244mF,totalcycleforinference_baseline_wctpl_244mF,num_cycle_op_allexp_baseline_wctpl_244mF,operator_time_allexp_baseline_wctpl_244mF,compute_counter_baseline_wctpl_244mF,communication_counter_baseline_wctpl_244mF=parsedata(f,1,8)

    expFiles=["./Exp_data/06_22_2022/Baseline_Lenet5_wctpl_intermittent_power_8Mhz_wo1_5reg_244mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_244mF_II,offtime_baseline_wctpl_244mF_II,classoutputs_baseline_wctpl_244mF_II,totalinftimeallexp_baseline_wctpl_244mF_II,totalcycleforinference_baseline_wctpl_244mF_II,num_cycle_op_allexp_baseline_wctpl_244mF_II,operator_time_allexp_baseline_wctpl_244mF_II,compute_counter_baseline_wctpl_244mF_II,communication_counter_baseline_wctpl_244mF_II=parsedata(f,1,8)
    # print(ontime_baseline_wctpl_244mF_II)

    expFiles=["./Exp_data/06_22_2022/Baseline_Lenet5_wctpl_intermittent_power_8Mhz_wo1_5reg_344mF.txt"]
    for idx, f in enumerate(expFiles):
        ontime_baseline_wctpl_344mF_II,offtime_baseline_wctpl_344mF_II,classoutputs_baseline_wctpl_344mF_II,totalinftimeallexp_baseline_wctpl_344mF_II,totalcycleforinference_baseline_wctpl_344mF_II,num_cycle_op_allexp_baseline_wctpl_344mF_II,operator_time_allexp_baseline_wctpl_344mF_II,compute_counter_baseline_wctpl_344mF_II,communication_counter_baseline_wctpl_344mF_II=parsedata(f,1,8)

    # expFiles=["./Exp_data/08_29_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_2_50mF_Dearlydie_2_Cearlydie_1.txt"]
    # for idx, f in enumerate(expFiles):
    #     ontime_ifpga_Dtile_7_Ctile_5,offtime_ifpga_Dtile_7_Ctile_5,classoutputs_ifpga_Dtile_7_Ctile_5,totalinftimeallexp_ifpga_Dtile_7_Ctile_5,totalcycleforinference_ifpga_Dtile_7_Ctile_5,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_5,operator_time_allexp_ifpga_Dtile_7_Ctile_5,compute_counter_ifpga_Dtile_7_Ctile_5,communication_counter_ifpga_Dtile_7_Ctile_5=parsedata(f,1,8)    
    # print(f'FPGA 50 mF{ontime_ifpga_Dtile_7_Ctile_5}{operator_time_allexp_ifpga_Dtile_7_Ctile_5}')

    # expFiles=["./Exp_data/10_28_2022/Bobber_Lenet5_wctpl_FR_intermittent_power_8Mhz_1v5reg_en_msp_no_early_die_osc_en_msp_Dtile7_Ctile2_50mF_9dBm.txt"]
    expFiles=["./Exp_data/11_03_2022/Bobber_Lenet5_wctpl_FR_intermittent_power_8Mhz_1v5reg_Dtile7_DDie1_Ctile5_Cdie2_50mF_9dBm_osc_disbymsp_no ext osc_3v3switch_byp.txt"]

    for idx, f in enumerate(expFiles):
        ontime_ifpga_Dtile_7_Ctile_5,offtime_ifpga_Dtile_7_Ctile_5,classoutputs_ifpga_Dtile_7_Ctile_5,totalinftimeallexp_ifpga_Dtile_7_Ctile_5,totalcycleforinference_ifpga_Dtile_7_Ctile_5,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_5,operator_time_allexp_ifpga_Dtile_7_Ctile_5,compute_counter_ifpga_Dtile_7_Ctile_5,communication_counter_ifpga_Dtile_7_Ctile_5=parsedata(f,1,8)    
    # print(f'{num_cycle_op_allexp_ifpga_Dtile_7_Ctile_5}')

    # expFiles=["./Exp_data/08_30_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_5_50mF_Dearlydie_1_Cearlydie_2.txt"]
    expFiles=["./Exp_data/11_03_2022/Bobber_Lenet5_wctpl_FR_intermittent_power_8Mhz_1v5reg_Dtile7_DDie1_Ctile5_Cdie2_50mF_9dBm_osc_disbymsp_no ext osc_3v3switch_byp.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_50mF_II,offtime_ifpga_50mF_II,classoutputs_ifpga_50mF_II,totalinftimeallexp_ifpga_50mF_II,totalcycleforinference_ifpga_50mF_II,num_cycle_op_allexp_ifpga_50mF_II,operator_time_allexp_ifpga_50mF_II,compute_counter_ifpga_50mF_II,communication_counter_ifpga_50mF_II=parsedata(f,1,8)    
    # print(totalinftimeallexp_ifpga_50mF_II)

    expFiles=["./Exp_data/08_30_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_5_50mF_Dearlydie_2_Cearlydie_2.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_50mF_III,offtime_ifpga_50mF_III,classoutputs_ifpga_50mF_III,totalinftimeallexp_ifpga_50mF_III,totalcycleforinference_ifpga_50mF_III,num_cycle_op_allexp_ifpga_50mF_III,operator_time_allexp_ifpga_50mF_III,compute_counter_ifpga_50mF_III,communication_counter_ifpga_50mF_III=parsedata(f,1,8)    
    # print(totalinftimeallexp_ifpga_50mF_III)

    expFiles=["./Exp_data/08_31_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_5_50mF_Dearlydie_2_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_50mF_IV,offtime_ifpga_50mF_IV,classoutputs_ifpga_50mF_IV,totalinftimeallexp_ifpga_50mF_IV,totalcycleforinference_ifpga_50mF_IV,num_cycle_op_allexp_ifpga_50mF_IV,operator_time_allexp_ifpga_50mF_IV,compute_counter_ifpga_50mF_IV,communication_counter_ifpga_50mF_IV=parsedata(f,1,8)    
    # print(totalinftimeallexp_ifpga_50mF_IV)


    expFiles=["./Exp_data/08_31_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2_144mF_Dearlydie_1_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_Dtile_7_Ctile_2_144mF,offtime_ifpga_Dtile_7_Ctile_2_144mF,classoutputs_ifpga_Dtile_7_Ctile_2_144mF,totalinftimeallexp_ifpga_Dtile_7_Ctile_2_144mF,totalcycleforinference_ifpga_Dtile_7_Ctile_2_144mF,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_2_144mF,operator_time_allexp_ifpga_Dtile_7_Ctile_2_144mF,compute_counter_ifpga_Dtile_7_Ctile_2_144mF,communication_counter_ifpga_Dtile_7_Ctile_2_144mF=parsedata(f,1,8)   
#      
    # print(f'ontime{ontime_ifpga_Dtile_7_Ctile_2_144mF}')

    expFiles=["./Exp_data/06_22_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2_244mF_Dearlydie_2_Cearlydie_3.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_Dtile_4_Ctile_2_244mF,offtime_ifpga_Dtile_4_Ctile_2_244mF,classoutputs_ifpga_Dtile_4_Ctile_2_244mF,totalinftimeallexp_ifpga_Dtile_4_Ctile_2_244mF,totalcycleforinference_ifpga_Dtile_4_Ctile_2_244mF,num_cycle_op_allexp_ifpga_Dtile_4_Ctile_2_244mF,operator_time_allexp_ifpga_Dtile_4_Ctile_2_244mF,compute_counter_ifpga_Dtile_4_Ctile_2_244mF,communication_counter_ifpga_Dtile_4_Ctile_2_244mF=parsedata(f,1,8)    
    # print(ontime_ifpga_Dtile_4_Ctile_2_244mF)

    expFiles=["./Exp_data/06_22_2022/MSP+fpga_Lenet5_cont_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2_344mF_Dearlydie_3_Cearlydie_4.txt"]
    for idx, f in enumerate(expFiles):
        ontime_ifpga_Dtile_2_Ctile_2_344mF,offtime_ifpga_Dtile_2_Ctile_2_344mF,classoutputs_ifpga_Dtile_2_Ctile_2_344mF,totalinftimeallexp_ifpga_Dtile_2_Ctile_2_344mF,totalcycleforinference_ifpga_Dtile_2_Ctile_2_344mF,num_cycle_op_allexp_ifpga_Dtile_2_Ctile_2_344mF,operator_time_allexp_ifpga_Dtile_2_Ctile_2_344mF,compute_counter_ifpga_Dtile_2_Ctile_2_344mF,communication_counter_ifpga_Dtile_2_Ctile_2_344mF=parsedata(f,1,8)    
    
    
###############EXP data for computation scheduling plot ####################
    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_2_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime,helpblip_intpower_offtime,classop,inftime_Dtile2_Ctile2_intpower,helpblip_intpower_numcycles,helpblip_intpower_op_numcycles,helpblip_intpower,helpblip_intpower_compute_counter,helpblip_intpower_commcounter= parsedata(f,1,8)
    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_5_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,classop,inftime_Dtile2_Ctile5_intpower,helpblip_intpower_numcycles_II,helpblip_intpower_op_numcycles_II,helpblip_intpower_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_2_conv_tile_10_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_III,helpblip_intpower_offtime_III,classop,inftime_Dtile2_Ctile10_intpower,helpblip_intpower_numcycles_III,helpblip_intpower_op_numcycles_III,helpblip_intpower_III,helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV,classop,inftime_Dtile4_Ctile2_intpower,helpblip_intpower_numcycles_IV,helpblip_intpower_op_numcycles_IV,helpblip_intpower_IV,helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_5_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_V,helpblip_intpower_offtime_V,classop,inftime_Dtile4_Ctile5_intpower_V,helpblip_intpower_numcycles_V,helpblip_intpower_op_numcycles_V,helpblip_intpower_V,helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_10_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI,classop,inftime_Dtile4_Ctile10_intpower,helpblip_intpower_numcycles_VI,helpblip_intpower_op_numcycles_VI,helpblip_intpower_VI,helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI= parsedata(f,1,8)


    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_2_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII,classop,inftime_Dtile7_Ctile2_intpower,helpblip_intpower_numcycles_VII,helpblip_intpower_op_numcycles_VII,helpblip_intpower_VII,helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_5_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII,classop,inftime_Dtile7_Ctile5_intpower,helpblip_intpower_numcycles_VIII,helpblip_intpower_op_numcycles_VIII,helpblip_intpower_VIII,helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_7_conv_tile_10_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX,classop,inftime_Dtile7_Ctile10_intpower,helpblip_intpower_numcycles_IX,helpblip_intpower_op_numcycles_IX,helpblip_intpower_IX,helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_2_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_X,helpblip_intpower_offtime_X,classop,inftime_Dtile1_Ctile14_intpower,helpblip_intpower_numcycles_X,helpblip_intpower_op_numcycles_X,helpblip_intpower_X,helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_5_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI,classop,inftime_Dtile1_Ctile14_intpower,helpblip_intpower_numcycles_XI,helpblip_intpower_op_numcycles_XI,helpblip_intpower_XI,helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_14_conv_tile_10_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII,classop,inftime_Dtile14_Ctile10_intpower,helpblip_intpower_numcycles_XII,helpblip_intpower_op_numcycles_XII,helpblip_intpower_XII,helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_2_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII,classop,inftime_Dtile28_Ctile2_intpower,helpblip_intpower_numcycles_XIII,helpblip_intpower_op_numcycles_XIII,helpblip_intpower_XIII,helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_5_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV,classop,inftime_Dtile28_Ctile5_intpower,helpblip_intpower_numcycles_XIV,helpblip_intpower_op_numcycles_XIV,helpblip_intpower_XIV,helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV= parsedata(f,1,8)

    expFiles=["./Exp_data/06_27_2022/MSP+fpga_Lenet5_int_power_8Mhz_spi4Mhz_depthwise_tile_28_conv_tile_10_344mF.txt"]
    for idx, f in enumerate(expFiles):
       helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV,classop,inftime_Dtile28_Ctile10_intpower,helpblip_intpower_numcycles_XV,helpblip_intpower_op_numcycles_XV,helpblip_intpower_XV,helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV= parsedata(f,1,8)


    # if(plot=='true'):    
        # plot_box_plot_latencies_intermittent_power(totalinftimeallexp_baseline_wctpl_II,ontime_baseline_wctpl_II,
        #                                             totalinftimeallexp_baseline_wctpl_144mF_II,ontime_baseline_wctpl_144mF_II,
        #                                             totalinftimeallexp_baseline_wctpl_244mF_II,ontime_baseline_wctpl_244mF_II,
        #                                             totalinftimeallexp_baseline_wctpl_344mF_II,ontime_baseline_wctpl_344mF_II,
        #                                             totalinftimeallexp_ifpga_Dtile_7_Ctile_5,ontime_ifpga_Dtile_7_Ctile_5,
        #                                             totalinftimeallexp_ifpga_Dtile_7_Ctile_2_144mF,ontime_ifpga_Dtile_7_Ctile_2_144mF,
        #                                             totalinftimeallexp_ifpga_Dtile_4_Ctile_2_244mF,ontime_ifpga_Dtile_4_Ctile_2_244mF,
        #                                             totalinftimeallexp_ifpga_Dtile_2_Ctile_2_344mF,ontime_ifpga_Dtile_2_Ctile_2_344mF,8)

        # numcyclesforinferenceplot_intermittent_power(totalcycleforinference_baseline_wctpl_II,ontime_baseline_wctpl_II,
        #                                             totalcycleforinference_baseline_wctpl_144mF_II,ontime_baseline_wctpl_144mF_II,
        #                                             totalcycleforinference_baseline_wctpl_244mF_II,ontime_baseline_wctpl_244mF_II,
        #                                             totalcycleforinference_baseline_wctpl_344mF_II,ontime_baseline_wctpl_344mF_II,
        #                                             totalcycleforinference_ifpga_Dtile_7_Ctile_5,ontime_ifpga_Dtile_7_Ctile_5,
        #                                             totalcycleforinference_ifpga_Dtile_7_Ctile_2_144mF,ontime_ifpga_Dtile_7_Ctile_2_144mF,
        #                                             totalcycleforinference_ifpga_Dtile_4_Ctile_2_244mF,ontime_ifpga_Dtile_4_Ctile_2_244mF,
        #                                             totalcycleforinference_ifpga_Dtile_2_Ctile_2_344mF,ontime_ifpga_Dtile_2_Ctile_2_344mF,8)

        
        # plot_comparebaseline_stacked_latencies_intermittent_power(operator_time_allexp_baseline,totalcycleforinference_baseline,compute_counter_baseline,communication_counter_baseline, ontime_baseline,offtime_baseline,
        #                                                       operator_time_allexp_baseline_wctpl_244mF_II,totalcycleforinference_baseline_wctpl_244mF_II,compute_counter_baseline_wctpl_244mF_II,communication_counter_baseline_wctpl_244mF_II, ontime_baseline_wctpl_244mF_II,offtime_baseline_wctpl_244mF_II,
        #                                                       operator_time_allexp_ifpga,totalcycleforinference_ifpga,compute_counter_ifpga,communication_counter_ifpga,ontime_ifpga,offtime_ifpga,
        #                                                       operator_time_allexp_ifpga_Dtile_7_Ctile_2_144mF,totalcycleforinference_ifpga_Dtile_7_Ctile_2_144mF,compute_counter_ifpga_Dtile_7_Ctile_2_144mF,communication_counter_ifpga_Dtile_7_Ctile_2_144mF,ontime_ifpga_Dtile_7_Ctile_2_144mF,offtime_ifpga_Dtile_7_Ctile_2_144mF,7,2)

        # plot_ifpga_bar_stacked(operator_time_allexp_baseline,totalcycleforinference_baseline,compute_counter_baseline,communication_counter_baseline, ontime_baseline,offtime_baseline,num_cycle_op_allexp_baseline,
        #                                                         operator_time_allexp_baseline_wctpl_144mF_II,totalcycleforinference_baseline_wctpl_144mF_II,compute_counter_baseline_wctpl_144mF_II,communication_counter_baseline_wctpl_144mF_II, ontime_baseline_wctpl_144mF_II,offtime_baseline_wctpl_144mF_II,num_cycle_op_allexp_baseline_wctpl_144mF_II,
        #                                                         operator_time_allexp_baseline_wctpl_244mF_II,totalcycleforinference_baseline_wctpl_244mF_II,compute_counter_baseline_wctpl_244mF_II,communication_counter_baseline_wctpl_244mF_II, ontime_baseline_wctpl_244mF_II,offtime_baseline_wctpl_244mF_II,num_cycle_op_allexp_baseline_wctpl_244mF_II,
        #                                                         operator_time_allexp_baseline_wctpl_344mF_II,totalcycleforinference_baseline_wctpl_344mF_II,compute_counter_baseline_wctpl_344mF_II,communication_counter_baseline_wctpl_344mF_II, ontime_baseline_wctpl_344mF_II,offtime_baseline_wctpl_344mF_II,num_cycle_op_allexp_baseline_wctpl_344mF_II,
        #                                                         operator_time_allexp_ifpga,totalcycleforinference_ifpga,compute_counter_ifpga,communication_counter_ifpga,ontime_ifpga,offtime_ifpga,num_cycle_op_allexp_baseline_wctpl,2,2,
        #                                                         operator_time_allexp_ifpga_Dtile_7_Ctile_2_144mF,totalcycleforinference_ifpga_Dtile_7_Ctile_2_144mF,compute_counter_ifpga_Dtile_7_Ctile_2_144mF,communication_counter_ifpga_Dtile_7_Ctile_2_144mF,ontime_ifpga_Dtile_7_Ctile_2_144mF,offtime_ifpga_Dtile_7_Ctile_2_144mF,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_2_144mF,7,2,
        #                                                         operator_time_allexp_ifpga_Dtile_4_Ctile_2_244mF,totalcycleforinference_ifpga_Dtile_4_Ctile_2_244mF,compute_counter_ifpga_Dtile_4_Ctile_2_244mF,communication_counter_ifpga_Dtile_4_Ctile_2_244mF,ontime_ifpga_Dtile_4_Ctile_2_244mF,offtime_ifpga_Dtile_4_Ctile_2_244mF,num_cycle_op_allexp_ifpga_Dtile_4_Ctile_2_244mF,4,2,
        #                                                         operator_time_allexp_ifpga_Dtile_2_Ctile_2_344mF,totalcycleforinference_ifpga_Dtile_2_Ctile_2_344mF,compute_counter_ifpga_Dtile_2_Ctile_2_344mF,communication_counter_ifpga_Dtile_2_Ctile_2_344mF,ontime_ifpga_Dtile_2_Ctile_2_344mF,offtime_ifpga_Dtile_2_Ctile_2_344mF,num_cycle_op_allexp_ifpga_Dtile_2_Ctile_2_344mF,4,2)
    plot_ifpga_stacked_rev0_1(operator_time_allexp_baseline,totalcycleforinference_baseline,compute_counter_baseline,communication_counter_baseline, ontime_baseline,offtime_baseline,num_cycle_op_allexp_baseline,
                                                                operator_time_allexp_baseline_wctpl_II,totalcycleforinference_baseline_wctpl_II,compute_counter_baseline_wctpl_II,communication_counter_baseline_wctpl_II, ontime_baseline_wctpl_II,offtime_baseline_wctpl_II,num_cycle_op_allexp_baseline_wctpl_II,
                                                                operator_time_allexp_baseline_wctpl,totalcycleforinference_baseline_wctpl,compute_counter_baseline_wctpl,communication_counter_baseline_wctpl, ontime_baseline_wctpl,offtime_baseline_wctpl,num_cycle_op_allexp_baseline_wctpl,
                                                                operator_time_allexp_baseline_wctpl_344mF_II,totalcycleforinference_baseline_wctpl_344mF_II,compute_counter_baseline_wctpl_344mF_II,communication_counter_baseline_wctpl_344mF_II, ontime_baseline_wctpl_344mF_II,offtime_baseline_wctpl_344mF_II,num_cycle_op_allexp_baseline_wctpl_344mF_II,
                                                                operator_time_allexp_ifpga,totalcycleforinference_ifpga,compute_counter_ifpga,communication_counter_ifpga,ontime_ifpga,offtime_ifpga,num_cycle_op_allexp_baseline_wctpl,2,2,
                                                                operator_time_allexp_ifpga_Dtile_7_Ctile_5,totalcycleforinference_ifpga_Dtile_7_Ctile_5,compute_counter_ifpga_Dtile_7_Ctile_5,communication_counter_ifpga_Dtile_7_Ctile_5,ontime_ifpga_Dtile_7_Ctile_5,offtime_ifpga_Dtile_7_Ctile_5,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_5,7,5,
                                                                # operator_time_allexp_ifpga_50mF_II,totalcycleforinference_ifpga_50mF_II,compute_counter_ifpga_50mF_II,communication_counter_ifpga_50mF_II,ontime_ifpga_50mF_II,offtime_ifpga_50mF_II,num_cycle_op_allexp_ifpga_50mF_II,7,2,
                                                                # operator_time_allexp_ifpga_50mF_III,totalcycleforinference_ifpga_50mF_III,compute_counter_ifpga_50mF_III,communication_counter_ifpga_50mF_III,ontime_ifpga_50mF_III,offtime_ifpga_50mF_III,num_cycle_op_allexp_ifpga_50mF_III,7,2,
                                                                # operator_time_allexp_ifpga_50mF_IV,totalcycleforinference_ifpga_50mF_IV,compute_counter_ifpga_50mF_IV,communication_counter_ifpga_50mF_IV,ontime_ifpga_50mF_IV,offtime_ifpga_50mF_IV,num_cycle_op_allexp_ifpga_50mF_IV,7,2)
                                                                # operator_time_allexp_ifpga_Dtile_7_Ctile_2_144mF,totalcycleforinference_ifpga_Dtile_7_Ctile_2_144mF,compute_counter_ifpga_Dtile_7_Ctile_2_144mF,communication_counter_ifpga_Dtile_7_Ctile_2_144mF,ontime_ifpga_Dtile_7_Ctile_2_144mF,offtime_ifpga_Dtile_7_Ctile_2_144mF,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_2_144mF,7,2,
                                                                # op_Dtile4_Ctile2,tcyc_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2,on_Dtile4_Ctile2,off_Dtile4_Ctile2,ncycop_Dtile4_Ctile2,4,2,
                                                                op_Dtile7_Ctile5,tcyc_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,on_Dtile7_Ctile5,off_Dtile7_Ctile5,ncycop_Dtile7_Ctile5,7,5,
                                                                op_Dtile4_Ctile2,tcyc_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2,on_Dtile4_Ctile2,off_Dtile4_Ctile2,ncycop_Dtile4_Ctile2,4,2)

                                                                # operator_time_allexp_ifpga_Dtile_4_Ctile_2_244mF,totalcycleforinference_ifpga_Dtile_4_Ctile_2_244mF,compute_counter_ifpga_Dtile_4_Ctile_2_244mF,communication_counter_ifpga_Dtile_4_Ctile_2_244mF,ontime_ifpga_Dtile_4_Ctile_2_244mF,offtime_ifpga_Dtile_4_Ctile_2_244mF,num_cycle_op_allexp_ifpga_Dtile_4_Ctile_2_244mF,4,2,
                                                                # op_Dtile2_Ctile2,tcyc_Dtile2_Ctile2,comp_Dtile2_Ctile2,comm_Dtile2_Ctile2,on_Dtile2_Ctile2,off_Dtile2_Ctile2,ncycop_Dtile2_Ctile2,2,2)

    # plot_computation_scheduling_344mf(inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1,
    #                             inftime_Dtile2_Ctile2,op_Dtile2_Ctile2,comp_Dtile2_Ctile2,comm_Dtile2_Ctile2, 
    #                             helpblip_intpower,helpblip_intpower_numcycles,helpblip_intpower_compute_counter,helpblip_intpower_commcounter,helpblip_intpower_ontime,helpblip_intpower_offtime,helpblip_intpower_op_numcycles,
    #                             inftime_Dtile2_Ctile5,op_Dtile2_Ctile5,comp_Dtile2_Ctile5,comm_Dtile2_Ctile5, 
    #                             helpblip_intpower_II,helpblip_intpower_numcycles_II,helpblip_intpower_compute_counter_II,helpblip_intpower_commcounter_II,helpblip_intpower_ontime_II,helpblip_intpower_offtime_II,helpblip_intpower_op_numcycles_II,
    #                             inftime_Dtile2_Ctile10,op_Dtile2_Ctile10,comp_Dtile2_Ctile10,comm_Dtile2_Ctile10, 
    #                             helpblip_intpower_III,helpblip_intpower_numcycles_III,helpblip_intpower_compute_counter_III,helpblip_intpower_commcounter_III,helpblip_intpower_ontime_III,helpblip_intpower_offtime_III,helpblip_intpower_op_numcycles_III,
    #                             inftime_Dtile4_Ctile2,op_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2, 
    #                             helpblip_intpower_IV,helpblip_intpower_numcycles_IV,helpblip_intpower_compute_counter_IV,helpblip_intpower_commcounter_IV,helpblip_intpower_ontime_IV,helpblip_intpower_offtime_IV,helpblip_intpower_op_numcycles_IV,
    #                             inftime_Dtile4_Ctile5,op_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5,
    #                             helpblip_intpower_V,helpblip_intpower_numcycles_V,helpblip_intpower_compute_counter_V,helpblip_intpower_commcounter_V,helpblip_intpower_ontime_V,helpblip_intpower_offtime_V,helpblip_intpower_op_numcycles_V,
    #                             inftime_Dtile4_Ctile10,op_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10,
    #                             helpblip_intpower_VI,helpblip_intpower_numcycles_VI,helpblip_intpower_compute_counter_VI,helpblip_intpower_commcounter_VI,helpblip_intpower_ontime_VI,helpblip_intpower_offtime_VI,helpblip_intpower_op_numcycles_VI,
    #                             inftime_Dtile7_Ctile2,op_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2,
    #                             helpblip_intpower_VII,helpblip_intpower_numcycles_VII,helpblip_intpower_compute_counter_VII,helpblip_intpower_commcounter_VII,helpblip_intpower_ontime_VII,helpblip_intpower_offtime_VII,helpblip_intpower_op_numcycles_VII,
    #                             inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
    #                             helpblip_intpower_VIII,helpblip_intpower_numcycles_VIII,helpblip_intpower_compute_counter_VIII,helpblip_intpower_commcounter_VIII,helpblip_intpower_ontime_VIII,helpblip_intpower_offtime_VIII,helpblip_intpower_op_numcycles_VIII,
    #                             inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
    #                             helpblip_intpower_IX,helpblip_intpower_numcycles_IX,helpblip_intpower_compute_counter_IX,helpblip_intpower_commcounter_IX,helpblip_intpower_ontime_IX,helpblip_intpower_offtime_IX,helpblip_intpower_op_numcycles_IX,
    #                             inftime_Dtile14_Ctile2,op_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2,
    #                             helpblip_intpower_X,helpblip_intpower_numcycles_X,helpblip_intpower_compute_counter_X,helpblip_intpower_commcounter_X,helpblip_intpower_ontime_X,helpblip_intpower_offtime_X,helpblip_intpower_op_numcycles_X,
    #                             inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
    #                             helpblip_intpower_XI,helpblip_intpower_numcycles_XI,helpblip_intpower_compute_counter_XI,helpblip_intpower_commcounter_XI,helpblip_intpower_ontime_XI,helpblip_intpower_offtime_XI,helpblip_intpower_op_numcycles_XI,
    #                             inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
    #                             helpblip_intpower_XII,helpblip_intpower_numcycles_XII,helpblip_intpower_compute_counter_XII,helpblip_intpower_commcounter_XII,helpblip_intpower_ontime_XII,helpblip_intpower_offtime_XII,helpblip_intpower_op_numcycles_XII,
    #                             inftime_Dtile28_Ctile2,op_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2,
    #                             helpblip_intpower_XIII,helpblip_intpower_numcycles_XIII,helpblip_intpower_compute_counter_XIII,helpblip_intpower_commcounter_XIII,helpblip_intpower_ontime_XIII,helpblip_intpower_offtime_XIII,helpblip_intpower_op_numcycles_XIII,
    #                             inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
    #                             helpblip_intpower_XIV,helpblip_intpower_numcycles_XIV,helpblip_intpower_compute_counter_XIV,helpblip_intpower_commcounter_XIV,helpblip_intpower_ontime_XIV,helpblip_intpower_offtime_XIV,helpblip_intpower_op_numcycles_XIV,
    #                             inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
    #                             helpblip_intpower_XV,helpblip_intpower_numcycles_XV,helpblip_intpower_compute_counter_XV,helpblip_intpower_commcounter_XV,helpblip_intpower_ontime_XV,helpblip_intpower_offtime_XV,helpblip_intpower_op_numcycles_XV)
    
    
    plot_energy_overhead(operator_time_allexp_baseline_wctpl_244mF_II,totalcycleforinference_baseline_wctpl_244mF_II,compute_counter_baseline_wctpl_244mF_II,communication_counter_baseline_wctpl_244mF_II, ontime_baseline_wctpl_244mF_II,offtime_baseline_wctpl_244mF_II,num_cycle_op_allexp_baseline_wctpl_244mF_II,
                        operator_time_allexp_ifpga_Dtile_7_Ctile_5,totalcycleforinference_ifpga_Dtile_7_Ctile_5,compute_counter_ifpga_Dtile_7_Ctile_5,communication_counter_ifpga_Dtile_7_Ctile_5,ontime_ifpga_Dtile_7_Ctile_5,offtime_ifpga_Dtile_7_Ctile_5,num_cycle_op_allexp_ifpga_Dtile_7_Ctile_5)

    
    

    expFiles=["./Exp_data/08_31_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_depthwise_tile_4_conv_tile_2_144mF_Dearlydie_1_Cearlydie_1.txt"]          
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile2_intpower,off_Dtile4_Ctile2_intpower,classopDtile4_Ctile2_intpower,inftime_Dtile4_Ctile2_intpower,tcyc_Dtile4_Ctile2_intpower,ncycop_Dtile4_Ctile2_intpower,op_Dtile4_Ctile2_intpower,comp_Dtile4_Ctile2_intpower,comm_Dtile4_Ctile2_intpower= parsedata(f,1,8)
    expFiles=["./Exp_data/09_19_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_4_conv_tile_5_144mF_Dearlydie_1_Cearlydie4.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile5_intpower,off_Dtile4_Ctile5_intpower,classop_Dtile4_Ctile5_intpower,inftime_Dtile4_Ctile5_intpower,tcyc_Dtile4_Ctile5_intpower,ncycop_Dtile4_Ctile5_intpower,op_Dtile4_Ctile5_intpower,comp_Dtile4_Ctile5_intpower,comm_Dtile4_Ctile5_intpower= parsedata(f,1,8)    
###########data for scheduling plot Intermittent power ##################

    expFiles=["./Exp_data/09_19_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_4_conv_tile_10_144mF_Dearlydie_1_Cearlydie_8.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile10_intpower,off_Dtile4_Ctile10_intpower,classop_Dtile4_Ctile10_intpower,inftime_Dtile4_Ctile10_intpower,tcyc_Dtile4_Ctile10_intpower,ncycop_Dtile4_Ctile10_intpower,op_Dtile4_Ctile10_intpower,comp_Dtile4_Ctile10_intpower,comm_Dtile4_Ctile10_intpower= parsedata(f,1,8)  
    expFiles=["./Exp_data/09_19_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_7_conv_tile_2_144mF_Dearlydie_2_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile2_intpower,off_Dtile7_Ctile2_intpower,classop_Dtile7_Ctile2_intpower,inftime_Dtile7_Ctile2_intpower,tcyc_Dtile7_Ctile2_intpower,ncycop_Dtile7_Ctile2_intpower,op_Dtile7_Ctile2_intpower,comp_Dtile7_Ctile2_intpower,comm_Dtile7_Ctile2_intpower= parsedata(f,1,8)
    #retake....
    expFiles=["./Exp_data/11_03_2022/Bobber_Lenet5_wctpl_FR_intermittent_power_8Mhz_1v5reg_Dtile7_DDie1_Ctile5_Cdie2_50mF_9dBm_osc_disbymsp_no ext osc_3v3switch_byp.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile5_intpower,off_Dtile7_Ctile5_intpower,classop_Dtile7_Ctile5_intpower,inftime_Dtile7_Ctile5_intpower,tcyc_Dtile7_Ctile5_intpower,ncycop_Dtile7_Ctile5_intpower,op_Dtile7_Ctile5_intpower,comp_Dtile7_Ctile5_intpower,comm_Dtile7_Ctile5_intpower= parsedata(f,1,8)
    #retake....                                                                                                                  
    expFiles=["./Exp_data/11_03_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_7_conv_tile_10_50mF_Dearlydie_1_Cearlydie_3_II.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile10_intpower,off_Dtile7_Ctile10_intpower,classop_Dtile7_Ctile10_intpower,inftime_Dtile7_Ctile10_intpower,tcyc_Dtile7_Ctile10_intpower,ncycop_Dtile7_Ctile10_intpower,op_Dtile7_Ctile10_intpower,comp_Dtile7_Ctile10_intpower,comm_Dtile7_Ctile10_intpower= parsedata(f,1,8)
    # print(op)

    expFiles=["./Exp_data/09_19_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_2_144mF_Dearlydie_6_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile2_intpower,off_Dtile14_Ctile2_intpower,classop_Dtile14_Ctile2_intpower,inftime_Dtile14_Ctile2_intpower,tcyc_Dtile14_Ctile2_intpower,ncycop_Dtile14_Ctile2_intpower,op_Dtile14_Ctile2_intpower,comp_Dtile14_Ctile2_intpower,comm_Dtile14_Ctile2_intpower = parsedata(f,1,8)

    expFiles=["./Exp_data/11_03_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_5_50mF_Dearlydie_2_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile5_intpower,off_Dtile14_Ctile5_intpower,classop_Dtile14_Ctile5_intpower,inftime_Dtile14_Ctile5_intpower,tcyc_Dtile14_Ctile5_intpower,ncycop_Dtile14_Ctile5_intpower,op_Dtile14_Ctile5_intpower,comp_Dtile14_Ctile5_intpower,comm_Dtile14_Ctile5_intpower = parsedata(f,1,8)

    expFiles=["./Exp_data/11_03_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_10_50mF_Dearlydie_2_Cearlydie_3.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile10_intpower,off_Dtile14_Ctile10_intpower,classop_Dtile14_Ctile10_intpower,inftime_Dtile14_Ctile10_intpower,tcyc_Dtile14_Ctile10_intpower,ncycop_Dtile14_Ctile10_intpower,op_Dtile14_Ctile10_intpower,comp_Dtile14_Ctile10_intpower,comm_Dtile14_Ctile10_intpower = parsedata(f,1,8)

    expFiles=["./Exp_data/09_19_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_2_144mF_Dearlydie_4_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile2_intpower,off_Dtile28_Ctile2_intpower,classop_Dtile28_Ctile2_intpower,inftime_Dtile28_Ctile2_intpower,tcyc_Dtile28_Ctile2_intpower,ncycop_Dtile28_Ctile2_intpower,op_Dtile28_Ctile2_intpower,comp_Dtile28_Ctile2_intpower,comm_Dtile28_Ctile2_intpower = parsedata(f,1,8)

    expFiles=["./Exp_data/11_03_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_5_50mF_Dearlydie_3_Cearlydie_1.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile5_intpower,off_Dtile28_Ctile5_intpower,classop_Dtile28_Ctile5_intpower,inftime_Dtile28_Ctile5_intpower,tcyc_Dtile28_Ctile5_intpower,ncycop_Dtile28_Ctile5_intpower,op_Dtile28_Ctile5_intpower,comp_Dtile28_Ctile5_intpower,comm_Dtile28_Ctile5_intpower = parsedata(f,1,8)

    expFiles=["./Exp_data/11_03_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_10_50mF_Dearlydie_3_Cearlydie_3.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile10_intpower,off_Dtile28_Ctile10_intpower,classop_Dtile28_Ctile10_intpower,inftime_Dtile28_Ctile10_intpower,tcyc_Dtile28_Ctile10_intpower,ncycop_Dtile28_Ctile10_intpower,op_Dtile28_Ctile10_intpower,comp_Dtile28_Ctile10_intpower,comm_Dtile28_Ctile10_intpower = parsedata(f,1,8)


###########data for scheduling plot with no early die ##################

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_4_conv_tile_2_144mF.txt"]          
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile2_intpower_noearly_die,off_Dtile4_Ctile2_intpower_noearly_die,classopDtile4_Ctile2_intpower_noearly_die,inftime_Dtile4_Ctile2_intpower_noearly_die,tcyc_Dtile4_Ctile2_intpower_noearly_die,ncycop_Dtile4_Ctile2_intpower_noearly_die,op_Dtile4_Ctile2_intpower_noearly_die,comp_Dtile4_Ctile2_intpower_noearly_die,comm_Dtile4_Ctile2_intpower_noearly_die= parsedata(f,1,8)
   
    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_4_conv_tile_5_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile5_intpower_noearly_die,off_Dtile4_Ctile5_intpower_noearly_die,classop_Dtile4_Ctile5_intpower_noearly_die,inftime_Dtile4_Ctile5_intpower_noearly_die,tcyc_Dtile4_Ctile5_intpower_noearly_die,ncycop_Dtile4_Ctile5_intpower_noearly_die,op_Dtile4_Ctile5_intpower_noearly_die,comp_Dtile4_Ctile5_intpower_noearly_die,comm_Dtile4_Ctile5_intpower_noearly_die= parsedata(f,1,8)    

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_4_conv_tile_10_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile4_Ctile10_intpower_noearly_die,off_Dtile4_Ctile10_intpower_noearly_die,classop_Dtile4_Ctile10_intpower_noearly_die,inftime_Dtile4_Ctile10_intpower_noearly_die,tcyc_Dtile4_Ctile10_intpower_noearly_die,ncycop_Dtile4_Ctile10_intpower_noearly_die,op_Dtile4_Ctile10_intpower_noearly_die,comp_Dtile4_Ctile10_intpower_noearly_die,comm_Dtile4_Ctile10_intpower_noearly_die= parsedata(f,1,8)  

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_7_conv_tile_2_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile2_intpower_noearly_die,off_Dtile7_Ctile2_intpower_noearly_die,classop_Dtile7_Ctile2_intpower_noearly_die,inftime_Dtile7_Ctile2_intpower_noearly_die,tcyc_Dtile7_Ctile2_intpower_noearly_die,ncycop_Dtile7_Ctile2_intpower_noearly_die,op_Dtile7_Ctile2_intpower_noearly_die,comp_Dtile7_Ctile2_intpower_noearly_die,comm_Dtile7_Ctile2_intpower_noearly_die= parsedata(f,1,8)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_7_conv_tile_5_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile5_intpower_noearly_die,off_Dtile7_Ctile5_intpower_noearly_die,classop_Dtile7_Ctile5_intpower_noearly_die,inftime_Dtile7_Ctile5_intpower_noearly_die,tcyc_Dtile7_Ctile5_intpower_noearly_die,ncycop_Dtile7_Ctile5_intpower_noearly_die,op_Dtile7_Ctile5_intpower_noearly_die,comp_Dtile7_Ctile5_intpower_noearly_die,comm_Dtile7_Ctile5_intpower_noearly_die= parsedata(f,1,8)
                                                                                                                           
    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_7_conv_tile_10_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile7_Ctile10_intpower_noearly_die,off_Dtile7_Ctile10_intpower_noearly_die,classop_Dtile7_Ctile10_intpower_noearly_die,inftime_Dtile7_Ctile10_intpower_noearly_die,tcyc_Dtile7_Ctile10_intpower_noearly_die,ncycop_Dtile7_Ctile10_intpower_noearly_die,op_Dtile7_Ctile10_intpower_noearly_die,comp_Dtile7_Ctile10_intpower_noearly_die,comm_Dtile7_Ctile10_intpower_noearly_die= parsedata(f,1,8)
    # print(op)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_2_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile2_intpower_noearly_die,off_Dtile14_Ctile2_intpower_noearly_die,classop_Dtile14_Ctile2_intpower_noearly_die,inftime_Dtile14_Ctile2_intpower_noearly_die,tcyc_Dtile14_Ctile2_intpower_noearly_die,ncycop_Dtile14_Ctile2_intpower_noearly_die,op_Dtile14_Ctile2_intpower_noearly_die,comp_Dtile14_Ctile2_intpower_noearly_die,comm_Dtile14_Ctile2_intpower_noearly_die = parsedata(f,1,8)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_5_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile5_intpower_noearly_die,off_Dtile14_Ctile5_intpower_noearly_die,classop_Dtile14_Ctile5_intpower_noearly_die,inftime_Dtile14_Ctile5_intpower_noearly_die,tcyc_Dtile14_Ctile5_intpower_noearly_die,ncycop_Dtile14_Ctile5_intpower_noearly_die,op_Dtile14_Ctile5_intpower_noearly_die,comp_Dtile14_Ctile5_intpower_noearly_die,comm_Dtile14_Ctile5_intpower_noearly_die = parsedata(f,1,8)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_14_conv_tile_10_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile14_Ctile10_intpower_noearly_die,off_Dtile14_Ctile10_intpower_noearly_die,classop_Dtile14_Ctile10_intpower_noearly_die,inftime_Dtile14_Ctile10_intpower_noearly_die,tcyc_Dtile14_Ctile10_intpower_noearly_die,ncycop_Dtile14_Ctile10_intpower_noearly_die,op_Dtile14_Ctile10_intpower_noearly_die,comp_Dtile14_Ctile10_intpower_noearly_die,comm_Dtile14_Ctile10_intpower_noearly_die = parsedata(f,1,8)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_2_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile2_intpower_noearly_die,off_Dtile28_Ctile2_intpower_noearly_die,classop_Dtile28_Ctile2_intpower_noearly_die,inftime_Dtile28_Ctile2_intpower_noearly_die,tcyc_Dtile28_Ctile2_intpower_noearly_die,ncycop_Dtile28_Ctile2_intpower_noearly_die,op_Dtile28_Ctile2_intpower_noearly_die,comp_Dtile28_Ctile2_intpower_noearly_die,comm_Dtile28_Ctile2_intpower_noearly_die = parsedata(f,1,8)
    
    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_5_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile5_intpower_noearly_die,off_Dtile28_Ctile5_intpower_noearly_die,classop_Dtile28_Ctile5_intpower_noearly_die,inftime_Dtile28_Ctile5_intpower_noearly_die,tcyc_Dtile28_Ctile5_intpower_noearly_die,ncycop_Dtile28_Ctile5_intpower_noearly_die,op_Dtile28_Ctile5_intpower_noearly_die,comp_Dtile28_Ctile5_intpowe_noearly_die,comm_Dtile28_Ctile5_intpower_noearly_die = parsedata(f,1,8)

    expFiles=["./Exp_data/09_21_2022/MSP+fpga_Lenet5_FR_8Mhz_spi4Mhz_int_power_depthwise_tile_28_conv_tile_10_144mF.txt"]
    for idx, f in enumerate(expFiles):
        on_Dtile28_Ctile10_intpower_noearly_die,off_Dtile28_Ctile10_intpower_noearly_die,classop_Dtile28_Ctile10_intpower_noearly_die,inftime_Dtile28_Ctile10_intpower_noearly_die,tcyc_Dtile28_Ctile10_intpower_noearly_die,ncycop_Dtile28_Ctile10_intpower_noearly_die,op_Dtile28_Ctile10_intpower_noearly_die,comp_Dtile28_Ctile10_intpower_noearly_die,comm_Dtile28_Ctile10_intpower_noearly_die = parsedata(f,1,8)





    # print(on_Dtile4_Ctile10_intpower)
    # plot_compare_perf_vs_tile_size_144mF(inftime_Dtile4_Ctile2_intpower,inftime_Dtile4_Ctile5_intpower,inftime_Dtile4_Ctile10_intpower,inftime_Dtile7_Ctile2_intpower,
    #                             inftime_Dtile7_Ctile5_intpower,inftime_Dtile7_Ctile10_intpower,inftime_Dtile14_Ctile2_intpower, inftime_Dtile14_Ctile5_intpower, 
    #                             inftime_Dtile14_Ctile10_intpower,inftime_Dtile28_Ctile2_intpower,inftime_Dtile28_Ctile5_intpower,inftime_Dtile28_Ctile10_intpower, dtile=[4,7,14,28],ctile=[2,5,10] )

    plot_compare_perf_vs_tile_size_50mF( inftime_Dtile7_Ctile5_intpower,inftime_Dtile7_Ctile10_intpower, inftime_Dtile14_Ctile5_intpower, 
                                inftime_Dtile14_Ctile10_intpower,inftime_Dtile28_Ctile5_intpower,inftime_Dtile28_Ctile10_intpower, dtile=[7,14,28],ctile=[5,10] )



    # plot_computation_scheduling_144mF(inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1, 
    #                                 inftime_Dtile4_Ctile2,op_Dtile4_Ctile2,comp_Dtile4_Ctile2,comm_Dtile4_Ctile2, 
    #                                 op_Dtile4_Ctile2_intpower,tcyc_Dtile4_Ctile2_intpower,comp_Dtile4_Ctile2_intpower,comm_Dtile4_Ctile2_intpower,on_Dtile4_Ctile2_intpower,off_Dtile4_Ctile2_intpower,ncycop_Dtile4_Ctile2_intpower,
    #                                 op_Dtile4_Ctile2_intpower_noearly_die,tcyc_Dtile4_Ctile2_intpower_noearly_die,comp_Dtile4_Ctile2_intpower_noearly_die,comm_Dtile4_Ctile2_intpower_noearly_die,on_Dtile4_Ctile2_intpower_noearly_die,off_Dtile4_Ctile2_intpower_noearly_die,ncycop_Dtile4_Ctile2_intpower_noearly_die,
                                   
    #                                 inftime_Dtile4_Ctile5,op_Dtile4_Ctile5,comp_Dtile4_Ctile5,comm_Dtile4_Ctile5,
    #                                 op_Dtile4_Ctile5_intpower,tcyc_Dtile4_Ctile5_intpower,comp_Dtile4_Ctile5_intpower,comm_Dtile4_Ctile5_intpower,on_Dtile4_Ctile5_intpower,off_Dtile4_Ctile5_intpower,ncycop_Dtile4_Ctile5_intpower,
    #                                 op_Dtile4_Ctile5_intpower_noearly_die,tcyc_Dtile4_Ctile5_intpower_noearly_die,comp_Dtile4_Ctile5_intpower_noearly_die,comm_Dtile4_Ctile5_intpower_noearly_die,on_Dtile4_Ctile5_intpower_noearly_die,off_Dtile4_Ctile5_intpower_noearly_die,ncycop_Dtile4_Ctile5_intpower_noearly_die,

    #                                 inftime_Dtile4_Ctile10,op_Dtile4_Ctile10,comp_Dtile4_Ctile10,comm_Dtile4_Ctile10,
    #                                 op_Dtile4_Ctile10_intpower,tcyc_Dtile4_Ctile10_intpower,comp_Dtile4_Ctile10_intpower,comm_Dtile4_Ctile10_intpower,on_Dtile4_Ctile10_intpower,off_Dtile4_Ctile10_intpower,ncycop_Dtile4_Ctile10_intpower,
    #                                 op_Dtile4_Ctile10_intpower_noearly_die,tcyc_Dtile4_Ctile10_intpower_noearly_die,comp_Dtile4_Ctile10_intpower_noearly_die,comm_Dtile4_Ctile10_intpower_noearly_die,on_Dtile4_Ctile10_intpower_noearly_die,off_Dtile4_Ctile10_intpower_noearly_die,ncycop_Dtile4_Ctile10_intpower_noearly_die,

    #                                 inftime_Dtile7_Ctile2,op_Dtile7_Ctile2,comp_Dtile7_Ctile2,comm_Dtile7_Ctile2,
    #                                 op_Dtile7_Ctile2_intpower,tcyc_Dtile7_Ctile2_intpower,comp_Dtile7_Ctile2_intpower,comm_Dtile7_Ctile2_intpower,on_Dtile7_Ctile2_intpower,off_Dtile7_Ctile2_intpower,ncycop_Dtile7_Ctile2_intpower,
    #                                 op_Dtile7_Ctile2_intpower_noearly_die,tcyc_Dtile7_Ctile2_intpower_noearly_die,comp_Dtile7_Ctile2_intpower_noearly_die,comm_Dtile7_Ctile2_intpower_noearly_die,on_Dtile7_Ctile2_intpower_noearly_die,off_Dtile7_Ctile2_intpower_noearly_die,ncycop_Dtile7_Ctile2_intpower_noearly_die,

    #                                 inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
    #                                 op_Dtile7_Ctile5_intpower,tcyc_Dtile7_Ctile5_intpower,comp_Dtile7_Ctile5_intpower,comm_Dtile7_Ctile5_intpower,on_Dtile7_Ctile5_intpower,off_Dtile7_Ctile5_intpower,ncycop_Dtile7_Ctile5_intpower,
    #                                 op_Dtile7_Ctile5_intpower_noearly_die,tcyc_Dtile7_Ctile5_intpower_noearly_die,comp_Dtile7_Ctile5_intpower_noearly_die,comm_Dtile7_Ctile5_intpower_noearly_die,on_Dtile7_Ctile5_intpower_noearly_die,off_Dtile7_Ctile5_intpower_noearly_die,ncycop_Dtile7_Ctile5_intpower_noearly_die,

    #                                 inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
    #                                 op_Dtile7_Ctile10_intpower,tcyc_Dtile7_Ctile10_intpower,comp_Dtile7_Ctile10_intpower,comm_Dtile7_Ctile10_intpower,on_Dtile7_Ctile10_intpower,off_Dtile7_Ctile10_intpower,ncycop_Dtile7_Ctile10_intpower,
    #                                 op_Dtile7_Ctile10_intpower_noearly_die,tcyc_Dtile7_Ctile10_intpower_noearly_die,comp_Dtile7_Ctile10_intpower_noearly_die,comm_Dtile7_Ctile10_intpower_noearly_die,on_Dtile7_Ctile10_intpower_noearly_die,off_Dtile7_Ctile10_intpower_noearly_die,ncycop_Dtile7_Ctile10_intpower_noearly_die,

    #                                 inftime_Dtile14_Ctile2,op_Dtile14_Ctile2,comp_Dtile14_Ctile2,comm_Dtile14_Ctile2,
    #                                 op_Dtile14_Ctile2_intpower,tcyc_Dtile14_Ctile2_intpower,comp_Dtile14_Ctile2_intpower,comm_Dtile14_Ctile2_intpower,on_Dtile14_Ctile2_intpower,off_Dtile14_Ctile2_intpower,ncycop_Dtile14_Ctile2_intpower,
    #                                 op_Dtile14_Ctile2_intpower_noearly_die,tcyc_Dtile14_Ctile2_intpower_noearly_die,comp_Dtile14_Ctile2_intpower_noearly_die,comm_Dtile14_Ctile2_intpower_noearly_die,on_Dtile14_Ctile2_intpower_noearly_die,off_Dtile14_Ctile2_intpower_noearly_die,ncycop_Dtile14_Ctile2_intpower_noearly_die,

    #                                 inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
    #                                 op_Dtile14_Ctile5_intpower,tcyc_Dtile14_Ctile5_intpower,comp_Dtile14_Ctile5_intpower,comm_Dtile14_Ctile5_intpower,on_Dtile14_Ctile5_intpower,off_Dtile14_Ctile5_intpower,ncycop_Dtile14_Ctile5_intpower,
    #                                 op_Dtile14_Ctile5_intpower_noearly_die,tcyc_Dtile14_Ctile5_intpower_noearly_die,comp_Dtile14_Ctile5_intpower_noearly_die,comm_Dtile14_Ctile5_intpower_noearly_die,on_Dtile14_Ctile5_intpower_noearly_die,off_Dtile14_Ctile5_intpower_noearly_die,ncycop_Dtile14_Ctile5_intpower_noearly_die,

    #                                 inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
    #                                 op_Dtile14_Ctile10_intpower,tcyc_Dtile14_Ctile10_intpower,comp_Dtile14_Ctile10_intpower,comm_Dtile14_Ctile10_intpower,on_Dtile14_Ctile10_intpower,off_Dtile14_Ctile10_intpower,ncycop_Dtile14_Ctile10_intpower,
    #                                 op_Dtile14_Ctile10_intpower_noearly_die,tcyc_Dtile14_Ctile10_intpower_noearly_die,comp_Dtile14_Ctile10_intpower_noearly_die,comm_Dtile14_Ctile10_intpower_noearly_die,on_Dtile14_Ctile10_intpower_noearly_die,off_Dtile14_Ctile10_intpower_noearly_die,ncycop_Dtile14_Ctile10_intpower_noearly_die,

    #                                 inftime_Dtile28_Ctile2,op_Dtile28_Ctile2,comp_Dtile28_Ctile2,comm_Dtile28_Ctile2,
    #                                 op_Dtile28_Ctile2_intpower,tcyc_Dtile28_Ctile2_intpower,comp_Dtile28_Ctile2_intpower,comm_Dtile28_Ctile2_intpower,on_Dtile28_Ctile2_intpower,off_Dtile28_Ctile2_intpower,ncycop_Dtile28_Ctile2_intpower,
    #                                 op_Dtile28_Ctile2_intpower_noearly_die,tcyc_Dtile28_Ctile2_intpower_noearly_die,comp_Dtile28_Ctile2_intpower_noearly_die,comm_Dtile28_Ctile2_intpower_noearly_die,on_Dtile28_Ctile2_intpower_noearly_die,off_Dtile28_Ctile2_intpower_noearly_die,ncycop_Dtile28_Ctile2_intpower_noearly_die,

    #                                 inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
    #                                 op_Dtile28_Ctile5_intpower,tcyc_Dtile28_Ctile5_intpower,comp_Dtile28_Ctile5_intpower,comm_Dtile28_Ctile5_intpower,on_Dtile28_Ctile5_intpower,off_Dtile28_Ctile5_intpower,ncycop_Dtile28_Ctile5_intpower,
    #                                 op_Dtile28_Ctile5_intpower_noearly_die,tcyc_Dtile28_Ctile5_intpower_noearly_die,comp_Dtile28_Ctile5_intpowe_noearly_die,comm_Dtile28_Ctile5_intpower_noearly_die,on_Dtile28_Ctile5_intpower_noearly_die,off_Dtile28_Ctile5_intpower_noearly_die,ncycop_Dtile28_Ctile5_intpower_noearly_die,

    #                                 inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
    #                                 op_Dtile28_Ctile10_intpower,tcyc_Dtile28_Ctile10_intpower,comp_Dtile28_Ctile10_intpower,comm_Dtile28_Ctile10_intpower,on_Dtile28_Ctile10_intpower,off_Dtile28_Ctile10_intpower,ncycop_Dtile28_Ctile10_intpower,
    #                                 op_Dtile28_Ctile10_intpower_noearly_die,tcyc_Dtile28_Ctile10_intpower_noearly_die,comp_Dtile28_Ctile10_intpower_noearly_die,comm_Dtile28_Ctile10_intpower_noearly_die,on_Dtile28_Ctile10_intpower_noearly_die,off_Dtile28_Ctile10_intpower_noearly_die,ncycop_Dtile28_Ctile10_intpower_noearly_die)
    
    plot_computation_scheduling_50mF(inftime_Dtile1_Ctile1,op_Dtile1_Ctile1,comp_Dtile1_Ctile1,comm_Dtile1_Ctile1, 
                            
                                    inftime_Dtile7_Ctile5,op_Dtile7_Ctile5,comp_Dtile7_Ctile5,comm_Dtile7_Ctile5,
                                    op_Dtile7_Ctile5_intpower,tcyc_Dtile7_Ctile5_intpower,comp_Dtile7_Ctile5_intpower,comm_Dtile7_Ctile5_intpower,on_Dtile7_Ctile5_intpower,off_Dtile7_Ctile5_intpower,ncycop_Dtile7_Ctile5_intpower,
                                    op_Dtile7_Ctile5_intpower_noearly_die,tcyc_Dtile7_Ctile5_intpower_noearly_die,comp_Dtile7_Ctile5_intpower_noearly_die,comm_Dtile7_Ctile5_intpower_noearly_die,on_Dtile7_Ctile5_intpower_noearly_die,off_Dtile7_Ctile5_intpower_noearly_die,ncycop_Dtile7_Ctile5_intpower_noearly_die,

                                    inftime_Dtile7_Ctile10,op_Dtile7_Ctile10,comp_Dtile7_Ctile10,comm_Dtile7_Ctile10,
                                    op_Dtile7_Ctile10_intpower,tcyc_Dtile7_Ctile10_intpower,comp_Dtile7_Ctile10_intpower,comm_Dtile7_Ctile10_intpower,on_Dtile7_Ctile10_intpower,off_Dtile7_Ctile10_intpower,ncycop_Dtile7_Ctile10_intpower,
                                    op_Dtile7_Ctile10_intpower_noearly_die,tcyc_Dtile7_Ctile10_intpower_noearly_die,comp_Dtile7_Ctile10_intpower_noearly_die,comm_Dtile7_Ctile10_intpower_noearly_die,on_Dtile7_Ctile10_intpower_noearly_die,off_Dtile7_Ctile10_intpower_noearly_die,ncycop_Dtile7_Ctile10_intpower_noearly_die,

                                    inftime_Dtile14_Ctile5,op_Dtile14_Ctile5,comp_Dtile14_Ctile5,comm_Dtile14_Ctile5,
                                    op_Dtile14_Ctile5_intpower,tcyc_Dtile14_Ctile5_intpower,comp_Dtile14_Ctile5_intpower,comm_Dtile14_Ctile5_intpower,on_Dtile14_Ctile5_intpower,off_Dtile14_Ctile5_intpower,ncycop_Dtile14_Ctile5_intpower,
                                    op_Dtile14_Ctile5_intpower_noearly_die,tcyc_Dtile14_Ctile5_intpower_noearly_die,comp_Dtile14_Ctile5_intpower_noearly_die,comm_Dtile14_Ctile5_intpower_noearly_die,on_Dtile14_Ctile5_intpower_noearly_die,off_Dtile14_Ctile5_intpower_noearly_die,ncycop_Dtile14_Ctile5_intpower_noearly_die,

                                    inftime_Dtile14_Ctile10,op_Dtile14_Ctile10,comp_Dtile14_Ctile10,comm_Dtile14_Ctile10,
                                    op_Dtile14_Ctile10_intpower,tcyc_Dtile14_Ctile10_intpower,comp_Dtile14_Ctile10_intpower,comm_Dtile14_Ctile10_intpower,on_Dtile14_Ctile10_intpower,off_Dtile14_Ctile10_intpower,ncycop_Dtile14_Ctile10_intpower,
                                    op_Dtile14_Ctile10_intpower_noearly_die,tcyc_Dtile14_Ctile10_intpower_noearly_die,comp_Dtile14_Ctile10_intpower_noearly_die,comm_Dtile14_Ctile10_intpower_noearly_die,on_Dtile14_Ctile10_intpower_noearly_die,off_Dtile14_Ctile10_intpower_noearly_die,ncycop_Dtile14_Ctile10_intpower_noearly_die,

                                    inftime_Dtile28_Ctile5,op_Dtile28_Ctile5,comp_Dtile28_Ctile5,comm_Dtile28_Ctile5,
                                    op_Dtile28_Ctile5_intpower,tcyc_Dtile28_Ctile5_intpower,comp_Dtile28_Ctile5_intpower,comm_Dtile28_Ctile5_intpower,on_Dtile28_Ctile5_intpower,off_Dtile28_Ctile5_intpower,ncycop_Dtile28_Ctile5_intpower,
                                    op_Dtile28_Ctile5_intpower_noearly_die,tcyc_Dtile28_Ctile5_intpower_noearly_die,comp_Dtile28_Ctile5_intpowe_noearly_die,comm_Dtile28_Ctile5_intpower_noearly_die,on_Dtile28_Ctile5_intpower_noearly_die,off_Dtile28_Ctile5_intpower_noearly_die,ncycop_Dtile28_Ctile5_intpower_noearly_die,

                                    inftime_Dtile28_Ctile10,op_Dtile28_Ctile10,comp_Dtile28_Ctile10,comm_Dtile28_Ctile10,
                                    op_Dtile28_Ctile10_intpower,tcyc_Dtile28_Ctile10_intpower,comp_Dtile28_Ctile10_intpower,comm_Dtile28_Ctile10_intpower,on_Dtile28_Ctile10_intpower,off_Dtile28_Ctile10_intpower,ncycop_Dtile28_Ctile10_intpower,
                                    op_Dtile28_Ctile10_intpower_noearly_die,tcyc_Dtile28_Ctile10_intpower_noearly_die,comp_Dtile28_Ctile10_intpower_noearly_die,comm_Dtile28_Ctile10_intpower_noearly_die,on_Dtile28_Ctile10_intpower_noearly_die,off_Dtile28_Ctile10_intpower_noearly_die,ncycop_Dtile28_Ctile10_intpower_noearly_die)
    

#########Int power diff optimization result #############


if __name__ == "__main__":
    main()