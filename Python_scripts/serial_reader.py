import serial
import time
import argparse
import numpy as np

parser = argparse.ArgumentParser()

# parser.add_argument("-p", "--port", help="Port name")
parser.add_argument('-p','--ports', nargs='+', required=True, help="Port names")
parser.add_argument("-o", "--output", default='terminal', help="Output to terminal/file/both")
parser.add_argument("-t", "--timestamps", help="timestamps = True/False")
parser.add_argument("-b", "--baudrate", default=115200, help="Baud Rate")


args = parser.parse_args()

serialPort = ['']*len(args.ports)
serialString = ['']*len(args.ports)

if args.timestamps == True:
    startTime = time.time()

for idx, port in enumerate(args.ports):
    serialPort[idx] = serial.Serial(port = port,
                            baudrate=args.baudrate,
                            bytesize=8,
                            timeout=2,
                            stopbits=serial.STOPBITS_ONE,
                            parity=serial.PARITY_NONE)
    # print(serialPort[idx])

board = 'msp430fr5994'
freq = '8MHz'
text = 'Lenet5_inference'
log = open(f"{board}_{freq}_{text}_eval.txt","w+")

while(1):  
    for port in range(len(args.ports)):
        # Wait until there is data waiting in the serial buffer
        if(serialPort[port].in_waiting > 0):
            # Read data out of the buffer until a carraige return / new line is found
            serialString[port] = serialPort[port].readline()
            # Write the contents of the serial data to a file
            if args.timestamps == True:
                if args.output == "file": 
                    log.write(str(time.time() - startTime))
                    log.write(',')
                else: 
                    print(str(time.time() - startTime))
                    
            if args.output == "file": 
                log.write(serialString[port].decode('Ascii', 'ignore'))
            elif args.output == "both":
                log.write(serialString[port].decode('Ascii', 'ignore'))
                print(serialString[port].decode('Ascii', 'ignore'))
            else:
                print(serialString[port].decode('Ascii', 'ignore'))

# serialPortDUT1.close()
# adc_log.close()
