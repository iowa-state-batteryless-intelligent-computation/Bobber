
# BOBBER: A Prototyping Platform for Batteryless Intermittent Accelerators.

# How to cite BOBBER repo
[DOI](https://doi.org/10.5281/zenodo.7439488): permanent DOI

##### Note: The above DOI is to cite the specific version of this repo, containing design files used to generate the results for BOBBER as reported in FPGA 2023. 

All versions of this repo can be cited using [DOI](https://doi.org/10.5281/zenodo.7240408)

[cite](https://doi.org/10.1145/3543622.3573046): To cite BOBBER paper
# Overview
This project is used to built a Heterogeneous Research Platform for BatteryLess Intermittent Accelerators using a Igloo Nano AGLN250VQFP100 FPGA and MSP430FR5994. 

# BOBBER Tools and Licenses for System Setup:
## MSP430
- Texas Instruments Code Compose Studio(CCS) Version: 9.2.0.00013 [Link](https://software-dl.ti.com/ccs/esd/documents/ccs_downloads.html).
- ARM GNU Embedded toolchain- 8.3.0.16 (Mitto Systems Limited).
- FRAM Utilities 3.10.0.10 [Link](https://www.ti.com/tool/MSP-FRAM-UTILITIES).

### Note: 

  To install FRAM utilities, manually download from the above URL to local disk c:/TI, where the CCS tool is installed by default. Open CCS tool, go to Windows/preferences/Code composer studio/products and add FRAM utilities from the local disk drive. For further information, look up the description in the above URL.


## Igloo Nano
- Libero IDE tool version 9.1 [Link](https://www.microsemi.com/product-directory/libero-soc/5507-libero-soc-v11-9-archive#downloads).
- Flash pro programmer board.

### Note: 
- Libero IDE tool requires a floating or a node locked license (if using Windows) to be installed in your workstation PC. This tool is required to program the FPGA. All the experiments reported in the paper use a Libero IDE tool with a node locked license. Libero offers a silver License, which is a free node locked license that expires in a year. The link to setup a node locked license can be found in the Libero SoC Installation and License Installation Guides tab in this [Link](https://www.microsemi.com/product-directory/libero-soc/5507-libero-soc-v11-9-archive#downloads).

## PCB design
- Autodesk Fusion 360 version 2.0.14337.

## Additional Equipment's/Hardware
- Agilent RF signal generator E4438C or any other RF signal generators.
- Workstation PC with Python 3.8.10, pyserial 3.4, matplotlib 3.5.0 and numpy 1.21.4 support to collect the logs.
- Two MSP-EXP430FR5994 LaunchPad™ Development Kits [Link](https://www.ti.com/tool/MSP-EXP430FR5994). 
  1. To program the target MSP430 in BOBBER. 
  2. A Sniffer Node to timestamp computations. 
- P2110 Powercast harvester Evaluation Board [Link](https://www.powercastco.com/products/development-kits/#P2110-EVB). 
- Micro USB cables. 
# Tutorial for Programming the hardware in BOBBER
## Initial Hardware setup
The hardware is a custom-built PCB. To reproduce the results from scratch a custom PCB must be fabricated using the [Gerber](BOBBER_Hardware_Platform/iFPGA_sch_rel1.0_rev1.1_2021-02-15/CAMOutputs/GerberFiles) Files. The Assembly BOM, Schematic PDF and design files (Eagle schematic and board files) are available [here](BOBBER_Hardware_Platform).
## Programming the FPGA
1. To program the FPGA import the project under the folder BOBBER-based_Convolution_Accelerator using File>Open Project
2. Make sure all the Licenses have been set up and the environment variable reflects the licenses for LIbero IDE.
3. Initially run the synthesis option under the design flow tab on the left by right click>Synthesize>run
4. Open the Constraint Place and Route by right click>Compile> Open Interactively
5. Compile the Design.
6. Open the IO Attribute Editor and make the Pin assignments as described in one of the tables above.
7. Layout the design and Generate the Programming file (.pdb file)
8. Connect the Flashpro programmer board to the JTAG connector and flash the binary using the Flashpro tool

### FPGA Pin Configuration
The following pin configuration is required to be assigned and programmed to the FPGA using the IO attribute editor of Libero IDE.

| FPGA (Child) | Function     |
| ------------ | --------     |
| Pin 77       | RESET        |  
| Pin 76       | Slave select |  
| Pin 96       | MOSI         |
| Pin 95       | MISO 	      |
| Pin 94       | SPI clock    |
| VCORE        | 1.2/1.5V Core Fabric Power|
| VBANK        | 3.3 I/O Bank Power|
| GND          | Ground       |
| Pin 78	     | Number of MAC's|
| Pin 79	     | MAC Done|
| Pin 98       | FPGA Clock   | // 13

## Programming the Microcontroller
To program the MCU in BOBBER, the following pin connections has to be made using Jumper cables between an MSP430 Launchpad kit and BOBBER.

| MSP430 Launchpad | BOBBER		 | Function     |
| ------- | ------------ | --------     |
|  J101.7 (UARTTXD)   | UART TX     | UART Receive |  
|  J101.5 (UARTRXD)   | UART RX | UART Transmit|
|  J101.3 (SBWTDIO)   | RST      | MCU RESET|
|  J101.1 (SWTCLK)   | TEST     | Test mode pin |  
|  3v3    | VOUT  | Main power   |
|  GND    | GND  | Ground   |


1. To program the Microcontroller with the application software make the connections as described in the above table.
2. Open CCS and import the whole project under the folder BOBBER-based_CNN_Application_Software using Project>Import CCS Projects.
3. Check the box Copy project into workspace while importing the existing project.
4. Copy the contents of the file [in430](BOBBER-based_CNN_Application_Software/in430.txt) to the header file in430.h, inside [here](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/DepthwiseCoreCompute.h) and [here](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/ConvCoreCompute.h)
5. Build the project making sure the project settings use the GCC compiler. Other Build project settings are mentioned in the README files inside the project folder.
6. Program the MCU making sure the connections are right.

Additional resources related to the launchpad's hardware design can be found at [here](https://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP-EXP430FR5994/latest/index_FDS.html).

# Reproducing results in the paper
- To generate the results reported in the paper with the existing experimental data, run the python scripts [Latency_Energy_plots](Python_scripts/ifpga_plots_rev0.1.py), [Power_overhead_plot](Python_scripts/bobber_energy_efficiency_w_poweroverhead.py) using

        python3 ifpga_plots_rev0.1.py
        python3 bobber_energy_efficiency_w_poweroverhead.py


# Evaluation Setup:
In order to evaluate the system, we use a Sniffer Node (MSP430FR5994 Launchapad Kit) which captures the inferences occurring in the Node Under Test(NUT). The NUT indicates when the device boots and when an inference is done through GPIO's and once an inference is done, the computed output result is sent using a single channel UART interface.
The Sniffer Node timestamps the GPIO indications and channelizes the received computed inference output using the backchannel UART

## Expected inference output:

      0000000000000000000000000000000000000000000000000000000000003f7f0000000000000000

### Programming the Sniffer
1. To program the Sniffer, import the CCS project under the folder BOBBER_Sniffer_Node_Software using Project>Import CCS Projects.
2. Build the code to make sure it compiles using CCS. 
3. Program a MSP430FR5994 Launchpad Kit by shorting jumper J101.


## Evaluation Procedure
To rerun the experimental results reported in the paper for BOBBER, clone into this repo first using.

    git clone https://git.ece.iastate.edu/iowa-state-batteryless-intelligent-computation/Bobber.git

The following connections has to be made using jumper cables between a Sniffer Node and BOBBER.

## Sniffer node and NUT Pin Config
| Sniffer | BOBBER		 | Function     |
| ------- | ------------ | --------     |
|  P1.4   | P7.2  MCU     | One inference + Data Movement counter |  
|  P1.5   | Pin 78 FPGA | No of computation|
|  P3.5   | VOUT MCU      | Power up and down|
|  P3.4   | P7.4 MCU      | One operator |  
|  P6.1   | P2.0 MCU      | UART RX 	|
|  GND    | GND  MCU      | Ground   |

Within BOBBER the following external pin connections must be made using Jumper cables.

## External Pin Configuration

| MCU | FPGA | Function   |
| ------- | ------------ | --------     |
|  P1.5   | Pin 77       | FPGA RESET        |  
|  P7.3   | Pin 76       | Slave select |  
|  VOUT    | VBANK        | 3.3 I/O bank Power |


Additionally short Jumper JP33 and JP5 is shorted with VOUT.

For additional functionalities or pin configuration needed for an application please read [Appendix_file](Appendix).

## Static Profiling for Continuous Power Measurements.
To run an inference under multiple optimization knobs as specified in the paper, the following static profiling has to be done to the [Application_software](BOBBER-based_CNN_Application_Software).

- For BOBBER
  1. Set intermittent_power_flag = 0 [here](BOBBER-based_CNN_Application_Software/acc_interface/acc_interface.cc)
  2. Inside Depthwise [kernel](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/depthwise_conv.h), set tile_size_width =  output_width/X and tile_size_height output_height/X, where X takes any value from [1,2,4,7,14,28]
  3. Inside Convolution [kernel](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/conv.h), set tile_size_width =  output_width/Y and tile_size_height output_height/Y, where Y takes any value from [1,5,10]
  4. The continuous power inference latency measurements uses tile_size_width = output_width/7 and tile_size_height output_height/7 for Depthwise and tile_size_width = output_width/5 and tile_size_height output_height/5 for Convolution kernel.

- For Baseline Accel
  1. Set intermittent_power_flag = 0 [here](BOBBER-based_CNN_Application_Software/acc_interface/acc_interface.cc)
  2. The continuous power inference latency measurements uses tile_size_width = output_width/1 and tile_size_height output_height/1 for Depthwise and tile_size_width = output_width/1 and tile_size_height output_height/1 for Convolution kernel.  

## Static Profiling for Intermittent Power Measurements.

For BOBBER
  1. Set intermittent_power_flag = 1 [here](BOBBER-based_CNN_Application_Software/acc_interface/acc_interface.cc)
  2. Inside Depthwise [kernel](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/depthwise_conv.h), set tile_size_width =  output_width/X and tile_size_height output_height/X, where X takes a value from [7,14,28]. Also map early_die_kernel_size as per the below table.
  3. Inside Convolution [Kernel](BOBBER-based_CNN_Application_Software/tensorflow/lite/kernels/internal/reference/integer_ops/conv.h), set tile_size_width =  output_width/Y and tile_size_height output_height/Y, where Y takes a value from [5,10]. Also map early_die_kernel_size as per the below table.
  5. The Intermittent power inference latency measurements uses tile_size_width = output_width/7 and tile_size_height output_height/7 for Depthwise and tile_size_width = output_width/5 and tile_size_height output_height/5 for Convolution kernel.

### Early-die configuration

| Dtile width| Dtile height		 | early_die_kernel_size     |
| ------- | ------------ | --------     |
|  7    | 7     | 1** |  
|  14   | 14 |2|
|  28    | 28     | 3|

** For the latency plot generated for the paper (for design point BOBBER (#Depthwise tiles=49 and #Convolution tiles=25)), the early_die_kernel_size is set as 2. 

| Ctile width| Ctile height		 | early_die_kernel_size     |
| ------- | ------------ | --------     |
| 5   | 5     | 1 |  
|  10   | 10 | 3|

## Continuous Power Measurements for BOBBER
### Note: Further notes are available [here](Python_scripts/README.md)
To re-run the experiments in continuous power, follow the below steps. 

1. Make the required connections as specified in the Evaluation Setup, between the Sniffer Node and BOBBER as well as the External Pin Configurations.
2. To dump the UART logs to a file, a [script](Python_scripts/serial_reader.py) is used. Make any changes in the file if required (eg: different log file name).
3. Identify the COM port details of your Sniffer Node and run the below command in the folder containing the [script](Python_scripts/serial_reader.py). Note: Replace x in ttySx, with the COM port number. 

       python3 serial_reader.py -p /dev/ttySx -o both

4. Press Button P5.6 of the Sniffer Node to start collecting the logs.
5. Make sure the Static Profiling for Continuous Power Measurements is done for the [project](BOBBER-based_CNN_Application_Software).
6. Program the MCU in BOBBER using this [project](BOBBER-based_CNN_Application_Software), by importing into CCS. 
### Note:
  For Baseline Accel, use the same CCS project but make the necessary static profiling changes. All the steps are the same.

7. Once the UART logs are dumped, copy the log file, and paste it [here](Python_scripts/Exp_data/). You can also specify this location in the serial_reader.py script.
8. Repeat this process for different tile sizes and store them separately.
9. Make the necessary changes to the plotting scripts [script](Python_scripts/ifpga_plots_rev0.1.py) to add the new files and re-run the plotting functions.
10. Run this for multiple inferences and discard the very first inference logs and keep one inference result, to get accurate results. 
11. Repeat this experiment for each design point, sweeping across different optimization knobs.

## Continuous Power Measurements for MSP430 results
To re-run the experiments in continuous power, follow the below steps.
  
  1. Build and program the MSP430 with the code [here](Baseline/LeNet5_Original_Baseline/)

  2. Folow the same steps 1-4 and 7 and 9 from above

## Continuous Power Measurements For MSP430 w-CTPL
To re-run the experiments in continuous power, follow the below steps.
  1. Build and program the MSP430 with the code [here](Baseline/LeNet_Baseline_with_ctpl/) 


  2. Follow the same steps 1-4 and 7 and 9 from above

## Intermittent Power Measurements for BOBBER
To re-run the experiments in Intermittent power, the results reported uses a 50 mF as an energy store. We additionally use a Powercast P2110b Evaluation board. Additional pin connections must be made as shown below on top of the static profiling needed for running in Intermittent power.

| MCU | Powercast EVB | Function   |
| ------- | ------------ | --------     |
|  P1.0   | RESET       | Harvester RESET|    |  
|  VOUT    | VBATT        | 3.3 Harvested  Power |
|  GND    | GND        | Ground|

Additionally the following changes has to be made in BOBBER

| BOBBER | Connection | Function   |
| ------- | ------------ | --------     |
|  JP23   | Open       | FPGA Bank1 3.3V |    |  
|  JP16    | Open        | FPGA Bank1 3.3V|
|  JP24    | Open        | VJTAG |
|  JP19    | Open        | VJTAG|
|  JP18   | Open       | FPGA Bank3 3.3V |    |  
|  JP20    | Open        | VPUMP |
|  OSCILLATOR.1    | P1.2        | Oscillator disable|

#### Note: OSCILLATOR.1 is soldered and connected externally to pin P1.2.
1. Make the required connections as specified in the Evaluation Setup, between the Sniffer Node and BOBBER as well as the External Pin Configurations.
2. Connect an RF Signal Generator to the Powercast P2110b Eval board and set the frequency to 915 MHz and RF signal power to 9dBm.
3. Additionally make the connections as above to run specifically in Intermittent power.
4. To dump the UART logs to a file, a [script](Python_scripts/serial_reader.py) is used. Make any changes in the file if required (eg: different log file name)
5. Identify the COM port details of your Sniffer Node and run the below command in the folder containing the [script](Python_scripts/serial_reader.py). Note: Replace x in ttySx, with the COM port number. 

       python3 serial_reader.py -p /dev/ttySx -o both

6. Press Button P5.6 of the Sniffer Node to start collecting the logs.
7. Make sure the Static Profiling for Intermittent Power Measurements is done in the [project](BOBBER-based_CNN_Application_Software).
8. Program the MCU in BOBBER using this [project](BOBBER-based_CNN_Application_Software), by importing into CCS. Note: To restart program execution, the MCU should be reprogrammed and cannot be RESET by Software.
9. Disconnect the programming MSP430 Launchpad kit right after programming and turn the RF signal generator ON by setting the RF power level to 9 dBm.
10. Once the UART logs are dumped, copy the log files, and paste it [here](Python_scripts/Exp_data/). You can also specify this location in the serial_reader.py script.
11. Make the necessary changes to the plotting scripts [script](Python_scripts/ifpga_plots_rev0.1.py) to add the new files and re-run the plotting functions.
12. Run this for multiple inferences and discard the first inference log and keep one inference result, to get accurate results.
13. Repeat this experiment for each design points, sweeping across different optimization knobs.

## Intermittent Power Measurements for MSP430 w-CTPL

To re-run the experiments in Intermittent power for MSP430 w-CTPL, follow the below steps.
  1. Build and program the MSP430 with the code [here](Baseline/LeNet_Baseline_with_ctpl/).

  2. Follow the same steps 1-6 and 9-12 from above.

# License
Copyright 2022 The BOBBER Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.