# Overview
BOBBER: A Prototyping Platform for Batteryless Intermittent Accelerators
# Release
Initial pre-release Rel 0.1 Rev 0.1: 
# How to cite this 
10.5281/zenodo.7240409 : permanent DOI
# Project detail
This project is used to built A Heterogeneous Research Platform for BatteryLess Intermittent Accelerators using a Igloo Nano AGLN250VQFP100 FPGA and MSP430FR5994. This release version of the project is for purpose of creating a DOI for the artefact evaluation purpose. The project will later be updated with other design files.